﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Object Facebook.MiniJSON.Json::Deserialize(System.String)
extern void Json_Deserialize_m972B61039CCA76FF8B9E7F10386DA79D70CFFC89 (void);
// 0x00000002 System.String Facebook.MiniJSON.Json::Serialize(System.Object)
extern void Json_Serialize_mAB9484F1CED0115971BEF516C9FBD288927A4147 (void);
// 0x00000003 System.Void Facebook.MiniJSON.Json::.cctor()
extern void Json__cctor_m85AF29F564421DE80116FA01BB29D2C01BDE63E6 (void);
// 0x00000004 System.Void Facebook.MiniJSON.Json_Parser::.ctor(System.String)
extern void Parser__ctor_mEAADBE91C3200D3A288CF26C97D1E36D59AA59C3 (void);
// 0x00000005 System.Char Facebook.MiniJSON.Json_Parser::get_PeekChar()
extern void Parser_get_PeekChar_m9C2525B091792F26DBD4F361D43C89967F8D9899 (void);
// 0x00000006 System.Char Facebook.MiniJSON.Json_Parser::get_NextChar()
extern void Parser_get_NextChar_mC7CD0C12A40F8366CD9DC58600F6BBF6FC4D830D (void);
// 0x00000007 System.String Facebook.MiniJSON.Json_Parser::get_NextWord()
extern void Parser_get_NextWord_mD556C0698948EE8A2C5F82A3E7A10ADD498CDED5 (void);
// 0x00000008 Facebook.MiniJSON.Json_Parser_TOKEN Facebook.MiniJSON.Json_Parser::get_NextToken()
extern void Parser_get_NextToken_m6374135A3390F406B24EF0DA215DE4D39002F582 (void);
// 0x00000009 System.Object Facebook.MiniJSON.Json_Parser::Parse(System.String)
extern void Parser_Parse_mA58ACF97751DFB361B80A27868CB6900DA936B1F (void);
// 0x0000000A System.Void Facebook.MiniJSON.Json_Parser::Dispose()
extern void Parser_Dispose_mF001AEC19B5898A39D396D16CE5690014EB2F578 (void);
// 0x0000000B System.Collections.Generic.Dictionary`2<System.String,System.Object> Facebook.MiniJSON.Json_Parser::ParseObject()
extern void Parser_ParseObject_m7ABFBBC8548B51369628118DD30174612E7ACFCF (void);
// 0x0000000C System.Collections.Generic.List`1<System.Object> Facebook.MiniJSON.Json_Parser::ParseArray()
extern void Parser_ParseArray_mCBF16AB48642FE0C778DF7BA643E83A70A8E11FA (void);
// 0x0000000D System.Object Facebook.MiniJSON.Json_Parser::ParseValue()
extern void Parser_ParseValue_m4ECD06FF0396EAB7C6A5A669EF7DDEAF5FC79CC8 (void);
// 0x0000000E System.Object Facebook.MiniJSON.Json_Parser::ParseByToken(Facebook.MiniJSON.Json_Parser_TOKEN)
extern void Parser_ParseByToken_m8D304812FA64EA087EB68CA33B1C58EB941D6C6D (void);
// 0x0000000F System.String Facebook.MiniJSON.Json_Parser::ParseString()
extern void Parser_ParseString_m900DA186BAE180C38158A5012156EF8DC3743877 (void);
// 0x00000010 System.Object Facebook.MiniJSON.Json_Parser::ParseNumber()
extern void Parser_ParseNumber_mB815AA6E9C267EB0828546A22FD821C8D780D993 (void);
// 0x00000011 System.Void Facebook.MiniJSON.Json_Parser::EatWhitespace()
extern void Parser_EatWhitespace_mDC9F0FBCDF96E2BA32F11E098C8C3F63926832E5 (void);
// 0x00000012 System.Void Facebook.MiniJSON.Json_Serializer::.ctor()
extern void Serializer__ctor_m9FDB3B04D734300C03C20DB104AD8BFBA288D624 (void);
// 0x00000013 System.String Facebook.MiniJSON.Json_Serializer::Serialize(System.Object)
extern void Serializer_Serialize_mFAE9186EA73D4505AF70292967968823C2F05C9E (void);
// 0x00000014 System.Void Facebook.MiniJSON.Json_Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_m1397C7120E65709AD9DA500CDF01DE9EF92CB0F3 (void);
// 0x00000015 System.Void Facebook.MiniJSON.Json_Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_mB914F03AC2293864586B1FE72FBF4D648B4896F0 (void);
// 0x00000016 System.Void Facebook.MiniJSON.Json_Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_mF83F4104DDE5FB0B38FA0E810CC3F1F703FFD1FF (void);
// 0x00000017 System.Void Facebook.MiniJSON.Json_Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_m8BF12319CF59FF944248AB52BF53B93FD9A1D085 (void);
// 0x00000018 System.Void Facebook.MiniJSON.Json_Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_mD96CF1C52799B1569D5DE29AC5110442746CF946 (void);
// 0x00000019 System.Void Facebook.Unity.AccessToken::.ctor(System.String,System.String,System.DateTime,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.DateTime>)
extern void AccessToken__ctor_m5A676F9B2D2060144483C76527373264DC8CCD5A (void);
// 0x0000001A Facebook.Unity.AccessToken Facebook.Unity.AccessToken::get_CurrentAccessToken()
extern void AccessToken_get_CurrentAccessToken_mFC3AD4FDC234BBC75D5F0162B433F071DEF451CE (void);
// 0x0000001B System.Void Facebook.Unity.AccessToken::set_CurrentAccessToken(Facebook.Unity.AccessToken)
extern void AccessToken_set_CurrentAccessToken_m3BBCD50AC99B44C2EEEFD6435E54102D6DF51813 (void);
// 0x0000001C System.String Facebook.Unity.AccessToken::get_TokenString()
extern void AccessToken_get_TokenString_m2BC6FC8BDBE71CC6E643277AE93511FC115334FC (void);
// 0x0000001D System.Void Facebook.Unity.AccessToken::set_TokenString(System.String)
extern void AccessToken_set_TokenString_mEB2E6FEA665B176D9F0D3DDEA2C76C4F23B150B0 (void);
// 0x0000001E System.DateTime Facebook.Unity.AccessToken::get_ExpirationTime()
extern void AccessToken_get_ExpirationTime_mEF979739A4BA8B3D24B6979504C0B6820DC8EDF5 (void);
// 0x0000001F System.Void Facebook.Unity.AccessToken::set_ExpirationTime(System.DateTime)
extern void AccessToken_set_ExpirationTime_mB9E9326A6123FD5A3A19D644926BF469C56A3103 (void);
// 0x00000020 System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AccessToken::get_Permissions()
extern void AccessToken_get_Permissions_m321B455AEDDE47865CD0A9377749F77C12F292EF (void);
// 0x00000021 System.Void Facebook.Unity.AccessToken::set_Permissions(System.Collections.Generic.IEnumerable`1<System.String>)
extern void AccessToken_set_Permissions_mC796D8F35BF513959F14862BFE70F555A6E7AE99 (void);
// 0x00000022 System.String Facebook.Unity.AccessToken::get_UserId()
extern void AccessToken_get_UserId_m08B2AD1A2C81B4724ABC88319FB884D545364099 (void);
// 0x00000023 System.Void Facebook.Unity.AccessToken::set_UserId(System.String)
extern void AccessToken_set_UserId_m3E269BC0D164067DA1CEAC2457B987BAF5139E7D (void);
// 0x00000024 System.Nullable`1<System.DateTime> Facebook.Unity.AccessToken::get_LastRefresh()
extern void AccessToken_get_LastRefresh_mCD48467DC14BFF3F710FAE823DB374FB5B0E348C (void);
// 0x00000025 System.Void Facebook.Unity.AccessToken::set_LastRefresh(System.Nullable`1<System.DateTime>)
extern void AccessToken_set_LastRefresh_m37C0B3CFD66DF99A6133BED6B889D0CBBD011E52 (void);
// 0x00000026 System.String Facebook.Unity.AccessToken::ToString()
extern void AccessToken_ToString_m2F7FE35EBD366BDE64F29ED25E6DA84F384129F4 (void);
// 0x00000027 System.String Facebook.Unity.AccessToken::ToJson()
extern void AccessToken_ToJson_m6E41B1D78FC2D4C5E6E7589D4EF8DACD819E2009 (void);
// 0x00000028 System.String Facebook.Unity.CallbackManager::AddFacebookDelegate(Facebook.Unity.FacebookDelegate`1<T>)
// 0x00000029 System.Void Facebook.Unity.CallbackManager::OnFacebookResponse(Facebook.Unity.IInternalResult)
extern void CallbackManager_OnFacebookResponse_m8E448D080D0DC61AA76714CC41D31BDAA6DDE0DE (void);
// 0x0000002A System.Void Facebook.Unity.CallbackManager::CallCallback(System.Object,Facebook.Unity.IResult)
extern void CallbackManager_CallCallback_m2EF00E171C381298E4509143109BB46B72411BDA (void);
// 0x0000002B System.Boolean Facebook.Unity.CallbackManager::TryCallCallback(System.Object,Facebook.Unity.IResult)
// 0x0000002C System.Void Facebook.Unity.CallbackManager::.ctor()
extern void CallbackManager__ctor_mA1881E8783422989E78577DBBBB0364014A77C0C (void);
// 0x0000002D UnityEngine.GameObject Facebook.Unity.ComponentFactory::get_FacebookGameObject()
extern void ComponentFactory_get_FacebookGameObject_m409E4D19F346A8C994B478F2906C45D00A5F2685 (void);
// 0x0000002E T Facebook.Unity.ComponentFactory::GetComponent(Facebook.Unity.ComponentFactory_IfNotExist)
// 0x0000002F T Facebook.Unity.ComponentFactory::AddComponent()
// 0x00000030 System.Uri Facebook.Unity.Constants::get_GraphUrl()
extern void Constants_get_GraphUrl_mBCCB75EE9A467000B6A2077401B11C796496D7DE (void);
// 0x00000031 System.String Facebook.Unity.Constants::get_GraphApiUserAgent()
extern void Constants_get_GraphApiUserAgent_m47F8FFD6911F6DEC75F8778D31C3455B37A70C0A (void);
// 0x00000032 System.Boolean Facebook.Unity.Constants::get_IsEditor()
extern void Constants_get_IsEditor_m2D932238515AC9BA84B28143296C97DE21221985 (void);
// 0x00000033 System.Boolean Facebook.Unity.Constants::get_IsWeb()
extern void Constants_get_IsWeb_m69867949DCB782E8376E7937516B535942479E35 (void);
// 0x00000034 System.Boolean Facebook.Unity.Constants::get_IsGameroom()
extern void Constants_get_IsGameroom_m28CBD1D05E77EE1EA44E5A08DC3CBF16E23CE34D (void);
// 0x00000035 System.String Facebook.Unity.Constants::get_UnitySDKUserAgentSuffixLegacy()
extern void Constants_get_UnitySDKUserAgentSuffixLegacy_m444C1614AC50CBFD76BD8D9B3B2CA225220D4F2E (void);
// 0x00000036 System.String Facebook.Unity.Constants::get_UnitySDKUserAgent()
extern void Constants_get_UnitySDKUserAgent_m78C304A6138E30655D4B8745FF95C38AF967423C (void);
// 0x00000037 System.Boolean Facebook.Unity.Constants::get_DebugMode()
extern void Constants_get_DebugMode_mB467536EC9EE9BA5076D37728276E7574B8914F7 (void);
// 0x00000038 Facebook.Unity.FacebookUnityPlatform Facebook.Unity.Constants::get_CurrentPlatform()
extern void Constants_get_CurrentPlatform_mC5DF32B4DFDF845C112822C7CBDAF7F42BDDDB6C (void);
// 0x00000039 Facebook.Unity.FacebookUnityPlatform Facebook.Unity.Constants::GetCurrentPlatform()
extern void Constants_GetCurrentPlatform_m132C8DE506B1560F84B01F726BE886CEE5F4062F (void);
// 0x0000003A System.String Facebook.Unity.FB::get_AppId()
extern void FB_get_AppId_m2D22BF99542B8823D44F95BDB1B80ACAB16924C1 (void);
// 0x0000003B System.Void Facebook.Unity.FB::set_AppId(System.String)
extern void FB_set_AppId_m93206625F15482B14F7E0359B3B89B2993EEE51E (void);
// 0x0000003C System.String Facebook.Unity.FB::get_ClientToken()
extern void FB_get_ClientToken_m76F39D2E0FE9036E120A5D41AF4A3394F4B94F71 (void);
// 0x0000003D System.Void Facebook.Unity.FB::set_ClientToken(System.String)
extern void FB_set_ClientToken_mAB17D781E4499338DD5BFFFD20163DEF91898604 (void);
// 0x0000003E System.String Facebook.Unity.FB::get_GraphApiVersion()
extern void FB_get_GraphApiVersion_m280C689EBA58B227BA2D60E630657160D8BA3B32 (void);
// 0x0000003F System.Void Facebook.Unity.FB::set_GraphApiVersion(System.String)
extern void FB_set_GraphApiVersion_mAD0958C6D6CF0BB088377C7981664AC8F177F12A (void);
// 0x00000040 System.Boolean Facebook.Unity.FB::get_IsLoggedIn()
extern void FB_get_IsLoggedIn_mC2EEE5953DA036DDB37F58879BFFCE918D749D11 (void);
// 0x00000041 System.Boolean Facebook.Unity.FB::get_IsInitialized()
extern void FB_get_IsInitialized_m765E5BFFF09B410247464719ACAB53674F3BADB0 (void);
// 0x00000042 System.Boolean Facebook.Unity.FB::get_LimitAppEventUsage()
extern void FB_get_LimitAppEventUsage_m1256735762FD834695F39690BEBEC151304BEDB3 (void);
// 0x00000043 System.Void Facebook.Unity.FB::set_LimitAppEventUsage(System.Boolean)
extern void FB_set_LimitAppEventUsage_m71A2FA7752E78B37FB583D34843E33B9C304669D (void);
// 0x00000044 Facebook.Unity.IFacebook Facebook.Unity.FB::get_FacebookImpl()
extern void FB_get_FacebookImpl_m49419982C5ED53433D081C0B7675905CDA9DB235 (void);
// 0x00000045 System.Void Facebook.Unity.FB::set_FacebookImpl(Facebook.Unity.IFacebook)
extern void FB_set_FacebookImpl_mE4B33601C3271186A89A1A68CAA7DA5F842D47A7 (void);
// 0x00000046 System.String Facebook.Unity.FB::get_FacebookDomain()
extern void FB_get_FacebookDomain_m7BF4E9FC39B531DDBB89AB06DDA489462BAD0FF3 (void);
// 0x00000047 System.Void Facebook.Unity.FB::set_FacebookDomain(System.String)
extern void FB_set_FacebookDomain_m6F7C8D5E08D2373C8008D8BF4056D736D15FA156 (void);
// 0x00000048 Facebook.Unity.FB_OnDLLLoaded Facebook.Unity.FB::get_OnDLLLoadedDelegate()
extern void FB_get_OnDLLLoadedDelegate_mD904DC4A7EA9F53D255DF8C00A62AA3485E0ABEB (void);
// 0x00000049 System.Void Facebook.Unity.FB::set_OnDLLLoadedDelegate(Facebook.Unity.FB_OnDLLLoaded)
extern void FB_set_OnDLLLoadedDelegate_mC890CFBC76498E485F92908D0E402163F275138B (void);
// 0x0000004A System.Void Facebook.Unity.FB::Init(Facebook.Unity.InitDelegate,Facebook.Unity.HideUnityDelegate,System.String)
extern void FB_Init_mDEC40011E21B7212F034FE1EA48522300F352C9A (void);
// 0x0000004B System.Void Facebook.Unity.FB::Init(System.String,System.String,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void FB_Init_m26187AA10513D5C820166C833B572E059C71D547 (void);
// 0x0000004C System.Void Facebook.Unity.FB::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void FB_LogInWithPublishPermissions_m2F46245F5A9896300B36E9E7EAA63E3EE4A6E08B (void);
// 0x0000004D System.Void Facebook.Unity.FB::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void FB_LogInWithReadPermissions_m8EABF64EC2B27C55DE0AEC7F79F879A1C72FF05A (void);
// 0x0000004E System.Void Facebook.Unity.FB::LogOut()
extern void FB_LogOut_mA9AC9D27FB0613C856438F45BF0CF05D9BC6A9F4 (void);
// 0x0000004F System.Void Facebook.Unity.FB::AppRequest(System.String,Facebook.Unity.OGActionType,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void FB_AppRequest_m57795DB1D9646F0A9F92F737FF2BD8575965843A (void);
// 0x00000050 System.Void Facebook.Unity.FB::AppRequest(System.String,Facebook.Unity.OGActionType,System.String,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void FB_AppRequest_m5DAD4A15DC945645C67F901EB5870C1E0B59405C (void);
// 0x00000051 System.Void Facebook.Unity.FB::AppRequest(System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void FB_AppRequest_mBC0F4037191A80EAD19F2D69C7DB32637F6BE3CE (void);
// 0x00000052 System.Void Facebook.Unity.FB::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void FB_ShareLink_mC4C16325D47EBEF4CF643638CBC592F6BDB6F21B (void);
// 0x00000053 System.Void Facebook.Unity.FB::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void FB_FeedShare_mB58C64045EB6548AD8C183FA5856D0F28457459A (void);
// 0x00000054 System.Void Facebook.Unity.FB::API(System.String,Facebook.Unity.HttpMethod,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void FB_API_m287EAA82141532EB5682F51C8BF9962F7222D532 (void);
// 0x00000055 System.Void Facebook.Unity.FB::API(System.String,Facebook.Unity.HttpMethod,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>,UnityEngine.WWWForm)
extern void FB_API_m97B7C3D516619A78D0F2CE1674894AF413762861 (void);
// 0x00000056 System.Void Facebook.Unity.FB::ActivateApp()
extern void FB_ActivateApp_m8D35D9FB52A258DA4710D26A6AE833EC362C4E39 (void);
// 0x00000057 System.Void Facebook.Unity.FB::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void FB_GetAppLink_m4966CBDEBF8FE50869677B45C37C3F6B74E13A8C (void);
// 0x00000058 System.Void Facebook.Unity.FB::ClearAppLink()
extern void FB_ClearAppLink_m5FBDB1FB1C5C51B29165DD1BE63DFD2B8D9A46C7 (void);
// 0x00000059 System.Void Facebook.Unity.FB::LogAppEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FB_LogAppEvent_m13EED7F9998210E48D0245E1C8206C712250A80A (void);
// 0x0000005A System.Void Facebook.Unity.FB::LogPurchase(System.Decimal,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FB_LogPurchase_mEB04282BBA29CD970D4A3EC16767387482FA53E9 (void);
// 0x0000005B System.Void Facebook.Unity.FB::LogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FB_LogPurchase_m0030D52FD8CE5D8BC0DC80F6EB0C82D3CA93F0A3 (void);
// 0x0000005C System.Void Facebook.Unity.FB::LogVersion()
extern void FB_LogVersion_mCED7DB7EBDA13FCB7AA123F0AEFA7FCBB1AE0585 (void);
// 0x0000005D System.Void Facebook.Unity.FB::.ctor()
extern void FB__ctor_mC73C493766C9869765539936F102B38E4E21AABE (void);
// 0x0000005E System.Void Facebook.Unity.FB::.cctor()
extern void FB__cctor_mF7F24CC1836485E69DC5B800CEA307D297AF3FE7 (void);
// 0x0000005F System.Void Facebook.Unity.FB_OnDLLLoaded::.ctor(System.Object,System.IntPtr)
extern void OnDLLLoaded__ctor_m03DF552F8620334ED72E4544CDD2C94BE4CEA08D (void);
// 0x00000060 System.Void Facebook.Unity.FB_OnDLLLoaded::Invoke()
extern void OnDLLLoaded_Invoke_m54A99FCA613AA21E9A7B2193939952B287D4F525 (void);
// 0x00000061 System.IAsyncResult Facebook.Unity.FB_OnDLLLoaded::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnDLLLoaded_BeginInvoke_m1F9C668AE67CEAB2F3B7BF29F5DC49BB07ED7A31 (void);
// 0x00000062 System.Void Facebook.Unity.FB_OnDLLLoaded::EndInvoke(System.IAsyncResult)
extern void OnDLLLoaded_EndInvoke_mE16D77F58B5D6CCF2EADBE16FE8CEBF8EEEC98F7 (void);
// 0x00000063 Facebook.Unity.Mobile.IMobileFacebook Facebook.Unity.FB_Mobile::get_MobileFacebookImpl()
extern void Mobile_get_MobileFacebookImpl_mE7556F9B4FE66323609E654E3AF2BD3280A026CA (void);
// 0x00000064 System.Boolean Facebook.Unity.FB_Mobile::IsImplicitPurchaseLoggingEnabled()
extern void Mobile_IsImplicitPurchaseLoggingEnabled_mCF389902E05A7359FF248DB5129825C822885A7A (void);
// 0x00000065 Facebook.Unity.FacebookGameObject Facebook.Unity.FB_CompiledFacebookLoader::get_FBGameObject()
// 0x00000066 System.Void Facebook.Unity.FB_CompiledFacebookLoader::Start()
extern void CompiledFacebookLoader_Start_m3D0EC7DCFC50D29795B927D903BE2B7EE3F92241 (void);
// 0x00000067 System.Void Facebook.Unity.FB_CompiledFacebookLoader::.ctor()
extern void CompiledFacebookLoader__ctor_mE9514E11C196C6C4E26E4283C21E9E847B1D8A5E (void);
// 0x00000068 System.Void Facebook.Unity.FB_<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m07B31A58843315B3E4D0A2A3D3F9C8747C85F248 (void);
// 0x00000069 System.Void Facebook.Unity.FB_<>c__DisplayClass35_0::<Init>b__0()
extern void U3CU3Ec__DisplayClass35_0_U3CInitU3Eb__0_m65F785A4A710A3C12E914552F51AC33E4A795BEA (void);
// 0x0000006A System.Void Facebook.Unity.FB_<>c__DisplayClass35_0::<Init>b__1()
extern void U3CU3Ec__DisplayClass35_0_U3CInitU3Eb__1_mDAD13E71D1B8572B8EC3427E0AF0437F394CA084 (void);
// 0x0000006B System.Void Facebook.Unity.FB_<>c__DisplayClass35_0::<Init>b__2()
extern void U3CU3Ec__DisplayClass35_0_U3CInitU3Eb__2_m767562A0458CE8E715DECE47431C3412EC0F1EE2 (void);
// 0x0000006C System.Void Facebook.Unity.FB_<>c__DisplayClass35_0::<Init>b__3()
extern void U3CU3Ec__DisplayClass35_0_U3CInitU3Eb__3_mBD9AE71C968B91DEBA11C6BCC501EFB444B624E3 (void);
// 0x0000006D System.Void Facebook.Unity.FB_<>c__DisplayClass35_0::<Init>b__4()
extern void U3CU3Ec__DisplayClass35_0_U3CInitU3Eb__4_m836E5F9F99D2E1A52F46DD6035BE0FFA4A851D4F (void);
// 0x0000006E System.Void Facebook.Unity.FacebookBase::.ctor(Facebook.Unity.CallbackManager)
extern void FacebookBase__ctor_mA8BC6D69FEF907902CB7F52126107ED9D40B9E02 (void);
// 0x0000006F System.Boolean Facebook.Unity.FacebookBase::get_LimitEventUsage()
// 0x00000070 System.Void Facebook.Unity.FacebookBase::set_LimitEventUsage(System.Boolean)
// 0x00000071 System.String Facebook.Unity.FacebookBase::get_SDKName()
// 0x00000072 System.String Facebook.Unity.FacebookBase::get_SDKVersion()
// 0x00000073 System.String Facebook.Unity.FacebookBase::get_SDKUserAgent()
extern void FacebookBase_get_SDKUserAgent_mF2887AD5BDB7227689D8A075D0E9634345993A7E (void);
// 0x00000074 System.Boolean Facebook.Unity.FacebookBase::get_LoggedIn()
extern void FacebookBase_get_LoggedIn_mC91BBC2768BA3E97C004BF5A2DE129676C80BAD3 (void);
// 0x00000075 System.Boolean Facebook.Unity.FacebookBase::get_Initialized()
extern void FacebookBase_get_Initialized_mF2C6C9D0BAE7E77FDE08C4DCA15AF12EB93B0DCC (void);
// 0x00000076 System.Void Facebook.Unity.FacebookBase::set_Initialized(System.Boolean)
extern void FacebookBase_set_Initialized_mDAB99FF264AFD17E62CE2E0A47E4D35783D1F040 (void);
// 0x00000077 Facebook.Unity.CallbackManager Facebook.Unity.FacebookBase::get_CallbackManager()
extern void FacebookBase_get_CallbackManager_mBB4EC92D8211AC4CAAFCB3823B901F75E24E537C (void);
// 0x00000078 System.Void Facebook.Unity.FacebookBase::set_CallbackManager(Facebook.Unity.CallbackManager)
extern void FacebookBase_set_CallbackManager_m5A1C5A8B0F49D230AC1842183A2FE9B461F04FEA (void);
// 0x00000079 System.Void Facebook.Unity.FacebookBase::Init(Facebook.Unity.InitDelegate)
extern void FacebookBase_Init_mF0DEC65A32CEE3EA9CA1E42D1C5857FDD10090A9 (void);
// 0x0000007A System.Void Facebook.Unity.FacebookBase::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
// 0x0000007B System.Void Facebook.Unity.FacebookBase::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
// 0x0000007C System.Void Facebook.Unity.FacebookBase::LogOut()
extern void FacebookBase_LogOut_m2B92288C9F62ADCF223794A0B6A5DFB8C3329598 (void);
// 0x0000007D System.Void Facebook.Unity.FacebookBase::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
// 0x0000007E System.Void Facebook.Unity.FacebookBase::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
// 0x0000007F System.Void Facebook.Unity.FacebookBase::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
// 0x00000080 System.Void Facebook.Unity.FacebookBase::API(System.String,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void FacebookBase_API_m8E99C456F7FE25B7E5EE99828F15CB54117C6B75 (void);
// 0x00000081 System.Void Facebook.Unity.FacebookBase::API(System.String,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void FacebookBase_API_m3066DA5FF9F21E0081A63983AE4F54A692A4F31C (void);
// 0x00000082 System.Void Facebook.Unity.FacebookBase::ActivateApp(System.String)
// 0x00000083 System.Void Facebook.Unity.FacebookBase::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
// 0x00000084 System.Void Facebook.Unity.FacebookBase::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x00000085 System.Void Facebook.Unity.FacebookBase::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x00000086 System.Void Facebook.Unity.FacebookBase::OnInitComplete(Facebook.Unity.ResultContainer)
extern void FacebookBase_OnInitComplete_m55A6458A9046218C6DFDC79F596071CE3DECFA5E (void);
// 0x00000087 System.Void Facebook.Unity.FacebookBase::OnLoginComplete(Facebook.Unity.ResultContainer)
// 0x00000088 System.Void Facebook.Unity.FacebookBase::OnLogoutComplete(Facebook.Unity.ResultContainer)
extern void FacebookBase_OnLogoutComplete_mF7A476CD7F61D94B664E334C180C23F4CE04BD7C (void);
// 0x00000089 System.Void Facebook.Unity.FacebookBase::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
// 0x0000008A System.Void Facebook.Unity.FacebookBase::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
// 0x0000008B System.Void Facebook.Unity.FacebookBase::OnShareLinkComplete(Facebook.Unity.ResultContainer)
// 0x0000008C System.Void Facebook.Unity.FacebookBase::ValidateAppRequestArgs(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void FacebookBase_ValidateAppRequestArgs_mAEE8F85CDDB31ECCA7D6B0C9696FCB20C7985936 (void);
// 0x0000008D System.Void Facebook.Unity.FacebookBase::OnAuthResponse(Facebook.Unity.LoginResult)
extern void FacebookBase_OnAuthResponse_mAB7E96567AC9DB0C460AE8D7A2157EAB02D5019B (void);
// 0x0000008E System.Collections.Generic.IDictionary`2<System.String,System.String> Facebook.Unity.FacebookBase::CopyByValue(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void FacebookBase_CopyByValue_m5DB6308CEB8094970A1A545C1E4300E353A24B15 (void);
// 0x0000008F System.Uri Facebook.Unity.FacebookBase::GetGraphUrl(System.String)
extern void FacebookBase_GetGraphUrl_mD37F29511EDB58929C37AE5960C2CC0234C48C08 (void);
// 0x00000090 System.Void Facebook.Unity.FacebookBase::<OnInitComplete>b__35_0(Facebook.Unity.ILoginResult)
extern void FacebookBase_U3COnInitCompleteU3Eb__35_0_mD92C0EF2D2C5BABEC3C925F188B4AF38F4B35976 (void);
// 0x00000091 System.Void Facebook.Unity.FacebookBase_<>c::.cctor()
extern void U3CU3Ec__cctor_m6506A0FBEF436C23C63EE7ECE70BCFF0533F9159 (void);
// 0x00000092 System.Void Facebook.Unity.FacebookBase_<>c::.ctor()
extern void U3CU3Ec__ctor_mFC2B71BD2E5E4037163B00C41E28305573E5A824 (void);
// 0x00000093 System.Boolean Facebook.Unity.FacebookBase_<>c::<ValidateAppRequestArgs>b__41_0(System.String)
extern void U3CU3Ec_U3CValidateAppRequestArgsU3Eb__41_0_m013696598894053F2262D8AB06615020055E358F (void);
// 0x00000094 System.Void Facebook.Unity.InitDelegate::.ctor(System.Object,System.IntPtr)
extern void InitDelegate__ctor_m638E3688B91CA78C3C97593EA85EDC43C9B651A1 (void);
// 0x00000095 System.Void Facebook.Unity.InitDelegate::Invoke()
extern void InitDelegate_Invoke_m364F872A6E2F2D65DCC8D6157517F23E92DC41C0 (void);
// 0x00000096 System.IAsyncResult Facebook.Unity.InitDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void InitDelegate_BeginInvoke_m95F07509701D7134A1D2C0DFF1A0150F01271B85 (void);
// 0x00000097 System.Void Facebook.Unity.InitDelegate::EndInvoke(System.IAsyncResult)
extern void InitDelegate_EndInvoke_mFE8582F7EF719F7BDB0C624B90782107DF1CAF1A (void);
// 0x00000098 System.Void Facebook.Unity.FacebookDelegate`1::.ctor(System.Object,System.IntPtr)
// 0x00000099 System.Void Facebook.Unity.FacebookDelegate`1::Invoke(T)
// 0x0000009A System.IAsyncResult Facebook.Unity.FacebookDelegate`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x0000009B System.Void Facebook.Unity.FacebookDelegate`1::EndInvoke(System.IAsyncResult)
// 0x0000009C System.Void Facebook.Unity.HideUnityDelegate::.ctor(System.Object,System.IntPtr)
extern void HideUnityDelegate__ctor_m1A4604B0620E95856B9F24983A754E19CE068E07 (void);
// 0x0000009D System.Void Facebook.Unity.HideUnityDelegate::Invoke(System.Boolean)
extern void HideUnityDelegate_Invoke_m1D7F4C7CAFE39644D2AC2CAFFF9115942E5675EF (void);
// 0x0000009E System.IAsyncResult Facebook.Unity.HideUnityDelegate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void HideUnityDelegate_BeginInvoke_mC7DFCE7615B532DF459D32D46DFA00ECB1B6C26B (void);
// 0x0000009F System.Void Facebook.Unity.HideUnityDelegate::EndInvoke(System.IAsyncResult)
extern void HideUnityDelegate_EndInvoke_m60AE1EB74D0205443CCFF8BD6453CF2E8BCB83A4 (void);
// 0x000000A0 Facebook.Unity.IFacebookImplementation Facebook.Unity.FacebookGameObject::get_Facebook()
extern void FacebookGameObject_get_Facebook_mF99AFAC61EECA0A3CAA17972DCB16979F4BB9494 (void);
// 0x000000A1 System.Void Facebook.Unity.FacebookGameObject::set_Facebook(Facebook.Unity.IFacebookImplementation)
extern void FacebookGameObject_set_Facebook_m077F56A9716B7C34D4B78C75D39A81CF28F5555A (void);
// 0x000000A2 System.Void Facebook.Unity.FacebookGameObject::Awake()
extern void FacebookGameObject_Awake_m1C9ED99A2A63E4C54503EF6174627580D9E3E6BD (void);
// 0x000000A3 System.Void Facebook.Unity.FacebookGameObject::OnInitComplete(System.String)
extern void FacebookGameObject_OnInitComplete_m74B99C1E3ED06A52991C82989AF47C458E144683 (void);
// 0x000000A4 System.Void Facebook.Unity.FacebookGameObject::OnLoginComplete(System.String)
extern void FacebookGameObject_OnLoginComplete_mE2B244ED30E79DAA91FCA0B3F9DFD75551A03C4E (void);
// 0x000000A5 System.Void Facebook.Unity.FacebookGameObject::OnLogoutComplete(System.String)
extern void FacebookGameObject_OnLogoutComplete_m0F63EBAE06A897EA87A674B8BBB3336CC1F74176 (void);
// 0x000000A6 System.Void Facebook.Unity.FacebookGameObject::OnGetAppLinkComplete(System.String)
extern void FacebookGameObject_OnGetAppLinkComplete_m207A0F329BF69A815D85CABDC3740180E32EC674 (void);
// 0x000000A7 System.Void Facebook.Unity.FacebookGameObject::OnAppRequestsComplete(System.String)
extern void FacebookGameObject_OnAppRequestsComplete_mDCE0754CA9ACECE25F13235A00168E9C74000072 (void);
// 0x000000A8 System.Void Facebook.Unity.FacebookGameObject::OnShareLinkComplete(System.String)
extern void FacebookGameObject_OnShareLinkComplete_mEE622A0652888798F0B6E72817822B13053752AF (void);
// 0x000000A9 System.Void Facebook.Unity.FacebookGameObject::OnAwake()
extern void FacebookGameObject_OnAwake_mBB38840A8EC5FAEE0D3FFFD58600FDF19169FD11 (void);
// 0x000000AA System.Void Facebook.Unity.FacebookGameObject::.ctor()
extern void FacebookGameObject__ctor_m3B901E098CBDEE9F4C898C1A1E34DF1BBFFF369E (void);
// 0x000000AB System.String Facebook.Unity.FacebookSdkVersion::get_Build()
extern void FacebookSdkVersion_get_Build_mA37D5E8676B72FA63C562EA5EEAE3F293A3933E0 (void);
// 0x000000AC System.Boolean Facebook.Unity.IFacebook::get_LoggedIn()
// 0x000000AD System.Boolean Facebook.Unity.IFacebook::get_LimitEventUsage()
// 0x000000AE System.Void Facebook.Unity.IFacebook::set_LimitEventUsage(System.Boolean)
// 0x000000AF System.String Facebook.Unity.IFacebook::get_SDKUserAgent()
// 0x000000B0 System.Boolean Facebook.Unity.IFacebook::get_Initialized()
// 0x000000B1 System.Void Facebook.Unity.IFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
// 0x000000B2 System.Void Facebook.Unity.IFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
// 0x000000B3 System.Void Facebook.Unity.IFacebook::LogOut()
// 0x000000B4 System.Void Facebook.Unity.IFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
// 0x000000B5 System.Void Facebook.Unity.IFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
// 0x000000B6 System.Void Facebook.Unity.IFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
// 0x000000B7 System.Void Facebook.Unity.IFacebook::API(System.String,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
// 0x000000B8 System.Void Facebook.Unity.IFacebook::API(System.String,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
// 0x000000B9 System.Void Facebook.Unity.IFacebook::ActivateApp(System.String)
// 0x000000BA System.Void Facebook.Unity.IFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
// 0x000000BB System.Void Facebook.Unity.IFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x000000BC System.Void Facebook.Unity.IFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x000000BD System.Void Facebook.Unity.IFacebookCallbackHandler::OnInitComplete(System.String)
// 0x000000BE System.Void Facebook.Unity.IFacebookCallbackHandler::OnLoginComplete(System.String)
// 0x000000BF System.Void Facebook.Unity.IFacebookCallbackHandler::OnAppRequestsComplete(System.String)
// 0x000000C0 System.Void Facebook.Unity.IFacebookCallbackHandler::OnShareLinkComplete(System.String)
// 0x000000C1 System.Void Facebook.Unity.IFacebookResultHandler::OnInitComplete(Facebook.Unity.ResultContainer)
// 0x000000C2 System.Void Facebook.Unity.IFacebookResultHandler::OnLoginComplete(Facebook.Unity.ResultContainer)
// 0x000000C3 System.Void Facebook.Unity.IFacebookResultHandler::OnLogoutComplete(Facebook.Unity.ResultContainer)
// 0x000000C4 System.Void Facebook.Unity.IFacebookResultHandler::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
// 0x000000C5 System.Void Facebook.Unity.IFacebookResultHandler::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
// 0x000000C6 System.Void Facebook.Unity.IFacebookResultHandler::OnShareLinkComplete(Facebook.Unity.ResultContainer)
// 0x000000C7 System.Void Facebook.Unity.MethodArguments::.ctor()
extern void MethodArguments__ctor_m553F1F675A365BD7EAEA407618085B1E85B86CF5 (void);
// 0x000000C8 System.Void Facebook.Unity.MethodArguments::.ctor(Facebook.Unity.MethodArguments)
extern void MethodArguments__ctor_mCD0FEAC74634F6FD43B4F6B5EB127ED7024FAAAD (void);
// 0x000000C9 System.Void Facebook.Unity.MethodArguments::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void MethodArguments__ctor_m98DD81AB8781696CE01A4AA728CEFF3F8D91F5B1 (void);
// 0x000000CA System.Void Facebook.Unity.MethodArguments::AddPrimative(System.String,T)
// 0x000000CB System.Void Facebook.Unity.MethodArguments::AddNullablePrimitive(System.String,System.Nullable`1<T>)
// 0x000000CC System.Void Facebook.Unity.MethodArguments::AddString(System.String,System.String)
extern void MethodArguments_AddString_m0AC2B248277E3BB0CD32BB2378B742FA1AF4901C (void);
// 0x000000CD System.Void Facebook.Unity.MethodArguments::AddCommaSeparatedList(System.String,System.Collections.Generic.IEnumerable`1<System.String>)
extern void MethodArguments_AddCommaSeparatedList_m2777FC9569E19FF94DF85DEEB717F85E392761BE (void);
// 0x000000CE System.Void Facebook.Unity.MethodArguments::AddDictionary(System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void MethodArguments_AddDictionary_mBEF8B6346088AB15B3676AE7BC1BCE7385907D09 (void);
// 0x000000CF System.Void Facebook.Unity.MethodArguments::AddList(System.String,System.Collections.Generic.IEnumerable`1<T>)
// 0x000000D0 System.Void Facebook.Unity.MethodArguments::AddUri(System.String,System.Uri)
extern void MethodArguments_AddUri_m194EB0E3882917D39C37D79D16B3D71637E83191 (void);
// 0x000000D1 System.String Facebook.Unity.MethodArguments::ToJsonString()
extern void MethodArguments_ToJsonString_mB4678DBDEA648BEE531004BFA562EA532627A3DE (void);
// 0x000000D2 System.Collections.Generic.Dictionary`2<System.String,System.String> Facebook.Unity.MethodArguments::ToStringDict(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void MethodArguments_ToStringDict_m96E5E453230DEF15EC3DFB9947B3428CF4C75861 (void);
// 0x000000D3 System.Void Facebook.Unity.MethodCall`1::.ctor(Facebook.Unity.FacebookBase,System.String)
// 0x000000D4 System.String Facebook.Unity.MethodCall`1::get_MethodName()
// 0x000000D5 System.Void Facebook.Unity.MethodCall`1::set_MethodName(System.String)
// 0x000000D6 Facebook.Unity.FacebookDelegate`1<T> Facebook.Unity.MethodCall`1::get_Callback()
// 0x000000D7 System.Void Facebook.Unity.MethodCall`1::set_Callback(Facebook.Unity.FacebookDelegate`1<T>)
// 0x000000D8 System.Void Facebook.Unity.MethodCall`1::set_FacebookImpl(Facebook.Unity.FacebookBase)
// 0x000000D9 System.Void Facebook.Unity.MethodCall`1::set_Parameters(Facebook.Unity.MethodArguments)
// 0x000000DA System.Void Facebook.Unity.MethodCall`1::Call(Facebook.Unity.MethodArguments)
// 0x000000DB System.Void Facebook.Unity.AccessTokenRefreshResult::.ctor(Facebook.Unity.ResultContainer)
extern void AccessTokenRefreshResult__ctor_m3B283FB973D96CCED9E2FCAF2DDAE5DCEE05C15E (void);
// 0x000000DC Facebook.Unity.AccessToken Facebook.Unity.AccessTokenRefreshResult::get_AccessToken()
extern void AccessTokenRefreshResult_get_AccessToken_mCA5BA422E199DE494C98EF7F1E0123620FE561F0 (void);
// 0x000000DD System.Void Facebook.Unity.AccessTokenRefreshResult::set_AccessToken(Facebook.Unity.AccessToken)
extern void AccessTokenRefreshResult_set_AccessToken_m78C498512639C4F8B59CC2EC474B80CEA6DF46E3 (void);
// 0x000000DE System.String Facebook.Unity.AccessTokenRefreshResult::ToString()
extern void AccessTokenRefreshResult_ToString_m3F52CFC846F0311E4B5053021FAD90DD0C94D220 (void);
// 0x000000DF System.Void Facebook.Unity.AppLinkResult::.ctor(Facebook.Unity.ResultContainer)
extern void AppLinkResult__ctor_m9D10E105C205A8AB5E6FF4B5CCE7C390D76E8724 (void);
// 0x000000E0 System.String Facebook.Unity.AppLinkResult::get_Url()
extern void AppLinkResult_get_Url_m0183A6631B0333CA8D8683C7C5E7DB1EF8AAB022 (void);
// 0x000000E1 System.Void Facebook.Unity.AppLinkResult::set_Url(System.String)
extern void AppLinkResult_set_Url_m5699010FA9132CD64D84A9858C3C09F1FAC2DBDD (void);
// 0x000000E2 System.String Facebook.Unity.AppLinkResult::get_TargetUrl()
extern void AppLinkResult_get_TargetUrl_m5AF605EE11256D8B85920204C7A1C0C3B99673DB (void);
// 0x000000E3 System.Void Facebook.Unity.AppLinkResult::set_TargetUrl(System.String)
extern void AppLinkResult_set_TargetUrl_m843A466406F9A1D67329C444319462406BE8CE90 (void);
// 0x000000E4 System.String Facebook.Unity.AppLinkResult::get_Ref()
extern void AppLinkResult_get_Ref_mA48E907F838181C82B1F6B4DAB73B1DF389660C2 (void);
// 0x000000E5 System.Void Facebook.Unity.AppLinkResult::set_Ref(System.String)
extern void AppLinkResult_set_Ref_m69406B2299AA226B6B22AAD9F09FC7DF0AC39F00 (void);
// 0x000000E6 System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.AppLinkResult::get_Extras()
extern void AppLinkResult_get_Extras_m4B7997A6BCF3C0906E58DE02E8FBE8F3344E86DD (void);
// 0x000000E7 System.Void Facebook.Unity.AppLinkResult::set_Extras(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void AppLinkResult_set_Extras_m22CD249CC5B4611B7BC992B883D07B061906F44C (void);
// 0x000000E8 System.String Facebook.Unity.AppLinkResult::ToString()
extern void AppLinkResult_ToString_m0122EA6DBED254E210DE83E18566BD94EFDFF830 (void);
// 0x000000E9 System.Void Facebook.Unity.AppRequestResult::.ctor(Facebook.Unity.ResultContainer)
extern void AppRequestResult__ctor_m9CFE5CCB55E06C2F89D5AE07EEFC86009148640F (void);
// 0x000000EA System.String Facebook.Unity.AppRequestResult::get_RequestID()
extern void AppRequestResult_get_RequestID_mF3C6D6F4EE63C4A96ADA75992FA224F7C8B56BAB (void);
// 0x000000EB System.Void Facebook.Unity.AppRequestResult::set_RequestID(System.String)
extern void AppRequestResult_set_RequestID_m58DE037A9089372FE258074E9F7828236407E915 (void);
// 0x000000EC System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AppRequestResult::get_To()
extern void AppRequestResult_get_To_mDCF143439DAD87DDF7F7E99EE42FD4F95B5B6853 (void);
// 0x000000ED System.Void Facebook.Unity.AppRequestResult::set_To(System.Collections.Generic.IEnumerable`1<System.String>)
extern void AppRequestResult_set_To_m360961FF55635DEBF06B4081412EFCCE5D787B20 (void);
// 0x000000EE System.String Facebook.Unity.AppRequestResult::ToString()
extern void AppRequestResult_ToString_m5E0FA54B3FE2EFD748AB3F744C5170D682B8019E (void);
// 0x000000EF System.Void Facebook.Unity.GraphResult::.ctor(UnityEngine.WWW)
extern void GraphResult__ctor_m1BC0212616C36CFC283E97A30BC0FE59D346EAA4 (void);
// 0x000000F0 System.Void Facebook.Unity.GraphResult::set_ResultList(System.Collections.Generic.IList`1<System.Object>)
extern void GraphResult_set_ResultList_m879FD2B5A926796F7DAC7AF0221DA2F64312FF00 (void);
// 0x000000F1 System.Void Facebook.Unity.GraphResult::set_Texture(UnityEngine.Texture2D)
extern void GraphResult_set_Texture_mD9642743F541436777BA59E068787AE897114761 (void);
// 0x000000F2 System.Void Facebook.Unity.GraphResult::Init(System.String)
extern void GraphResult_Init_m136A90BD553319F6B8BF4F24F6ED1355171C939F (void);
// 0x000000F3 System.String Facebook.Unity.IInternalResult::get_CallbackId()
// 0x000000F4 System.String Facebook.Unity.IResult::get_Error()
// 0x000000F5 System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.IResult::get_ResultDictionary()
// 0x000000F6 System.Void Facebook.Unity.LoginResult::.ctor(Facebook.Unity.ResultContainer)
extern void LoginResult__ctor_mD1810851AA9A258021A6A3C6E807387FBDBADEBA (void);
// 0x000000F7 Facebook.Unity.AccessToken Facebook.Unity.LoginResult::get_AccessToken()
extern void LoginResult_get_AccessToken_m3E683AB48B469DDFA1339E5F018F037F40309667 (void);
// 0x000000F8 System.Void Facebook.Unity.LoginResult::set_AccessToken(Facebook.Unity.AccessToken)
extern void LoginResult_set_AccessToken_m1F404268E6BA26FA484508DA3AE3C027D257225C (void);
// 0x000000F9 System.String Facebook.Unity.LoginResult::ToString()
extern void LoginResult_ToString_m6F56A19B00621A5ED0C5EF233A05638A559F7817 (void);
// 0x000000FA System.Void Facebook.Unity.LoginResult::.cctor()
extern void LoginResult__cctor_m40FAC52926CEE52C50E56AA6CF57D8144D9C254E (void);
// 0x000000FB System.Void Facebook.Unity.PayResult::.ctor(Facebook.Unity.ResultContainer)
extern void PayResult__ctor_m12E078D8239CE20538DAA0B4CF4A36D2E16CA06C (void);
// 0x000000FC System.Int64 Facebook.Unity.PayResult::get_ErrorCode()
extern void PayResult_get_ErrorCode_mFAD4EF7DECBD43807E5399050FA5DF6D92032128 (void);
// 0x000000FD System.String Facebook.Unity.PayResult::ToString()
extern void PayResult_ToString_mE30534F4D6FE1084AF9F982B5FE4EFD738AE88A8 (void);
// 0x000000FE System.Void Facebook.Unity.ResultBase::.ctor(Facebook.Unity.ResultContainer)
extern void ResultBase__ctor_m06339C42987D74E7508EFFF42E3B442F07ABADD9 (void);
// 0x000000FF System.Void Facebook.Unity.ResultBase::.ctor(Facebook.Unity.ResultContainer,System.String,System.Boolean)
extern void ResultBase__ctor_mE54080B5FAC22E3A79E6183C86C0A24F2841A7C3 (void);
// 0x00000100 System.String Facebook.Unity.ResultBase::get_Error()
extern void ResultBase_get_Error_m9B2CB507FC8710071BBC98D3BC4BC2CE955A5EBF (void);
// 0x00000101 System.Void Facebook.Unity.ResultBase::set_Error(System.String)
extern void ResultBase_set_Error_mA79A34249FB05045FEC1D001F6079859947ECB03 (void);
// 0x00000102 System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultBase::get_ResultDictionary()
extern void ResultBase_get_ResultDictionary_mD7A894868B8B6AAD534006F2C7AF6125889BD53F (void);
// 0x00000103 System.Void Facebook.Unity.ResultBase::set_ResultDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultBase_set_ResultDictionary_m8C7AFB9266DD67FC9A63EFF676D735DABF0F8C1C (void);
// 0x00000104 System.String Facebook.Unity.ResultBase::get_RawResult()
extern void ResultBase_get_RawResult_m560FEC7F374443E9FC6A13A8AA0BC050647390E3 (void);
// 0x00000105 System.Void Facebook.Unity.ResultBase::set_RawResult(System.String)
extern void ResultBase_set_RawResult_m1DDC204ED4D768556114B4728BA7F0A0317F34ED (void);
// 0x00000106 System.Boolean Facebook.Unity.ResultBase::get_Cancelled()
extern void ResultBase_get_Cancelled_m3746E4DD75E9920347C86D65A056CFC44057855B (void);
// 0x00000107 System.Void Facebook.Unity.ResultBase::set_Cancelled(System.Boolean)
extern void ResultBase_set_Cancelled_mD8A619C2E4B109483811DEB68FA4739557419E4E (void);
// 0x00000108 System.String Facebook.Unity.ResultBase::get_CallbackId()
extern void ResultBase_get_CallbackId_m55ED0D34F6F77059BEF560D61BE9D9C8AFDE57DA (void);
// 0x00000109 System.Void Facebook.Unity.ResultBase::set_CallbackId(System.String)
extern void ResultBase_set_CallbackId_mEB6E0E719A57BB0567E926A3D7BEB9AF95C1C07F (void);
// 0x0000010A System.Nullable`1<System.Int64> Facebook.Unity.ResultBase::get_CanvasErrorCode()
extern void ResultBase_get_CanvasErrorCode_m6514A860C12D8B3D347C3902AA76644088BE3C02 (void);
// 0x0000010B System.Void Facebook.Unity.ResultBase::set_CanvasErrorCode(System.Nullable`1<System.Int64>)
extern void ResultBase_set_CanvasErrorCode_m83213A57E092845E9D6F91D85460555512783BA0 (void);
// 0x0000010C System.String Facebook.Unity.ResultBase::ToString()
extern void ResultBase_ToString_m1726DD872BF67CC1AA3921F30529994F55B42D74 (void);
// 0x0000010D System.Void Facebook.Unity.ResultBase::Init(Facebook.Unity.ResultContainer,System.String,System.Boolean,System.String)
extern void ResultBase_Init_m8FA9A3F5AE4BEC2F679A719459DD515665FBC983 (void);
// 0x0000010E System.String Facebook.Unity.ResultBase::GetErrorValue(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultBase_GetErrorValue_m12D7A740EA1A542A9BEC1068616AB68C4F39CC4A (void);
// 0x0000010F System.Boolean Facebook.Unity.ResultBase::GetCancelledValue(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultBase_GetCancelledValue_m50832E83E3A02AD0F57446CE33165E2118E71C8F (void);
// 0x00000110 System.String Facebook.Unity.ResultBase::GetCallbackId(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultBase_GetCallbackId_m7A14088D5D4FC2D1A7B0BE1FAB02CC7368C1FA88 (void);
// 0x00000111 System.Void Facebook.Unity.ResultContainer::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultContainer__ctor_m27DF61D20D816EB82666C07D75308075489CFCB2 (void);
// 0x00000112 System.Void Facebook.Unity.ResultContainer::.ctor(System.String)
extern void ResultContainer__ctor_m59976A61C4C6205111114A6B134063594AE544D2 (void);
// 0x00000113 System.String Facebook.Unity.ResultContainer::get_RawResult()
extern void ResultContainer_get_RawResult_m9C15E9CF5169B25AA93B499777B53D0B2F2FCB05 (void);
// 0x00000114 System.Void Facebook.Unity.ResultContainer::set_RawResult(System.String)
extern void ResultContainer_set_RawResult_m4BFB96584896E72A00636176455D01FCF8B8C230 (void);
// 0x00000115 System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultContainer::get_ResultDictionary()
extern void ResultContainer_get_ResultDictionary_m5431E7FD6F5EF5735BB539E77604B9A98F4FFE1C (void);
// 0x00000116 System.Void Facebook.Unity.ResultContainer::set_ResultDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultContainer_set_ResultDictionary_mFE3745393910DA1BD6C052EBCCED08560E453DC2 (void);
// 0x00000117 System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultContainer::GetWebFormattedResponseDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void ResultContainer_GetWebFormattedResponseDictionary_m9DB804A24B903B7BCA56B9E3F0B649C56206407F (void);
// 0x00000118 System.Void Facebook.Unity.ShareResult::.ctor(Facebook.Unity.ResultContainer)
extern void ShareResult__ctor_m35188E40DE537543889C68BE1D485FD6309E117D (void);
// 0x00000119 System.String Facebook.Unity.ShareResult::get_PostId()
extern void ShareResult_get_PostId_mCEFB90DD2DEA504897A0866C1D2DA63067385F7F (void);
// 0x0000011A System.Void Facebook.Unity.ShareResult::set_PostId(System.String)
extern void ShareResult_set_PostId_m12828954E2DD74323354A545EB9B1E1A2C30AF5D (void);
// 0x0000011B System.String Facebook.Unity.ShareResult::get_PostIDKey()
extern void ShareResult_get_PostIDKey_m49656A1ACD481B53CE19C5242952FBA03CC32FC0 (void);
// 0x0000011C System.String Facebook.Unity.ShareResult::ToString()
extern void ShareResult_ToString_mE93DCD9E5154263B22D06AE593E971721D9B36CB (void);
// 0x0000011D System.Void Facebook.Unity.AsyncRequestString::Post(System.Uri,System.Collections.Generic.Dictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_Post_mF798AD6971F004AB7F8E55CF5CC0903AEB6D99BC (void);
// 0x0000011E System.Void Facebook.Unity.AsyncRequestString::Get(System.Uri,System.Collections.Generic.Dictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_Get_m2550311682EEB20667A07119F7E31378C0F6EB9B (void);
// 0x0000011F System.Void Facebook.Unity.AsyncRequestString::Request(System.Uri,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_Request_m7F8CB77DF62263184608F2653722D97969C2D675 (void);
// 0x00000120 System.Void Facebook.Unity.AsyncRequestString::Request(System.Uri,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_Request_m1ADE5253A3BDF38A3905F38C8E064BB1EE36A7F0 (void);
// 0x00000121 System.Collections.IEnumerator Facebook.Unity.AsyncRequestString::Start()
extern void AsyncRequestString_Start_m0FB3015D0D982AD6DE0F14C3CAB1BE136F7DC3C4 (void);
// 0x00000122 Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetUrl(System.Uri)
extern void AsyncRequestString_SetUrl_mB4D220EDB7E14460387CFCF39603D21C6FC3EFCF (void);
// 0x00000123 Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetMethod(Facebook.Unity.HttpMethod)
extern void AsyncRequestString_SetMethod_mC0F03004C2DAA50622C4518051F0E9B016F27EB4 (void);
// 0x00000124 Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetFormData(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void AsyncRequestString_SetFormData_m6A2521AAE2FDA6F7B432E9A39629C61632FE7CB2 (void);
// 0x00000125 Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetQuery(UnityEngine.WWWForm)
extern void AsyncRequestString_SetQuery_m13638A1A6C284DB8416B5878E8A4577502457E19 (void);
// 0x00000126 Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString::SetCallback(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestString_SetCallback_m02C78EF786B9D7490280F736DF11E1CE9A95AA68 (void);
// 0x00000127 System.Void Facebook.Unity.AsyncRequestString::.ctor()
extern void AsyncRequestString__ctor_m08C5358236DFB5E7352A9DB508C9C466C05C62FB (void);
// 0x00000128 System.Void Facebook.Unity.AsyncRequestString_<Start>d__9::.ctor(System.Int32)
extern void U3CStartU3Ed__9__ctor_m6BB670973C33592061C21CD55A60A27A0432C79E (void);
// 0x00000129 System.Void Facebook.Unity.AsyncRequestString_<Start>d__9::System.IDisposable.Dispose()
extern void U3CStartU3Ed__9_System_IDisposable_Dispose_m42C35A13588970E8EF9DCD40BE5DA7DD9F7E1F4A (void);
// 0x0000012A System.Boolean Facebook.Unity.AsyncRequestString_<Start>d__9::MoveNext()
extern void U3CStartU3Ed__9_MoveNext_m8C9082F8D94D45B54FA8174DBF8094B2915DD9B8 (void);
// 0x0000012B System.Object Facebook.Unity.AsyncRequestString_<Start>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m234D8FEEEF6E863D283B7766E08AD29240F49248 (void);
// 0x0000012C System.Void Facebook.Unity.AsyncRequestString_<Start>d__9::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m15BB00FD306990BD4C304549DA963BCDF1135493 (void);
// 0x0000012D System.Object Facebook.Unity.AsyncRequestString_<Start>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_mF369F51D1E867C8BEDA92CE3E9DE08D5AE6EED41 (void);
// 0x0000012E System.Void Facebook.Unity.FacebookLogger::.cctor()
extern void FacebookLogger__cctor_m877F937C6B306C9595D44A0C25551BFC7AB02518 (void);
// 0x0000012F Facebook.Unity.IFacebookLogger Facebook.Unity.FacebookLogger::get_Instance()
extern void FacebookLogger_get_Instance_mD6A0D838FA1AC842407AD40C5B52E61C1E459B1A (void);
// 0x00000130 System.Void Facebook.Unity.FacebookLogger::set_Instance(Facebook.Unity.IFacebookLogger)
extern void FacebookLogger_set_Instance_m51C6929F7DA379E17A98B96B813950B906C2C5EA (void);
// 0x00000131 System.Void Facebook.Unity.FacebookLogger::Log(System.String)
extern void FacebookLogger_Log_m5719F401DCC106DD29F235E3EB4330F2182EAF95 (void);
// 0x00000132 System.Void Facebook.Unity.FacebookLogger::Info(System.String)
extern void FacebookLogger_Info_m22301791BEEA8EA1A7DFA5BB7A36644E1A95FFB2 (void);
// 0x00000133 System.Void Facebook.Unity.FacebookLogger::Warn(System.String)
extern void FacebookLogger_Warn_m7F07108ADB4A2E6352CC636033D8498EEBB13755 (void);
// 0x00000134 System.Void Facebook.Unity.FacebookLogger::Warn(System.String,System.String[])
extern void FacebookLogger_Warn_mA4121A837E17FC0EA423C0B6402784A5EC701493 (void);
// 0x00000135 System.Void Facebook.Unity.FacebookLogger_DebugLogger::.ctor()
extern void DebugLogger__ctor_m396258C2A00DC5153FA35BA4A9CC9084A3FBA0E1 (void);
// 0x00000136 System.Void Facebook.Unity.FacebookLogger_DebugLogger::Log(System.String)
extern void DebugLogger_Log_m4EA1B55897F163431606119D880CF49E3454DE6E (void);
// 0x00000137 System.Void Facebook.Unity.FacebookLogger_DebugLogger::Info(System.String)
extern void DebugLogger_Info_mD39CEAB8ED3FA26721C8E7A8D8E681BE7E346D29 (void);
// 0x00000138 System.Void Facebook.Unity.FacebookLogger_DebugLogger::Warn(System.String)
extern void DebugLogger_Warn_m901260E8BC4778245A980345DCD3E8FEF1781595 (void);
// 0x00000139 System.Void Facebook.Unity.IFacebookLogger::Log(System.String)
// 0x0000013A System.Void Facebook.Unity.IFacebookLogger::Info(System.String)
// 0x0000013B System.Void Facebook.Unity.IFacebookLogger::Warn(System.String)
// 0x0000013C System.Boolean Facebook.Unity.Utilities::TryGetValue(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,T&)
// 0x0000013D System.Int64 Facebook.Unity.Utilities::TotalSeconds(System.DateTime)
extern void Utilities_TotalSeconds_mCDD723DDC296E2969B8A3DEFEACE952C8D8F8DB9 (void);
// 0x0000013E T Facebook.Unity.Utilities::GetValueOrDefault(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.String,System.Boolean)
// 0x0000013F System.String Facebook.Unity.Utilities::ToCommaSeparateList(System.Collections.Generic.IEnumerable`1<System.String>)
extern void Utilities_ToCommaSeparateList_m1885DA634E5650319164F3847B31935E787ADE59 (void);
// 0x00000140 System.String Facebook.Unity.Utilities::AbsoluteUrlOrEmptyString(System.Uri)
extern void Utilities_AbsoluteUrlOrEmptyString_mC38E9CE9B4F420CA43E70C53ED6A7842EF0357C4 (void);
// 0x00000141 System.String Facebook.Unity.Utilities::GetUserAgent(System.String,System.String)
extern void Utilities_GetUserAgent_mEA4D736A7DB8D7E2BCC5A31094E7B340547CD90B (void);
// 0x00000142 System.String Facebook.Unity.Utilities::ToJson(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ToJson_m372CA72DC95E3AB103C3DF1CCD168BD7BA851E71 (void);
// 0x00000143 Facebook.Unity.AccessToken Facebook.Unity.Utilities::ParseAccessTokenFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParseAccessTokenFromResult_mA68036BB24FA4DDD5703167E3F0E185CC1535C77 (void);
// 0x00000144 System.String Facebook.Unity.Utilities::ToStringNullOk(System.Object)
extern void Utilities_ToStringNullOk_m9DE8F796FB5A8077E532297D100A2074A97CFCBB (void);
// 0x00000145 System.String Facebook.Unity.Utilities::FormatToString(System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern void Utilities_FormatToString_mCB8E2BBFCA716FD8E465FF909B8E7F9193DB154C (void);
// 0x00000146 System.DateTime Facebook.Unity.Utilities::ParseExpirationDateFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParseExpirationDateFromResult_m4EEC3DBBCCB46F7EC703DEB30F9A0A227205ADFE (void);
// 0x00000147 System.Nullable`1<System.DateTime> Facebook.Unity.Utilities::ParseLastRefreshFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParseLastRefreshFromResult_m2E446B8A141DB4317C36E0D8F30B3341A7961DEF (void);
// 0x00000148 System.Collections.Generic.ICollection`1<System.String> Facebook.Unity.Utilities::ParsePermissionFromResult(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void Utilities_ParsePermissionFromResult_m95293DC5F3403DCB439AEE86C06D555881061E5A (void);
// 0x00000149 System.DateTime Facebook.Unity.Utilities::FromTimestamp(System.Int32)
extern void Utilities_FromTimestamp_m80830EE87D0A4DB5E0AE3E00CC2EBE16F5F57276 (void);
// 0x0000014A System.Void Facebook.Unity.Utilities_Callback`1::.ctor(System.Object,System.IntPtr)
// 0x0000014B System.Void Facebook.Unity.Utilities_Callback`1::Invoke(T)
// 0x0000014C System.IAsyncResult Facebook.Unity.Utilities_Callback`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x0000014D System.Void Facebook.Unity.Utilities_Callback`1::EndInvoke(System.IAsyncResult)
// 0x0000014E System.Void Facebook.Unity.Utilities_<>c::.cctor()
extern void U3CU3Ec__cctor_m2BBA4F68877C79C40F70DFD41FC21C19BF9EFE82 (void);
// 0x0000014F System.Void Facebook.Unity.Utilities_<>c::.ctor()
extern void U3CU3Ec__ctor_m2A69079DA8CB9E378C1E82C96843922858057634 (void);
// 0x00000150 System.String Facebook.Unity.Utilities_<>c::<ParsePermissionFromResult>b__18_0(System.Object)
extern void U3CU3Ec_U3CParsePermissionFromResultU3Eb__18_0_mAEE86ED0231FAF6C425351B5644B8FEF4FC32C6C (void);
// 0x00000151 Facebook.Unity.IAsyncRequestStringWrapper Facebook.Unity.FBUnityUtility::get_AsyncRequestStringWrapper()
extern void FBUnityUtility_get_AsyncRequestStringWrapper_m0C1C83A5691F39AF0BB18374A1D3C8951DA36FB1 (void);
// 0x00000152 System.Void Facebook.Unity.AsyncRequestStringWrapper::Request(System.Uri,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestStringWrapper_Request_m8CBBF2379D8FE5E0125C83972F4A6D42976BBEEB (void);
// 0x00000153 System.Void Facebook.Unity.AsyncRequestStringWrapper::Request(System.Uri,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
extern void AsyncRequestStringWrapper_Request_m06F3163D8E24671D6A5DAFABFE0CD08A481B8D7A (void);
// 0x00000154 System.Void Facebook.Unity.AsyncRequestStringWrapper::.ctor()
extern void AsyncRequestStringWrapper__ctor_m91A9C46521BC925FA57D6250C0F3E72D0192B982 (void);
// 0x00000155 System.Void Facebook.Unity.IAsyncRequestStringWrapper::Request(System.Uri,Facebook.Unity.HttpMethod,UnityEngine.WWWForm,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
// 0x00000156 System.Void Facebook.Unity.IAsyncRequestStringWrapper::Request(System.Uri,Facebook.Unity.HttpMethod,System.Collections.Generic.IDictionary`2<System.String,System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>)
// 0x00000157 System.Void Facebook.Unity.FacebookScheduler::Schedule(System.Action,System.Int64)
extern void FacebookScheduler_Schedule_mBDDB19927805460E07C70C709C6AE9B626441473 (void);
// 0x00000158 System.Collections.IEnumerator Facebook.Unity.FacebookScheduler::DelayEvent(System.Action,System.Int64)
extern void FacebookScheduler_DelayEvent_m5DFC95422E9FAFA95B30A4D0CFE6AEB1699D9420 (void);
// 0x00000159 System.Void Facebook.Unity.FacebookScheduler::.ctor()
extern void FacebookScheduler__ctor_mCB064B5DAE532108037117E17E12A7D5E8B84E6B (void);
// 0x0000015A System.Void Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::.ctor(System.Int32)
extern void U3CDelayEventU3Ed__1__ctor_m3BDF150EA0C8450F46C4F9A9BE52368C0DD05275 (void);
// 0x0000015B System.Void Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::System.IDisposable.Dispose()
extern void U3CDelayEventU3Ed__1_System_IDisposable_Dispose_m4BFCD4A4E9EA045E39CC19CD61C94ED55790F429 (void);
// 0x0000015C System.Boolean Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::MoveNext()
extern void U3CDelayEventU3Ed__1_MoveNext_m0892580F8D079DD35AA81C65EF4896913C4A531B (void);
// 0x0000015D System.Object Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayEventU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m067FB91ADFF63904D451D96ECDE70F54EDCDAE8F (void);
// 0x0000015E System.Void Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::System.Collections.IEnumerator.Reset()
extern void U3CDelayEventU3Ed__1_System_Collections_IEnumerator_Reset_m39FBF76EE1E8D7DDE3431F38AF80527713764F28 (void);
// 0x0000015F System.Object Facebook.Unity.FacebookScheduler_<DelayEvent>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CDelayEventU3Ed__1_System_Collections_IEnumerator_get_Current_m9E2EF3525E3D4F10DD9B5354623113B37FFC3759 (void);
// 0x00000160 System.Void Facebook.Unity.CodelessIAPAutoLog::handlePurchaseCompleted(System.Object)
extern void CodelessIAPAutoLog_handlePurchaseCompleted_mC02DFC40960C519217BDC1464EEB25857DD9143D (void);
// 0x00000161 System.Void Facebook.Unity.CodelessIAPAutoLog::addListenerToIAPButtons(System.Object)
extern void CodelessIAPAutoLog_addListenerToIAPButtons_mCCBFD3BA9544F3F1F3C2CA42541D7E33ED63AD7C (void);
// 0x00000162 System.Void Facebook.Unity.CodelessIAPAutoLog::addListenerToGameObject(UnityEngine.Object,System.Object)
extern void CodelessIAPAutoLog_addListenerToGameObject_m4F71BEFC1B80FB8A574D964FFAF9743E1D718F0C (void);
// 0x00000163 System.Type Facebook.Unity.CodelessIAPAutoLog::FindTypeInAssemblies(System.String,System.String)
extern void CodelessIAPAutoLog_FindTypeInAssemblies_m6DA5CCA1B5FE34C2FE23AC7717D56C7144480237 (void);
// 0x00000164 UnityEngine.Object[] Facebook.Unity.CodelessIAPAutoLog::FindObjectsOfTypeByName(System.String,System.String)
extern void CodelessIAPAutoLog_FindObjectsOfTypeByName_m32A93315F67F718E14286BB8728D4386FEC65E2E (void);
// 0x00000165 System.Object Facebook.Unity.CodelessIAPAutoLog::GetField(System.Object,System.String)
extern void CodelessIAPAutoLog_GetField_mAD794FC197434958098A15886D80CAABC501BA0D (void);
// 0x00000166 System.Object Facebook.Unity.CodelessIAPAutoLog::GetProperty(System.Object,System.String)
extern void CodelessIAPAutoLog_GetProperty_m01B1790C06C37A6DDE28B701A3796A5B10487A90 (void);
// 0x00000167 System.Void Facebook.Unity.CodelessCrawler::Awake()
extern void CodelessCrawler_Awake_m122935662B32601495D279C99F66CF0F496C254B (void);
// 0x00000168 System.Void Facebook.Unity.CodelessCrawler::CaptureViewHierarchy(System.String)
extern void CodelessCrawler_CaptureViewHierarchy_m95B9785D9E65A1B5FDB75DDCBB686C0AAB608179 (void);
// 0x00000169 System.Collections.IEnumerator Facebook.Unity.CodelessCrawler::GenSnapshot()
extern void CodelessCrawler_GenSnapshot_m60CAE989176661A35F8E6CB3F01778EF29F471ED (void);
// 0x0000016A System.Void Facebook.Unity.CodelessCrawler::SendAndroid(System.String)
extern void CodelessCrawler_SendAndroid_mD53041A08AC0EDEEDE11BE528D219DFB5E0F76FD (void);
// 0x0000016B System.Void Facebook.Unity.CodelessCrawler::SendIos(System.String)
extern void CodelessCrawler_SendIos_m1C0553A15F2D9BC15B4779970B979960FF5D09EE (void);
// 0x0000016C System.String Facebook.Unity.CodelessCrawler::GenBase64Screenshot()
extern void CodelessCrawler_GenBase64Screenshot_m23C9B87522FEA309D5C249383FEE9AC76830D7BD (void);
// 0x0000016D System.String Facebook.Unity.CodelessCrawler::GenViewJson()
extern void CodelessCrawler_GenViewJson_mFC3DB02D3A304843E1CDD1BBC6B6AA52FE9D6411 (void);
// 0x0000016E System.Void Facebook.Unity.CodelessCrawler::GenChild(UnityEngine.GameObject,System.Text.StringBuilder)
extern void CodelessCrawler_GenChild_m2027B31C3EFF862D152211F4C9812BD5634E7713 (void);
// 0x0000016F System.Void Facebook.Unity.CodelessCrawler::onActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void CodelessCrawler_onActiveSceneChanged_m003D2F66E130FCCC15520965E564629350BCFD59 (void);
// 0x00000170 System.Void Facebook.Unity.CodelessCrawler::updateMainCamera()
extern void CodelessCrawler_updateMainCamera_mC012344A45CCAF6C31D704EB660788E9C09689A6 (void);
// 0x00000171 UnityEngine.Vector2 Facebook.Unity.CodelessCrawler::getScreenCoordinate(UnityEngine.Vector3,UnityEngine.RenderMode)
extern void CodelessCrawler_getScreenCoordinate_mA06133DEF7068578B226D0101A5F822BBC5CF52A (void);
// 0x00000172 System.String Facebook.Unity.CodelessCrawler::getClasstypeBitmaskButton()
extern void CodelessCrawler_getClasstypeBitmaskButton_m5B63C0EE98A0DE603B05DD093C10C50A427589D7 (void);
// 0x00000173 System.String Facebook.Unity.CodelessCrawler::getVisibility(UnityEngine.GameObject)
extern void CodelessCrawler_getVisibility_m4609F2D89D1C82F2FF63F80263B0440B45D3BDC9 (void);
// 0x00000174 System.Void Facebook.Unity.CodelessCrawler::.ctor()
extern void CodelessCrawler__ctor_m48EC1A12B70ACD64C4DA1D63FE06EF83A566ECF6 (void);
// 0x00000175 System.Void Facebook.Unity.CodelessCrawler::.cctor()
extern void CodelessCrawler__cctor_mC144265EDABE72A6FEBF9307EE48742C541DCA80 (void);
// 0x00000176 System.Void Facebook.Unity.CodelessCrawler_<GenSnapshot>d__4::.ctor(System.Int32)
extern void U3CGenSnapshotU3Ed__4__ctor_m2CC855A63A9F4C19FB0E4B1DF371AD29038E9537 (void);
// 0x00000177 System.Void Facebook.Unity.CodelessCrawler_<GenSnapshot>d__4::System.IDisposable.Dispose()
extern void U3CGenSnapshotU3Ed__4_System_IDisposable_Dispose_m7B48F1C45F5D63CE54DBDAA0A88B3B23BFF27D7A (void);
// 0x00000178 System.Boolean Facebook.Unity.CodelessCrawler_<GenSnapshot>d__4::MoveNext()
extern void U3CGenSnapshotU3Ed__4_MoveNext_mC8BA872F2A8A0DE01D4F1F089395ECABF732975A (void);
// 0x00000179 System.Object Facebook.Unity.CodelessCrawler_<GenSnapshot>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenSnapshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m95B393232911560E3725C741205D4A7C3EDAB827 (void);
// 0x0000017A System.Void Facebook.Unity.CodelessCrawler_<GenSnapshot>d__4::System.Collections.IEnumerator.Reset()
extern void U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_Reset_mFE9720AF54CA96F0E9A2D7F576C2AA77ED137F42 (void);
// 0x0000017B System.Object Facebook.Unity.CodelessCrawler_<GenSnapshot>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_get_Current_m24AD9DEDCE5DE8396C27B872338086FF0238F983 (void);
// 0x0000017C Facebook.Unity.FBSDKEventBindingManager Facebook.Unity.CodelessUIInteractEvent::get_eventBindingManager()
extern void CodelessUIInteractEvent_get_eventBindingManager_m381B353313ABBC497D95C4B1763058C9BF37122D (void);
// 0x0000017D System.Void Facebook.Unity.CodelessUIInteractEvent::set_eventBindingManager(Facebook.Unity.FBSDKEventBindingManager)
extern void CodelessUIInteractEvent_set_eventBindingManager_m0B0076AF2135B0CF8F358F706A032DBC5CBF2590 (void);
// 0x0000017E System.Void Facebook.Unity.CodelessUIInteractEvent::Awake()
extern void CodelessUIInteractEvent_Awake_m9FDD437EB4C25C5469E53E58888ACC34526EE44A (void);
// 0x0000017F System.Void Facebook.Unity.CodelessUIInteractEvent::SetLoggerInitAndroid()
extern void CodelessUIInteractEvent_SetLoggerInitAndroid_mAD0ED4ED266F04CBFC94403902B558041A535ADB (void);
// 0x00000180 System.Void Facebook.Unity.CodelessUIInteractEvent::SetLoggerInitIos()
extern void CodelessUIInteractEvent_SetLoggerInitIos_m7CC5CFCF4CE8C7FA0BA2112A46F10F3CB833A94D (void);
// 0x00000181 System.Void Facebook.Unity.CodelessUIInteractEvent::Update()
extern void CodelessUIInteractEvent_Update_m10E5AFCF66162E40B4879E70FB0C75F7832E1A56 (void);
// 0x00000182 System.Void Facebook.Unity.CodelessUIInteractEvent::OnReceiveMapping(System.String)
extern void CodelessUIInteractEvent_OnReceiveMapping_m769B55CFC5E7BEE4A6FAA8892A245B944AD3A61E (void);
// 0x00000183 System.Void Facebook.Unity.CodelessUIInteractEvent::.ctor()
extern void CodelessUIInteractEvent__ctor_mC9C6BE903A313BAFB69E72D2A7E2DCCA6966B5D5 (void);
// 0x00000184 System.Boolean Facebook.Unity.FBSDKViewHiearchy::CheckGameObjectMatchPath(UnityEngine.GameObject,System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent>)
extern void FBSDKViewHiearchy_CheckGameObjectMatchPath_mE6757F0129E99E26E25809240D2FD0C807194584 (void);
// 0x00000185 System.Boolean Facebook.Unity.FBSDKViewHiearchy::CheckPathMatchPath(System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent>,System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent>)
extern void FBSDKViewHiearchy_CheckPathMatchPath_mBEB1F5E129FC8A1C08EDF4E51FDEFDC59B31835E (void);
// 0x00000186 System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent> Facebook.Unity.FBSDKViewHiearchy::GetPath(UnityEngine.GameObject)
extern void FBSDKViewHiearchy_GetPath_m3E31C3FFA973A5F3C5FC5CCD1DC5D88D6CC29A11 (void);
// 0x00000187 System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent> Facebook.Unity.FBSDKViewHiearchy::GetPath(UnityEngine.GameObject,System.Int32)
extern void FBSDKViewHiearchy_GetPath_m6A6D2C49106C366F2CAE90880AE69462DBEB4E63 (void);
// 0x00000188 UnityEngine.GameObject Facebook.Unity.FBSDKViewHiearchy::GetParent(UnityEngine.GameObject)
extern void FBSDKViewHiearchy_GetParent_m5AFE735223FCC31E191BD5B3559AA02F3AE538D3 (void);
// 0x00000189 System.Collections.Generic.Dictionary`2<System.String,System.Object> Facebook.Unity.FBSDKViewHiearchy::GetAttribute(UnityEngine.GameObject,UnityEngine.GameObject)
extern void FBSDKViewHiearchy_GetAttribute_m74488C8C0E105C5DB2AC7B92FA30FB492A6C8064 (void);
// 0x0000018A System.Void Facebook.Unity.FBSDKCodelessPathComponent::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FBSDKCodelessPathComponent__ctor_m6DFE3D5A9C27B449F739C6E2BFEEC243EA39ECDA (void);
// 0x0000018B System.String Facebook.Unity.FBSDKCodelessPathComponent::get_className()
extern void FBSDKCodelessPathComponent_get_className_m09BAED691F4C67F2252A48C83ED46590574514B9 (void);
// 0x0000018C System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_className(System.String)
extern void FBSDKCodelessPathComponent_set_className_m7ED0A9732DD6B7CD58B5E5D531B11D2B146932FE (void);
// 0x0000018D System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_text(System.String)
extern void FBSDKCodelessPathComponent_set_text_m05688A2469F92FD15712EC554CDC72DF9B1CF116 (void);
// 0x0000018E System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_hint(System.String)
extern void FBSDKCodelessPathComponent_set_hint_m0F2A558021A0D66CE7481E1D60F5FC11766DD129 (void);
// 0x0000018F System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_desc(System.String)
extern void FBSDKCodelessPathComponent_set_desc_m41E454C69A5C31745AA2F9A475706C3F061FAC03 (void);
// 0x00000190 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_tag(System.String)
extern void FBSDKCodelessPathComponent_set_tag_m931F672CD6D7F0AB7ECBA05CC4B9BB9A38E4214D (void);
// 0x00000191 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_index(System.Int64)
extern void FBSDKCodelessPathComponent_set_index_mB5360131D0A771A3E6C4AAD2B12BB92E120C1E3A (void);
// 0x00000192 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_section(System.Int64)
extern void FBSDKCodelessPathComponent_set_section_m527F5057AF490050A4FCD54CE716F443740F1B5E (void);
// 0x00000193 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_row(System.Int64)
extern void FBSDKCodelessPathComponent_set_row_mE4A25B8D2C34160991BBA80C77275D5C6E39187B (void);
// 0x00000194 System.Void Facebook.Unity.FBSDKCodelessPathComponent::set_matchBitmask(System.Int64)
extern void FBSDKCodelessPathComponent_set_matchBitmask_m9BF6CE6F617EE14E445E61267FC157436B52471B (void);
// 0x00000195 System.Void Facebook.Unity.FBSDKEventBinding::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void FBSDKEventBinding__ctor_mC34E7CFB9B20C143E707FE05A80094E6172E315B (void);
// 0x00000196 System.String Facebook.Unity.FBSDKEventBinding::get_eventName()
extern void FBSDKEventBinding_get_eventName_mE83C43E0925079076AF0E20C32DEB2F61581DA7E (void);
// 0x00000197 System.Void Facebook.Unity.FBSDKEventBinding::set_eventName(System.String)
extern void FBSDKEventBinding_set_eventName_mFCCEAAAF52E285636D5F13E63DF2FC117338BA34 (void);
// 0x00000198 System.String Facebook.Unity.FBSDKEventBinding::get_eventType()
extern void FBSDKEventBinding_get_eventType_m6264DB8ADA66A6B03A1E1CF4CE8A1BF53663A31E (void);
// 0x00000199 System.Void Facebook.Unity.FBSDKEventBinding::set_eventType(System.String)
extern void FBSDKEventBinding_set_eventType_m31934EC0D2116F9B3AB926052A15AB410161A4DC (void);
// 0x0000019A System.String Facebook.Unity.FBSDKEventBinding::get_appVersion()
extern void FBSDKEventBinding_get_appVersion_m8A11D02FE83546BF3B3FD10E03FFDA2E55027FFD (void);
// 0x0000019B System.Void Facebook.Unity.FBSDKEventBinding::set_appVersion(System.String)
extern void FBSDKEventBinding_set_appVersion_m08B508F110FD8B82342A4B3463BFF1B59BF04FB7 (void);
// 0x0000019C System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent> Facebook.Unity.FBSDKEventBinding::get_path()
extern void FBSDKEventBinding_get_path_m1DE2BA2A5F20CC87F0030AD112C7713E0E8CC56C (void);
// 0x0000019D System.Void Facebook.Unity.FBSDKEventBinding::set_path(System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent>)
extern void FBSDKEventBinding_set_path_m31B6964D84810597EF0086F6DFFDC25AF4AD1256 (void);
// 0x0000019E System.Collections.Generic.List`1<Facebook.Unity.FBSDKEventBinding> Facebook.Unity.FBSDKEventBindingManager::get_eventBindings()
extern void FBSDKEventBindingManager_get_eventBindings_m31BB6F5AC87C718BB94C45E287DF441349E4777C (void);
// 0x0000019F System.Void Facebook.Unity.FBSDKEventBindingManager::set_eventBindings(System.Collections.Generic.List`1<Facebook.Unity.FBSDKEventBinding>)
extern void FBSDKEventBindingManager_set_eventBindings_m40CFAC43E709F14BBB98F32D71B6C766D919ECF2 (void);
// 0x000001A0 System.Void Facebook.Unity.FBSDKEventBindingManager::.ctor(System.Collections.Generic.List`1<System.Object>)
extern void FBSDKEventBindingManager__ctor_m28E10077C286A5C45198A9DA47F0E62F1BE9A475 (void);
// 0x000001A1 System.Void Facebook.Unity.Gameroom.GameroomFacebook::.ctor()
extern void GameroomFacebook__ctor_mFC65BE8624D6B0750CAA3F5E2DC427AC812382B6 (void);
// 0x000001A2 System.Void Facebook.Unity.Gameroom.GameroomFacebook::.ctor(Facebook.Unity.Gameroom.IGameroomWrapper,Facebook.Unity.CallbackManager)
extern void GameroomFacebook__ctor_mEE40C6C90C3EB3BD02904633DA6178C9971E14E1 (void);
// 0x000001A3 System.Boolean Facebook.Unity.Gameroom.GameroomFacebook::get_LimitEventUsage()
extern void GameroomFacebook_get_LimitEventUsage_mDAAD73F475D00D918E228AAE134F8C62E265D924 (void);
// 0x000001A4 System.Void Facebook.Unity.Gameroom.GameroomFacebook::set_LimitEventUsage(System.Boolean)
extern void GameroomFacebook_set_LimitEventUsage_m88E828FFC76A481276FF5FAA59E0F9FDF1789C6B (void);
// 0x000001A5 System.String Facebook.Unity.Gameroom.GameroomFacebook::get_SDKName()
extern void GameroomFacebook_get_SDKName_m2000B1EB1CB0025DCA5ACD189784C487CDBD8036 (void);
// 0x000001A6 System.String Facebook.Unity.Gameroom.GameroomFacebook::get_SDKVersion()
extern void GameroomFacebook_get_SDKVersion_m01EF49380A314A877C3A8A4FC184C7516EC0284C (void);
// 0x000001A7 System.Void Facebook.Unity.Gameroom.GameroomFacebook::Init(System.String,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void GameroomFacebook_Init_m40FBB6E36B6BBA0680C00B52D451D366391F2910 (void);
// 0x000001A8 System.Void Facebook.Unity.Gameroom.GameroomFacebook::ActivateApp(System.String)
extern void GameroomFacebook_ActivateApp_m6125853EC5963BA1E10702414084ACBBE42824EC (void);
// 0x000001A9 System.Void Facebook.Unity.Gameroom.GameroomFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void GameroomFacebook_AppEventsLogEvent_m5D15BD0E27FBFD9F1F8D5B8A6827B6855E5AF508 (void);
// 0x000001AA System.Void Facebook.Unity.Gameroom.GameroomFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void GameroomFacebook_AppEventsLogPurchase_mDACD10E24CFA39A4C8680C290C70B5A54439F803 (void);
// 0x000001AB System.Void Facebook.Unity.Gameroom.GameroomFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void GameroomFacebook_AppRequest_mBBFAC58D7B58DA3B408B609D392D646916AAD0F4 (void);
// 0x000001AC System.Void Facebook.Unity.Gameroom.GameroomFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void GameroomFacebook_FeedShare_mF458A342C6108A52614BF074E01487227E967A86 (void);
// 0x000001AD System.Void Facebook.Unity.Gameroom.GameroomFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void GameroomFacebook_ShareLink_m7D5AD82190FB28411A073C701B864CB67B38B123 (void);
// 0x000001AE System.Void Facebook.Unity.Gameroom.GameroomFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void GameroomFacebook_GetAppLink_m65224B9E284B56569AB4535EB8EB6A428EE015A0 (void);
// 0x000001AF System.Void Facebook.Unity.Gameroom.GameroomFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void GameroomFacebook_LogInWithPublishPermissions_mFF6B98820EA1FA0FE3696F2030FC94B033657057 (void);
// 0x000001B0 System.Void Facebook.Unity.Gameroom.GameroomFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void GameroomFacebook_LogInWithReadPermissions_mF9A0B6ED9BFD69485A8393148E1EB8006DEE9A3D (void);
// 0x000001B1 System.Void Facebook.Unity.Gameroom.GameroomFacebook::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
extern void GameroomFacebook_OnAppRequestsComplete_mBAB89FBC32D5D7CBAF25077EA067BF9751E3654C (void);
// 0x000001B2 System.Void Facebook.Unity.Gameroom.GameroomFacebook::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
extern void GameroomFacebook_OnGetAppLinkComplete_m977F242EE6115AB93E49499BEEF52A7011C41E39 (void);
// 0x000001B3 System.Void Facebook.Unity.Gameroom.GameroomFacebook::OnLoginComplete(Facebook.Unity.ResultContainer)
extern void GameroomFacebook_OnLoginComplete_m20D6B69BE5A51C84C5DD4888A2F194028D40FDD8 (void);
// 0x000001B4 System.Void Facebook.Unity.Gameroom.GameroomFacebook::OnShareLinkComplete(Facebook.Unity.ResultContainer)
extern void GameroomFacebook_OnShareLinkComplete_mFAB4F4323BBAF7AF7764DDEB48ED5C28AE10F750 (void);
// 0x000001B5 System.Boolean Facebook.Unity.Gameroom.GameroomFacebook::HaveReceivedPipeResponse()
extern void GameroomFacebook_HaveReceivedPipeResponse_m6272B63D0E24C4C857056290B7DC8A0CAE225A86 (void);
// 0x000001B6 System.String Facebook.Unity.Gameroom.GameroomFacebook::GetPipeResponse(System.String)
extern void GameroomFacebook_GetPipeResponse_mADC4101C9B0263C5D55FF2DCD12CE41FE99957F6 (void);
// 0x000001B7 Facebook.Unity.Gameroom.IGameroomWrapper Facebook.Unity.Gameroom.GameroomFacebook::GetGameroomWrapper()
extern void GameroomFacebook_GetGameroomWrapper_m5C91145F425EE1CCC06AC242C558B482ADC38C3D (void);
// 0x000001B8 System.Void Facebook.Unity.Gameroom.GameroomFacebook::LoginWithPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void GameroomFacebook_LoginWithPermissions_mDADBE008020C0518FEFA172307E7B0D7D9F8868D (void);
// 0x000001B9 System.Void Facebook.Unity.Gameroom.GameroomFacebook_OnComplete::.ctor(System.Object,System.IntPtr)
extern void OnComplete__ctor_m344BDA66D339789102DA523A32B2F28FAB5F3C87 (void);
// 0x000001BA System.Void Facebook.Unity.Gameroom.GameroomFacebook_OnComplete::Invoke(Facebook.Unity.ResultContainer)
extern void OnComplete_Invoke_mE62C5A8F4A615C47A92906C689DD40E7D750D132 (void);
// 0x000001BB System.IAsyncResult Facebook.Unity.Gameroom.GameroomFacebook_OnComplete::BeginInvoke(Facebook.Unity.ResultContainer,System.AsyncCallback,System.Object)
extern void OnComplete_BeginInvoke_m97D6900EB0000E7714DA3899DD01EE3F2C890254 (void);
// 0x000001BC System.Void Facebook.Unity.Gameroom.GameroomFacebook_OnComplete::EndInvoke(System.IAsyncResult)
extern void OnComplete_EndInvoke_mCE9D3218C50E35392510668032BE8A2661A9ABAF (void);
// 0x000001BD Facebook.Unity.Gameroom.IGameroomFacebookImplementation Facebook.Unity.Gameroom.GameroomFacebookGameObject::get_GameroomFacebookImpl()
extern void GameroomFacebookGameObject_get_GameroomFacebookImpl_mC09192402584AD9D54A7B98F78B5BD0E7B45AFDA (void);
// 0x000001BE System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject::WaitForResponse(Facebook.Unity.Gameroom.GameroomFacebook_OnComplete,System.String)
extern void GameroomFacebookGameObject_WaitForResponse_m96714BF1B7DC9A1B710C34D16518251869AD8698 (void);
// 0x000001BF System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject::OnAwake()
extern void GameroomFacebookGameObject_OnAwake_m93AC653304B54D591E4D7790159DE746790B102E (void);
// 0x000001C0 System.Collections.IEnumerator Facebook.Unity.Gameroom.GameroomFacebookGameObject::WaitForPipeResponse(Facebook.Unity.Gameroom.GameroomFacebook_OnComplete,System.String)
extern void GameroomFacebookGameObject_WaitForPipeResponse_m48FAB97EFF06775119721589B3E7C6C029110C78 (void);
// 0x000001C1 System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject::.ctor()
extern void GameroomFacebookGameObject__ctor_m6041717A48BB70B7CC05488AC390DA46D0A457D1 (void);
// 0x000001C2 System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::.ctor(System.Int32)
extern void U3CWaitForPipeResponseU3Ed__4__ctor_m1605B666386B4CBC70B5E982D78E4F1D741F4684 (void);
// 0x000001C3 System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::System.IDisposable.Dispose()
extern void U3CWaitForPipeResponseU3Ed__4_System_IDisposable_Dispose_mD084FB4715BC24D2FFC3E2B6422779DB8EAE6539 (void);
// 0x000001C4 System.Boolean Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::MoveNext()
extern void U3CWaitForPipeResponseU3Ed__4_MoveNext_m174EB7DF36A038C4B31468EBA4CF6747A95737CD (void);
// 0x000001C5 System.Object Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForPipeResponseU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m84E38B840147F3A5917B180C6D749019688E40C5 (void);
// 0x000001C6 System.Void Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::System.Collections.IEnumerator.Reset()
extern void U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_Reset_mD3093021205012F8C2816C904A6A683428C4678A (void);
// 0x000001C7 System.Object Facebook.Unity.Gameroom.GameroomFacebookGameObject_<WaitForPipeResponse>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_get_Current_m6D39801691E6DF32B424F34BCE020EDFE70A4FBE (void);
// 0x000001C8 Facebook.Unity.FacebookGameObject Facebook.Unity.Gameroom.GameroomFacebookLoader::get_FBGameObject()
extern void GameroomFacebookLoader_get_FBGameObject_m0A331676A70B9C35D4DC38BA784F2D52189604C6 (void);
// 0x000001C9 System.Void Facebook.Unity.Gameroom.GameroomFacebookLoader::.ctor()
extern void GameroomFacebookLoader__ctor_mE1885E6A2C11E0DD420314A54E85AFA39DF16892 (void);
// 0x000001CA System.Boolean Facebook.Unity.Gameroom.IGameroomFacebookImplementation::HaveReceivedPipeResponse()
// 0x000001CB System.String Facebook.Unity.Gameroom.IGameroomFacebookImplementation::GetPipeResponse(System.String)
// 0x000001CC System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.Gameroom.IGameroomWrapper::get_PipeResponse()
// 0x000001CD System.Void Facebook.Unity.Gameroom.IGameroomWrapper::set_PipeResponse(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
// 0x000001CE System.Void Facebook.Unity.Gameroom.IGameroomWrapper::Init(Facebook.Unity.Gameroom.GameroomFacebook_OnComplete)
// 0x000001CF System.Void Facebook.Unity.Gameroom.IGameroomWrapper::DoLoginRequest(System.String,System.String,System.String,Facebook.Unity.Gameroom.GameroomFacebook_OnComplete)
// 0x000001D0 System.Void Facebook.Unity.Gameroom.IGameroomWrapper::DoFeedShareRequest(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,Facebook.Unity.Gameroom.GameroomFacebook_OnComplete)
// 0x000001D1 System.Void Facebook.Unity.Gameroom.IGameroomWrapper::DoAppRequestRequest(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,Facebook.Unity.Gameroom.GameroomFacebook_OnComplete)
// 0x000001D2 System.Void Facebook.Unity.Editor.EditorFacebook::.ctor(Facebook.Unity.Editor.IEditorWrapper,Facebook.Unity.CallbackManager)
extern void EditorFacebook__ctor_mAE29F2FC6C569787A51DC7A1A24301E48B4CA11D (void);
// 0x000001D3 System.Void Facebook.Unity.Editor.EditorFacebook::.ctor()
extern void EditorFacebook__ctor_m34DA93E753FE13830FF1BF8A3864DA2BC3FF14E5 (void);
// 0x000001D4 System.Boolean Facebook.Unity.Editor.EditorFacebook::get_LimitEventUsage()
extern void EditorFacebook_get_LimitEventUsage_m7C137601DBCF3B1FD94D45F2B9AF08A6E0727D8E (void);
// 0x000001D5 System.Void Facebook.Unity.Editor.EditorFacebook::set_LimitEventUsage(System.Boolean)
extern void EditorFacebook_set_LimitEventUsage_mC0B538ED6A81A566361C58188834271F641F12CF (void);
// 0x000001D6 System.String Facebook.Unity.Editor.EditorFacebook::get_SDKName()
extern void EditorFacebook_get_SDKName_mC5FCF0CF29A69878A349C2645646316F52E1F687 (void);
// 0x000001D7 System.String Facebook.Unity.Editor.EditorFacebook::get_SDKVersion()
extern void EditorFacebook_get_SDKVersion_mEC6160E21094F1FFA07901E3EA929D13187643CA (void);
// 0x000001D8 Facebook.Unity.IFacebookCallbackHandler Facebook.Unity.Editor.EditorFacebook::get_EditorGameObject()
extern void EditorFacebook_get_EditorGameObject_m547C05A8380AB2DC1FFF45BB9DB8C8ABB19C533F (void);
// 0x000001D9 System.Void Facebook.Unity.Editor.EditorFacebook::Init(Facebook.Unity.InitDelegate)
extern void EditorFacebook_Init_mB3B16ADAA349C3EB97AA931D772F615EBECD8E9B (void);
// 0x000001DA System.Void Facebook.Unity.Editor.EditorFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void EditorFacebook_LogInWithReadPermissions_m02BFB49D953CCC7731CF381D264466FFA9A7475F (void);
// 0x000001DB System.Void Facebook.Unity.Editor.EditorFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void EditorFacebook_LogInWithPublishPermissions_m1BD9E2D308E461F04D0D4B4EBD80CA2913D06156 (void);
// 0x000001DC System.Void Facebook.Unity.Editor.EditorFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void EditorFacebook_AppRequest_m3B7A7EB6E4DD6BFAE79EC07C549B6AB6B9DFF797 (void);
// 0x000001DD System.Void Facebook.Unity.Editor.EditorFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void EditorFacebook_ShareLink_m6346DBA3291547E2F4AA576948AB95B6D8DC9527 (void);
// 0x000001DE System.Void Facebook.Unity.Editor.EditorFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void EditorFacebook_FeedShare_mE3E722E30DF4D47C356277E3D53D5BFE03732937 (void);
// 0x000001DF System.Void Facebook.Unity.Editor.EditorFacebook::ActivateApp(System.String)
extern void EditorFacebook_ActivateApp_m19AA9F4F23BDF8AC9504DD542696D6CA4FF9B17E (void);
// 0x000001E0 System.Void Facebook.Unity.Editor.EditorFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void EditorFacebook_GetAppLink_m377FCED9D8563E9479564BB8841DDFD053813975 (void);
// 0x000001E1 System.Void Facebook.Unity.Editor.EditorFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void EditorFacebook_AppEventsLogEvent_m9F6D148CD8A8D9ABB9257E8D3A978C060D41311A (void);
// 0x000001E2 System.Void Facebook.Unity.Editor.EditorFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void EditorFacebook_AppEventsLogPurchase_mCD93D37B1927FE7837CA7DF5A3B6ECA5DD6430D0 (void);
// 0x000001E3 System.Boolean Facebook.Unity.Editor.EditorFacebook::IsImplicitPurchaseLoggingEnabled()
extern void EditorFacebook_IsImplicitPurchaseLoggingEnabled_m8FC6C27E0E097C8D4D72F9DDA1D89B46E2F5FF28 (void);
// 0x000001E4 System.Void Facebook.Unity.Editor.EditorFacebook::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnAppRequestsComplete_mA9D6B389B5DDC8D3EFD19AF1F6B17173439AFFDD (void);
// 0x000001E5 System.Void Facebook.Unity.Editor.EditorFacebook::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnGetAppLinkComplete_m31BD0C06EACDB6F1FA81842D9333F946A7268F38 (void);
// 0x000001E6 System.Void Facebook.Unity.Editor.EditorFacebook::OnLoginComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnLoginComplete_mF081184A345ADA1C23B758B6F079094F39D81A07 (void);
// 0x000001E7 System.Void Facebook.Unity.Editor.EditorFacebook::OnShareLinkComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnShareLinkComplete_mBFB7CD8CD1A2DCE5845889DE5A53A47AC7E2BFC6 (void);
// 0x000001E8 System.Void Facebook.Unity.Editor.EditorFacebook::OnFetchDeferredAppLinkComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnFetchDeferredAppLinkComplete_mDFF5249FE04A39E68BF561AA47CD39AD42981B45 (void);
// 0x000001E9 System.Void Facebook.Unity.Editor.EditorFacebook::OnPayComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnPayComplete_m8EE654252C30BC419540468CECAB5A1B9E569047 (void);
// 0x000001EA System.Void Facebook.Unity.Editor.EditorFacebook::OnRefreshCurrentAccessTokenComplete(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnRefreshCurrentAccessTokenComplete_m0AFEB6BD6FAF69712259C1BF9D340A4DC5AA8577 (void);
// 0x000001EB System.Void Facebook.Unity.Editor.EditorFacebook::OnFacebookAuthResponseChange(Facebook.Unity.ResultContainer)
extern void EditorFacebook_OnFacebookAuthResponseChange_m19BE0072E690BD94F8A407A51223C81B4F5C6F9D (void);
// 0x000001EC System.Void Facebook.Unity.Editor.EditorFacebook::OnUrlResponse(System.String)
extern void EditorFacebook_OnUrlResponse_m28080FB6889839BA52986A586EB2B19D00CF1DD5 (void);
// 0x000001ED System.Void Facebook.Unity.Editor.EditorFacebook::OnHideUnity(System.Boolean)
extern void EditorFacebook_OnHideUnity_mAB68B5D8AF184A66BC97B7E6A6986123BC316310 (void);
// 0x000001EE System.Void Facebook.Unity.Editor.EditorFacebookGameObject::OnAwake()
extern void EditorFacebookGameObject_OnAwake_m955DA845F99EC24FC27FA5ABD61C61D48FDFB1E7 (void);
// 0x000001EF System.Void Facebook.Unity.Editor.EditorFacebookGameObject::OnEnable()
extern void EditorFacebookGameObject_OnEnable_m48977F4BE04A6DF01BC0182BD7664864CA9BF803 (void);
// 0x000001F0 System.Void Facebook.Unity.Editor.EditorFacebookGameObject::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void EditorFacebookGameObject_OnSceneLoaded_m0564228080BD19F1595F66D01A5E631A3E968111 (void);
// 0x000001F1 System.Void Facebook.Unity.Editor.EditorFacebookGameObject::OnDisable()
extern void EditorFacebookGameObject_OnDisable_m2DB7257DEB4CDDA5B4B7C070D66F878478224CF7 (void);
// 0x000001F2 System.Void Facebook.Unity.Editor.EditorFacebookGameObject::onPurchaseCompleteHandler(System.Object)
extern void EditorFacebookGameObject_onPurchaseCompleteHandler_m4CA8DECE7050444E8F5B67E0A4255FF9EAB37C00 (void);
// 0x000001F3 System.Void Facebook.Unity.Editor.EditorFacebookGameObject::.ctor()
extern void EditorFacebookGameObject__ctor_mD14AF9C7AA2CF9AD8B308C30C3BEC6746024CA1D (void);
// 0x000001F4 Facebook.Unity.FacebookGameObject Facebook.Unity.Editor.EditorFacebookLoader::get_FBGameObject()
extern void EditorFacebookLoader_get_FBGameObject_m11D5C150A4B054CE1BF7FBEB39BD0E6E663F1D8E (void);
// 0x000001F5 System.Void Facebook.Unity.Editor.EditorFacebookLoader::.ctor()
extern void EditorFacebookLoader__ctor_mE7BE35D26E2C2D1367758251BC07A6A0103606B7 (void);
// 0x000001F6 Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer> Facebook.Unity.Editor.EditorFacebookMockDialog::get_Callback()
extern void EditorFacebookMockDialog_get_Callback_m5F4B98ED90D34716B22536E745E9340469F82600 (void);
// 0x000001F7 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::set_Callback(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>)
extern void EditorFacebookMockDialog_set_Callback_m24C61C31263945D8A3DD487438994847CCFBF434 (void);
// 0x000001F8 System.String Facebook.Unity.Editor.EditorFacebookMockDialog::get_CallbackID()
extern void EditorFacebookMockDialog_get_CallbackID_mE9740A50CEF913E55EC2EF3F939AF1EB4E9EB0AF (void);
// 0x000001F9 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::set_CallbackID(System.String)
extern void EditorFacebookMockDialog_set_CallbackID_mEFFFD7E9D658A2B588124D5A5A6EAAD27797F273 (void);
// 0x000001FA System.String Facebook.Unity.Editor.EditorFacebookMockDialog::get_DialogTitle()
// 0x000001FB System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::Start()
extern void EditorFacebookMockDialog_Start_mA62EFE923AB60A964CC23C95C5E370CCB4BC5F71 (void);
// 0x000001FC System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::OnGUI()
extern void EditorFacebookMockDialog_OnGUI_m27C4036EACBBDFCB8BCE8EBE09D69CD994436DED (void);
// 0x000001FD System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::DoGui()
// 0x000001FE System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::SendSuccessResult()
// 0x000001FF System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::SendCancelResult()
extern void EditorFacebookMockDialog_SendCancelResult_m0603629A2E8E4E9EB6501E4C8EF691466F659101 (void);
// 0x00000200 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::SendErrorResult(System.String)
extern void EditorFacebookMockDialog_SendErrorResult_m2C6EEBBB935316BC474361C85903BD993910ADF0 (void);
// 0x00000201 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::OnGUIDialog(System.Int32)
extern void EditorFacebookMockDialog_OnGUIDialog_mB24DF172D4DC26E9285FF4BDD51F9374C4CCCE0A (void);
// 0x00000202 System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::.ctor()
extern void EditorFacebookMockDialog__ctor_m5C3DB7B6A022EDE2D5001595E3CACE5AF0924483 (void);
// 0x00000203 System.Void Facebook.Unity.Editor.EditorWrapper::.ctor(Facebook.Unity.IFacebookCallbackHandler)
extern void EditorWrapper__ctor_m8409ED19BAEEC02D62A096B746D1525FD332E64D (void);
// 0x00000204 System.Void Facebook.Unity.Editor.EditorWrapper::Init()
extern void EditorWrapper_Init_mE8DE143565B89BCB21364508F35D2FAE0C9C02A0 (void);
// 0x00000205 System.Void Facebook.Unity.Editor.EditorWrapper::ShowLoginMockDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
extern void EditorWrapper_ShowLoginMockDialog_m32480619D2B0837381391938C48B424693EB6517 (void);
// 0x00000206 System.Void Facebook.Unity.Editor.EditorWrapper::ShowAppRequestMockDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String)
extern void EditorWrapper_ShowAppRequestMockDialog_m4BEE47AFB38D0A882B6EC6171A1A66CA66ED146F (void);
// 0x00000207 System.Void Facebook.Unity.Editor.EditorWrapper::ShowMockShareDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
extern void EditorWrapper_ShowMockShareDialog_m8448595A36B5BBA63C4ADBB63626FAF20FE5A039 (void);
// 0x00000208 System.Void Facebook.Unity.Editor.EditorWrapper::ShowEmptyMockDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
extern void EditorWrapper_ShowEmptyMockDialog_m210AFDEB0405C184AE554D94160D898371063FF6 (void);
// 0x00000209 System.Void Facebook.Unity.Editor.IEditorWrapper::Init()
// 0x0000020A System.Void Facebook.Unity.Editor.IEditorWrapper::ShowLoginMockDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
// 0x0000020B System.Void Facebook.Unity.Editor.IEditorWrapper::ShowAppRequestMockDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String)
// 0x0000020C System.Void Facebook.Unity.Editor.IEditorWrapper::ShowMockShareDialog(Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>,System.String,System.String)
// 0x0000020D System.String Facebook.Unity.Editor.Dialogs.EmptyMockDialog::get_EmptyDialogTitle()
extern void EmptyMockDialog_get_EmptyDialogTitle_mEF83B654A4D364EC3F6BA150FF4AE4396BBFB01A (void);
// 0x0000020E System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::set_EmptyDialogTitle(System.String)
extern void EmptyMockDialog_set_EmptyDialogTitle_m0308542A4C36DB94E9313EAEDEADA666D075E2CD (void);
// 0x0000020F System.String Facebook.Unity.Editor.Dialogs.EmptyMockDialog::get_DialogTitle()
extern void EmptyMockDialog_get_DialogTitle_m1D071DB8838AA2CEE9158265D816BF9E399B783F (void);
// 0x00000210 System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::DoGui()
extern void EmptyMockDialog_DoGui_m0CE7A00EDF88A4E7D63DA324DBA250393059CC59 (void);
// 0x00000211 System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::SendSuccessResult()
extern void EmptyMockDialog_SendSuccessResult_m06E66BE4777F7C9F90A5AE18C32E17D7715A55C4 (void);
// 0x00000212 System.Void Facebook.Unity.Editor.Dialogs.EmptyMockDialog::.ctor()
extern void EmptyMockDialog__ctor_m7EC201CB86C63676E454A21034FF90174FE6BDCD (void);
// 0x00000213 System.String Facebook.Unity.Editor.Dialogs.MockLoginDialog::get_DialogTitle()
extern void MockLoginDialog_get_DialogTitle_mB30DD0A9469E423A36F5394A90D05E7DD95AB767 (void);
// 0x00000214 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::DoGui()
extern void MockLoginDialog_DoGui_m4E9E6113B794839E40B5059DED1A893419584B6F (void);
// 0x00000215 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::SendSuccessResult()
extern void MockLoginDialog_SendSuccessResult_m31A257F367DAD01B1D07C859BAD4E0DA88E2B456 (void);
// 0x00000216 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::.ctor()
extern void MockLoginDialog__ctor_m499168C6D3E1398D1710712C9DCE845D20BB707E (void);
// 0x00000217 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog::<SendSuccessResult>b__4_0(Facebook.Unity.IGraphResult)
extern void MockLoginDialog_U3CSendSuccessResultU3Eb__4_0_m0BE5F47C2AB2EDB47AA4D1489BC2C20F63AF1E38 (void);
// 0x00000218 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m29935D6F81F1BB5747178C13DC3E55CC83035D6F (void);
// 0x00000219 System.Void Facebook.Unity.Editor.Dialogs.MockLoginDialog_<>c__DisplayClass4_0::<SendSuccessResult>b__1(Facebook.Unity.IGraphResult)
extern void U3CU3Ec__DisplayClass4_0_U3CSendSuccessResultU3Eb__1_mE1F0AA70E5A7FBCBB206B98BFA996771B39665FF (void);
// 0x0000021A System.String Facebook.Unity.Editor.Dialogs.MockShareDialog::get_SubTitle()
extern void MockShareDialog_get_SubTitle_m005AF11050A428A4F19F3E714293731EB5519D2C (void);
// 0x0000021B System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::set_SubTitle(System.String)
extern void MockShareDialog_set_SubTitle_mFD33AAAA0628763A9CC751317ED89B5B56140BFD (void);
// 0x0000021C System.String Facebook.Unity.Editor.Dialogs.MockShareDialog::get_DialogTitle()
extern void MockShareDialog_get_DialogTitle_m01C01BF38BBCED8F5980E79640DE2E37D7CE529C (void);
// 0x0000021D System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::DoGui()
extern void MockShareDialog_DoGui_mBC8E23A0F094F31A5FDF6FC33CB1674500F715AC (void);
// 0x0000021E System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::SendSuccessResult()
extern void MockShareDialog_SendSuccessResult_m21F5E6F1A1183C71A3E39A5C4F0CEC8A752A3B38 (void);
// 0x0000021F System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::SendCancelResult()
extern void MockShareDialog_SendCancelResult_m67C9FDDEBA6FF56EC9746426FAE9E1437056B63E (void);
// 0x00000220 System.String Facebook.Unity.Editor.Dialogs.MockShareDialog::GenerateFakePostID()
extern void MockShareDialog_GenerateFakePostID_m89B0CB66118ABFC13B2E1A7FCB6EACDF75AF4AAE (void);
// 0x00000221 System.Void Facebook.Unity.Editor.Dialogs.MockShareDialog::.ctor()
extern void MockShareDialog__ctor_mC460306812D8853D20C89A67286BD6D17D4DAF9A (void);
// 0x00000222 System.Boolean Facebook.Unity.Mobile.IMobileFacebook::IsImplicitPurchaseLoggingEnabled()
// 0x00000223 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnFetchDeferredAppLinkComplete(Facebook.Unity.ResultContainer)
// 0x00000224 System.Void Facebook.Unity.Mobile.IMobileFacebookResultHandler::OnRefreshCurrentAccessTokenComplete(Facebook.Unity.ResultContainer)
// 0x00000225 System.Void Facebook.Unity.Mobile.MobileFacebook::.ctor(Facebook.Unity.CallbackManager)
extern void MobileFacebook__ctor_m7F2F46E7E7B30A5311D42904C5917481C3F295B7 (void);
// 0x00000226 System.Boolean Facebook.Unity.Mobile.MobileFacebook::IsImplicitPurchaseLoggingEnabled()
// 0x00000227 System.Void Facebook.Unity.Mobile.MobileFacebook::OnLoginComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnLoginComplete_m6BE5BC218821E482C68BB79753EDBC0AF9566267 (void);
// 0x00000228 System.Void Facebook.Unity.Mobile.MobileFacebook::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnGetAppLinkComplete_m8D67E1FAD6FCC094A12DB261784B1428E21CD39D (void);
// 0x00000229 System.Void Facebook.Unity.Mobile.MobileFacebook::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnAppRequestsComplete_m064D4CB5DBFF9441814E14AA14C075A310E1BFD3 (void);
// 0x0000022A System.Void Facebook.Unity.Mobile.MobileFacebook::OnFetchDeferredAppLinkComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnFetchDeferredAppLinkComplete_mAAB8ABED8B610B953D6B815774B45170787D35F2 (void);
// 0x0000022B System.Void Facebook.Unity.Mobile.MobileFacebook::OnShareLinkComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnShareLinkComplete_m6AE100F74500A6FE0E523EF43CE1F7376E6F09A8 (void);
// 0x0000022C System.Void Facebook.Unity.Mobile.MobileFacebook::OnRefreshCurrentAccessTokenComplete(Facebook.Unity.ResultContainer)
extern void MobileFacebook_OnRefreshCurrentAccessTokenComplete_m5F9C745D510A9DECABFD3928CFDAB3795C5813BD (void);
// 0x0000022D Facebook.Unity.Mobile.IMobileFacebookImplementation Facebook.Unity.Mobile.MobileFacebookGameObject::get_MobileFacebook()
extern void MobileFacebookGameObject_get_MobileFacebook_m955341BBEADEB1CBB64DE4F9E9B615181DEDD26A (void);
// 0x0000022E System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnFetchDeferredAppLinkComplete(System.String)
extern void MobileFacebookGameObject_OnFetchDeferredAppLinkComplete_mE5C94C955EFED281B6F72FC6F5A89D53D4BA8C1E (void);
// 0x0000022F System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::OnRefreshCurrentAccessTokenComplete(System.String)
extern void MobileFacebookGameObject_OnRefreshCurrentAccessTokenComplete_m0D87DA3A46598BDFF9FBBB30BBC8CB20A377B6FB (void);
// 0x00000230 System.Void Facebook.Unity.Mobile.MobileFacebookGameObject::.ctor()
extern void MobileFacebookGameObject__ctor_m5BD724C5A630B3A1915B2467A1F11AA9DED83B1C (void);
// 0x00000231 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::Init(System.String,System.Boolean,System.String,System.String)
// 0x00000232 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogInWithReadPermissions(System.Int32,System.String)
// 0x00000233 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogInWithPublishPermissions(System.Int32,System.String)
// 0x00000234 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogOut()
// 0x00000235 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::ShareLink(System.Int32,System.String,System.String,System.String,System.String)
// 0x00000236 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::FeedShare(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
// 0x00000237 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::AppRequest(System.Int32,System.String,System.String,System.String,System.String[],System.Int32,System.String,System.String[],System.Int32,System.Boolean,System.Int32,System.String,System.String)
// 0x00000238 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::FBAppEventsActivateApp()
// 0x00000239 System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogAppEvent(System.String,System.Double,System.Int32,System.String[],System.String[])
// 0x0000023A System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::LogPurchaseAppEvent(System.Double,System.String,System.Int32,System.String[],System.String[])
// 0x0000023B System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::FBAppEventsSetLimitEventUsage(System.Boolean)
// 0x0000023C System.Void Facebook.Unity.Mobile.IOS.IIOSWrapper::GetAppLink(System.Int32)
// 0x0000023D System.String Facebook.Unity.Mobile.IOS.IIOSWrapper::FBSdkVersion()
// 0x0000023E System.String Facebook.Unity.Mobile.IOS.IIOSWrapper::FBGetUserID()
// 0x0000023F System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::.ctor()
extern void IOSFacebook__ctor_mE9092C7C32FC81E39F37E0B812BBD99A976BA2D0 (void);
// 0x00000240 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::.ctor(Facebook.Unity.Mobile.IOS.IIOSWrapper,Facebook.Unity.CallbackManager)
extern void IOSFacebook__ctor_m2C5F57FB7EBD31E69E9E8E485D5F1C06BDA4271E (void);
// 0x00000241 System.Boolean Facebook.Unity.Mobile.IOS.IOSFacebook::get_LimitEventUsage()
extern void IOSFacebook_get_LimitEventUsage_m08C6C33813B53C9BFCFDD31462832DC2DFF0D7E5 (void);
// 0x00000242 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::set_LimitEventUsage(System.Boolean)
extern void IOSFacebook_set_LimitEventUsage_m69406DE87559377646635FE111FDFE64ED4BF880 (void);
// 0x00000243 System.String Facebook.Unity.Mobile.IOS.IOSFacebook::get_SDKName()
extern void IOSFacebook_get_SDKName_m2CE9B620F09FEC908575F2F98CBF3F5127E8491B (void);
// 0x00000244 System.String Facebook.Unity.Mobile.IOS.IOSFacebook::get_SDKVersion()
extern void IOSFacebook_get_SDKVersion_mBAD3752FD4AB92410F8745E4E6D627EEE036DE79 (void);
// 0x00000245 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::Init(System.String,System.Boolean,System.String,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void IOSFacebook_Init_mC339495FB634964DD397A2E408A20980C2B5832D (void);
// 0x00000246 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void IOSFacebook_LogInWithReadPermissions_mC7F47768CA6C859F288FABF11B235D7CC177B5B3 (void);
// 0x00000247 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void IOSFacebook_LogInWithPublishPermissions_m3B65EA6651EB3212E66687E348FAEB3CD5F054C0 (void);
// 0x00000248 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::LogOut()
extern void IOSFacebook_LogOut_m66F72FA5259F4B7F566EB9FA9B541053E4FB586C (void);
// 0x00000249 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void IOSFacebook_AppRequest_mB7916D5679064B88427D185648D4CEFC1A2372A2 (void);
// 0x0000024A System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void IOSFacebook_ShareLink_mB91D22E0A4F07801F62DE90D4F938C22D51F7083 (void);
// 0x0000024B System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void IOSFacebook_FeedShare_m332EF7C19B5BB52DAC61DC7785079FE865969EEE (void);
// 0x0000024C System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void IOSFacebook_AppEventsLogEvent_mC659F54A53EC415823D80CD204F7126B2655FBE0 (void);
// 0x0000024D System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void IOSFacebook_AppEventsLogPurchase_mF5286D681D71472253CE6FB2E7A5ADFDCCFC17F5 (void);
// 0x0000024E System.Boolean Facebook.Unity.Mobile.IOS.IOSFacebook::IsImplicitPurchaseLoggingEnabled()
extern void IOSFacebook_IsImplicitPurchaseLoggingEnabled_mE7F50E80E21D062F6753000D842534547A21B17F (void);
// 0x0000024F System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::ActivateApp(System.String)
extern void IOSFacebook_ActivateApp_m5E0923429FFA69CBF4A9C313FA3C829CBC33F242 (void);
// 0x00000250 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void IOSFacebook_GetAppLink_m3191D32F37EB7F449E52155DE157534B7712314A (void);
// 0x00000251 Facebook.Unity.Mobile.IOS.IIOSWrapper Facebook.Unity.Mobile.IOS.IOSFacebook::GetIOSWrapper()
extern void IOSFacebook_GetIOSWrapper_m20BB4E4D64EF6ECFEAA6067B8291FAC0247E62CE (void);
// 0x00000252 Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict Facebook.Unity.Mobile.IOS.IOSFacebook::MarshallDict(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void IOSFacebook_MarshallDict_m9641A0BA15C953707463D0228008E16769DB295C (void);
// 0x00000253 System.Int32 Facebook.Unity.Mobile.IOS.IOSFacebook::AddCallback(Facebook.Unity.FacebookDelegate`1<T>)
// 0x00000254 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::.ctor()
extern void NativeDict__ctor_m0F9530F5B90EE9B00B926B27270B7C82F8270AC8 (void);
// 0x00000255 System.Int32 Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::get_NumEntries()
extern void NativeDict_get_NumEntries_mAE4481858E3A75AFCB55344B3955BB261ED582AB (void);
// 0x00000256 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::set_NumEntries(System.Int32)
extern void NativeDict_set_NumEntries_m2B01015F005F402BF73B351D223A1775BBA10404 (void);
// 0x00000257 System.String[] Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::get_Keys()
extern void NativeDict_get_Keys_mC1AD4DADCA658F2F01893D98C1484DF3D3CF0732 (void);
// 0x00000258 System.Void Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::set_Keys(System.String[])
extern void NativeDict_set_Keys_m003BE548ED7F10FEFA7AD36111F98F594CBAF836 (void);
// 0x00000259 System.String[] Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::get_Values()
extern void NativeDict_get_Values_m598E0F0F327AACF854A88CB60D7DEE2462877F5E (void);
// 0x0000025A System.Void Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::set_Values(System.String[])
extern void NativeDict_set_Values_m61F3F99F851A541DB40481EBC4EB4B86FC20F40C (void);
// 0x0000025B System.Void Facebook.Unity.Mobile.IOS.IOSFacebookGameObject::.ctor()
extern void IOSFacebookGameObject__ctor_m63EC89911EB6FD1D09C7D2326DEDD00182DD9D61 (void);
// 0x0000025C Facebook.Unity.FacebookGameObject Facebook.Unity.Mobile.IOS.IOSFacebookLoader::get_FBGameObject()
extern void IOSFacebookLoader_get_FBGameObject_m7CE629B334A3CD2407F3C8AC29AE276E5CF940DD (void);
// 0x0000025D System.Void Facebook.Unity.Mobile.IOS.IOSFacebookLoader::.ctor()
extern void IOSFacebookLoader__ctor_mF736AD21858D85A1AE3CCD69C3FF35A6F0687126 (void);
// 0x0000025E System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::.ctor()
extern void AndroidFacebook__ctor_mCE9B27E577B5A6B480E490CCCEFE0EAA1AEFD420 (void);
// 0x0000025F System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::.ctor(Facebook.Unity.Mobile.Android.IAndroidWrapper,Facebook.Unity.CallbackManager)
extern void AndroidFacebook__ctor_m06B102FCF1D25922DD8E102EB4AADB9680C4E927 (void);
// 0x00000260 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::set_KeyHash(System.String)
extern void AndroidFacebook_set_KeyHash_m011A0E2B79448395F98CA451DB6E723DAC27E264 (void);
// 0x00000261 System.Boolean Facebook.Unity.Mobile.Android.AndroidFacebook::get_LimitEventUsage()
extern void AndroidFacebook_get_LimitEventUsage_m94249500E4A7AD7A7BFBAEF62B14AC4BA36CBB31 (void);
// 0x00000262 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::set_LimitEventUsage(System.Boolean)
extern void AndroidFacebook_set_LimitEventUsage_mF8BF8360027BDCDE57E3458697D268FD946B9AEA (void);
// 0x00000263 System.String Facebook.Unity.Mobile.Android.AndroidFacebook::get_SDKName()
extern void AndroidFacebook_get_SDKName_m15D829440EAA71FEA605845E29D9419E007786DA (void);
// 0x00000264 System.String Facebook.Unity.Mobile.Android.AndroidFacebook::get_SDKVersion()
extern void AndroidFacebook_get_SDKVersion_mF214EA76A1522B0FB8152F0F42992C73D7BE239B (void);
// 0x00000265 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::Init(System.String,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void AndroidFacebook_Init_mAC0526C93B99BFFB47AEFF550B3BD5116A5158B0 (void);
// 0x00000266 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void AndroidFacebook_LogInWithReadPermissions_m82C35A6F14C5116D7A855CFC33DF71AD3546928F (void);
// 0x00000267 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void AndroidFacebook_LogInWithPublishPermissions_m36829BDFFC8669BD5933BCF30F558E2D59C1663C (void);
// 0x00000268 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::LogOut()
extern void AndroidFacebook_LogOut_mA0934F37AA226D3C89EE1451F80051DE8123A15F (void);
// 0x00000269 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void AndroidFacebook_AppRequest_mA5C375668A094E1F0A106F4C1B781C3570DC697B (void);
// 0x0000026A System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void AndroidFacebook_ShareLink_m2950BE87151F48F12436848487D8B0037442E56C (void);
// 0x0000026B System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void AndroidFacebook_FeedShare_m0BCDB85B71B730944FD27A861C3F4D1A3581CC3B (void);
// 0x0000026C System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void AndroidFacebook_GetAppLink_m128D5EDACE4E8F82C1E056D47E36C0D17C927A89 (void);
// 0x0000026D System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AndroidFacebook_AppEventsLogEvent_m634ACA27D524358DA99A841E0AC08EA8152E3236 (void);
// 0x0000026E System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AndroidFacebook_AppEventsLogPurchase_mA45E0AF11D8DC8E929B33826B4CF7F94E7B5A5E6 (void);
// 0x0000026F System.Boolean Facebook.Unity.Mobile.Android.AndroidFacebook::IsImplicitPurchaseLoggingEnabled()
extern void AndroidFacebook_IsImplicitPurchaseLoggingEnabled_mABA8DA2A8804C90782C496BF3B6F3DBC4FD1F762 (void);
// 0x00000270 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::ActivateApp(System.String)
extern void AndroidFacebook_ActivateApp_m0507787406F0D02C01BB1B2623BD038B00701BF6 (void);
// 0x00000271 Facebook.Unity.Mobile.Android.IAndroidWrapper Facebook.Unity.Mobile.Android.AndroidFacebook::GetAndroidWrapper()
extern void AndroidFacebook_GetAndroidWrapper_m21D59B70F3A9B1BEAD200D2FCE70C9DB32B6FDE6 (void);
// 0x00000272 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook::CallFB(System.String,System.String)
extern void AndroidFacebook_CallFB_m944042FB6A254C92B893413CD86914CEDE97C6B5 (void);
// 0x00000273 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook_JavaMethodCall`1::.ctor(Facebook.Unity.Mobile.Android.AndroidFacebook,System.String)
// 0x00000274 System.Void Facebook.Unity.Mobile.Android.AndroidFacebook_JavaMethodCall`1::Call(Facebook.Unity.MethodArguments)
// 0x00000275 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnAwake()
extern void AndroidFacebookGameObject_OnAwake_mC22A910DF1D87EEDDB8DF5ADF999B2BC24A8767D (void);
// 0x00000276 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnEnable()
extern void AndroidFacebookGameObject_OnEnable_mE03E7E37F9E5E84F53484902DDD657CFD70F1D0E (void);
// 0x00000277 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void AndroidFacebookGameObject_OnSceneLoaded_m5734C9E724CDD20EBF60E9E59186FEF2AD049F2F (void);
// 0x00000278 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::OnDisable()
extern void AndroidFacebookGameObject_OnDisable_m6380145C5FB31F572815359A3F49A30E3B48B251 (void);
// 0x00000279 System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::onPurchaseCompleteHandler(System.Object)
extern void AndroidFacebookGameObject_onPurchaseCompleteHandler_m436BCA06DB06273DAC01EE4DCA1470E5022E95E7 (void);
// 0x0000027A System.Void Facebook.Unity.Mobile.Android.AndroidFacebookGameObject::.ctor()
extern void AndroidFacebookGameObject__ctor_m02107DE14B5E3309778871C2BFF1BED8CC5A1EC1 (void);
// 0x0000027B Facebook.Unity.FacebookGameObject Facebook.Unity.Mobile.Android.AndroidFacebookLoader::get_FBGameObject()
extern void AndroidFacebookLoader_get_FBGameObject_mC608EFA90A0806F001718C88A3B357E0FA2E20D0 (void);
// 0x0000027C System.Void Facebook.Unity.Mobile.Android.AndroidFacebookLoader::.ctor()
extern void AndroidFacebookLoader__ctor_m6CA0C30F88650A2AA7B7C575E2699C1457C736B5 (void);
// 0x0000027D T Facebook.Unity.Mobile.Android.IAndroidWrapper::CallStatic(System.String)
// 0x0000027E System.Void Facebook.Unity.Mobile.Android.IAndroidWrapper::CallStatic(System.String,System.Object[])
// 0x0000027F System.Void Facebook.Unity.Canvas.CanvasFacebook::.ctor()
extern void CanvasFacebook__ctor_mA4D167636BD0D7EB88AB643BD5A5D609222533C0 (void);
// 0x00000280 System.Void Facebook.Unity.Canvas.CanvasFacebook::.ctor(Facebook.Unity.Canvas.ICanvasJSWrapper,Facebook.Unity.CallbackManager)
extern void CanvasFacebook__ctor_mEE95A791A05E10B695DB6A5A269106A4705E9680 (void);
// 0x00000281 Facebook.Unity.Canvas.ICanvasJSWrapper Facebook.Unity.Canvas.CanvasFacebook::GetCanvasJSWrapper()
extern void CanvasFacebook_GetCanvasJSWrapper_mBE28994B0A9B10E1E43906E9939CD2D9B98AF44B (void);
// 0x00000282 System.Boolean Facebook.Unity.Canvas.CanvasFacebook::get_LimitEventUsage()
extern void CanvasFacebook_get_LimitEventUsage_m9CC1E166137F93C3F6309501CD7771D451825416 (void);
// 0x00000283 System.Void Facebook.Unity.Canvas.CanvasFacebook::set_LimitEventUsage(System.Boolean)
extern void CanvasFacebook_set_LimitEventUsage_m7C9FBA3A9FB7FC2FD156908F81C806D3F14C7835 (void);
// 0x00000284 System.String Facebook.Unity.Canvas.CanvasFacebook::get_SDKName()
extern void CanvasFacebook_get_SDKName_mB12643F6D6191B0447D27F31B370D585D1F708E3 (void);
// 0x00000285 System.String Facebook.Unity.Canvas.CanvasFacebook::get_SDKVersion()
extern void CanvasFacebook_get_SDKVersion_mA4C4B80613CE630EAF789E3BB4A86D2ECB7A038C (void);
// 0x00000286 System.String Facebook.Unity.Canvas.CanvasFacebook::get_SDKUserAgent()
extern void CanvasFacebook_get_SDKUserAgent_m15416A6893C67B62E191C902B97E3289C1E86398 (void);
// 0x00000287 System.Void Facebook.Unity.Canvas.CanvasFacebook::Init(System.String,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,System.Boolean,System.String,System.Boolean,Facebook.Unity.HideUnityDelegate,Facebook.Unity.InitDelegate)
extern void CanvasFacebook_Init_mB72799BDC8E626693272459BCDFFE7DAD0BBB670 (void);
// 0x00000288 System.Void Facebook.Unity.Canvas.CanvasFacebook::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void CanvasFacebook_LogInWithPublishPermissions_mC353DDCE84164548555CF6A948201EADBDA29FAE (void);
// 0x00000289 System.Void Facebook.Unity.Canvas.CanvasFacebook::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern void CanvasFacebook_LogInWithReadPermissions_mD261916C730BC43785C8A59DE43C53117C4FBCF6 (void);
// 0x0000028A System.Void Facebook.Unity.Canvas.CanvasFacebook::LogOut()
extern void CanvasFacebook_LogOut_m0104B2DA460D12F33A79034A5D94CC045E240355 (void);
// 0x0000028B System.Void Facebook.Unity.Canvas.CanvasFacebook::AppRequest(System.String,System.Nullable`1<Facebook.Unity.OGActionType>,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern void CanvasFacebook_AppRequest_m75CFF9CA211D83887DD31B20650E9AABD02A83F7 (void);
// 0x0000028C System.Void Facebook.Unity.Canvas.CanvasFacebook::ActivateApp(System.String)
extern void CanvasFacebook_ActivateApp_mB360FE1F1257D3724AADC33D477A4AA113314BFD (void);
// 0x0000028D System.Void Facebook.Unity.Canvas.CanvasFacebook::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void CanvasFacebook_ShareLink_m3DE77F75C20DC08DE2EE30DD1A5E485F1A5B4F96 (void);
// 0x0000028E System.Void Facebook.Unity.Canvas.CanvasFacebook::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern void CanvasFacebook_FeedShare_m3B66B53A13331D75718D0DE35861E2D6127DC429 (void);
// 0x0000028F System.Void Facebook.Unity.Canvas.CanvasFacebook::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern void CanvasFacebook_GetAppLink_m20829C7B93857C2BFF2FB3D5D4CDB876F0ECD7B4 (void);
// 0x00000290 System.Void Facebook.Unity.Canvas.CanvasFacebook::AppEventsLogEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void CanvasFacebook_AppEventsLogEvent_mF2A8EF4EC50971415D810B68FB774CEA9878BB48 (void);
// 0x00000291 System.Void Facebook.Unity.Canvas.CanvasFacebook::AppEventsLogPurchase(System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void CanvasFacebook_AppEventsLogPurchase_m2B456DE69973EF1638AA93718DD0629AFEB8F95C (void);
// 0x00000292 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnLoginComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnLoginComplete_m67E4E75435099825AF37673F36F8BB5C26438C74 (void);
// 0x00000293 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnGetAppLinkComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnGetAppLinkComplete_mAD0E60051CA62CB023AD725576C7C89837B58FC4 (void);
// 0x00000294 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnFacebookAuthResponseChange(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnFacebookAuthResponseChange_m84947A7108A5E24CA74CA205BD3A0EB052124025 (void);
// 0x00000295 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnPayComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnPayComplete_mC9DD0864D37BF0A841CFFE9B46482A8A3754DEAD (void);
// 0x00000296 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnAppRequestsComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnAppRequestsComplete_m2FA9E0C6549F4D01A595628C5D8D8436129F6F87 (void);
// 0x00000297 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnShareLinkComplete(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_OnShareLinkComplete_mF068F2BB7C7A926C0A89442C308114BD5EF0416C (void);
// 0x00000298 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnUrlResponse(System.String)
extern void CanvasFacebook_OnUrlResponse_m0BCD31B33F46AC840BCA2E5172810A719E650024 (void);
// 0x00000299 System.Void Facebook.Unity.Canvas.CanvasFacebook::OnHideUnity(System.Boolean)
extern void CanvasFacebook_OnHideUnity_m396828CFC14CFC7825EA04EF9E6CB968B60E4576 (void);
// 0x0000029A System.Void Facebook.Unity.Canvas.CanvasFacebook::FormatAuthResponse(Facebook.Unity.ResultContainer,Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer>)
extern void CanvasFacebook_FormatAuthResponse_mC122E02995F35CA78D3F6E35E8A49CD140FC7CCB (void);
// 0x0000029B System.Void Facebook.Unity.Canvas.CanvasFacebook::<OnLoginComplete>b__37_0(Facebook.Unity.ResultContainer)
extern void CanvasFacebook_U3COnLoginCompleteU3Eb__37_0_m121CFE841950FB9AAFF57727BF86856DCFECD845 (void);
// 0x0000029C System.Void Facebook.Unity.Canvas.CanvasFacebook_CanvasUIMethodCall`1::.ctor(Facebook.Unity.Canvas.CanvasFacebook,System.String,System.String)
// 0x0000029D System.Void Facebook.Unity.Canvas.CanvasFacebook_CanvasUIMethodCall`1::Call(Facebook.Unity.MethodArguments)
// 0x0000029E System.Void Facebook.Unity.Canvas.CanvasFacebook_CanvasUIMethodCall`1::UI(System.String,Facebook.Unity.MethodArguments,Facebook.Unity.FacebookDelegate`1<T>)
// 0x0000029F System.Void Facebook.Unity.Canvas.CanvasFacebook_<>c::.cctor()
extern void U3CU3Ec__cctor_m46DF256AF9268C9BC6FD061C7715BBDA620CD619 (void);
// 0x000002A0 System.Void Facebook.Unity.Canvas.CanvasFacebook_<>c::.ctor()
extern void U3CU3Ec__ctor_m42F0BBEB665594B5BE49452C39AB3137578C30B2 (void);
// 0x000002A1 System.Void Facebook.Unity.Canvas.CanvasFacebook_<>c::<OnFacebookAuthResponseChange>b__40_0(Facebook.Unity.ResultContainer)
extern void U3CU3Ec_U3COnFacebookAuthResponseChangeU3Eb__40_0_mA4E30AADC07FEC8F3CAC1A2DA589038C041657CA (void);
// 0x000002A2 System.Void Facebook.Unity.Canvas.CanvasFacebook_<>c__DisplayClass47_0::.ctor()
extern void U3CU3Ec__DisplayClass47_0__ctor_m015B73B35ED98A71F35BBEC6B6353AE7FE273EE9 (void);
// 0x000002A3 System.Void Facebook.Unity.Canvas.CanvasFacebook_<>c__DisplayClass47_0::<FormatAuthResponse>b__0(Facebook.Unity.IGraphResult)
extern void U3CU3Ec__DisplayClass47_0_U3CFormatAuthResponseU3Eb__0_m1D8F01E2205DCEFDC415AA764F1A309FC1E38E12 (void);
// 0x000002A4 Facebook.Unity.Canvas.ICanvasFacebookImplementation Facebook.Unity.Canvas.CanvasFacebookGameObject::get_CanvasFacebookImpl()
extern void CanvasFacebookGameObject_get_CanvasFacebookImpl_mCBC8E0C14005EC7783FFCEACE216395E3AB90334 (void);
// 0x000002A5 System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnPayComplete(System.String)
extern void CanvasFacebookGameObject_OnPayComplete_mF97E518F290BB454F0B33C3E1355584E375B7F16 (void);
// 0x000002A6 System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnFacebookAuthResponseChange(System.String)
extern void CanvasFacebookGameObject_OnFacebookAuthResponseChange_mBC109A88D9182A9E625EEE1901A2C56B85DE7FF1 (void);
// 0x000002A7 System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnUrlResponse(System.String)
extern void CanvasFacebookGameObject_OnUrlResponse_mEDA593152628394EEA48DBE3AA98C7037EEA548E (void);
// 0x000002A8 System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnHideUnity(System.Boolean)
extern void CanvasFacebookGameObject_OnHideUnity_mDE03D83510F04E2588EEE7CE3F2FED70941091C1 (void);
// 0x000002A9 System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::OnAwake()
extern void CanvasFacebookGameObject_OnAwake_m9A9BD2E92E1F60B05FD330B36CA2916D2CD1A9F3 (void);
// 0x000002AA System.Void Facebook.Unity.Canvas.CanvasFacebookGameObject::.ctor()
extern void CanvasFacebookGameObject__ctor_mD2692E48EC1C16E675CC8919569609BA16B4E279 (void);
// 0x000002AB Facebook.Unity.FacebookGameObject Facebook.Unity.Canvas.CanvasFacebookLoader::get_FBGameObject()
extern void CanvasFacebookLoader_get_FBGameObject_m4694FBD84F431765392E087BAD0F9B54D37DA2D3 (void);
// 0x000002AC System.Void Facebook.Unity.Canvas.CanvasFacebookLoader::.ctor()
extern void CanvasFacebookLoader__ctor_m9CC68D1BBFBB04D81C17F3A84D89A79223E209C9 (void);
// 0x000002AD System.Void Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler::OnPayComplete(System.String)
// 0x000002AE System.Void Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler::OnFacebookAuthResponseChange(System.String)
// 0x000002AF System.Void Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler::OnUrlResponse(System.String)
// 0x000002B0 System.Void Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler::OnHideUnity(System.Boolean)
// 0x000002B1 System.Void Facebook.Unity.Canvas.ICanvasFacebookResultHandler::OnPayComplete(Facebook.Unity.ResultContainer)
// 0x000002B2 System.Void Facebook.Unity.Canvas.ICanvasFacebookResultHandler::OnFacebookAuthResponseChange(Facebook.Unity.ResultContainer)
// 0x000002B3 System.Void Facebook.Unity.Canvas.ICanvasFacebookResultHandler::OnUrlResponse(System.String)
// 0x000002B4 System.Void Facebook.Unity.Canvas.ICanvasFacebookResultHandler::OnHideUnity(System.Boolean)
// 0x000002B5 System.String Facebook.Unity.Canvas.ICanvasJSWrapper::GetSDKVersion()
// 0x000002B6 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::DisableFullScreen()
// 0x000002B7 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::Init(System.String,System.String,System.Int32,System.String,System.Int32)
// 0x000002B8 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::Login(System.Collections.Generic.IEnumerable`1<System.String>,System.String)
// 0x000002B9 System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::Logout()
// 0x000002BA System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::ActivateApp()
// 0x000002BB System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::LogAppEvent(System.String,System.Nullable`1<System.Single>,System.String)
// 0x000002BC System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::LogPurchase(System.Single,System.String,System.String)
// 0x000002BD System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::Ui(System.String,System.String,System.String)
// 0x000002BE System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::InitScreenPosition()
// 0x000002BF System.Void Facebook.Unity.Canvas.JsBridge::Start()
extern void JsBridge_Start_m4961FD63DF2AE788A939E85F9FB415719D3F86C3 (void);
// 0x000002C0 System.Void Facebook.Unity.Canvas.JsBridge::OnLoginComplete(System.String)
extern void JsBridge_OnLoginComplete_mB6370711671734B4252E9E372B30C29266DF9917 (void);
// 0x000002C1 System.Void Facebook.Unity.Canvas.JsBridge::OnFacebookAuthResponseChange(System.String)
extern void JsBridge_OnFacebookAuthResponseChange_m3379164A26B1184E9390036F087531B0696A0753 (void);
// 0x000002C2 System.Void Facebook.Unity.Canvas.JsBridge::OnPayComplete(System.String)
extern void JsBridge_OnPayComplete_m4FDCA451487C52F2CEAA20C06977FEA9BBDE163F (void);
// 0x000002C3 System.Void Facebook.Unity.Canvas.JsBridge::OnAppRequestsComplete(System.String)
extern void JsBridge_OnAppRequestsComplete_m9643B9A3F366CA1FE3D41DD83BC97713E2C2998D (void);
// 0x000002C4 System.Void Facebook.Unity.Canvas.JsBridge::OnShareLinkComplete(System.String)
extern void JsBridge_OnShareLinkComplete_m6FA81C45A9ABA83521F4A75F243D621E1DC1142D (void);
// 0x000002C5 System.Void Facebook.Unity.Canvas.JsBridge::OnFacebookFocus(System.String)
extern void JsBridge_OnFacebookFocus_mABE3BD9E1F077D927E89D62923AA0519F36D2E15 (void);
// 0x000002C6 System.Void Facebook.Unity.Canvas.JsBridge::OnInitComplete(System.String)
extern void JsBridge_OnInitComplete_m84DAFC312AD6E4DD9F459359389C4671881DDF20 (void);
// 0x000002C7 System.Void Facebook.Unity.Canvas.JsBridge::OnUrlResponse(System.String)
extern void JsBridge_OnUrlResponse_mD05D544199431003845015CFD404B6DCD3846173 (void);
// 0x000002C8 System.Void Facebook.Unity.Canvas.JsBridge::.ctor()
extern void JsBridge__ctor_m5EC7E0D5AA8E7FA4E473F7B3EB8D7DF639F44A28 (void);
static Il2CppMethodPointer s_methodPointers[712] = 
{
	Json_Deserialize_m972B61039CCA76FF8B9E7F10386DA79D70CFFC89,
	Json_Serialize_mAB9484F1CED0115971BEF516C9FBD288927A4147,
	Json__cctor_m85AF29F564421DE80116FA01BB29D2C01BDE63E6,
	Parser__ctor_mEAADBE91C3200D3A288CF26C97D1E36D59AA59C3,
	Parser_get_PeekChar_m9C2525B091792F26DBD4F361D43C89967F8D9899,
	Parser_get_NextChar_mC7CD0C12A40F8366CD9DC58600F6BBF6FC4D830D,
	Parser_get_NextWord_mD556C0698948EE8A2C5F82A3E7A10ADD498CDED5,
	Parser_get_NextToken_m6374135A3390F406B24EF0DA215DE4D39002F582,
	Parser_Parse_mA58ACF97751DFB361B80A27868CB6900DA936B1F,
	Parser_Dispose_mF001AEC19B5898A39D396D16CE5690014EB2F578,
	Parser_ParseObject_m7ABFBBC8548B51369628118DD30174612E7ACFCF,
	Parser_ParseArray_mCBF16AB48642FE0C778DF7BA643E83A70A8E11FA,
	Parser_ParseValue_m4ECD06FF0396EAB7C6A5A669EF7DDEAF5FC79CC8,
	Parser_ParseByToken_m8D304812FA64EA087EB68CA33B1C58EB941D6C6D,
	Parser_ParseString_m900DA186BAE180C38158A5012156EF8DC3743877,
	Parser_ParseNumber_mB815AA6E9C267EB0828546A22FD821C8D780D993,
	Parser_EatWhitespace_mDC9F0FBCDF96E2BA32F11E098C8C3F63926832E5,
	Serializer__ctor_m9FDB3B04D734300C03C20DB104AD8BFBA288D624,
	Serializer_Serialize_mFAE9186EA73D4505AF70292967968823C2F05C9E,
	Serializer_SerializeValue_m1397C7120E65709AD9DA500CDF01DE9EF92CB0F3,
	Serializer_SerializeObject_mB914F03AC2293864586B1FE72FBF4D648B4896F0,
	Serializer_SerializeArray_mF83F4104DDE5FB0B38FA0E810CC3F1F703FFD1FF,
	Serializer_SerializeString_m8BF12319CF59FF944248AB52BF53B93FD9A1D085,
	Serializer_SerializeOther_mD96CF1C52799B1569D5DE29AC5110442746CF946,
	AccessToken__ctor_m5A676F9B2D2060144483C76527373264DC8CCD5A,
	AccessToken_get_CurrentAccessToken_mFC3AD4FDC234BBC75D5F0162B433F071DEF451CE,
	AccessToken_set_CurrentAccessToken_m3BBCD50AC99B44C2EEEFD6435E54102D6DF51813,
	AccessToken_get_TokenString_m2BC6FC8BDBE71CC6E643277AE93511FC115334FC,
	AccessToken_set_TokenString_mEB2E6FEA665B176D9F0D3DDEA2C76C4F23B150B0,
	AccessToken_get_ExpirationTime_mEF979739A4BA8B3D24B6979504C0B6820DC8EDF5,
	AccessToken_set_ExpirationTime_mB9E9326A6123FD5A3A19D644926BF469C56A3103,
	AccessToken_get_Permissions_m321B455AEDDE47865CD0A9377749F77C12F292EF,
	AccessToken_set_Permissions_mC796D8F35BF513959F14862BFE70F555A6E7AE99,
	AccessToken_get_UserId_m08B2AD1A2C81B4724ABC88319FB884D545364099,
	AccessToken_set_UserId_m3E269BC0D164067DA1CEAC2457B987BAF5139E7D,
	AccessToken_get_LastRefresh_mCD48467DC14BFF3F710FAE823DB374FB5B0E348C,
	AccessToken_set_LastRefresh_m37C0B3CFD66DF99A6133BED6B889D0CBBD011E52,
	AccessToken_ToString_m2F7FE35EBD366BDE64F29ED25E6DA84F384129F4,
	AccessToken_ToJson_m6E41B1D78FC2D4C5E6E7589D4EF8DACD819E2009,
	NULL,
	CallbackManager_OnFacebookResponse_m8E448D080D0DC61AA76714CC41D31BDAA6DDE0DE,
	CallbackManager_CallCallback_m2EF00E171C381298E4509143109BB46B72411BDA,
	NULL,
	CallbackManager__ctor_mA1881E8783422989E78577DBBBB0364014A77C0C,
	ComponentFactory_get_FacebookGameObject_m409E4D19F346A8C994B478F2906C45D00A5F2685,
	NULL,
	NULL,
	Constants_get_GraphUrl_mBCCB75EE9A467000B6A2077401B11C796496D7DE,
	Constants_get_GraphApiUserAgent_m47F8FFD6911F6DEC75F8778D31C3455B37A70C0A,
	Constants_get_IsEditor_m2D932238515AC9BA84B28143296C97DE21221985,
	Constants_get_IsWeb_m69867949DCB782E8376E7937516B535942479E35,
	Constants_get_IsGameroom_m28CBD1D05E77EE1EA44E5A08DC3CBF16E23CE34D,
	Constants_get_UnitySDKUserAgentSuffixLegacy_m444C1614AC50CBFD76BD8D9B3B2CA225220D4F2E,
	Constants_get_UnitySDKUserAgent_m78C304A6138E30655D4B8745FF95C38AF967423C,
	Constants_get_DebugMode_mB467536EC9EE9BA5076D37728276E7574B8914F7,
	Constants_get_CurrentPlatform_mC5DF32B4DFDF845C112822C7CBDAF7F42BDDDB6C,
	Constants_GetCurrentPlatform_m132C8DE506B1560F84B01F726BE886CEE5F4062F,
	FB_get_AppId_m2D22BF99542B8823D44F95BDB1B80ACAB16924C1,
	FB_set_AppId_m93206625F15482B14F7E0359B3B89B2993EEE51E,
	FB_get_ClientToken_m76F39D2E0FE9036E120A5D41AF4A3394F4B94F71,
	FB_set_ClientToken_mAB17D781E4499338DD5BFFFD20163DEF91898604,
	FB_get_GraphApiVersion_m280C689EBA58B227BA2D60E630657160D8BA3B32,
	FB_set_GraphApiVersion_mAD0958C6D6CF0BB088377C7981664AC8F177F12A,
	FB_get_IsLoggedIn_mC2EEE5953DA036DDB37F58879BFFCE918D749D11,
	FB_get_IsInitialized_m765E5BFFF09B410247464719ACAB53674F3BADB0,
	FB_get_LimitAppEventUsage_m1256735762FD834695F39690BEBEC151304BEDB3,
	FB_set_LimitAppEventUsage_m71A2FA7752E78B37FB583D34843E33B9C304669D,
	FB_get_FacebookImpl_m49419982C5ED53433D081C0B7675905CDA9DB235,
	FB_set_FacebookImpl_mE4B33601C3271186A89A1A68CAA7DA5F842D47A7,
	FB_get_FacebookDomain_m7BF4E9FC39B531DDBB89AB06DDA489462BAD0FF3,
	FB_set_FacebookDomain_m6F7C8D5E08D2373C8008D8BF4056D736D15FA156,
	FB_get_OnDLLLoadedDelegate_mD904DC4A7EA9F53D255DF8C00A62AA3485E0ABEB,
	FB_set_OnDLLLoadedDelegate_mC890CFBC76498E485F92908D0E402163F275138B,
	FB_Init_mDEC40011E21B7212F034FE1EA48522300F352C9A,
	FB_Init_m26187AA10513D5C820166C833B572E059C71D547,
	FB_LogInWithPublishPermissions_m2F46245F5A9896300B36E9E7EAA63E3EE4A6E08B,
	FB_LogInWithReadPermissions_m8EABF64EC2B27C55DE0AEC7F79F879A1C72FF05A,
	FB_LogOut_mA9AC9D27FB0613C856438F45BF0CF05D9BC6A9F4,
	FB_AppRequest_m57795DB1D9646F0A9F92F737FF2BD8575965843A,
	FB_AppRequest_m5DAD4A15DC945645C67F901EB5870C1E0B59405C,
	FB_AppRequest_mBC0F4037191A80EAD19F2D69C7DB32637F6BE3CE,
	FB_ShareLink_mC4C16325D47EBEF4CF643638CBC592F6BDB6F21B,
	FB_FeedShare_mB58C64045EB6548AD8C183FA5856D0F28457459A,
	FB_API_m287EAA82141532EB5682F51C8BF9962F7222D532,
	FB_API_m97B7C3D516619A78D0F2CE1674894AF413762861,
	FB_ActivateApp_m8D35D9FB52A258DA4710D26A6AE833EC362C4E39,
	FB_GetAppLink_m4966CBDEBF8FE50869677B45C37C3F6B74E13A8C,
	FB_ClearAppLink_m5FBDB1FB1C5C51B29165DD1BE63DFD2B8D9A46C7,
	FB_LogAppEvent_m13EED7F9998210E48D0245E1C8206C712250A80A,
	FB_LogPurchase_mEB04282BBA29CD970D4A3EC16767387482FA53E9,
	FB_LogPurchase_m0030D52FD8CE5D8BC0DC80F6EB0C82D3CA93F0A3,
	FB_LogVersion_mCED7DB7EBDA13FCB7AA123F0AEFA7FCBB1AE0585,
	FB__ctor_mC73C493766C9869765539936F102B38E4E21AABE,
	FB__cctor_mF7F24CC1836485E69DC5B800CEA307D297AF3FE7,
	OnDLLLoaded__ctor_m03DF552F8620334ED72E4544CDD2C94BE4CEA08D,
	OnDLLLoaded_Invoke_m54A99FCA613AA21E9A7B2193939952B287D4F525,
	OnDLLLoaded_BeginInvoke_m1F9C668AE67CEAB2F3B7BF29F5DC49BB07ED7A31,
	OnDLLLoaded_EndInvoke_mE16D77F58B5D6CCF2EADBE16FE8CEBF8EEEC98F7,
	Mobile_get_MobileFacebookImpl_mE7556F9B4FE66323609E654E3AF2BD3280A026CA,
	Mobile_IsImplicitPurchaseLoggingEnabled_mCF389902E05A7359FF248DB5129825C822885A7A,
	NULL,
	CompiledFacebookLoader_Start_m3D0EC7DCFC50D29795B927D903BE2B7EE3F92241,
	CompiledFacebookLoader__ctor_mE9514E11C196C6C4E26E4283C21E9E847B1D8A5E,
	U3CU3Ec__DisplayClass35_0__ctor_m07B31A58843315B3E4D0A2A3D3F9C8747C85F248,
	U3CU3Ec__DisplayClass35_0_U3CInitU3Eb__0_m65F785A4A710A3C12E914552F51AC33E4A795BEA,
	U3CU3Ec__DisplayClass35_0_U3CInitU3Eb__1_mDAD13E71D1B8572B8EC3427E0AF0437F394CA084,
	U3CU3Ec__DisplayClass35_0_U3CInitU3Eb__2_m767562A0458CE8E715DECE47431C3412EC0F1EE2,
	U3CU3Ec__DisplayClass35_0_U3CInitU3Eb__3_mBD9AE71C968B91DEBA11C6BCC501EFB444B624E3,
	U3CU3Ec__DisplayClass35_0_U3CInitU3Eb__4_m836E5F9F99D2E1A52F46DD6035BE0FFA4A851D4F,
	FacebookBase__ctor_mA8BC6D69FEF907902CB7F52126107ED9D40B9E02,
	NULL,
	NULL,
	NULL,
	NULL,
	FacebookBase_get_SDKUserAgent_mF2887AD5BDB7227689D8A075D0E9634345993A7E,
	FacebookBase_get_LoggedIn_mC91BBC2768BA3E97C004BF5A2DE129676C80BAD3,
	FacebookBase_get_Initialized_mF2C6C9D0BAE7E77FDE08C4DCA15AF12EB93B0DCC,
	FacebookBase_set_Initialized_mDAB99FF264AFD17E62CE2E0A47E4D35783D1F040,
	FacebookBase_get_CallbackManager_mBB4EC92D8211AC4CAAFCB3823B901F75E24E537C,
	FacebookBase_set_CallbackManager_m5A1C5A8B0F49D230AC1842183A2FE9B461F04FEA,
	FacebookBase_Init_mF0DEC65A32CEE3EA9CA1E42D1C5857FDD10090A9,
	NULL,
	NULL,
	FacebookBase_LogOut_m2B92288C9F62ADCF223794A0B6A5DFB8C3329598,
	NULL,
	NULL,
	NULL,
	FacebookBase_API_m8E99C456F7FE25B7E5EE99828F15CB54117C6B75,
	FacebookBase_API_m3066DA5FF9F21E0081A63983AE4F54A692A4F31C,
	NULL,
	NULL,
	NULL,
	NULL,
	FacebookBase_OnInitComplete_m55A6458A9046218C6DFDC79F596071CE3DECFA5E,
	NULL,
	FacebookBase_OnLogoutComplete_mF7A476CD7F61D94B664E334C180C23F4CE04BD7C,
	NULL,
	NULL,
	NULL,
	FacebookBase_ValidateAppRequestArgs_mAEE8F85CDDB31ECCA7D6B0C9696FCB20C7985936,
	FacebookBase_OnAuthResponse_mAB7E96567AC9DB0C460AE8D7A2157EAB02D5019B,
	FacebookBase_CopyByValue_m5DB6308CEB8094970A1A545C1E4300E353A24B15,
	FacebookBase_GetGraphUrl_mD37F29511EDB58929C37AE5960C2CC0234C48C08,
	FacebookBase_U3COnInitCompleteU3Eb__35_0_mD92C0EF2D2C5BABEC3C925F188B4AF38F4B35976,
	U3CU3Ec__cctor_m6506A0FBEF436C23C63EE7ECE70BCFF0533F9159,
	U3CU3Ec__ctor_mFC2B71BD2E5E4037163B00C41E28305573E5A824,
	U3CU3Ec_U3CValidateAppRequestArgsU3Eb__41_0_m013696598894053F2262D8AB06615020055E358F,
	InitDelegate__ctor_m638E3688B91CA78C3C97593EA85EDC43C9B651A1,
	InitDelegate_Invoke_m364F872A6E2F2D65DCC8D6157517F23E92DC41C0,
	InitDelegate_BeginInvoke_m95F07509701D7134A1D2C0DFF1A0150F01271B85,
	InitDelegate_EndInvoke_mFE8582F7EF719F7BDB0C624B90782107DF1CAF1A,
	NULL,
	NULL,
	NULL,
	NULL,
	HideUnityDelegate__ctor_m1A4604B0620E95856B9F24983A754E19CE068E07,
	HideUnityDelegate_Invoke_m1D7F4C7CAFE39644D2AC2CAFFF9115942E5675EF,
	HideUnityDelegate_BeginInvoke_mC7DFCE7615B532DF459D32D46DFA00ECB1B6C26B,
	HideUnityDelegate_EndInvoke_m60AE1EB74D0205443CCFF8BD6453CF2E8BCB83A4,
	FacebookGameObject_get_Facebook_mF99AFAC61EECA0A3CAA17972DCB16979F4BB9494,
	FacebookGameObject_set_Facebook_m077F56A9716B7C34D4B78C75D39A81CF28F5555A,
	FacebookGameObject_Awake_m1C9ED99A2A63E4C54503EF6174627580D9E3E6BD,
	FacebookGameObject_OnInitComplete_m74B99C1E3ED06A52991C82989AF47C458E144683,
	FacebookGameObject_OnLoginComplete_mE2B244ED30E79DAA91FCA0B3F9DFD75551A03C4E,
	FacebookGameObject_OnLogoutComplete_m0F63EBAE06A897EA87A674B8BBB3336CC1F74176,
	FacebookGameObject_OnGetAppLinkComplete_m207A0F329BF69A815D85CABDC3740180E32EC674,
	FacebookGameObject_OnAppRequestsComplete_mDCE0754CA9ACECE25F13235A00168E9C74000072,
	FacebookGameObject_OnShareLinkComplete_mEE622A0652888798F0B6E72817822B13053752AF,
	FacebookGameObject_OnAwake_mBB38840A8EC5FAEE0D3FFFD58600FDF19169FD11,
	FacebookGameObject__ctor_m3B901E098CBDEE9F4C898C1A1E34DF1BBFFF369E,
	FacebookSdkVersion_get_Build_mA37D5E8676B72FA63C562EA5EEAE3F293A3933E0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MethodArguments__ctor_m553F1F675A365BD7EAEA407618085B1E85B86CF5,
	MethodArguments__ctor_mCD0FEAC74634F6FD43B4F6B5EB127ED7024FAAAD,
	MethodArguments__ctor_m98DD81AB8781696CE01A4AA728CEFF3F8D91F5B1,
	NULL,
	NULL,
	MethodArguments_AddString_m0AC2B248277E3BB0CD32BB2378B742FA1AF4901C,
	MethodArguments_AddCommaSeparatedList_m2777FC9569E19FF94DF85DEEB717F85E392761BE,
	MethodArguments_AddDictionary_mBEF8B6346088AB15B3676AE7BC1BCE7385907D09,
	NULL,
	MethodArguments_AddUri_m194EB0E3882917D39C37D79D16B3D71637E83191,
	MethodArguments_ToJsonString_mB4678DBDEA648BEE531004BFA562EA532627A3DE,
	MethodArguments_ToStringDict_m96E5E453230DEF15EC3DFB9947B3428CF4C75861,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AccessTokenRefreshResult__ctor_m3B283FB973D96CCED9E2FCAF2DDAE5DCEE05C15E,
	AccessTokenRefreshResult_get_AccessToken_mCA5BA422E199DE494C98EF7F1E0123620FE561F0,
	AccessTokenRefreshResult_set_AccessToken_m78C498512639C4F8B59CC2EC474B80CEA6DF46E3,
	AccessTokenRefreshResult_ToString_m3F52CFC846F0311E4B5053021FAD90DD0C94D220,
	AppLinkResult__ctor_m9D10E105C205A8AB5E6FF4B5CCE7C390D76E8724,
	AppLinkResult_get_Url_m0183A6631B0333CA8D8683C7C5E7DB1EF8AAB022,
	AppLinkResult_set_Url_m5699010FA9132CD64D84A9858C3C09F1FAC2DBDD,
	AppLinkResult_get_TargetUrl_m5AF605EE11256D8B85920204C7A1C0C3B99673DB,
	AppLinkResult_set_TargetUrl_m843A466406F9A1D67329C444319462406BE8CE90,
	AppLinkResult_get_Ref_mA48E907F838181C82B1F6B4DAB73B1DF389660C2,
	AppLinkResult_set_Ref_m69406B2299AA226B6B22AAD9F09FC7DF0AC39F00,
	AppLinkResult_get_Extras_m4B7997A6BCF3C0906E58DE02E8FBE8F3344E86DD,
	AppLinkResult_set_Extras_m22CD249CC5B4611B7BC992B883D07B061906F44C,
	AppLinkResult_ToString_m0122EA6DBED254E210DE83E18566BD94EFDFF830,
	AppRequestResult__ctor_m9CFE5CCB55E06C2F89D5AE07EEFC86009148640F,
	AppRequestResult_get_RequestID_mF3C6D6F4EE63C4A96ADA75992FA224F7C8B56BAB,
	AppRequestResult_set_RequestID_m58DE037A9089372FE258074E9F7828236407E915,
	AppRequestResult_get_To_mDCF143439DAD87DDF7F7E99EE42FD4F95B5B6853,
	AppRequestResult_set_To_m360961FF55635DEBF06B4081412EFCCE5D787B20,
	AppRequestResult_ToString_m5E0FA54B3FE2EFD748AB3F744C5170D682B8019E,
	GraphResult__ctor_m1BC0212616C36CFC283E97A30BC0FE59D346EAA4,
	GraphResult_set_ResultList_m879FD2B5A926796F7DAC7AF0221DA2F64312FF00,
	GraphResult_set_Texture_mD9642743F541436777BA59E068787AE897114761,
	GraphResult_Init_m136A90BD553319F6B8BF4F24F6ED1355171C939F,
	NULL,
	NULL,
	NULL,
	LoginResult__ctor_mD1810851AA9A258021A6A3C6E807387FBDBADEBA,
	LoginResult_get_AccessToken_m3E683AB48B469DDFA1339E5F018F037F40309667,
	LoginResult_set_AccessToken_m1F404268E6BA26FA484508DA3AE3C027D257225C,
	LoginResult_ToString_m6F56A19B00621A5ED0C5EF233A05638A559F7817,
	LoginResult__cctor_m40FAC52926CEE52C50E56AA6CF57D8144D9C254E,
	PayResult__ctor_m12E078D8239CE20538DAA0B4CF4A36D2E16CA06C,
	PayResult_get_ErrorCode_mFAD4EF7DECBD43807E5399050FA5DF6D92032128,
	PayResult_ToString_mE30534F4D6FE1084AF9F982B5FE4EFD738AE88A8,
	ResultBase__ctor_m06339C42987D74E7508EFFF42E3B442F07ABADD9,
	ResultBase__ctor_mE54080B5FAC22E3A79E6183C86C0A24F2841A7C3,
	ResultBase_get_Error_m9B2CB507FC8710071BBC98D3BC4BC2CE955A5EBF,
	ResultBase_set_Error_mA79A34249FB05045FEC1D001F6079859947ECB03,
	ResultBase_get_ResultDictionary_mD7A894868B8B6AAD534006F2C7AF6125889BD53F,
	ResultBase_set_ResultDictionary_m8C7AFB9266DD67FC9A63EFF676D735DABF0F8C1C,
	ResultBase_get_RawResult_m560FEC7F374443E9FC6A13A8AA0BC050647390E3,
	ResultBase_set_RawResult_m1DDC204ED4D768556114B4728BA7F0A0317F34ED,
	ResultBase_get_Cancelled_m3746E4DD75E9920347C86D65A056CFC44057855B,
	ResultBase_set_Cancelled_mD8A619C2E4B109483811DEB68FA4739557419E4E,
	ResultBase_get_CallbackId_m55ED0D34F6F77059BEF560D61BE9D9C8AFDE57DA,
	ResultBase_set_CallbackId_mEB6E0E719A57BB0567E926A3D7BEB9AF95C1C07F,
	ResultBase_get_CanvasErrorCode_m6514A860C12D8B3D347C3902AA76644088BE3C02,
	ResultBase_set_CanvasErrorCode_m83213A57E092845E9D6F91D85460555512783BA0,
	ResultBase_ToString_m1726DD872BF67CC1AA3921F30529994F55B42D74,
	ResultBase_Init_m8FA9A3F5AE4BEC2F679A719459DD515665FBC983,
	ResultBase_GetErrorValue_m12D7A740EA1A542A9BEC1068616AB68C4F39CC4A,
	ResultBase_GetCancelledValue_m50832E83E3A02AD0F57446CE33165E2118E71C8F,
	ResultBase_GetCallbackId_m7A14088D5D4FC2D1A7B0BE1FAB02CC7368C1FA88,
	ResultContainer__ctor_m27DF61D20D816EB82666C07D75308075489CFCB2,
	ResultContainer__ctor_m59976A61C4C6205111114A6B134063594AE544D2,
	ResultContainer_get_RawResult_m9C15E9CF5169B25AA93B499777B53D0B2F2FCB05,
	ResultContainer_set_RawResult_m4BFB96584896E72A00636176455D01FCF8B8C230,
	ResultContainer_get_ResultDictionary_m5431E7FD6F5EF5735BB539E77604B9A98F4FFE1C,
	ResultContainer_set_ResultDictionary_mFE3745393910DA1BD6C052EBCCED08560E453DC2,
	ResultContainer_GetWebFormattedResponseDictionary_m9DB804A24B903B7BCA56B9E3F0B649C56206407F,
	ShareResult__ctor_m35188E40DE537543889C68BE1D485FD6309E117D,
	ShareResult_get_PostId_mCEFB90DD2DEA504897A0866C1D2DA63067385F7F,
	ShareResult_set_PostId_m12828954E2DD74323354A545EB9B1E1A2C30AF5D,
	ShareResult_get_PostIDKey_m49656A1ACD481B53CE19C5242952FBA03CC32FC0,
	ShareResult_ToString_mE93DCD9E5154263B22D06AE593E971721D9B36CB,
	AsyncRequestString_Post_mF798AD6971F004AB7F8E55CF5CC0903AEB6D99BC,
	AsyncRequestString_Get_m2550311682EEB20667A07119F7E31378C0F6EB9B,
	AsyncRequestString_Request_m7F8CB77DF62263184608F2653722D97969C2D675,
	AsyncRequestString_Request_m1ADE5253A3BDF38A3905F38C8E064BB1EE36A7F0,
	AsyncRequestString_Start_m0FB3015D0D982AD6DE0F14C3CAB1BE136F7DC3C4,
	AsyncRequestString_SetUrl_mB4D220EDB7E14460387CFCF39603D21C6FC3EFCF,
	AsyncRequestString_SetMethod_mC0F03004C2DAA50622C4518051F0E9B016F27EB4,
	AsyncRequestString_SetFormData_m6A2521AAE2FDA6F7B432E9A39629C61632FE7CB2,
	AsyncRequestString_SetQuery_m13638A1A6C284DB8416B5878E8A4577502457E19,
	AsyncRequestString_SetCallback_m02C78EF786B9D7490280F736DF11E1CE9A95AA68,
	AsyncRequestString__ctor_m08C5358236DFB5E7352A9DB508C9C466C05C62FB,
	U3CStartU3Ed__9__ctor_m6BB670973C33592061C21CD55A60A27A0432C79E,
	U3CStartU3Ed__9_System_IDisposable_Dispose_m42C35A13588970E8EF9DCD40BE5DA7DD9F7E1F4A,
	U3CStartU3Ed__9_MoveNext_m8C9082F8D94D45B54FA8174DBF8094B2915DD9B8,
	U3CStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m234D8FEEEF6E863D283B7766E08AD29240F49248,
	U3CStartU3Ed__9_System_Collections_IEnumerator_Reset_m15BB00FD306990BD4C304549DA963BCDF1135493,
	U3CStartU3Ed__9_System_Collections_IEnumerator_get_Current_mF369F51D1E867C8BEDA92CE3E9DE08D5AE6EED41,
	FacebookLogger__cctor_m877F937C6B306C9595D44A0C25551BFC7AB02518,
	FacebookLogger_get_Instance_mD6A0D838FA1AC842407AD40C5B52E61C1E459B1A,
	FacebookLogger_set_Instance_m51C6929F7DA379E17A98B96B813950B906C2C5EA,
	FacebookLogger_Log_m5719F401DCC106DD29F235E3EB4330F2182EAF95,
	FacebookLogger_Info_m22301791BEEA8EA1A7DFA5BB7A36644E1A95FFB2,
	FacebookLogger_Warn_m7F07108ADB4A2E6352CC636033D8498EEBB13755,
	FacebookLogger_Warn_mA4121A837E17FC0EA423C0B6402784A5EC701493,
	DebugLogger__ctor_m396258C2A00DC5153FA35BA4A9CC9084A3FBA0E1,
	DebugLogger_Log_m4EA1B55897F163431606119D880CF49E3454DE6E,
	DebugLogger_Info_mD39CEAB8ED3FA26721C8E7A8D8E681BE7E346D29,
	DebugLogger_Warn_m901260E8BC4778245A980345DCD3E8FEF1781595,
	NULL,
	NULL,
	NULL,
	NULL,
	Utilities_TotalSeconds_mCDD723DDC296E2969B8A3DEFEACE952C8D8F8DB9,
	NULL,
	Utilities_ToCommaSeparateList_m1885DA634E5650319164F3847B31935E787ADE59,
	Utilities_AbsoluteUrlOrEmptyString_mC38E9CE9B4F420CA43E70C53ED6A7842EF0357C4,
	Utilities_GetUserAgent_mEA4D736A7DB8D7E2BCC5A31094E7B340547CD90B,
	Utilities_ToJson_m372CA72DC95E3AB103C3DF1CCD168BD7BA851E71,
	Utilities_ParseAccessTokenFromResult_mA68036BB24FA4DDD5703167E3F0E185CC1535C77,
	Utilities_ToStringNullOk_m9DE8F796FB5A8077E532297D100A2074A97CFCBB,
	Utilities_FormatToString_mCB8E2BBFCA716FD8E465FF909B8E7F9193DB154C,
	Utilities_ParseExpirationDateFromResult_m4EEC3DBBCCB46F7EC703DEB30F9A0A227205ADFE,
	Utilities_ParseLastRefreshFromResult_m2E446B8A141DB4317C36E0D8F30B3341A7961DEF,
	Utilities_ParsePermissionFromResult_m95293DC5F3403DCB439AEE86C06D555881061E5A,
	Utilities_FromTimestamp_m80830EE87D0A4DB5E0AE3E00CC2EBE16F5F57276,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_m2BBA4F68877C79C40F70DFD41FC21C19BF9EFE82,
	U3CU3Ec__ctor_m2A69079DA8CB9E378C1E82C96843922858057634,
	U3CU3Ec_U3CParsePermissionFromResultU3Eb__18_0_mAEE86ED0231FAF6C425351B5644B8FEF4FC32C6C,
	FBUnityUtility_get_AsyncRequestStringWrapper_m0C1C83A5691F39AF0BB18374A1D3C8951DA36FB1,
	AsyncRequestStringWrapper_Request_m8CBBF2379D8FE5E0125C83972F4A6D42976BBEEB,
	AsyncRequestStringWrapper_Request_m06F3163D8E24671D6A5DAFABFE0CD08A481B8D7A,
	AsyncRequestStringWrapper__ctor_m91A9C46521BC925FA57D6250C0F3E72D0192B982,
	NULL,
	NULL,
	FacebookScheduler_Schedule_mBDDB19927805460E07C70C709C6AE9B626441473,
	FacebookScheduler_DelayEvent_m5DFC95422E9FAFA95B30A4D0CFE6AEB1699D9420,
	FacebookScheduler__ctor_mCB064B5DAE532108037117E17E12A7D5E8B84E6B,
	U3CDelayEventU3Ed__1__ctor_m3BDF150EA0C8450F46C4F9A9BE52368C0DD05275,
	U3CDelayEventU3Ed__1_System_IDisposable_Dispose_m4BFCD4A4E9EA045E39CC19CD61C94ED55790F429,
	U3CDelayEventU3Ed__1_MoveNext_m0892580F8D079DD35AA81C65EF4896913C4A531B,
	U3CDelayEventU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m067FB91ADFF63904D451D96ECDE70F54EDCDAE8F,
	U3CDelayEventU3Ed__1_System_Collections_IEnumerator_Reset_m39FBF76EE1E8D7DDE3431F38AF80527713764F28,
	U3CDelayEventU3Ed__1_System_Collections_IEnumerator_get_Current_m9E2EF3525E3D4F10DD9B5354623113B37FFC3759,
	CodelessIAPAutoLog_handlePurchaseCompleted_mC02DFC40960C519217BDC1464EEB25857DD9143D,
	CodelessIAPAutoLog_addListenerToIAPButtons_mCCBFD3BA9544F3F1F3C2CA42541D7E33ED63AD7C,
	CodelessIAPAutoLog_addListenerToGameObject_m4F71BEFC1B80FB8A574D964FFAF9743E1D718F0C,
	CodelessIAPAutoLog_FindTypeInAssemblies_m6DA5CCA1B5FE34C2FE23AC7717D56C7144480237,
	CodelessIAPAutoLog_FindObjectsOfTypeByName_m32A93315F67F718E14286BB8728D4386FEC65E2E,
	CodelessIAPAutoLog_GetField_mAD794FC197434958098A15886D80CAABC501BA0D,
	CodelessIAPAutoLog_GetProperty_m01B1790C06C37A6DDE28B701A3796A5B10487A90,
	CodelessCrawler_Awake_m122935662B32601495D279C99F66CF0F496C254B,
	CodelessCrawler_CaptureViewHierarchy_m95B9785D9E65A1B5FDB75DDCBB686C0AAB608179,
	CodelessCrawler_GenSnapshot_m60CAE989176661A35F8E6CB3F01778EF29F471ED,
	CodelessCrawler_SendAndroid_mD53041A08AC0EDEEDE11BE528D219DFB5E0F76FD,
	CodelessCrawler_SendIos_m1C0553A15F2D9BC15B4779970B979960FF5D09EE,
	CodelessCrawler_GenBase64Screenshot_m23C9B87522FEA309D5C249383FEE9AC76830D7BD,
	CodelessCrawler_GenViewJson_mFC3DB02D3A304843E1CDD1BBC6B6AA52FE9D6411,
	CodelessCrawler_GenChild_m2027B31C3EFF862D152211F4C9812BD5634E7713,
	CodelessCrawler_onActiveSceneChanged_m003D2F66E130FCCC15520965E564629350BCFD59,
	CodelessCrawler_updateMainCamera_mC012344A45CCAF6C31D704EB660788E9C09689A6,
	CodelessCrawler_getScreenCoordinate_mA06133DEF7068578B226D0101A5F822BBC5CF52A,
	CodelessCrawler_getClasstypeBitmaskButton_m5B63C0EE98A0DE603B05DD093C10C50A427589D7,
	CodelessCrawler_getVisibility_m4609F2D89D1C82F2FF63F80263B0440B45D3BDC9,
	CodelessCrawler__ctor_m48EC1A12B70ACD64C4DA1D63FE06EF83A566ECF6,
	CodelessCrawler__cctor_mC144265EDABE72A6FEBF9307EE48742C541DCA80,
	U3CGenSnapshotU3Ed__4__ctor_m2CC855A63A9F4C19FB0E4B1DF371AD29038E9537,
	U3CGenSnapshotU3Ed__4_System_IDisposable_Dispose_m7B48F1C45F5D63CE54DBDAA0A88B3B23BFF27D7A,
	U3CGenSnapshotU3Ed__4_MoveNext_mC8BA872F2A8A0DE01D4F1F089395ECABF732975A,
	U3CGenSnapshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m95B393232911560E3725C741205D4A7C3EDAB827,
	U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_Reset_mFE9720AF54CA96F0E9A2D7F576C2AA77ED137F42,
	U3CGenSnapshotU3Ed__4_System_Collections_IEnumerator_get_Current_m24AD9DEDCE5DE8396C27B872338086FF0238F983,
	CodelessUIInteractEvent_get_eventBindingManager_m381B353313ABBC497D95C4B1763058C9BF37122D,
	CodelessUIInteractEvent_set_eventBindingManager_m0B0076AF2135B0CF8F358F706A032DBC5CBF2590,
	CodelessUIInteractEvent_Awake_m9FDD437EB4C25C5469E53E58888ACC34526EE44A,
	CodelessUIInteractEvent_SetLoggerInitAndroid_mAD0ED4ED266F04CBFC94403902B558041A535ADB,
	CodelessUIInteractEvent_SetLoggerInitIos_m7CC5CFCF4CE8C7FA0BA2112A46F10F3CB833A94D,
	CodelessUIInteractEvent_Update_m10E5AFCF66162E40B4879E70FB0C75F7832E1A56,
	CodelessUIInteractEvent_OnReceiveMapping_m769B55CFC5E7BEE4A6FAA8892A245B944AD3A61E,
	CodelessUIInteractEvent__ctor_mC9C6BE903A313BAFB69E72D2A7E2DCCA6966B5D5,
	FBSDKViewHiearchy_CheckGameObjectMatchPath_mE6757F0129E99E26E25809240D2FD0C807194584,
	FBSDKViewHiearchy_CheckPathMatchPath_mBEB1F5E129FC8A1C08EDF4E51FDEFDC59B31835E,
	FBSDKViewHiearchy_GetPath_m3E31C3FFA973A5F3C5FC5CCD1DC5D88D6CC29A11,
	FBSDKViewHiearchy_GetPath_m6A6D2C49106C366F2CAE90880AE69462DBEB4E63,
	FBSDKViewHiearchy_GetParent_m5AFE735223FCC31E191BD5B3559AA02F3AE538D3,
	FBSDKViewHiearchy_GetAttribute_m74488C8C0E105C5DB2AC7B92FA30FB492A6C8064,
	FBSDKCodelessPathComponent__ctor_m6DFE3D5A9C27B449F739C6E2BFEEC243EA39ECDA,
	FBSDKCodelessPathComponent_get_className_m09BAED691F4C67F2252A48C83ED46590574514B9,
	FBSDKCodelessPathComponent_set_className_m7ED0A9732DD6B7CD58B5E5D531B11D2B146932FE,
	FBSDKCodelessPathComponent_set_text_m05688A2469F92FD15712EC554CDC72DF9B1CF116,
	FBSDKCodelessPathComponent_set_hint_m0F2A558021A0D66CE7481E1D60F5FC11766DD129,
	FBSDKCodelessPathComponent_set_desc_m41E454C69A5C31745AA2F9A475706C3F061FAC03,
	FBSDKCodelessPathComponent_set_tag_m931F672CD6D7F0AB7ECBA05CC4B9BB9A38E4214D,
	FBSDKCodelessPathComponent_set_index_mB5360131D0A771A3E6C4AAD2B12BB92E120C1E3A,
	FBSDKCodelessPathComponent_set_section_m527F5057AF490050A4FCD54CE716F443740F1B5E,
	FBSDKCodelessPathComponent_set_row_mE4A25B8D2C34160991BBA80C77275D5C6E39187B,
	FBSDKCodelessPathComponent_set_matchBitmask_m9BF6CE6F617EE14E445E61267FC157436B52471B,
	FBSDKEventBinding__ctor_mC34E7CFB9B20C143E707FE05A80094E6172E315B,
	FBSDKEventBinding_get_eventName_mE83C43E0925079076AF0E20C32DEB2F61581DA7E,
	FBSDKEventBinding_set_eventName_mFCCEAAAF52E285636D5F13E63DF2FC117338BA34,
	FBSDKEventBinding_get_eventType_m6264DB8ADA66A6B03A1E1CF4CE8A1BF53663A31E,
	FBSDKEventBinding_set_eventType_m31934EC0D2116F9B3AB926052A15AB410161A4DC,
	FBSDKEventBinding_get_appVersion_m8A11D02FE83546BF3B3FD10E03FFDA2E55027FFD,
	FBSDKEventBinding_set_appVersion_m08B508F110FD8B82342A4B3463BFF1B59BF04FB7,
	FBSDKEventBinding_get_path_m1DE2BA2A5F20CC87F0030AD112C7713E0E8CC56C,
	FBSDKEventBinding_set_path_m31B6964D84810597EF0086F6DFFDC25AF4AD1256,
	FBSDKEventBindingManager_get_eventBindings_m31BB6F5AC87C718BB94C45E287DF441349E4777C,
	FBSDKEventBindingManager_set_eventBindings_m40CFAC43E709F14BBB98F32D71B6C766D919ECF2,
	FBSDKEventBindingManager__ctor_m28E10077C286A5C45198A9DA47F0E62F1BE9A475,
	GameroomFacebook__ctor_mFC65BE8624D6B0750CAA3F5E2DC427AC812382B6,
	GameroomFacebook__ctor_mEE40C6C90C3EB3BD02904633DA6178C9971E14E1,
	GameroomFacebook_get_LimitEventUsage_mDAAD73F475D00D918E228AAE134F8C62E265D924,
	GameroomFacebook_set_LimitEventUsage_m88E828FFC76A481276FF5FAA59E0F9FDF1789C6B,
	GameroomFacebook_get_SDKName_m2000B1EB1CB0025DCA5ACD189784C487CDBD8036,
	GameroomFacebook_get_SDKVersion_m01EF49380A314A877C3A8A4FC184C7516EC0284C,
	GameroomFacebook_Init_m40FBB6E36B6BBA0680C00B52D451D366391F2910,
	GameroomFacebook_ActivateApp_m6125853EC5963BA1E10702414084ACBBE42824EC,
	GameroomFacebook_AppEventsLogEvent_m5D15BD0E27FBFD9F1F8D5B8A6827B6855E5AF508,
	GameroomFacebook_AppEventsLogPurchase_mDACD10E24CFA39A4C8680C290C70B5A54439F803,
	GameroomFacebook_AppRequest_mBBFAC58D7B58DA3B408B609D392D646916AAD0F4,
	GameroomFacebook_FeedShare_mF458A342C6108A52614BF074E01487227E967A86,
	GameroomFacebook_ShareLink_m7D5AD82190FB28411A073C701B864CB67B38B123,
	GameroomFacebook_GetAppLink_m65224B9E284B56569AB4535EB8EB6A428EE015A0,
	GameroomFacebook_LogInWithPublishPermissions_mFF6B98820EA1FA0FE3696F2030FC94B033657057,
	GameroomFacebook_LogInWithReadPermissions_mF9A0B6ED9BFD69485A8393148E1EB8006DEE9A3D,
	GameroomFacebook_OnAppRequestsComplete_mBAB89FBC32D5D7CBAF25077EA067BF9751E3654C,
	GameroomFacebook_OnGetAppLinkComplete_m977F242EE6115AB93E49499BEEF52A7011C41E39,
	GameroomFacebook_OnLoginComplete_m20D6B69BE5A51C84C5DD4888A2F194028D40FDD8,
	GameroomFacebook_OnShareLinkComplete_mFAB4F4323BBAF7AF7764DDEB48ED5C28AE10F750,
	GameroomFacebook_HaveReceivedPipeResponse_m6272B63D0E24C4C857056290B7DC8A0CAE225A86,
	GameroomFacebook_GetPipeResponse_mADC4101C9B0263C5D55FF2DCD12CE41FE99957F6,
	GameroomFacebook_GetGameroomWrapper_m5C91145F425EE1CCC06AC242C558B482ADC38C3D,
	GameroomFacebook_LoginWithPermissions_mDADBE008020C0518FEFA172307E7B0D7D9F8868D,
	OnComplete__ctor_m344BDA66D339789102DA523A32B2F28FAB5F3C87,
	OnComplete_Invoke_mE62C5A8F4A615C47A92906C689DD40E7D750D132,
	OnComplete_BeginInvoke_m97D6900EB0000E7714DA3899DD01EE3F2C890254,
	OnComplete_EndInvoke_mCE9D3218C50E35392510668032BE8A2661A9ABAF,
	GameroomFacebookGameObject_get_GameroomFacebookImpl_mC09192402584AD9D54A7B98F78B5BD0E7B45AFDA,
	GameroomFacebookGameObject_WaitForResponse_m96714BF1B7DC9A1B710C34D16518251869AD8698,
	GameroomFacebookGameObject_OnAwake_m93AC653304B54D591E4D7790159DE746790B102E,
	GameroomFacebookGameObject_WaitForPipeResponse_m48FAB97EFF06775119721589B3E7C6C029110C78,
	GameroomFacebookGameObject__ctor_m6041717A48BB70B7CC05488AC390DA46D0A457D1,
	U3CWaitForPipeResponseU3Ed__4__ctor_m1605B666386B4CBC70B5E982D78E4F1D741F4684,
	U3CWaitForPipeResponseU3Ed__4_System_IDisposable_Dispose_mD084FB4715BC24D2FFC3E2B6422779DB8EAE6539,
	U3CWaitForPipeResponseU3Ed__4_MoveNext_m174EB7DF36A038C4B31468EBA4CF6747A95737CD,
	U3CWaitForPipeResponseU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m84E38B840147F3A5917B180C6D749019688E40C5,
	U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_Reset_mD3093021205012F8C2816C904A6A683428C4678A,
	U3CWaitForPipeResponseU3Ed__4_System_Collections_IEnumerator_get_Current_m6D39801691E6DF32B424F34BCE020EDFE70A4FBE,
	GameroomFacebookLoader_get_FBGameObject_m0A331676A70B9C35D4DC38BA784F2D52189604C6,
	GameroomFacebookLoader__ctor_mE1885E6A2C11E0DD420314A54E85AFA39DF16892,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EditorFacebook__ctor_mAE29F2FC6C569787A51DC7A1A24301E48B4CA11D,
	EditorFacebook__ctor_m34DA93E753FE13830FF1BF8A3864DA2BC3FF14E5,
	EditorFacebook_get_LimitEventUsage_m7C137601DBCF3B1FD94D45F2B9AF08A6E0727D8E,
	EditorFacebook_set_LimitEventUsage_mC0B538ED6A81A566361C58188834271F641F12CF,
	EditorFacebook_get_SDKName_mC5FCF0CF29A69878A349C2645646316F52E1F687,
	EditorFacebook_get_SDKVersion_mEC6160E21094F1FFA07901E3EA929D13187643CA,
	EditorFacebook_get_EditorGameObject_m547C05A8380AB2DC1FFF45BB9DB8C8ABB19C533F,
	EditorFacebook_Init_mB3B16ADAA349C3EB97AA931D772F615EBECD8E9B,
	EditorFacebook_LogInWithReadPermissions_m02BFB49D953CCC7731CF381D264466FFA9A7475F,
	EditorFacebook_LogInWithPublishPermissions_m1BD9E2D308E461F04D0D4B4EBD80CA2913D06156,
	EditorFacebook_AppRequest_m3B7A7EB6E4DD6BFAE79EC07C549B6AB6B9DFF797,
	EditorFacebook_ShareLink_m6346DBA3291547E2F4AA576948AB95B6D8DC9527,
	EditorFacebook_FeedShare_mE3E722E30DF4D47C356277E3D53D5BFE03732937,
	EditorFacebook_ActivateApp_m19AA9F4F23BDF8AC9504DD542696D6CA4FF9B17E,
	EditorFacebook_GetAppLink_m377FCED9D8563E9479564BB8841DDFD053813975,
	EditorFacebook_AppEventsLogEvent_m9F6D148CD8A8D9ABB9257E8D3A978C060D41311A,
	EditorFacebook_AppEventsLogPurchase_mCD93D37B1927FE7837CA7DF5A3B6ECA5DD6430D0,
	EditorFacebook_IsImplicitPurchaseLoggingEnabled_m8FC6C27E0E097C8D4D72F9DDA1D89B46E2F5FF28,
	EditorFacebook_OnAppRequestsComplete_mA9D6B389B5DDC8D3EFD19AF1F6B17173439AFFDD,
	EditorFacebook_OnGetAppLinkComplete_m31BD0C06EACDB6F1FA81842D9333F946A7268F38,
	EditorFacebook_OnLoginComplete_mF081184A345ADA1C23B758B6F079094F39D81A07,
	EditorFacebook_OnShareLinkComplete_mBFB7CD8CD1A2DCE5845889DE5A53A47AC7E2BFC6,
	EditorFacebook_OnFetchDeferredAppLinkComplete_mDFF5249FE04A39E68BF561AA47CD39AD42981B45,
	EditorFacebook_OnPayComplete_m8EE654252C30BC419540468CECAB5A1B9E569047,
	EditorFacebook_OnRefreshCurrentAccessTokenComplete_m0AFEB6BD6FAF69712259C1BF9D340A4DC5AA8577,
	EditorFacebook_OnFacebookAuthResponseChange_m19BE0072E690BD94F8A407A51223C81B4F5C6F9D,
	EditorFacebook_OnUrlResponse_m28080FB6889839BA52986A586EB2B19D00CF1DD5,
	EditorFacebook_OnHideUnity_mAB68B5D8AF184A66BC97B7E6A6986123BC316310,
	EditorFacebookGameObject_OnAwake_m955DA845F99EC24FC27FA5ABD61C61D48FDFB1E7,
	EditorFacebookGameObject_OnEnable_m48977F4BE04A6DF01BC0182BD7664864CA9BF803,
	EditorFacebookGameObject_OnSceneLoaded_m0564228080BD19F1595F66D01A5E631A3E968111,
	EditorFacebookGameObject_OnDisable_m2DB7257DEB4CDDA5B4B7C070D66F878478224CF7,
	EditorFacebookGameObject_onPurchaseCompleteHandler_m4CA8DECE7050444E8F5B67E0A4255FF9EAB37C00,
	EditorFacebookGameObject__ctor_mD14AF9C7AA2CF9AD8B308C30C3BEC6746024CA1D,
	EditorFacebookLoader_get_FBGameObject_m11D5C150A4B054CE1BF7FBEB39BD0E6E663F1D8E,
	EditorFacebookLoader__ctor_mE7BE35D26E2C2D1367758251BC07A6A0103606B7,
	EditorFacebookMockDialog_get_Callback_m5F4B98ED90D34716B22536E745E9340469F82600,
	EditorFacebookMockDialog_set_Callback_m24C61C31263945D8A3DD487438994847CCFBF434,
	EditorFacebookMockDialog_get_CallbackID_mE9740A50CEF913E55EC2EF3F939AF1EB4E9EB0AF,
	EditorFacebookMockDialog_set_CallbackID_mEFFFD7E9D658A2B588124D5A5A6EAAD27797F273,
	NULL,
	EditorFacebookMockDialog_Start_mA62EFE923AB60A964CC23C95C5E370CCB4BC5F71,
	EditorFacebookMockDialog_OnGUI_m27C4036EACBBDFCB8BCE8EBE09D69CD994436DED,
	NULL,
	NULL,
	EditorFacebookMockDialog_SendCancelResult_m0603629A2E8E4E9EB6501E4C8EF691466F659101,
	EditorFacebookMockDialog_SendErrorResult_m2C6EEBBB935316BC474361C85903BD993910ADF0,
	EditorFacebookMockDialog_OnGUIDialog_mB24DF172D4DC26E9285FF4BDD51F9374C4CCCE0A,
	EditorFacebookMockDialog__ctor_m5C3DB7B6A022EDE2D5001595E3CACE5AF0924483,
	EditorWrapper__ctor_m8409ED19BAEEC02D62A096B746D1525FD332E64D,
	EditorWrapper_Init_mE8DE143565B89BCB21364508F35D2FAE0C9C02A0,
	EditorWrapper_ShowLoginMockDialog_m32480619D2B0837381391938C48B424693EB6517,
	EditorWrapper_ShowAppRequestMockDialog_m4BEE47AFB38D0A882B6EC6171A1A66CA66ED146F,
	EditorWrapper_ShowMockShareDialog_m8448595A36B5BBA63C4ADBB63626FAF20FE5A039,
	EditorWrapper_ShowEmptyMockDialog_m210AFDEB0405C184AE554D94160D898371063FF6,
	NULL,
	NULL,
	NULL,
	NULL,
	EmptyMockDialog_get_EmptyDialogTitle_mEF83B654A4D364EC3F6BA150FF4AE4396BBFB01A,
	EmptyMockDialog_set_EmptyDialogTitle_m0308542A4C36DB94E9313EAEDEADA666D075E2CD,
	EmptyMockDialog_get_DialogTitle_m1D071DB8838AA2CEE9158265D816BF9E399B783F,
	EmptyMockDialog_DoGui_m0CE7A00EDF88A4E7D63DA324DBA250393059CC59,
	EmptyMockDialog_SendSuccessResult_m06E66BE4777F7C9F90A5AE18C32E17D7715A55C4,
	EmptyMockDialog__ctor_m7EC201CB86C63676E454A21034FF90174FE6BDCD,
	MockLoginDialog_get_DialogTitle_mB30DD0A9469E423A36F5394A90D05E7DD95AB767,
	MockLoginDialog_DoGui_m4E9E6113B794839E40B5059DED1A893419584B6F,
	MockLoginDialog_SendSuccessResult_m31A257F367DAD01B1D07C859BAD4E0DA88E2B456,
	MockLoginDialog__ctor_m499168C6D3E1398D1710712C9DCE845D20BB707E,
	MockLoginDialog_U3CSendSuccessResultU3Eb__4_0_m0BE5F47C2AB2EDB47AA4D1489BC2C20F63AF1E38,
	U3CU3Ec__DisplayClass4_0__ctor_m29935D6F81F1BB5747178C13DC3E55CC83035D6F,
	U3CU3Ec__DisplayClass4_0_U3CSendSuccessResultU3Eb__1_mE1F0AA70E5A7FBCBB206B98BFA996771B39665FF,
	MockShareDialog_get_SubTitle_m005AF11050A428A4F19F3E714293731EB5519D2C,
	MockShareDialog_set_SubTitle_mFD33AAAA0628763A9CC751317ED89B5B56140BFD,
	MockShareDialog_get_DialogTitle_m01C01BF38BBCED8F5980E79640DE2E37D7CE529C,
	MockShareDialog_DoGui_mBC8E23A0F094F31A5FDF6FC33CB1674500F715AC,
	MockShareDialog_SendSuccessResult_m21F5E6F1A1183C71A3E39A5C4F0CEC8A752A3B38,
	MockShareDialog_SendCancelResult_m67C9FDDEBA6FF56EC9746426FAE9E1437056B63E,
	MockShareDialog_GenerateFakePostID_m89B0CB66118ABFC13B2E1A7FCB6EACDF75AF4AAE,
	MockShareDialog__ctor_mC460306812D8853D20C89A67286BD6D17D4DAF9A,
	NULL,
	NULL,
	NULL,
	MobileFacebook__ctor_m7F2F46E7E7B30A5311D42904C5917481C3F295B7,
	NULL,
	MobileFacebook_OnLoginComplete_m6BE5BC218821E482C68BB79753EDBC0AF9566267,
	MobileFacebook_OnGetAppLinkComplete_m8D67E1FAD6FCC094A12DB261784B1428E21CD39D,
	MobileFacebook_OnAppRequestsComplete_m064D4CB5DBFF9441814E14AA14C075A310E1BFD3,
	MobileFacebook_OnFetchDeferredAppLinkComplete_mAAB8ABED8B610B953D6B815774B45170787D35F2,
	MobileFacebook_OnShareLinkComplete_m6AE100F74500A6FE0E523EF43CE1F7376E6F09A8,
	MobileFacebook_OnRefreshCurrentAccessTokenComplete_m5F9C745D510A9DECABFD3928CFDAB3795C5813BD,
	MobileFacebookGameObject_get_MobileFacebook_m955341BBEADEB1CBB64DE4F9E9B615181DEDD26A,
	MobileFacebookGameObject_OnFetchDeferredAppLinkComplete_mE5C94C955EFED281B6F72FC6F5A89D53D4BA8C1E,
	MobileFacebookGameObject_OnRefreshCurrentAccessTokenComplete_m0D87DA3A46598BDFF9FBBB30BBC8CB20A377B6FB,
	MobileFacebookGameObject__ctor_m5BD724C5A630B3A1915B2467A1F11AA9DED83B1C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IOSFacebook__ctor_mE9092C7C32FC81E39F37E0B812BBD99A976BA2D0,
	IOSFacebook__ctor_m2C5F57FB7EBD31E69E9E8E485D5F1C06BDA4271E,
	IOSFacebook_get_LimitEventUsage_m08C6C33813B53C9BFCFDD31462832DC2DFF0D7E5,
	IOSFacebook_set_LimitEventUsage_m69406DE87559377646635FE111FDFE64ED4BF880,
	IOSFacebook_get_SDKName_m2CE9B620F09FEC908575F2F98CBF3F5127E8491B,
	IOSFacebook_get_SDKVersion_mBAD3752FD4AB92410F8745E4E6D627EEE036DE79,
	IOSFacebook_Init_mC339495FB634964DD397A2E408A20980C2B5832D,
	IOSFacebook_LogInWithReadPermissions_mC7F47768CA6C859F288FABF11B235D7CC177B5B3,
	IOSFacebook_LogInWithPublishPermissions_m3B65EA6651EB3212E66687E348FAEB3CD5F054C0,
	IOSFacebook_LogOut_m66F72FA5259F4B7F566EB9FA9B541053E4FB586C,
	IOSFacebook_AppRequest_mB7916D5679064B88427D185648D4CEFC1A2372A2,
	IOSFacebook_ShareLink_mB91D22E0A4F07801F62DE90D4F938C22D51F7083,
	IOSFacebook_FeedShare_m332EF7C19B5BB52DAC61DC7785079FE865969EEE,
	IOSFacebook_AppEventsLogEvent_mC659F54A53EC415823D80CD204F7126B2655FBE0,
	IOSFacebook_AppEventsLogPurchase_mF5286D681D71472253CE6FB2E7A5ADFDCCFC17F5,
	IOSFacebook_IsImplicitPurchaseLoggingEnabled_mE7F50E80E21D062F6753000D842534547A21B17F,
	IOSFacebook_ActivateApp_m5E0923429FFA69CBF4A9C313FA3C829CBC33F242,
	IOSFacebook_GetAppLink_m3191D32F37EB7F449E52155DE157534B7712314A,
	IOSFacebook_GetIOSWrapper_m20BB4E4D64EF6ECFEAA6067B8291FAC0247E62CE,
	IOSFacebook_MarshallDict_m9641A0BA15C953707463D0228008E16769DB295C,
	NULL,
	NativeDict__ctor_m0F9530F5B90EE9B00B926B27270B7C82F8270AC8,
	NativeDict_get_NumEntries_mAE4481858E3A75AFCB55344B3955BB261ED582AB,
	NativeDict_set_NumEntries_m2B01015F005F402BF73B351D223A1775BBA10404,
	NativeDict_get_Keys_mC1AD4DADCA658F2F01893D98C1484DF3D3CF0732,
	NativeDict_set_Keys_m003BE548ED7F10FEFA7AD36111F98F594CBAF836,
	NativeDict_get_Values_m598E0F0F327AACF854A88CB60D7DEE2462877F5E,
	NativeDict_set_Values_m61F3F99F851A541DB40481EBC4EB4B86FC20F40C,
	IOSFacebookGameObject__ctor_m63EC89911EB6FD1D09C7D2326DEDD00182DD9D61,
	IOSFacebookLoader_get_FBGameObject_m7CE629B334A3CD2407F3C8AC29AE276E5CF940DD,
	IOSFacebookLoader__ctor_mF736AD21858D85A1AE3CCD69C3FF35A6F0687126,
	AndroidFacebook__ctor_mCE9B27E577B5A6B480E490CCCEFE0EAA1AEFD420,
	AndroidFacebook__ctor_m06B102FCF1D25922DD8E102EB4AADB9680C4E927,
	AndroidFacebook_set_KeyHash_m011A0E2B79448395F98CA451DB6E723DAC27E264,
	AndroidFacebook_get_LimitEventUsage_m94249500E4A7AD7A7BFBAEF62B14AC4BA36CBB31,
	AndroidFacebook_set_LimitEventUsage_mF8BF8360027BDCDE57E3458697D268FD946B9AEA,
	AndroidFacebook_get_SDKName_m15D829440EAA71FEA605845E29D9419E007786DA,
	AndroidFacebook_get_SDKVersion_mF214EA76A1522B0FB8152F0F42992C73D7BE239B,
	AndroidFacebook_Init_mAC0526C93B99BFFB47AEFF550B3BD5116A5158B0,
	AndroidFacebook_LogInWithReadPermissions_m82C35A6F14C5116D7A855CFC33DF71AD3546928F,
	AndroidFacebook_LogInWithPublishPermissions_m36829BDFFC8669BD5933BCF30F558E2D59C1663C,
	AndroidFacebook_LogOut_mA0934F37AA226D3C89EE1451F80051DE8123A15F,
	AndroidFacebook_AppRequest_mA5C375668A094E1F0A106F4C1B781C3570DC697B,
	AndroidFacebook_ShareLink_m2950BE87151F48F12436848487D8B0037442E56C,
	AndroidFacebook_FeedShare_m0BCDB85B71B730944FD27A861C3F4D1A3581CC3B,
	AndroidFacebook_GetAppLink_m128D5EDACE4E8F82C1E056D47E36C0D17C927A89,
	AndroidFacebook_AppEventsLogEvent_m634ACA27D524358DA99A841E0AC08EA8152E3236,
	AndroidFacebook_AppEventsLogPurchase_mA45E0AF11D8DC8E929B33826B4CF7F94E7B5A5E6,
	AndroidFacebook_IsImplicitPurchaseLoggingEnabled_mABA8DA2A8804C90782C496BF3B6F3DBC4FD1F762,
	AndroidFacebook_ActivateApp_m0507787406F0D02C01BB1B2623BD038B00701BF6,
	AndroidFacebook_GetAndroidWrapper_m21D59B70F3A9B1BEAD200D2FCE70C9DB32B6FDE6,
	AndroidFacebook_CallFB_m944042FB6A254C92B893413CD86914CEDE97C6B5,
	NULL,
	NULL,
	AndroidFacebookGameObject_OnAwake_mC22A910DF1D87EEDDB8DF5ADF999B2BC24A8767D,
	AndroidFacebookGameObject_OnEnable_mE03E7E37F9E5E84F53484902DDD657CFD70F1D0E,
	AndroidFacebookGameObject_OnSceneLoaded_m5734C9E724CDD20EBF60E9E59186FEF2AD049F2F,
	AndroidFacebookGameObject_OnDisable_m6380145C5FB31F572815359A3F49A30E3B48B251,
	AndroidFacebookGameObject_onPurchaseCompleteHandler_m436BCA06DB06273DAC01EE4DCA1470E5022E95E7,
	AndroidFacebookGameObject__ctor_m02107DE14B5E3309778871C2BFF1BED8CC5A1EC1,
	AndroidFacebookLoader_get_FBGameObject_mC608EFA90A0806F001718C88A3B357E0FA2E20D0,
	AndroidFacebookLoader__ctor_m6CA0C30F88650A2AA7B7C575E2699C1457C736B5,
	NULL,
	NULL,
	CanvasFacebook__ctor_mA4D167636BD0D7EB88AB643BD5A5D609222533C0,
	CanvasFacebook__ctor_mEE95A791A05E10B695DB6A5A269106A4705E9680,
	CanvasFacebook_GetCanvasJSWrapper_mBE28994B0A9B10E1E43906E9939CD2D9B98AF44B,
	CanvasFacebook_get_LimitEventUsage_m9CC1E166137F93C3F6309501CD7771D451825416,
	CanvasFacebook_set_LimitEventUsage_m7C9FBA3A9FB7FC2FD156908F81C806D3F14C7835,
	CanvasFacebook_get_SDKName_mB12643F6D6191B0447D27F31B370D585D1F708E3,
	CanvasFacebook_get_SDKVersion_mA4C4B80613CE630EAF789E3BB4A86D2ECB7A038C,
	CanvasFacebook_get_SDKUserAgent_m15416A6893C67B62E191C902B97E3289C1E86398,
	CanvasFacebook_Init_mB72799BDC8E626693272459BCDFFE7DAD0BBB670,
	CanvasFacebook_LogInWithPublishPermissions_mC353DDCE84164548555CF6A948201EADBDA29FAE,
	CanvasFacebook_LogInWithReadPermissions_mD261916C730BC43785C8A59DE43C53117C4FBCF6,
	CanvasFacebook_LogOut_m0104B2DA460D12F33A79034A5D94CC045E240355,
	CanvasFacebook_AppRequest_m75CFF9CA211D83887DD31B20650E9AABD02A83F7,
	CanvasFacebook_ActivateApp_mB360FE1F1257D3724AADC33D477A4AA113314BFD,
	CanvasFacebook_ShareLink_m3DE77F75C20DC08DE2EE30DD1A5E485F1A5B4F96,
	CanvasFacebook_FeedShare_m3B66B53A13331D75718D0DE35861E2D6127DC429,
	CanvasFacebook_GetAppLink_m20829C7B93857C2BFF2FB3D5D4CDB876F0ECD7B4,
	CanvasFacebook_AppEventsLogEvent_mF2A8EF4EC50971415D810B68FB774CEA9878BB48,
	CanvasFacebook_AppEventsLogPurchase_m2B456DE69973EF1638AA93718DD0629AFEB8F95C,
	CanvasFacebook_OnLoginComplete_m67E4E75435099825AF37673F36F8BB5C26438C74,
	CanvasFacebook_OnGetAppLinkComplete_mAD0E60051CA62CB023AD725576C7C89837B58FC4,
	CanvasFacebook_OnFacebookAuthResponseChange_m84947A7108A5E24CA74CA205BD3A0EB052124025,
	CanvasFacebook_OnPayComplete_mC9DD0864D37BF0A841CFFE9B46482A8A3754DEAD,
	CanvasFacebook_OnAppRequestsComplete_m2FA9E0C6549F4D01A595628C5D8D8436129F6F87,
	CanvasFacebook_OnShareLinkComplete_mF068F2BB7C7A926C0A89442C308114BD5EF0416C,
	CanvasFacebook_OnUrlResponse_m0BCD31B33F46AC840BCA2E5172810A719E650024,
	CanvasFacebook_OnHideUnity_m396828CFC14CFC7825EA04EF9E6CB968B60E4576,
	CanvasFacebook_FormatAuthResponse_mC122E02995F35CA78D3F6E35E8A49CD140FC7CCB,
	CanvasFacebook_U3COnLoginCompleteU3Eb__37_0_m121CFE841950FB9AAFF57727BF86856DCFECD845,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_m46DF256AF9268C9BC6FD061C7715BBDA620CD619,
	U3CU3Ec__ctor_m42F0BBEB665594B5BE49452C39AB3137578C30B2,
	U3CU3Ec_U3COnFacebookAuthResponseChangeU3Eb__40_0_mA4E30AADC07FEC8F3CAC1A2DA589038C041657CA,
	U3CU3Ec__DisplayClass47_0__ctor_m015B73B35ED98A71F35BBEC6B6353AE7FE273EE9,
	U3CU3Ec__DisplayClass47_0_U3CFormatAuthResponseU3Eb__0_m1D8F01E2205DCEFDC415AA764F1A309FC1E38E12,
	CanvasFacebookGameObject_get_CanvasFacebookImpl_mCBC8E0C14005EC7783FFCEACE216395E3AB90334,
	CanvasFacebookGameObject_OnPayComplete_mF97E518F290BB454F0B33C3E1355584E375B7F16,
	CanvasFacebookGameObject_OnFacebookAuthResponseChange_mBC109A88D9182A9E625EEE1901A2C56B85DE7FF1,
	CanvasFacebookGameObject_OnUrlResponse_mEDA593152628394EEA48DBE3AA98C7037EEA548E,
	CanvasFacebookGameObject_OnHideUnity_mDE03D83510F04E2588EEE7CE3F2FED70941091C1,
	CanvasFacebookGameObject_OnAwake_m9A9BD2E92E1F60B05FD330B36CA2916D2CD1A9F3,
	CanvasFacebookGameObject__ctor_mD2692E48EC1C16E675CC8919569609BA16B4E279,
	CanvasFacebookLoader_get_FBGameObject_m4694FBD84F431765392E087BAD0F9B54D37DA2D3,
	CanvasFacebookLoader__ctor_m9CC68D1BBFBB04D81C17F3A84D89A79223E209C9,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsBridge_Start_m4961FD63DF2AE788A939E85F9FB415719D3F86C3,
	JsBridge_OnLoginComplete_mB6370711671734B4252E9E372B30C29266DF9917,
	JsBridge_OnFacebookAuthResponseChange_m3379164A26B1184E9390036F087531B0696A0753,
	JsBridge_OnPayComplete_m4FDCA451487C52F2CEAA20C06977FEA9BBDE163F,
	JsBridge_OnAppRequestsComplete_m9643B9A3F366CA1FE3D41DD83BC97713E2C2998D,
	JsBridge_OnShareLinkComplete_m6FA81C45A9ABA83521F4A75F243D621E1DC1142D,
	JsBridge_OnFacebookFocus_mABE3BD9E1F077D927E89D62923AA0519F36D2E15,
	JsBridge_OnInitComplete_m84DAFC312AD6E4DD9F459359389C4671881DDF20,
	JsBridge_OnUrlResponse_mD05D544199431003845015CFD404B6DCD3846173,
	JsBridge__ctor_m5EC7E0D5AA8E7FA4E473F7B3EB8D7DF639F44A28,
};
static const int32_t s_InvokerIndices[712] = 
{
	0,
	0,
	3,
	26,
	238,
	238,
	14,
	10,
	0,
	23,
	14,
	14,
	14,
	34,
	14,
	14,
	23,
	23,
	0,
	26,
	26,
	26,
	26,
	26,
	1831,
	4,
	154,
	14,
	26,
	110,
	319,
	14,
	26,
	14,
	26,
	998,
	999,
	14,
	14,
	-1,
	26,
	137,
	-1,
	23,
	4,
	-1,
	-1,
	4,
	4,
	49,
	49,
	49,
	4,
	4,
	49,
	106,
	106,
	4,
	154,
	4,
	154,
	4,
	154,
	49,
	49,
	49,
	821,
	4,
	154,
	4,
	154,
	4,
	154,
	186,
	1832,
	137,
	137,
	3,
	1833,
	1834,
	1835,
	1836,
	1837,
	564,
	564,
	3,
	154,
	3,
	1838,
	1839,
	1840,
	3,
	23,
	3,
	124,
	23,
	105,
	26,
	4,
	49,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	89,
	31,
	14,
	14,
	14,
	89,
	89,
	31,
	14,
	26,
	26,
	27,
	27,
	23,
	1841,
	791,
	1842,
	1843,
	1843,
	26,
	26,
	1844,
	1845,
	26,
	26,
	26,
	26,
	26,
	26,
	1841,
	26,
	28,
	28,
	26,
	3,
	23,
	9,
	124,
	23,
	105,
	26,
	-1,
	-1,
	-1,
	-1,
	124,
	31,
	1302,
	26,
	14,
	26,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	4,
	89,
	89,
	31,
	14,
	89,
	27,
	27,
	23,
	1841,
	791,
	1842,
	1843,
	1843,
	26,
	26,
	1844,
	1845,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	26,
	26,
	-1,
	-1,
	27,
	27,
	27,
	-1,
	27,
	14,
	0,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	26,
	26,
	26,
	14,
	14,
	14,
	26,
	14,
	26,
	14,
	3,
	26,
	172,
	14,
	26,
	147,
	14,
	26,
	14,
	26,
	14,
	26,
	89,
	31,
	14,
	26,
	1846,
	1847,
	14,
	1848,
	0,
	114,
	0,
	26,
	26,
	14,
	26,
	14,
	26,
	28,
	26,
	14,
	26,
	4,
	14,
	186,
	186,
	564,
	564,
	14,
	28,
	34,
	28,
	28,
	28,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	3,
	4,
	154,
	154,
	154,
	154,
	137,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	-1,
	1849,
	-1,
	0,
	0,
	1,
	0,
	0,
	0,
	2,
	95,
	1850,
	0,
	1851,
	-1,
	-1,
	-1,
	-1,
	3,
	23,
	28,
	4,
	1843,
	1843,
	23,
	1843,
	1843,
	171,
	1852,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	154,
	154,
	137,
	1,
	1,
	1,
	1,
	23,
	26,
	14,
	154,
	154,
	4,
	4,
	137,
	1853,
	3,
	1854,
	4,
	0,
	23,
	3,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	26,
	23,
	3,
	3,
	23,
	26,
	23,
	135,
	135,
	0,
	119,
	0,
	1,
	26,
	14,
	26,
	26,
	26,
	26,
	26,
	200,
	200,
	200,
	200,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	26,
	23,
	27,
	89,
	31,
	14,
	14,
	197,
	26,
	1844,
	1845,
	1841,
	1842,
	791,
	26,
	27,
	27,
	26,
	26,
	26,
	26,
	89,
	28,
	4,
	27,
	124,
	26,
	205,
	26,
	14,
	27,
	23,
	105,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	23,
	89,
	28,
	14,
	26,
	26,
	428,
	1855,
	1856,
	27,
	23,
	89,
	31,
	14,
	14,
	4,
	26,
	27,
	27,
	1841,
	791,
	1842,
	26,
	26,
	1844,
	1845,
	89,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	31,
	23,
	23,
	1857,
	23,
	26,
	23,
	14,
	23,
	14,
	26,
	14,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	32,
	23,
	26,
	23,
	197,
	27,
	197,
	197,
	23,
	197,
	27,
	197,
	14,
	26,
	14,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	26,
	23,
	26,
	14,
	26,
	14,
	23,
	23,
	23,
	14,
	23,
	89,
	26,
	26,
	26,
	89,
	26,
	26,
	26,
	26,
	26,
	26,
	14,
	26,
	26,
	23,
	886,
	62,
	62,
	23,
	419,
	1858,
	1859,
	23,
	1860,
	1861,
	31,
	32,
	14,
	14,
	23,
	27,
	89,
	31,
	14,
	14,
	1172,
	27,
	27,
	23,
	1841,
	791,
	1842,
	1844,
	1845,
	89,
	26,
	26,
	4,
	0,
	-1,
	23,
	10,
	32,
	14,
	26,
	14,
	26,
	23,
	14,
	23,
	23,
	27,
	26,
	89,
	31,
	14,
	14,
	197,
	27,
	27,
	23,
	1841,
	791,
	1842,
	26,
	1844,
	1845,
	89,
	26,
	4,
	27,
	-1,
	-1,
	23,
	23,
	1857,
	23,
	26,
	23,
	14,
	23,
	-1,
	27,
	23,
	27,
	4,
	89,
	31,
	14,
	14,
	14,
	1862,
	27,
	27,
	23,
	1841,
	26,
	791,
	1842,
	26,
	1844,
	1845,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	31,
	137,
	26,
	-1,
	-1,
	-1,
	3,
	23,
	26,
	23,
	26,
	14,
	26,
	26,
	26,
	31,
	23,
	23,
	14,
	23,
	26,
	26,
	26,
	31,
	26,
	26,
	26,
	31,
	14,
	23,
	1291,
	27,
	23,
	23,
	1844,
	1845,
	197,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[11] = 
{
	{ 0x0200001D, { 11, 3 } },
	{ 0x02000066, { 17, 5 } },
	{ 0x0200006B, { 22, 6 } },
	{ 0x0600002B, { 0, 3 } },
	{ 0x0600002E, { 3, 3 } },
	{ 0x0600002F, { 6, 1 } },
	{ 0x060000CA, { 7, 1 } },
	{ 0x060000CB, { 8, 3 } },
	{ 0x0600013C, { 14, 1 } },
	{ 0x0600013E, { 15, 1 } },
	{ 0x06000253, { 16, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[28] = 
{
	{ (Il2CppRGCTXDataType)2, 17559 },
	{ (Il2CppRGCTXDataType)2, 17560 },
	{ (Il2CppRGCTXDataType)3, 9908 },
	{ (Il2CppRGCTXDataType)3, 9909 },
	{ (Il2CppRGCTXDataType)2, 16744 },
	{ (Il2CppRGCTXDataType)3, 9910 },
	{ (Il2CppRGCTXDataType)3, 9911 },
	{ (Il2CppRGCTXDataType)2, 16818 },
	{ (Il2CppRGCTXDataType)3, 9912 },
	{ (Il2CppRGCTXDataType)3, 9913 },
	{ (Il2CppRGCTXDataType)2, 16820 },
	{ (Il2CppRGCTXDataType)3, 9914 },
	{ (Il2CppRGCTXDataType)3, 9915 },
	{ (Il2CppRGCTXDataType)3, 9916 },
	{ (Il2CppRGCTXDataType)2, 16886 },
	{ (Il2CppRGCTXDataType)3, 9917 },
	{ (Il2CppRGCTXDataType)3, 9918 },
	{ (Il2CppRGCTXDataType)3, 9919 },
	{ (Il2CppRGCTXDataType)2, 16992 },
	{ (Il2CppRGCTXDataType)3, 9920 },
	{ (Il2CppRGCTXDataType)3, 9921 },
	{ (Il2CppRGCTXDataType)3, 9922 },
	{ (Il2CppRGCTXDataType)3, 9923 },
	{ (Il2CppRGCTXDataType)2, 17006 },
	{ (Il2CppRGCTXDataType)3, 9924 },
	{ (Il2CppRGCTXDataType)3, 9925 },
	{ (Il2CppRGCTXDataType)3, 9926 },
	{ (Il2CppRGCTXDataType)3, 9927 },
};
extern const Il2CppCodeGenModule g_Facebook_UnityCodeGenModule;
const Il2CppCodeGenModule g_Facebook_UnityCodeGenModule = 
{
	"Facebook.Unity.dll",
	712,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	11,
	s_rgctxIndices,
	28,
	s_rgctxValues,
	NULL,
};
