﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Facebook.Unity.IOS.IOSWrapper::Init(System.String,System.Boolean,System.String,System.String)
extern void IOSWrapper_Init_mE39EA2EBA1259F94D8AF17CF2F7525DE85082289 (void);
// 0x00000002 System.Void Facebook.Unity.IOS.IOSWrapper::LogInWithReadPermissions(System.Int32,System.String)
extern void IOSWrapper_LogInWithReadPermissions_m9DD49FA35431B397CD4A850786C978C8F926711C (void);
// 0x00000003 System.Void Facebook.Unity.IOS.IOSWrapper::LogInWithPublishPermissions(System.Int32,System.String)
extern void IOSWrapper_LogInWithPublishPermissions_m5A7B0497A8F57B0201E41ED3E4489007B842845C (void);
// 0x00000004 System.Void Facebook.Unity.IOS.IOSWrapper::LogOut()
extern void IOSWrapper_LogOut_m00092A877BD521F5F945BFC981F8D170C10CF639 (void);
// 0x00000005 System.Void Facebook.Unity.IOS.IOSWrapper::SetPushNotificationsDeviceTokenString(System.String)
extern void IOSWrapper_SetPushNotificationsDeviceTokenString_m044B8F7EE84DCFA55760E3CF99973E99FF119358 (void);
// 0x00000006 System.Void Facebook.Unity.IOS.IOSWrapper::SetShareDialogMode(System.Int32)
extern void IOSWrapper_SetShareDialogMode_mCD77567F95AC2BE75878B4FE5FEFF51AFBAABA94 (void);
// 0x00000007 System.Void Facebook.Unity.IOS.IOSWrapper::ShareLink(System.Int32,System.String,System.String,System.String,System.String)
extern void IOSWrapper_ShareLink_m48376579EC0A2B28ED6FDC57369766EB9B03E70A (void);
// 0x00000008 System.Void Facebook.Unity.IOS.IOSWrapper::FeedShare(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void IOSWrapper_FeedShare_m0C86309192740933734E90221AFA87C1F9E092DA (void);
// 0x00000009 System.Void Facebook.Unity.IOS.IOSWrapper::AppRequest(System.Int32,System.String,System.String,System.String,System.String[],System.Int32,System.String,System.String[],System.Int32,System.Boolean,System.Int32,System.String,System.String)
extern void IOSWrapper_AppRequest_mE0A4A89DA6CED85A7659DD5FF2AFF5D126E28D8D (void);
// 0x0000000A System.Void Facebook.Unity.IOS.IOSWrapper::FBAppEventsActivateApp()
extern void IOSWrapper_FBAppEventsActivateApp_mA18B733183DE3A411C3BABA5F887AD9C82C929EB (void);
// 0x0000000B System.Void Facebook.Unity.IOS.IOSWrapper::LogAppEvent(System.String,System.Double,System.Int32,System.String[],System.String[])
extern void IOSWrapper_LogAppEvent_m7DE61BA1928BB92A47D58CEF043F1330E78756B7 (void);
// 0x0000000C System.Void Facebook.Unity.IOS.IOSWrapper::LogPurchaseAppEvent(System.Double,System.String,System.Int32,System.String[],System.String[])
extern void IOSWrapper_LogPurchaseAppEvent_m90A04609103826EA93B2AB693767BF0C5BD38979 (void);
// 0x0000000D System.Void Facebook.Unity.IOS.IOSWrapper::FBAppEventsSetLimitEventUsage(System.Boolean)
extern void IOSWrapper_FBAppEventsSetLimitEventUsage_m41242F79ED5090D2F2BFCC387B401711E43F3670 (void);
// 0x0000000E System.Void Facebook.Unity.IOS.IOSWrapper::FBAutoLogAppEventsEnabled(System.Boolean)
extern void IOSWrapper_FBAutoLogAppEventsEnabled_m72451F074DFC220965F0DC439C774755B617E84D (void);
// 0x0000000F System.Void Facebook.Unity.IOS.IOSWrapper::FBAdvertiserIDCollectionEnabled(System.Boolean)
extern void IOSWrapper_FBAdvertiserIDCollectionEnabled_mDC88345CA65DE4A30D01FF0682DDEFE0F9972735 (void);
// 0x00000010 System.Void Facebook.Unity.IOS.IOSWrapper::GetAppLink(System.Int32)
extern void IOSWrapper_GetAppLink_m77C3C186F19133519E6516678A5A74326034126C (void);
// 0x00000011 System.String Facebook.Unity.IOS.IOSWrapper::FBSdkVersion()
extern void IOSWrapper_FBSdkVersion_m6A56E9209DF25BFA2F7FF13A1C41DBEEC3B75820 (void);
// 0x00000012 System.Void Facebook.Unity.IOS.IOSWrapper::FBSetUserID(System.String)
extern void IOSWrapper_FBSetUserID_m70AFC921E0AF1CA94F5F895748A60EB5BB8699FC (void);
// 0x00000013 System.String Facebook.Unity.IOS.IOSWrapper::FBGetUserID()
extern void IOSWrapper_FBGetUserID_mACF66E5A4E2C75260F95E72AA1257C388C1908D7 (void);
// 0x00000014 System.Void Facebook.Unity.IOS.IOSWrapper::UpdateUserProperties(System.Int32,System.String[],System.String[])
extern void IOSWrapper_UpdateUserProperties_mD367C212EAA66FD67EF32ACC0FEFF6B49768F5DF (void);
// 0x00000015 System.Void Facebook.Unity.IOS.IOSWrapper::FetchDeferredAppLink(System.Int32)
extern void IOSWrapper_FetchDeferredAppLink_m789231D1FEF0ABB1C85592A7CD8F9AA86F57B105 (void);
// 0x00000016 System.Void Facebook.Unity.IOS.IOSWrapper::RefreshCurrentAccessToken(System.Int32)
extern void IOSWrapper_RefreshCurrentAccessToken_m213BFDC773185FBFB5147E2AD39736DE395DF7E0 (void);
// 0x00000017 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBInit(System.String,System.Boolean,System.String,System.String)
extern void IOSWrapper_IOSFBInit_m580D0709E4E080219E6E20510B2F4DC678ED98FF (void);
// 0x00000018 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogInWithReadPermissions(System.Int32,System.String)
extern void IOSWrapper_IOSFBLogInWithReadPermissions_m62DE77B7D166514F7053211DF66695D6BCE50859 (void);
// 0x00000019 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogInWithPublishPermissions(System.Int32,System.String)
extern void IOSWrapper_IOSFBLogInWithPublishPermissions_m8908EA2C9A106BA27190667AC2B2883E335895E2 (void);
// 0x0000001A System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogOut()
extern void IOSWrapper_IOSFBLogOut_m9E8367BDB2AA255EAB8ED71AEFF0F5FEC2FBE827 (void);
// 0x0000001B System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetPushNotificationsDeviceTokenString(System.String)
extern void IOSWrapper_IOSFBSetPushNotificationsDeviceTokenString_mB3D1CEC42E9AEA38A839B55593A327208D2A6120 (void);
// 0x0000001C System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetShareDialogMode(System.Int32)
extern void IOSWrapper_IOSFBSetShareDialogMode_mCA7883395558AA1F27264134C6A85B7D746A2AAB (void);
// 0x0000001D System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBShareLink(System.Int32,System.String,System.String,System.String,System.String)
extern void IOSWrapper_IOSFBShareLink_m69B624EEEB70F2D40AA1CE545D49DB28EB7EE673 (void);
// 0x0000001E System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBFeedShare(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void IOSWrapper_IOSFBFeedShare_mE2E4832C79D670A2AB5F2B1F8D3AE3E9C6A6FD96 (void);
// 0x0000001F System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppRequest(System.Int32,System.String,System.String,System.String,System.String[],System.Int32,System.String,System.String[],System.Int32,System.Boolean,System.Int32,System.String,System.String)
extern void IOSWrapper_IOSFBAppRequest_mCB40DDF8BA0DF0EA72250AEE0DC9E2A5D4688651 (void);
// 0x00000020 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsActivateApp()
extern void IOSWrapper_IOSFBAppEventsActivateApp_m5C83AE70AC023734CE2AA71B28FC8B11ECED9665 (void);
// 0x00000021 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsLogEvent(System.String,System.Double,System.Int32,System.String[],System.String[])
extern void IOSWrapper_IOSFBAppEventsLogEvent_mDCD08B49E852692DCD3A505AC5FBFAAD678110ED (void);
// 0x00000022 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsLogPurchase(System.Double,System.String,System.Int32,System.String[],System.String[])
extern void IOSWrapper_IOSFBAppEventsLogPurchase_mAFB5BBD024E105242CB6BAD15DCA2CD119428F8E (void);
// 0x00000023 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsSetLimitEventUsage(System.Boolean)
extern void IOSWrapper_IOSFBAppEventsSetLimitEventUsage_mF50EF696AE1CD8B8B34436653F64A45CC3744B8D (void);
// 0x00000024 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAutoLogAppEventsEnabled(System.Boolean)
extern void IOSWrapper_IOSFBAutoLogAppEventsEnabled_mDBE2CE191710E1BC029C4C4A78536B192BEC23C0 (void);
// 0x00000025 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAdvertiserIDCollectionEnabled(System.Boolean)
extern void IOSWrapper_IOSFBAdvertiserIDCollectionEnabled_m6CFFC36CE4E91754EB7BD2F8EEE91845C7F28992 (void);
// 0x00000026 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBGetAppLink(System.Int32)
extern void IOSWrapper_IOSFBGetAppLink_mE25B16BFEC16B6CEAFB4942995C287D6ECCFB1C9 (void);
// 0x00000027 System.String Facebook.Unity.IOS.IOSWrapper::IOSFBSdkVersion()
extern void IOSWrapper_IOSFBSdkVersion_m06FA64CF7DAEFAAEA319DE028CC5D0AD2B7E3E8D (void);
// 0x00000028 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBFetchDeferredAppLink(System.Int32)
extern void IOSWrapper_IOSFBFetchDeferredAppLink_m029A575443E8084E6C6F1A2BE80C2AD92DDAA8AC (void);
// 0x00000029 System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBRefreshCurrentAccessToken(System.Int32)
extern void IOSWrapper_IOSFBRefreshCurrentAccessToken_m0108DC6341A86335A9231712936E42560E5C523D (void);
// 0x0000002A System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetUserID(System.String)
extern void IOSWrapper_IOSFBSetUserID_mFBA1C21A89794C2CFFA81D2B01A42131DB743667 (void);
// 0x0000002B System.String Facebook.Unity.IOS.IOSWrapper::IOSFBGetUserID()
extern void IOSWrapper_IOSFBGetUserID_mD193BFBD7C08F17876F1EA0256BEC824397A8017 (void);
// 0x0000002C System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBUpdateUserProperties(System.Int32,System.String[],System.String[])
extern void IOSWrapper_IOSFBUpdateUserProperties_m39F7266860C0D7612F4340755867502963FD5522 (void);
// 0x0000002D System.Void Facebook.Unity.IOS.IOSWrapper::.ctor()
extern void IOSWrapper__ctor_m2DD27812C18136918172FB64AE65568587FDDDD2 (void);
static Il2CppMethodPointer s_methodPointers[45] = 
{
	IOSWrapper_Init_mE39EA2EBA1259F94D8AF17CF2F7525DE85082289,
	IOSWrapper_LogInWithReadPermissions_m9DD49FA35431B397CD4A850786C978C8F926711C,
	IOSWrapper_LogInWithPublishPermissions_m5A7B0497A8F57B0201E41ED3E4489007B842845C,
	IOSWrapper_LogOut_m00092A877BD521F5F945BFC981F8D170C10CF639,
	IOSWrapper_SetPushNotificationsDeviceTokenString_m044B8F7EE84DCFA55760E3CF99973E99FF119358,
	IOSWrapper_SetShareDialogMode_mCD77567F95AC2BE75878B4FE5FEFF51AFBAABA94,
	IOSWrapper_ShareLink_m48376579EC0A2B28ED6FDC57369766EB9B03E70A,
	IOSWrapper_FeedShare_m0C86309192740933734E90221AFA87C1F9E092DA,
	IOSWrapper_AppRequest_mE0A4A89DA6CED85A7659DD5FF2AFF5D126E28D8D,
	IOSWrapper_FBAppEventsActivateApp_mA18B733183DE3A411C3BABA5F887AD9C82C929EB,
	IOSWrapper_LogAppEvent_m7DE61BA1928BB92A47D58CEF043F1330E78756B7,
	IOSWrapper_LogPurchaseAppEvent_m90A04609103826EA93B2AB693767BF0C5BD38979,
	IOSWrapper_FBAppEventsSetLimitEventUsage_m41242F79ED5090D2F2BFCC387B401711E43F3670,
	IOSWrapper_FBAutoLogAppEventsEnabled_m72451F074DFC220965F0DC439C774755B617E84D,
	IOSWrapper_FBAdvertiserIDCollectionEnabled_mDC88345CA65DE4A30D01FF0682DDEFE0F9972735,
	IOSWrapper_GetAppLink_m77C3C186F19133519E6516678A5A74326034126C,
	IOSWrapper_FBSdkVersion_m6A56E9209DF25BFA2F7FF13A1C41DBEEC3B75820,
	IOSWrapper_FBSetUserID_m70AFC921E0AF1CA94F5F895748A60EB5BB8699FC,
	IOSWrapper_FBGetUserID_mACF66E5A4E2C75260F95E72AA1257C388C1908D7,
	IOSWrapper_UpdateUserProperties_mD367C212EAA66FD67EF32ACC0FEFF6B49768F5DF,
	IOSWrapper_FetchDeferredAppLink_m789231D1FEF0ABB1C85592A7CD8F9AA86F57B105,
	IOSWrapper_RefreshCurrentAccessToken_m213BFDC773185FBFB5147E2AD39736DE395DF7E0,
	IOSWrapper_IOSFBInit_m580D0709E4E080219E6E20510B2F4DC678ED98FF,
	IOSWrapper_IOSFBLogInWithReadPermissions_m62DE77B7D166514F7053211DF66695D6BCE50859,
	IOSWrapper_IOSFBLogInWithPublishPermissions_m8908EA2C9A106BA27190667AC2B2883E335895E2,
	IOSWrapper_IOSFBLogOut_m9E8367BDB2AA255EAB8ED71AEFF0F5FEC2FBE827,
	IOSWrapper_IOSFBSetPushNotificationsDeviceTokenString_mB3D1CEC42E9AEA38A839B55593A327208D2A6120,
	IOSWrapper_IOSFBSetShareDialogMode_mCA7883395558AA1F27264134C6A85B7D746A2AAB,
	IOSWrapper_IOSFBShareLink_m69B624EEEB70F2D40AA1CE545D49DB28EB7EE673,
	IOSWrapper_IOSFBFeedShare_mE2E4832C79D670A2AB5F2B1F8D3AE3E9C6A6FD96,
	IOSWrapper_IOSFBAppRequest_mCB40DDF8BA0DF0EA72250AEE0DC9E2A5D4688651,
	IOSWrapper_IOSFBAppEventsActivateApp_m5C83AE70AC023734CE2AA71B28FC8B11ECED9665,
	IOSWrapper_IOSFBAppEventsLogEvent_mDCD08B49E852692DCD3A505AC5FBFAAD678110ED,
	IOSWrapper_IOSFBAppEventsLogPurchase_mAFB5BBD024E105242CB6BAD15DCA2CD119428F8E,
	IOSWrapper_IOSFBAppEventsSetLimitEventUsage_mF50EF696AE1CD8B8B34436653F64A45CC3744B8D,
	IOSWrapper_IOSFBAutoLogAppEventsEnabled_mDBE2CE191710E1BC029C4C4A78536B192BEC23C0,
	IOSWrapper_IOSFBAdvertiserIDCollectionEnabled_m6CFFC36CE4E91754EB7BD2F8EEE91845C7F28992,
	IOSWrapper_IOSFBGetAppLink_mE25B16BFEC16B6CEAFB4942995C287D6ECCFB1C9,
	IOSWrapper_IOSFBSdkVersion_m06FA64CF7DAEFAAEA319DE028CC5D0AD2B7E3E8D,
	IOSWrapper_IOSFBFetchDeferredAppLink_m029A575443E8084E6C6F1A2BE80C2AD92DDAA8AC,
	IOSWrapper_IOSFBRefreshCurrentAccessToken_m0108DC6341A86335A9231712936E42560E5C523D,
	IOSWrapper_IOSFBSetUserID_mFBA1C21A89794C2CFFA81D2B01A42131DB743667,
	IOSWrapper_IOSFBGetUserID_mD193BFBD7C08F17876F1EA0256BEC824397A8017,
	IOSWrapper_IOSFBUpdateUserProperties_m39F7266860C0D7612F4340755867502963FD5522,
	IOSWrapper__ctor_m2DD27812C18136918172FB64AE65568587FDDDD2,
};
static const int32_t s_InvokerIndices[45] = 
{
	886,
	62,
	62,
	23,
	26,
	32,
	419,
	1858,
	1859,
	23,
	1860,
	1861,
	31,
	31,
	31,
	32,
	14,
	26,
	14,
	358,
	32,
	32,
	1863,
	566,
	566,
	3,
	154,
	164,
	1864,
	1865,
	1866,
	3,
	1867,
	1868,
	821,
	821,
	821,
	164,
	4,
	164,
	164,
	154,
	4,
	1625,
	23,
};
extern const Il2CppCodeGenModule g_Facebook_Unity_IOSCodeGenModule;
const Il2CppCodeGenModule g_Facebook_Unity_IOSCodeGenModule = 
{
	"Facebook.Unity.IOS.dll",
	45,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
