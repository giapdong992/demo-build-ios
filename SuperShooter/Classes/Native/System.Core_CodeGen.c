﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B (void);
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64 (void);
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712 (void);
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E (void);
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722 (void);
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088 (void);
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC (void);
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6 (void);
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349 (void);
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA (void);
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8 (void);
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0 (void);
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23 (void);
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD (void);
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1 (void);
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4 (void);
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91 (void);
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED (void);
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC (void);
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146 (void);
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E (void);
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC (void);
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586 (void);
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139 (void);
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672 (void);
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F (void);
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C (void);
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A (void);
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700 (void);
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA (void);
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B (void);
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F (void);
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F (void);
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA (void);
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438 (void);
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22 (void);
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44 (void);
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA (void);
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F (void);
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD (void);
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA (void);
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24 (void);
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA (void);
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463 (void);
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325 (void);
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x0000002F System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000030 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000031 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000032 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000033 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000035 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000036 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000037 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000038 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000039 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003A System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000003B System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003C System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000003D TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000003E System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000003F System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000040 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000041 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000042 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000043 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000044 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000045 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000046 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000047 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000048 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000049 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000004A System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000004B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000004C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000004D System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000004E System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000004F System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000050 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000051 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000052 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000053 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000054 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000055 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000056 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000057 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000058 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000059 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000005A System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000005B System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000005C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000005D System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000005E System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x0000005F System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000060 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000061 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000062 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000063 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000064 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000065 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000066 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000067 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000068 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000069 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000006A TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000006B System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x0000006C TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000006D System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000006E System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000006F System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000070 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000071 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000072 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000073 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000074 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000075 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000076 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000077 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000078 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000079 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000007A System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000007B System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000007C System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000007D System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000007E System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000007F System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000080 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000081 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000082 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000083 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000084 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000085 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000086 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000087 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000088 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000089 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[137] = 
{
	SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B,
	AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64,
	AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712,
	AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E,
	AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722,
	AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088,
	AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC,
	AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6,
	AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349,
	AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA,
	AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8,
	AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0,
	AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23,
	AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD,
	AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1,
	AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4,
	AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91,
	AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED,
	AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC,
	AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146,
	AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E,
	AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC,
	AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586,
	AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139,
	AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672,
	AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F,
	AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C,
	AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A,
	AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700,
	AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA,
	AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B,
	AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F,
	AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F,
	AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA,
	AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438,
	AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22,
	AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44,
	AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA,
	AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F,
	AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD,
	AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA,
	AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24,
	AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA,
	AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463,
	AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[137] = 
{
	0,
	23,
	10,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	105,
	14,
	105,
	31,
	23,
	23,
	23,
	23,
	23,
	105,
	105,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	886,
	27,
	37,
	197,
	197,
	3,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[29] = 
{
	{ 0x02000008, { 48, 4 } },
	{ 0x02000009, { 52, 9 } },
	{ 0x0200000A, { 63, 7 } },
	{ 0x0200000B, { 72, 10 } },
	{ 0x0200000C, { 84, 11 } },
	{ 0x0200000D, { 98, 9 } },
	{ 0x0200000E, { 110, 12 } },
	{ 0x0200000F, { 125, 1 } },
	{ 0x02000010, { 126, 2 } },
	{ 0x02000011, { 128, 4 } },
	{ 0x02000012, { 132, 21 } },
	{ 0x02000014, { 153, 2 } },
	{ 0x06000031, { 0, 10 } },
	{ 0x06000032, { 10, 10 } },
	{ 0x06000033, { 20, 5 } },
	{ 0x06000034, { 25, 5 } },
	{ 0x06000035, { 30, 3 } },
	{ 0x06000036, { 33, 2 } },
	{ 0x06000037, { 35, 4 } },
	{ 0x06000038, { 39, 3 } },
	{ 0x06000039, { 42, 1 } },
	{ 0x0600003A, { 43, 3 } },
	{ 0x0600003B, { 46, 2 } },
	{ 0x0600004B, { 61, 2 } },
	{ 0x06000050, { 70, 2 } },
	{ 0x06000055, { 82, 2 } },
	{ 0x0600005B, { 95, 3 } },
	{ 0x06000060, { 107, 3 } },
	{ 0x06000065, { 122, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[155] = 
{
	{ (Il2CppRGCTXDataType)2, 17480 },
	{ (Il2CppRGCTXDataType)3, 9716 },
	{ (Il2CppRGCTXDataType)2, 17481 },
	{ (Il2CppRGCTXDataType)2, 17482 },
	{ (Il2CppRGCTXDataType)3, 9717 },
	{ (Il2CppRGCTXDataType)2, 17483 },
	{ (Il2CppRGCTXDataType)2, 17484 },
	{ (Il2CppRGCTXDataType)3, 9718 },
	{ (Il2CppRGCTXDataType)2, 17485 },
	{ (Il2CppRGCTXDataType)3, 9719 },
	{ (Il2CppRGCTXDataType)2, 17486 },
	{ (Il2CppRGCTXDataType)3, 9720 },
	{ (Il2CppRGCTXDataType)2, 17487 },
	{ (Il2CppRGCTXDataType)2, 17488 },
	{ (Il2CppRGCTXDataType)3, 9721 },
	{ (Il2CppRGCTXDataType)2, 17489 },
	{ (Il2CppRGCTXDataType)2, 17490 },
	{ (Il2CppRGCTXDataType)3, 9722 },
	{ (Il2CppRGCTXDataType)2, 17491 },
	{ (Il2CppRGCTXDataType)3, 9723 },
	{ (Il2CppRGCTXDataType)2, 17492 },
	{ (Il2CppRGCTXDataType)3, 9724 },
	{ (Il2CppRGCTXDataType)3, 9725 },
	{ (Il2CppRGCTXDataType)2, 14095 },
	{ (Il2CppRGCTXDataType)3, 9726 },
	{ (Il2CppRGCTXDataType)2, 17493 },
	{ (Il2CppRGCTXDataType)3, 9727 },
	{ (Il2CppRGCTXDataType)3, 9728 },
	{ (Il2CppRGCTXDataType)2, 14102 },
	{ (Il2CppRGCTXDataType)3, 9729 },
	{ (Il2CppRGCTXDataType)2, 17494 },
	{ (Il2CppRGCTXDataType)3, 9730 },
	{ (Il2CppRGCTXDataType)3, 9731 },
	{ (Il2CppRGCTXDataType)2, 14108 },
	{ (Il2CppRGCTXDataType)3, 9732 },
	{ (Il2CppRGCTXDataType)2, 17495 },
	{ (Il2CppRGCTXDataType)2, 17496 },
	{ (Il2CppRGCTXDataType)2, 14109 },
	{ (Il2CppRGCTXDataType)2, 17497 },
	{ (Il2CppRGCTXDataType)2, 14111 },
	{ (Il2CppRGCTXDataType)2, 17498 },
	{ (Il2CppRGCTXDataType)3, 9733 },
	{ (Il2CppRGCTXDataType)2, 14114 },
	{ (Il2CppRGCTXDataType)2, 14116 },
	{ (Il2CppRGCTXDataType)2, 17499 },
	{ (Il2CppRGCTXDataType)3, 9734 },
	{ (Il2CppRGCTXDataType)2, 17500 },
	{ (Il2CppRGCTXDataType)2, 14119 },
	{ (Il2CppRGCTXDataType)3, 9735 },
	{ (Il2CppRGCTXDataType)3, 9736 },
	{ (Il2CppRGCTXDataType)2, 14123 },
	{ (Il2CppRGCTXDataType)3, 9737 },
	{ (Il2CppRGCTXDataType)3, 9738 },
	{ (Il2CppRGCTXDataType)2, 14135 },
	{ (Il2CppRGCTXDataType)2, 17501 },
	{ (Il2CppRGCTXDataType)3, 9739 },
	{ (Il2CppRGCTXDataType)3, 9740 },
	{ (Il2CppRGCTXDataType)2, 14137 },
	{ (Il2CppRGCTXDataType)2, 17390 },
	{ (Il2CppRGCTXDataType)3, 9741 },
	{ (Il2CppRGCTXDataType)3, 9742 },
	{ (Il2CppRGCTXDataType)2, 17502 },
	{ (Il2CppRGCTXDataType)3, 9743 },
	{ (Il2CppRGCTXDataType)3, 9744 },
	{ (Il2CppRGCTXDataType)2, 14147 },
	{ (Il2CppRGCTXDataType)2, 17503 },
	{ (Il2CppRGCTXDataType)3, 9745 },
	{ (Il2CppRGCTXDataType)3, 9746 },
	{ (Il2CppRGCTXDataType)3, 9305 },
	{ (Il2CppRGCTXDataType)3, 9747 },
	{ (Il2CppRGCTXDataType)2, 17504 },
	{ (Il2CppRGCTXDataType)3, 9748 },
	{ (Il2CppRGCTXDataType)3, 9749 },
	{ (Il2CppRGCTXDataType)2, 14159 },
	{ (Il2CppRGCTXDataType)2, 17505 },
	{ (Il2CppRGCTXDataType)3, 9750 },
	{ (Il2CppRGCTXDataType)3, 9751 },
	{ (Il2CppRGCTXDataType)3, 9752 },
	{ (Il2CppRGCTXDataType)3, 9753 },
	{ (Il2CppRGCTXDataType)3, 9754 },
	{ (Il2CppRGCTXDataType)3, 9311 },
	{ (Il2CppRGCTXDataType)3, 9755 },
	{ (Il2CppRGCTXDataType)2, 17506 },
	{ (Il2CppRGCTXDataType)3, 9756 },
	{ (Il2CppRGCTXDataType)3, 9757 },
	{ (Il2CppRGCTXDataType)2, 14172 },
	{ (Il2CppRGCTXDataType)2, 17507 },
	{ (Il2CppRGCTXDataType)3, 9758 },
	{ (Il2CppRGCTXDataType)3, 9759 },
	{ (Il2CppRGCTXDataType)2, 14174 },
	{ (Il2CppRGCTXDataType)2, 17508 },
	{ (Il2CppRGCTXDataType)3, 9760 },
	{ (Il2CppRGCTXDataType)3, 9761 },
	{ (Il2CppRGCTXDataType)2, 17509 },
	{ (Il2CppRGCTXDataType)3, 9762 },
	{ (Il2CppRGCTXDataType)3, 9763 },
	{ (Il2CppRGCTXDataType)2, 17510 },
	{ (Il2CppRGCTXDataType)3, 9764 },
	{ (Il2CppRGCTXDataType)3, 9765 },
	{ (Il2CppRGCTXDataType)2, 14189 },
	{ (Il2CppRGCTXDataType)2, 17511 },
	{ (Il2CppRGCTXDataType)3, 9766 },
	{ (Il2CppRGCTXDataType)3, 9767 },
	{ (Il2CppRGCTXDataType)3, 9768 },
	{ (Il2CppRGCTXDataType)3, 9322 },
	{ (Il2CppRGCTXDataType)2, 17512 },
	{ (Il2CppRGCTXDataType)3, 9769 },
	{ (Il2CppRGCTXDataType)3, 9770 },
	{ (Il2CppRGCTXDataType)2, 17513 },
	{ (Il2CppRGCTXDataType)3, 9771 },
	{ (Il2CppRGCTXDataType)3, 9772 },
	{ (Il2CppRGCTXDataType)2, 14205 },
	{ (Il2CppRGCTXDataType)2, 17514 },
	{ (Il2CppRGCTXDataType)3, 9773 },
	{ (Il2CppRGCTXDataType)3, 9774 },
	{ (Il2CppRGCTXDataType)3, 9775 },
	{ (Il2CppRGCTXDataType)3, 9776 },
	{ (Il2CppRGCTXDataType)3, 9777 },
	{ (Il2CppRGCTXDataType)3, 9778 },
	{ (Il2CppRGCTXDataType)3, 9328 },
	{ (Il2CppRGCTXDataType)2, 17515 },
	{ (Il2CppRGCTXDataType)3, 9779 },
	{ (Il2CppRGCTXDataType)3, 9780 },
	{ (Il2CppRGCTXDataType)2, 17516 },
	{ (Il2CppRGCTXDataType)3, 9781 },
	{ (Il2CppRGCTXDataType)3, 9782 },
	{ (Il2CppRGCTXDataType)3, 9783 },
	{ (Il2CppRGCTXDataType)3, 9784 },
	{ (Il2CppRGCTXDataType)2, 17517 },
	{ (Il2CppRGCTXDataType)2, 14235 },
	{ (Il2CppRGCTXDataType)2, 14233 },
	{ (Il2CppRGCTXDataType)2, 17518 },
	{ (Il2CppRGCTXDataType)3, 9785 },
	{ (Il2CppRGCTXDataType)2, 17519 },
	{ (Il2CppRGCTXDataType)3, 9786 },
	{ (Il2CppRGCTXDataType)3, 9787 },
	{ (Il2CppRGCTXDataType)3, 9788 },
	{ (Il2CppRGCTXDataType)2, 14239 },
	{ (Il2CppRGCTXDataType)3, 9789 },
	{ (Il2CppRGCTXDataType)3, 9790 },
	{ (Il2CppRGCTXDataType)2, 14242 },
	{ (Il2CppRGCTXDataType)3, 9791 },
	{ (Il2CppRGCTXDataType)1, 17520 },
	{ (Il2CppRGCTXDataType)2, 14241 },
	{ (Il2CppRGCTXDataType)3, 9792 },
	{ (Il2CppRGCTXDataType)1, 14241 },
	{ (Il2CppRGCTXDataType)1, 14239 },
	{ (Il2CppRGCTXDataType)2, 17521 },
	{ (Il2CppRGCTXDataType)2, 14241 },
	{ (Il2CppRGCTXDataType)3, 9793 },
	{ (Il2CppRGCTXDataType)3, 9794 },
	{ (Il2CppRGCTXDataType)3, 9795 },
	{ (Il2CppRGCTXDataType)2, 14240 },
	{ (Il2CppRGCTXDataType)3, 9796 },
	{ (Il2CppRGCTXDataType)2, 14253 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	137,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	29,
	s_rgctxIndices,
	155,
	s_rgctxValues,
	NULL,
};
