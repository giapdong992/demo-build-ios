﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Int32 Facebook.Unity.Settings.FacebookSettings::get_SelectedAppIndex()
extern void FacebookSettings_get_SelectedAppIndex_m2EC480ADF724345DFD8E05519877A2F46A1238FC (void);
// 0x00000002 System.Void Facebook.Unity.Settings.FacebookSettings::set_SelectedAppIndex(System.Int32)
extern void FacebookSettings_set_SelectedAppIndex_mFEE448C1AC465FC83B45462B48026D700A4E468D (void);
// 0x00000003 System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppIds()
extern void FacebookSettings_get_AppIds_m213F9082C11ED86A61337946DBA5881971DB389A (void);
// 0x00000004 System.Void Facebook.Unity.Settings.FacebookSettings::set_AppIds(System.Collections.Generic.List`1<System.String>)
extern void FacebookSettings_set_AppIds_mFEAE5C06EBBFE37F87D23ABC9D6E2F52229890F8 (void);
// 0x00000005 System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppLabels()
extern void FacebookSettings_get_AppLabels_m0B090EF43E95C8FB73619F3AEF0C75D3AA3BA359 (void);
// 0x00000006 System.Void Facebook.Unity.Settings.FacebookSettings::set_AppLabels(System.Collections.Generic.List`1<System.String>)
extern void FacebookSettings_set_AppLabels_m5731795E2EDB99EB7C0F0A61C9DA7098635B7357 (void);
// 0x00000007 System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_ClientTokens()
extern void FacebookSettings_get_ClientTokens_mDF6BC3A0D7BC2CD76E1181445577437A491AA341 (void);
// 0x00000008 System.Void Facebook.Unity.Settings.FacebookSettings::set_ClientTokens(System.Collections.Generic.List`1<System.String>)
extern void FacebookSettings_set_ClientTokens_m2B44D9306D07402C28C80D052D1F4E100BB7B39E (void);
// 0x00000009 System.String Facebook.Unity.Settings.FacebookSettings::get_AppId()
extern void FacebookSettings_get_AppId_m006AE7342E7C2E7A46BEDE9BF5F1E9F3B533AF1B (void);
// 0x0000000A System.String Facebook.Unity.Settings.FacebookSettings::get_ClientToken()
extern void FacebookSettings_get_ClientToken_mA9CF546F9A73544DE706EC537F8FDE24EB8FB2A4 (void);
// 0x0000000B System.Boolean Facebook.Unity.Settings.FacebookSettings::get_IsValidAppId()
extern void FacebookSettings_get_IsValidAppId_mF0D6D09A9632E2DE2C51F515BA990C0ABABF90D9 (void);
// 0x0000000C System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Cookie()
extern void FacebookSettings_get_Cookie_m0C87EA644FF0E4AF344A849466638C196C888C20 (void);
// 0x0000000D System.Void Facebook.Unity.Settings.FacebookSettings::set_Cookie(System.Boolean)
extern void FacebookSettings_set_Cookie_m75D92C414BCD12999B0252B48E3DA92862C98C6B (void);
// 0x0000000E System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Logging()
extern void FacebookSettings_get_Logging_m693DFA3C03B6CB9BD152F452663C2CB7476AE27B (void);
// 0x0000000F System.Void Facebook.Unity.Settings.FacebookSettings::set_Logging(System.Boolean)
extern void FacebookSettings_set_Logging_mB6B75791D46B8B8F576694DEC4F2EC35F7A9EBB9 (void);
// 0x00000010 System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Status()
extern void FacebookSettings_get_Status_mE4DAEE25F212765BAEAA789789D485B393186679 (void);
// 0x00000011 System.Void Facebook.Unity.Settings.FacebookSettings::set_Status(System.Boolean)
extern void FacebookSettings_set_Status_m5B328C7ED836EACC4589E195F2D401007DC2253D (void);
// 0x00000012 System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Xfbml()
extern void FacebookSettings_get_Xfbml_m5C1F97F309C373134DD15DFCE257C209DD1AA385 (void);
// 0x00000013 System.Void Facebook.Unity.Settings.FacebookSettings::set_Xfbml(System.Boolean)
extern void FacebookSettings_set_Xfbml_mAF217018F5283A25BA1755E620CB3AC84C7EC917 (void);
// 0x00000014 System.String Facebook.Unity.Settings.FacebookSettings::get_IosURLSuffix()
extern void FacebookSettings_get_IosURLSuffix_m472E416713BBDD6653141AB0FB400A5270063B39 (void);
// 0x00000015 System.Void Facebook.Unity.Settings.FacebookSettings::set_IosURLSuffix(System.String)
extern void FacebookSettings_set_IosURLSuffix_m3C0E030A70E79C62302F1E4FDE305B4471E11F2B (void);
// 0x00000016 System.String Facebook.Unity.Settings.FacebookSettings::get_ChannelUrl()
extern void FacebookSettings_get_ChannelUrl_m136B1C28CEB1B078BA95F45F96C22FCC4CFD1B9C (void);
// 0x00000017 System.Boolean Facebook.Unity.Settings.FacebookSettings::get_FrictionlessRequests()
extern void FacebookSettings_get_FrictionlessRequests_mE87DE80B8A25477536E5C56D040BC1AB83815641 (void);
// 0x00000018 System.Void Facebook.Unity.Settings.FacebookSettings::set_FrictionlessRequests(System.Boolean)
extern void FacebookSettings_set_FrictionlessRequests_m506B05AAACB062C97B9889BC40184801ACFFE10F (void);
// 0x00000019 System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_UrlSchemes> Facebook.Unity.Settings.FacebookSettings::get_AppLinkSchemes()
extern void FacebookSettings_get_AppLinkSchemes_m981840EDB39AB30ABC84D4A7236B339797D3124D (void);
// 0x0000001A System.Void Facebook.Unity.Settings.FacebookSettings::set_AppLinkSchemes(System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_UrlSchemes>)
extern void FacebookSettings_set_AppLinkSchemes_m4626609FA7E32934E12FA655D641B6735BB788C0 (void);
// 0x0000001B System.String Facebook.Unity.Settings.FacebookSettings::get_UploadAccessToken()
extern void FacebookSettings_get_UploadAccessToken_mC2EE58F6EE93AAA5552353390EC80E27E1A91F4A (void);
// 0x0000001C System.Void Facebook.Unity.Settings.FacebookSettings::set_UploadAccessToken(System.String)
extern void FacebookSettings_set_UploadAccessToken_m0E7538A7F94CF09920976B1BB642059D0AED1A88 (void);
// 0x0000001D System.Boolean Facebook.Unity.Settings.FacebookSettings::get_AutoLogAppEventsEnabled()
extern void FacebookSettings_get_AutoLogAppEventsEnabled_m70BF5DEC22AF2E878525B1E1E5C58743B361402F (void);
// 0x0000001E System.Void Facebook.Unity.Settings.FacebookSettings::set_AutoLogAppEventsEnabled(System.Boolean)
extern void FacebookSettings_set_AutoLogAppEventsEnabled_m1D92F0AD0A3AF85A4FF5B77441757601C21F53AE (void);
// 0x0000001F System.Boolean Facebook.Unity.Settings.FacebookSettings::get_AdvertiserIDCollectionEnabled()
extern void FacebookSettings_get_AdvertiserIDCollectionEnabled_mC0BBD06C17C6DA1DE04AFF774A80D0A841FA33B0 (void);
// 0x00000020 System.Void Facebook.Unity.Settings.FacebookSettings::set_AdvertiserIDCollectionEnabled(System.Boolean)
extern void FacebookSettings_set_AdvertiserIDCollectionEnabled_m68A9D35770597F1F6E90BB60843830F890600965 (void);
// 0x00000021 Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_Instance()
extern void FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880 (void);
// 0x00000022 Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_NullableInstance()
extern void FacebookSettings_get_NullableInstance_mE0BB0B0DB862CA279FBD0C2E792EFCE753D66612 (void);
// 0x00000023 System.Void Facebook.Unity.Settings.FacebookSettings::RegisterChangeEventCallback(Facebook.Unity.Settings.FacebookSettings_OnChangeCallback)
extern void FacebookSettings_RegisterChangeEventCallback_mD13D0239F493B9701BC8891ABBC5A00980F5BA14 (void);
// 0x00000024 System.Void Facebook.Unity.Settings.FacebookSettings::UnregisterChangeEventCallback(Facebook.Unity.Settings.FacebookSettings_OnChangeCallback)
extern void FacebookSettings_UnregisterChangeEventCallback_m64059E33CBD8EDDC51B52AB839554C5BA23A8D16 (void);
// 0x00000025 System.Void Facebook.Unity.Settings.FacebookSettings::SettingsChanged()
extern void FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC (void);
// 0x00000026 System.Void Facebook.Unity.Settings.FacebookSettings::.ctor()
extern void FacebookSettings__ctor_m057BF97646BE5245A135F7D9B2357458224FF1DD (void);
// 0x00000027 System.Void Facebook.Unity.Settings.FacebookSettings::.cctor()
extern void FacebookSettings__cctor_m6E56A8CDDB184AD6C0DA6C146401499E39B30BA3 (void);
// 0x00000028 System.Void Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::.ctor(System.Object,System.IntPtr)
extern void OnChangeCallback__ctor_mB3995D4A5937814ADE512BB368B3C7823A3E6618 (void);
// 0x00000029 System.Void Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::Invoke()
extern void OnChangeCallback_Invoke_m78D9FCC43C84E280EB8C185DDED01B0D703BE9F7 (void);
// 0x0000002A System.IAsyncResult Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnChangeCallback_BeginInvoke_m49246D0FA85E4631BDC7C128360C3BF151A2B2D4 (void);
// 0x0000002B System.Void Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::EndInvoke(System.IAsyncResult)
extern void OnChangeCallback_EndInvoke_m28F0DDFBB86AB43DB00C2BEF2A16EDD9B5AD3243 (void);
// 0x0000002C System.Void Facebook.Unity.Settings.FacebookSettings_UrlSchemes::.ctor(System.Collections.Generic.List`1<System.String>)
extern void UrlSchemes__ctor_m78C1F53E102E5C40F1BC41F4573CA6E32B3979BF (void);
// 0x0000002D System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings_UrlSchemes::get_Schemes()
extern void UrlSchemes_get_Schemes_m47CCC295C6B98BC9AEA06CC5E165CE8DCF3B83BB (void);
// 0x0000002E System.Void Facebook.Unity.Settings.FacebookSettings_UrlSchemes::set_Schemes(System.Collections.Generic.List`1<System.String>)
extern void UrlSchemes_set_Schemes_mA556AE7F1B56A33A701F6C9A0834386457D4891B (void);
// 0x0000002F System.Void Facebook.Unity.Settings.FacebookSettings_<>c::.cctor()
extern void U3CU3Ec__cctor_m40EF99CA45A4E5F71A02C65A95129BFAC50AC840 (void);
// 0x00000030 System.Void Facebook.Unity.Settings.FacebookSettings_<>c::.ctor()
extern void U3CU3Ec__ctor_m66DCDF10B154EA0E16E26F3E08D86F52B2C8915F (void);
// 0x00000031 System.Void Facebook.Unity.Settings.FacebookSettings_<>c::<SettingsChanged>b__76_0(Facebook.Unity.Settings.FacebookSettings_OnChangeCallback)
extern void U3CU3Ec_U3CSettingsChangedU3Eb__76_0_m0C9EF39EDE99D2588A5985F239B388C18BBC8A66 (void);
static Il2CppMethodPointer s_methodPointers[49] = 
{
	FacebookSettings_get_SelectedAppIndex_m2EC480ADF724345DFD8E05519877A2F46A1238FC,
	FacebookSettings_set_SelectedAppIndex_mFEE448C1AC465FC83B45462B48026D700A4E468D,
	FacebookSettings_get_AppIds_m213F9082C11ED86A61337946DBA5881971DB389A,
	FacebookSettings_set_AppIds_mFEAE5C06EBBFE37F87D23ABC9D6E2F52229890F8,
	FacebookSettings_get_AppLabels_m0B090EF43E95C8FB73619F3AEF0C75D3AA3BA359,
	FacebookSettings_set_AppLabels_m5731795E2EDB99EB7C0F0A61C9DA7098635B7357,
	FacebookSettings_get_ClientTokens_mDF6BC3A0D7BC2CD76E1181445577437A491AA341,
	FacebookSettings_set_ClientTokens_m2B44D9306D07402C28C80D052D1F4E100BB7B39E,
	FacebookSettings_get_AppId_m006AE7342E7C2E7A46BEDE9BF5F1E9F3B533AF1B,
	FacebookSettings_get_ClientToken_mA9CF546F9A73544DE706EC537F8FDE24EB8FB2A4,
	FacebookSettings_get_IsValidAppId_mF0D6D09A9632E2DE2C51F515BA990C0ABABF90D9,
	FacebookSettings_get_Cookie_m0C87EA644FF0E4AF344A849466638C196C888C20,
	FacebookSettings_set_Cookie_m75D92C414BCD12999B0252B48E3DA92862C98C6B,
	FacebookSettings_get_Logging_m693DFA3C03B6CB9BD152F452663C2CB7476AE27B,
	FacebookSettings_set_Logging_mB6B75791D46B8B8F576694DEC4F2EC35F7A9EBB9,
	FacebookSettings_get_Status_mE4DAEE25F212765BAEAA789789D485B393186679,
	FacebookSettings_set_Status_m5B328C7ED836EACC4589E195F2D401007DC2253D,
	FacebookSettings_get_Xfbml_m5C1F97F309C373134DD15DFCE257C209DD1AA385,
	FacebookSettings_set_Xfbml_mAF217018F5283A25BA1755E620CB3AC84C7EC917,
	FacebookSettings_get_IosURLSuffix_m472E416713BBDD6653141AB0FB400A5270063B39,
	FacebookSettings_set_IosURLSuffix_m3C0E030A70E79C62302F1E4FDE305B4471E11F2B,
	FacebookSettings_get_ChannelUrl_m136B1C28CEB1B078BA95F45F96C22FCC4CFD1B9C,
	FacebookSettings_get_FrictionlessRequests_mE87DE80B8A25477536E5C56D040BC1AB83815641,
	FacebookSettings_set_FrictionlessRequests_m506B05AAACB062C97B9889BC40184801ACFFE10F,
	FacebookSettings_get_AppLinkSchemes_m981840EDB39AB30ABC84D4A7236B339797D3124D,
	FacebookSettings_set_AppLinkSchemes_m4626609FA7E32934E12FA655D641B6735BB788C0,
	FacebookSettings_get_UploadAccessToken_mC2EE58F6EE93AAA5552353390EC80E27E1A91F4A,
	FacebookSettings_set_UploadAccessToken_m0E7538A7F94CF09920976B1BB642059D0AED1A88,
	FacebookSettings_get_AutoLogAppEventsEnabled_m70BF5DEC22AF2E878525B1E1E5C58743B361402F,
	FacebookSettings_set_AutoLogAppEventsEnabled_m1D92F0AD0A3AF85A4FF5B77441757601C21F53AE,
	FacebookSettings_get_AdvertiserIDCollectionEnabled_mC0BBD06C17C6DA1DE04AFF774A80D0A841FA33B0,
	FacebookSettings_set_AdvertiserIDCollectionEnabled_m68A9D35770597F1F6E90BB60843830F890600965,
	FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880,
	FacebookSettings_get_NullableInstance_mE0BB0B0DB862CA279FBD0C2E792EFCE753D66612,
	FacebookSettings_RegisterChangeEventCallback_mD13D0239F493B9701BC8891ABBC5A00980F5BA14,
	FacebookSettings_UnregisterChangeEventCallback_m64059E33CBD8EDDC51B52AB839554C5BA23A8D16,
	FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC,
	FacebookSettings__ctor_m057BF97646BE5245A135F7D9B2357458224FF1DD,
	FacebookSettings__cctor_m6E56A8CDDB184AD6C0DA6C146401499E39B30BA3,
	OnChangeCallback__ctor_mB3995D4A5937814ADE512BB368B3C7823A3E6618,
	OnChangeCallback_Invoke_m78D9FCC43C84E280EB8C185DDED01B0D703BE9F7,
	OnChangeCallback_BeginInvoke_m49246D0FA85E4631BDC7C128360C3BF151A2B2D4,
	OnChangeCallback_EndInvoke_m28F0DDFBB86AB43DB00C2BEF2A16EDD9B5AD3243,
	UrlSchemes__ctor_m78C1F53E102E5C40F1BC41F4573CA6E32B3979BF,
	UrlSchemes_get_Schemes_m47CCC295C6B98BC9AEA06CC5E165CE8DCF3B83BB,
	UrlSchemes_set_Schemes_mA556AE7F1B56A33A701F6C9A0834386457D4891B,
	U3CU3Ec__cctor_m40EF99CA45A4E5F71A02C65A95129BFAC50AC840,
	U3CU3Ec__ctor_m66DCDF10B154EA0E16E26F3E08D86F52B2C8915F,
	U3CU3Ec_U3CSettingsChangedU3Eb__76_0_m0C9EF39EDE99D2588A5985F239B388C18BBC8A66,
};
static const int32_t s_InvokerIndices[49] = 
{
	106,
	164,
	4,
	154,
	4,
	154,
	4,
	154,
	4,
	4,
	49,
	49,
	821,
	49,
	821,
	49,
	821,
	49,
	821,
	4,
	154,
	4,
	49,
	821,
	4,
	154,
	4,
	154,
	49,
	821,
	49,
	821,
	4,
	4,
	154,
	154,
	3,
	23,
	3,
	124,
	23,
	105,
	26,
	26,
	14,
	26,
	3,
	23,
	26,
};
extern const Il2CppCodeGenModule g_Facebook_Unity_SettingsCodeGenModule;
const Il2CppCodeGenModule g_Facebook_Unity_SettingsCodeGenModule = 
{
	"Facebook.Unity.Settings.dll",
	49,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
