﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// BlockSpawnController
struct BlockSpawnController_tA7EE1271EC33CDCE82BF8A6E8C6B225A02BAE516;
// FinishLinePosition
struct FinishLinePosition_tC83CE07F7971D2D016843ED2F59E6E9C5008D6FE;
// GUIManager
struct GUIManager_tDFF7E793464A0B061DBB5565BC73EC9457BA75BB;
// GameAnalyticsSDK.GameAnalytics
struct GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E;
// GameAnalyticsSDK.Setup.Settings
struct Settings_tAF7F5B61F6B519B10CED42C18502A4ED0DD3FE49;
// GameManager
struct GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89;
// LevelController
struct LevelController_tF5B6CBA811D10D3EEAC896B8FCFF852C3D58930D;
// Losepopup
struct Losepopup_t8B2116C321F683CF632AF47B4D4405FB71E1FFBA;
// ScoreController
struct ScoreController_t922A5B08EFA00C15E3A48C8AD2C0729E9B2D5207;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.Action`2<System.Object,System.Single>
struct Action_2_t69121CC132451DFD4D151CC9FEEFC91909A937A0;
// System.Action`2<System.String,System.Single>
struct Action_2_tD053D0BAA29F99F68652212454226A67BE949793;
// System.Action`4<System.Boolean,System.Single,System.Object,System.Object>
struct Action_4_tF771E40FB7247CDC261F257C3C983FD4D3A42FE9;
// System.Action`4<System.Boolean,System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct Action_4_tCEEFFACC1F7C2A50B1B6A47E17A88F7AC26C5AEB;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Object>[]
struct EntryU5BU5D_tED6676A6535375B72FAC3BE58CAB9DC30BE201CD;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.String>[]
struct EntryU5BU5D_t034347107F1D23C91DE1D712EA637D904789415C;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Object>
struct KeyCollection_t9BB02C59F6A73E760F875CCD8AA6D1A1ACE61970;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>
struct KeyCollection_tC73654392B284B89334464107B696C9BD89776D9;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Object>
struct ValueCollection_t059E2E9F339047F683CA7038812E624189E7F0CD;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.String>
struct ValueCollection_tA3B972EF56F7C97E35054155C33556C55FAAFD43;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1F07EAC22CC1D4F279164B144240E4718BD7E7A9;
// System.Collections.Generic.List`1<System.Xml.XmlQualifiedName>
struct List_1_tCAF7C69A9EA5F50BF5587DD8E7657B89C6866626;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0;
// System.Collections.Generic.Queue`1<Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/GameAnalyticsEvent>
struct Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerable
struct IEnumerable_tD74549CEA1AA48E768382B94FEACBB07E2E3FA2C;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.Xml.DomNameTable
struct DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A;
// System.Xml.EmptyEnumerator
struct EmptyEnumerator_t7702FAD652CDD694DC3D136712CA48C48FFB0DC3;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t2FE768B806BA73C644AEE436491F2C3E04039CF1;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F;
// System.Xml.XmlAttribute
struct XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA;
// System.Xml.XmlAttributeCollection
struct XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E;
// System.Xml.XmlDocument
struct XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97;
// System.Xml.XmlElement
struct XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC;
// System.Xml.XmlImplementation
struct XmlImplementation_t7C01D70C3943C7AA929C106D360E90E2D576E1EF;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E;
// System.Xml.XmlName
struct XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682;
// System.Xml.XmlNamedNodeMap
struct XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31;
// System.Xml.XmlNode
struct XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB;
// System.Xml.XmlNodeChangedEventHandler
struct XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838;
// System.Xml.XmlResolver
struct XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280;
// System.Xml.XmlWriter
struct XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869;
// System.Xml.XmlWriterSettings
struct XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.ILogger
struct ILogger_t572B66532D8EB6E76240476A788384A26D70866F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734;
// UnityEngine.TextGenerator
struct TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.FontData
struct FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// Voodoo.Sauce.Internal.Analytics.AnalyticsManager/<>c__DisplayClass17_0
struct U3CU3Ec__DisplayClass17_0_t5B0D56F5953B463EFAEE9D14CBA56AEF599C09D1;
// Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider
struct GameAnalyticsProvider_tB512F4FF8602E4C3A071276A4AF633834E91EB20;
// Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/DesignEvent
struct DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82;
// Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/GameAnalyticsEvent
struct GameAnalyticsEvent_tC785053EBE27E395C98E2111584454333DD866D2;
// Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/GameAnalyticsEvent[]
struct GameAnalyticsEventU5BU5D_tCD7ED58D068104061E3D5EAC3E58DD8FBE79912E;
// Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/ProgressEvent
struct ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3;
// Voodoo.Sauce.Internal.Analytics.IAnalyticsProvider
struct IAnalyticsProvider_t068EAC8917C2BBC1AF8AD85E21D41F2B23EBA931;
// Voodoo.Sauce.Internal.TinySauceBehaviour
struct TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5;
// Voodoo.Sauce.Internal.TinySauceSettings
struct TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B;
// Winpopup
struct Winpopup_t3BEDB5A4227E29A985A517FB8F2214ABA2396F0C;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tD053D0BAA29F99F68652212454226A67BE949793_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_4_tCEEFFACC1F7C2A50B1B6A47E17A88F7AC26C5AEB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AnalyticsManager_t3183DE15E51166BDEC995DB2C8952B1950C82148_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GAProgressionStatus_tBA172450460B29A032AA13C714D6A2CADD010E62_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IAnalyticsProvider_t068EAC8917C2BBC1AF8AD85E21D41F2B23EBA931_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerable_tD74549CEA1AA48E768382B94FEACBB07E2E3FA2C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ILogger_t572B66532D8EB6E76240476A788384A26D70866F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VoodooLog_t3E10EB33560DF558C5ADC45DF8FFF5ADCFFF9A24_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral097ECD89364746904BE8F4DCBC24E564C943F7F8;
IL2CPP_EXTERN_C String_t* _stringLiteral099600A10A944114AAC406D136B625FB416DD779;
IL2CPP_EXTERN_C String_t* _stringLiteral19E4C1BBB9DCB1CCCF84033DF6077C233FAA301C;
IL2CPP_EXTERN_C String_t* _stringLiteral1C9C5631B79CAE61BFD7B812E162914C6B04C5FB;
IL2CPP_EXTERN_C String_t* _stringLiteral1D92DDEB89F9C2D23B93C5046B3EF3A6C20A635F;
IL2CPP_EXTERN_C String_t* _stringLiteral4D8E850DA5E1602C6FE45219DAAD6E932B115613;
IL2CPP_EXTERN_C String_t* _stringLiteral4FDCDF9A6AD02645E48F4C62332CACDFBFCC6BF5;
IL2CPP_EXTERN_C String_t* _stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C;
IL2CPP_EXTERN_C String_t* _stringLiteral70D6D05A55F416E3D28E1C5CB4D517D7495F4E19;
IL2CPP_EXTERN_C String_t* _stringLiteral7C7F5D049FAD2569721D446C4A811F9BD5DA5393;
IL2CPP_EXTERN_C String_t* _stringLiteral8BE4551C194CDBAEA1CA023939C560EA448E1CB4;
IL2CPP_EXTERN_C String_t* _stringLiteral9C96764F709FCE75AFDF092580F09418CB63AB15;
IL2CPP_EXTERN_C String_t* _stringLiteralA5A3766367B4512FF38BBC68AA06EDC7CA9B95C0;
IL2CPP_EXTERN_C String_t* _stringLiteralAB478F3EFC840EEBAB919DFF1B9512286F70C10C;
IL2CPP_EXTERN_C String_t* _stringLiteralACB138D47B861B40670D08FD59CB04F3A5D13FD9;
IL2CPP_EXTERN_C String_t* _stringLiteralB015E89A88FE62554D403ED2FC90E599C38D3D82;
IL2CPP_EXTERN_C String_t* _stringLiteralBA8AB5A0280B953AA97435FF8946CBCBB2755A27;
IL2CPP_EXTERN_C String_t* _stringLiteralC5BEEF2DE776A9EB64B6A612ABEAC7157E1E00A4;
IL2CPP_EXTERN_C String_t* _stringLiteralD2005CC206CCBFDEDF2BE43A200CB050C538BDB5;
IL2CPP_EXTERN_C String_t* _stringLiteralD3C3F362DE50006549F1D2418450180E59B63353;
IL2CPP_EXTERN_C String_t* _stringLiteralD7525D28A8645F1ED44D717CC1E92F39E9D6EB3E;
IL2CPP_EXTERN_C String_t* _stringLiteralE4BBE5B7A4C1EB55652965AEE885DD59BD2EE7F4;
IL2CPP_EXTERN_C String_t* _stringLiteralE59BD5CB77309247AE077BC6D900A334ED2E99CC;
IL2CPP_EXTERN_C String_t* _stringLiteralEAEF99A09E561A86004BAEB87A80BB4CFCE8CE67;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_2__ctor_m24FAA059B2074E94759D35E159056E5D30671E04_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_4__ctor_m71C0D7B7176F00D40BEF875619B60EB87DF02BE6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_GetEnumerator_m3378B4792B81EF81397CB9D9A761BD7149BD27F5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m16C0E963A012498CD27422B463DB327BA4C7A321_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m6E6A22A8620F5A5582BB67E367BE5086D7D895A6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mBEC9B470213860581893E0F197CAAE657B8B6C69_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameAnalyticsProvider_OnGameFinished_m9F975CB121DDE865EDDDF875B70FD83136B2A9D8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameAnalyticsProvider_OnGameStarted_mEAC6338759F4E680D93D78C630694211CCA4401E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameAnalyticsProvider_TrackCustomEvent_m06AEFC1C2384CDF19C60A23DCFB227387598565C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameAnalyticsProvider_TrackCustomEvent_mB625F71BC4E98012C3393805C0DBAB9FD5F2C66A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisGameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E_m3EFE312F9182AD6A1F6D1400B06C364312A74864_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyValuePair_2_get_Key_m434E29A1251E81B5A2124466105823011C462BF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyValuePair_2_get_Value_mEAF4B15DEEAC6EB29683A5C6569F0F50B4DEBDA2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1__ctor_m11F9C228CFDF836DDFCD7880C09CB4098AB9D7F2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1__ctor_mAB819EE77BD7E38C1E0E494C307D52F42DB259AE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_HasValue_m6C7E30F1E2D85F0A4AB37F0F6685E37607F26231_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_HasValue_mB664E2C41CADA8413EF8842E6601B8C696A7CE15_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_Value_m8ED77F1776BBC65874AF9D0ED769FF7B6B918DA2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_Value_mA8BB683CA6A8C5BF448A737FB5A2AF63C730B3E5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisGameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E_m756FB0FECAAC6808A917DF06536CB78C8FBCEDDC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Queue_1_Clear_mFA93B7441B1DE6E0C9B424A6B1F7F397FFE1027C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Queue_1_Dequeue_mF79FD12769126B693964FFFAC56583A24767CFC5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Queue_1_Enqueue_m983F9917753FF76DDDB83133E87A476CFCB0474F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Queue_1__ctor_mD8195A92FD232CFFF98EB6FC64127CE36F972241_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Queue_1_get_Count_mDA340F16177C88272289E749C4E60CE81F4784B7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Resources_Load_TisTinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B_m5A64B8561C0240EE2D968668B0B3A97CFC58B058_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Singleton_1_get_instance_m0D5DA7AAAEF2F4368826A7C6C88F129C79B6EE9B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TinySauceBehaviour_Awake_mFAAA01A24AFF626349B4700FE195B4249FB2CCF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t DesignEvent_Track_mEAC9146E85F410568B20984DC4D56AFF9E940D66_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameAnalyticsProvider_Initialize_m294805F45E57906DEE3B2E7633EF71BAC8F14354_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameAnalyticsProvider_OnGameFinished_m9F975CB121DDE865EDDDF875B70FD83136B2A9D8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameAnalyticsProvider_OnGameStarted_mEAC6338759F4E680D93D78C630694211CCA4401E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameAnalyticsProvider_RegisterEvents_mDE6B88888D1EADBC9A28A94FA3ADB62F3D822388_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameAnalyticsProvider_TrackCustomEvent_m06AEFC1C2384CDF19C60A23DCFB227387598565C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameAnalyticsProvider_TrackCustomEvent_mB625F71BC4E98012C3393805C0DBAB9FD5F2C66A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameAnalyticsProvider_UnregisterEvents_mC334E0CD3E15B9E6775C12989F863BD09A2A50F8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameAnalyticsWrapper_Disable_m60C77A631514F58F9C58B4001B27DC46A3D5D22C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameAnalyticsWrapper_Initialize_m6A2269DECEC4E5C5D8107255BBF78CE06DA96FE5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameAnalyticsWrapper_InstantiateGameAnalytics_mD26002F3EBDA3080C19C03B6ACFCDBF428A6D199_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameAnalyticsWrapper_TrackDesignEvent_mEFBC717B6DC026CB9A3CA0A3B9EE55B576D15040_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameAnalyticsWrapper_TrackProgressEvent_m7DE043963ECAC5503F3118CCDD85775EA66B0664_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameAnalyticsWrapper__cctor_m53D4076D48F07C37FAFF4D0FCB728A7B238DFA51_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ManifestUtils_Add_mBFB87E1A709BF3A7EBD8A6D26F6B4FCBBBB66675_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ManifestUtils_FindAttribute_mADD12182712B3FF205596CDA738DE551C111C7A2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ManifestUtils_FindElementWithAndroidName_m040EF519E28229C677A7625260255A813176EC39_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ManifestUtils_GetAndroidElementName_mC68A244A12CA5110AE421509EC1D90B18F30B050_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ManifestUtils_LoadFromFile_m6D0336D0716E4CBF02268B16E0D7099939C301D8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ManifestUtils_LoadFromString_m49896DCFF05217BCE4279ACF6CDE7FC7B607825F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ManifestUtils_ReplaceKeys_mC82D1EA16D1BB27023A9C83F66B8C59971A0CFD1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ManifestUtils_SaveDocumentInFile_m7BB668EEBB671FCC3F827635800D4EA483725B57_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ManifestUtils_SetApplicationAttributes_mD8E4D1597CCD5EB79D87947EB02AC5B781B9A759_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ProgressEvent_Track_m756CD0C4E2F7DDA8217449F7B1470385323B9322_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TinySauceBehaviour_Awake_mFAAA01A24AFF626349B4700FE195B4249FB2CCF2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TinySauceBehaviour_InitAnalytics_m0E97CA2D3FA96886CD3E6FB732B13084D73C90FA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TinySauceBehaviour_OnApplicationPause_m6EA76A02E0EB2780ECE18C572E36F494375C792F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TinySauceSettings_Load_m6B57575CD43B33BAE0FDDDC7CEDCCC3CF850C747_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass17_0_U3CInitializeU3Eb__0_mEC0C58D2E555B5F9A7A2BA7BDA7D27F292CDD1A7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoodooLog_DisableLogs_mB02FC2E65F7485E3AC4F142F6550E5CC21A90B69_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoodooLog_EnableLogs_mCF301D8664674A872023E1673BD6A4A65D989B46_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoodooLog_Format_m3B92528E7DEBA4AE65FE81F97571702522A85DFE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoodooLog_Initialize_m9C4412D348DE823CC4535F0327D0985EC68B5B74_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoodooLog_IsLogsEnabled_mD4375E25A3B9DDFDF46A87CEA0E869977AAE749B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoodooLog_LogE_mE4E1FC2258F9C025F5E965B9D6FE302F8346ED69_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoodooLog_LogW_m1B8171158AD9DCD531714EA008F3EE5F6115ED74_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoodooLog_Log_mA7C46EE6B665A3BCC85F83F844B24039AEE02D71_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Winpopup_NextLevel_m32266E8FE87DC639A4A06D85480C43F2806B7FC7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Winpopup_Visible_m1A79AFA1415DE6AFA54D3736B3E030C85C67588D_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct  Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tED6676A6535375B72FAC3BE58CAB9DC30BE201CD* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t9BB02C59F6A73E760F875CCD8AA6D1A1ACE61970 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t059E2E9F339047F683CA7038812E624189E7F0CD * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___entries_1)); }
	inline EntryU5BU5D_tED6676A6535375B72FAC3BE58CAB9DC30BE201CD* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tED6676A6535375B72FAC3BE58CAB9DC30BE201CD** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tED6676A6535375B72FAC3BE58CAB9DC30BE201CD* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___keys_7)); }
	inline KeyCollection_t9BB02C59F6A73E760F875CCD8AA6D1A1ACE61970 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t9BB02C59F6A73E760F875CCD8AA6D1A1ACE61970 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t9BB02C59F6A73E760F875CCD8AA6D1A1ACE61970 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___values_8)); }
	inline ValueCollection_t059E2E9F339047F683CA7038812E624189E7F0CD * get_values_8() const { return ___values_8; }
	inline ValueCollection_t059E2E9F339047F683CA7038812E624189E7F0CD ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t059E2E9F339047F683CA7038812E624189E7F0CD * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct  Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t034347107F1D23C91DE1D712EA637D904789415C* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_tC73654392B284B89334464107B696C9BD89776D9 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tA3B972EF56F7C97E35054155C33556C55FAAFD43 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___entries_1)); }
	inline EntryU5BU5D_t034347107F1D23C91DE1D712EA637D904789415C* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t034347107F1D23C91DE1D712EA637D904789415C** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t034347107F1D23C91DE1D712EA637D904789415C* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___keys_7)); }
	inline KeyCollection_tC73654392B284B89334464107B696C9BD89776D9 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_tC73654392B284B89334464107B696C9BD89776D9 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_tC73654392B284B89334464107B696C9BD89776D9 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___values_8)); }
	inline ValueCollection_tA3B972EF56F7C97E35054155C33556C55FAAFD43 * get_values_8() const { return ___values_8; }
	inline ValueCollection_tA3B972EF56F7C97E35054155C33556C55FAAFD43 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tA3B972EF56F7C97E35054155C33556C55FAAFD43 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.Queue`1<System.Object>
struct  Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_4;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0, ____array_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__array_0() const { return ____array_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__tail_2() { return static_cast<int32_t>(offsetof(Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0, ____tail_2)); }
	inline int32_t get__tail_2() const { return ____tail_2; }
	inline int32_t* get_address_of__tail_2() { return &____tail_2; }
	inline void set__tail_2(int32_t value)
	{
		____tail_2 = value;
	}

	inline static int32_t get_offset_of__size_3() { return static_cast<int32_t>(offsetof(Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0, ____size_3)); }
	inline int32_t get__size_3() const { return ____size_3; }
	inline int32_t* get_address_of__size_3() { return &____size_3; }
	inline void set__size_3(int32_t value)
	{
		____size_3 = value;
	}

	inline static int32_t get_offset_of__version_4() { return static_cast<int32_t>(offsetof(Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0, ____version_4)); }
	inline int32_t get__version_4() const { return ____version_4; }
	inline int32_t* get_address_of__version_4() { return &____version_4; }
	inline void set__version_4(int32_t value)
	{
		____version_4 = value;
	}
};


// System.Collections.Generic.Queue`1<Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_GameAnalyticsEvent>
struct  Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	GameAnalyticsEventU5BU5D_tCD7ED58D068104061E3D5EAC3E58DD8FBE79912E* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_4;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3, ____array_0)); }
	inline GameAnalyticsEventU5BU5D_tCD7ED58D068104061E3D5EAC3E58DD8FBE79912E* get__array_0() const { return ____array_0; }
	inline GameAnalyticsEventU5BU5D_tCD7ED58D068104061E3D5EAC3E58DD8FBE79912E** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(GameAnalyticsEventU5BU5D_tCD7ED58D068104061E3D5EAC3E58DD8FBE79912E* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__tail_2() { return static_cast<int32_t>(offsetof(Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3, ____tail_2)); }
	inline int32_t get__tail_2() const { return ____tail_2; }
	inline int32_t* get_address_of__tail_2() { return &____tail_2; }
	inline void set__tail_2(int32_t value)
	{
		____tail_2 = value;
	}

	inline static int32_t get_offset_of__size_3() { return static_cast<int32_t>(offsetof(Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3, ____size_3)); }
	inline int32_t get__size_3() const { return ____size_3; }
	inline int32_t* get_address_of__size_3() { return &____size_3; }
	inline void set__size_3(int32_t value)
	{
		____size_3 = value;
	}

	inline static int32_t get_offset_of__version_4() { return static_cast<int32_t>(offsetof(Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3, ____version_4)); }
	inline int32_t get__version_4() const { return ____version_4; }
	inline int32_t* get_address_of__version_4() { return &____version_4; }
	inline void set__version_4(int32_t value)
	{
		____version_4 = value;
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// System.Xml.XmlNode
struct  XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNode::parentNode
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___parentNode_0;

public:
	inline static int32_t get_offset_of_parentNode_0() { return static_cast<int32_t>(offsetof(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB, ___parentNode_0)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_parentNode_0() const { return ___parentNode_0; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_parentNode_0() { return &___parentNode_0; }
	inline void set_parentNode_0(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___parentNode_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parentNode_0), (void*)value);
	}
};


// System.Xml.XmlNodeList
struct  XmlNodeList_t6A2162EDB563F1707F00C5156460E1073244C8E7  : public RuntimeObject
{
public:

public:
};


// System.Xml.XmlWriter
struct  XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869  : public RuntimeObject
{
public:

public:
};


// Voodoo.Sauce.Internal.Analytics.AnalyticsManager_<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_t5B0D56F5953B463EFAEE9D14CBA56AEF599C09D1  : public RuntimeObject
{
public:
	// System.Boolean Voodoo.Sauce.Internal.Analytics.AnalyticsManager_<>c__DisplayClass17_0::consent
	bool ___consent_0;

public:
	inline static int32_t get_offset_of_consent_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t5B0D56F5953B463EFAEE9D14CBA56AEF599C09D1, ___consent_0)); }
	inline bool get_consent_0() const { return ___consent_0; }
	inline bool* get_address_of_consent_0() { return &___consent_0; }
	inline void set_consent_0(bool value)
	{
		___consent_0 = value;
	}
};


// Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider
struct  GameAnalyticsProvider_tB512F4FF8602E4C3A071276A4AF633834E91EB20  : public RuntimeObject
{
public:

public:
};


// Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper
struct  GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC  : public RuntimeObject
{
public:

public:
};

struct GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields
{
public:
	// System.Boolean Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::_isInitialized
	bool ____isInitialized_1;
	// System.Boolean Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::_isDisabled
	bool ____isDisabled_2;
	// System.Collections.Generic.Queue`1<Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_GameAnalyticsEvent> Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::QueuedEvents
	Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * ___QueuedEvents_3;

public:
	inline static int32_t get_offset_of__isInitialized_1() { return static_cast<int32_t>(offsetof(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields, ____isInitialized_1)); }
	inline bool get__isInitialized_1() const { return ____isInitialized_1; }
	inline bool* get_address_of__isInitialized_1() { return &____isInitialized_1; }
	inline void set__isInitialized_1(bool value)
	{
		____isInitialized_1 = value;
	}

	inline static int32_t get_offset_of__isDisabled_2() { return static_cast<int32_t>(offsetof(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields, ____isDisabled_2)); }
	inline bool get__isDisabled_2() const { return ____isDisabled_2; }
	inline bool* get_address_of__isDisabled_2() { return &____isDisabled_2; }
	inline void set__isDisabled_2(bool value)
	{
		____isDisabled_2 = value;
	}

	inline static int32_t get_offset_of_QueuedEvents_3() { return static_cast<int32_t>(offsetof(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields, ___QueuedEvents_3)); }
	inline Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * get_QueuedEvents_3() const { return ___QueuedEvents_3; }
	inline Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 ** get_address_of_QueuedEvents_3() { return &___QueuedEvents_3; }
	inline void set_QueuedEvents_3(Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * value)
	{
		___QueuedEvents_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___QueuedEvents_3), (void*)value);
	}
};


// Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_GameAnalyticsEvent
struct  GameAnalyticsEvent_tC785053EBE27E395C98E2111584454333DD866D2  : public RuntimeObject
{
public:

public:
};


// Voodoo.Sauce.Internal.Utils.ManifestUtils
struct  ManifestUtils_t0C1D6D398072D21FCD8DC3F5DF05F252D770A950  : public RuntimeObject
{
public:

public:
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct  Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_1), (void*)value);
	}
};


// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
struct  KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_1), (void*)value);
	}
};


// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth365_29), (void*)value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth366_30), (void*)value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<System.Single>
struct  Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// System.Xml.XmlAttribute
struct  XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA  : public XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB
{
public:
	// System.Xml.XmlName System.Xml.XmlAttribute::name
	XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * ___name_1;
	// System.Xml.XmlLinkedNode System.Xml.XmlAttribute::lastChild
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * ___lastChild_2;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA, ___name_1)); }
	inline XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * get_name_1() const { return ___name_1; }
	inline XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 ** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_1), (void*)value);
	}

	inline static int32_t get_offset_of_lastChild_2() { return static_cast<int32_t>(offsetof(XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA, ___lastChild_2)); }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * get_lastChild_2() const { return ___lastChild_2; }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E ** get_address_of_lastChild_2() { return &___lastChild_2; }
	inline void set_lastChild_2(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * value)
	{
		___lastChild_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastChild_2), (void*)value);
	}
};


// System.Xml.XmlDocument
struct  XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97  : public XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB
{
public:
	// System.Xml.XmlImplementation System.Xml.XmlDocument::implementation
	XmlImplementation_t7C01D70C3943C7AA929C106D360E90E2D576E1EF * ___implementation_1;
	// System.Xml.DomNameTable System.Xml.XmlDocument::domNameTable
	DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A * ___domNameTable_2;
	// System.Xml.XmlLinkedNode System.Xml.XmlDocument::lastChild
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * ___lastChild_3;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocument::entities
	XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 * ___entities_4;
	// System.Collections.Hashtable System.Xml.XmlDocument::htElementIdMap
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___htElementIdMap_5;
	// System.Collections.Hashtable System.Xml.XmlDocument::htElementIDAttrDecl
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___htElementIDAttrDecl_6;
	// System.Xml.Schema.SchemaInfo System.Xml.XmlDocument::schemaInfo
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * ___schemaInfo_7;
	// System.Xml.Schema.XmlSchemaSet System.Xml.XmlDocument::schemas
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * ___schemas_8;
	// System.Boolean System.Xml.XmlDocument::reportValidity
	bool ___reportValidity_9;
	// System.Boolean System.Xml.XmlDocument::actualLoadingStatus
	bool ___actualLoadingStatus_10;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeInsertingDelegate
	XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * ___onNodeInsertingDelegate_11;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeInsertedDelegate
	XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * ___onNodeInsertedDelegate_12;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeRemovingDelegate
	XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * ___onNodeRemovingDelegate_13;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeRemovedDelegate
	XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * ___onNodeRemovedDelegate_14;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeChangingDelegate
	XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * ___onNodeChangingDelegate_15;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeChangedDelegate
	XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * ___onNodeChangedDelegate_16;
	// System.Boolean System.Xml.XmlDocument::fEntRefNodesPresent
	bool ___fEntRefNodesPresent_17;
	// System.Boolean System.Xml.XmlDocument::fCDataNodesPresent
	bool ___fCDataNodesPresent_18;
	// System.Boolean System.Xml.XmlDocument::preserveWhitespace
	bool ___preserveWhitespace_19;
	// System.Boolean System.Xml.XmlDocument::isLoading
	bool ___isLoading_20;
	// System.String System.Xml.XmlDocument::strDocumentName
	String_t* ___strDocumentName_21;
	// System.String System.Xml.XmlDocument::strDocumentFragmentName
	String_t* ___strDocumentFragmentName_22;
	// System.String System.Xml.XmlDocument::strCommentName
	String_t* ___strCommentName_23;
	// System.String System.Xml.XmlDocument::strTextName
	String_t* ___strTextName_24;
	// System.String System.Xml.XmlDocument::strCDataSectionName
	String_t* ___strCDataSectionName_25;
	// System.String System.Xml.XmlDocument::strEntityName
	String_t* ___strEntityName_26;
	// System.String System.Xml.XmlDocument::strID
	String_t* ___strID_27;
	// System.String System.Xml.XmlDocument::strXmlns
	String_t* ___strXmlns_28;
	// System.String System.Xml.XmlDocument::strXml
	String_t* ___strXml_29;
	// System.String System.Xml.XmlDocument::strSpace
	String_t* ___strSpace_30;
	// System.String System.Xml.XmlDocument::strLang
	String_t* ___strLang_31;
	// System.String System.Xml.XmlDocument::strEmpty
	String_t* ___strEmpty_32;
	// System.String System.Xml.XmlDocument::strNonSignificantWhitespaceName
	String_t* ___strNonSignificantWhitespaceName_33;
	// System.String System.Xml.XmlDocument::strSignificantWhitespaceName
	String_t* ___strSignificantWhitespaceName_34;
	// System.String System.Xml.XmlDocument::strReservedXmlns
	String_t* ___strReservedXmlns_35;
	// System.String System.Xml.XmlDocument::strReservedXml
	String_t* ___strReservedXml_36;
	// System.String System.Xml.XmlDocument::baseURI
	String_t* ___baseURI_37;
	// System.Xml.XmlResolver System.Xml.XmlDocument::resolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___resolver_38;
	// System.Boolean System.Xml.XmlDocument::bSetResolver
	bool ___bSetResolver_39;
	// System.Object System.Xml.XmlDocument::objLock
	RuntimeObject * ___objLock_40;

public:
	inline static int32_t get_offset_of_implementation_1() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___implementation_1)); }
	inline XmlImplementation_t7C01D70C3943C7AA929C106D360E90E2D576E1EF * get_implementation_1() const { return ___implementation_1; }
	inline XmlImplementation_t7C01D70C3943C7AA929C106D360E90E2D576E1EF ** get_address_of_implementation_1() { return &___implementation_1; }
	inline void set_implementation_1(XmlImplementation_t7C01D70C3943C7AA929C106D360E90E2D576E1EF * value)
	{
		___implementation_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___implementation_1), (void*)value);
	}

	inline static int32_t get_offset_of_domNameTable_2() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___domNameTable_2)); }
	inline DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A * get_domNameTable_2() const { return ___domNameTable_2; }
	inline DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A ** get_address_of_domNameTable_2() { return &___domNameTable_2; }
	inline void set_domNameTable_2(DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A * value)
	{
		___domNameTable_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___domNameTable_2), (void*)value);
	}

	inline static int32_t get_offset_of_lastChild_3() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___lastChild_3)); }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * get_lastChild_3() const { return ___lastChild_3; }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E ** get_address_of_lastChild_3() { return &___lastChild_3; }
	inline void set_lastChild_3(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * value)
	{
		___lastChild_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastChild_3), (void*)value);
	}

	inline static int32_t get_offset_of_entities_4() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___entities_4)); }
	inline XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 * get_entities_4() const { return ___entities_4; }
	inline XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 ** get_address_of_entities_4() { return &___entities_4; }
	inline void set_entities_4(XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 * value)
	{
		___entities_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entities_4), (void*)value);
	}

	inline static int32_t get_offset_of_htElementIdMap_5() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___htElementIdMap_5)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_htElementIdMap_5() const { return ___htElementIdMap_5; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_htElementIdMap_5() { return &___htElementIdMap_5; }
	inline void set_htElementIdMap_5(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___htElementIdMap_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___htElementIdMap_5), (void*)value);
	}

	inline static int32_t get_offset_of_htElementIDAttrDecl_6() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___htElementIDAttrDecl_6)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_htElementIDAttrDecl_6() const { return ___htElementIDAttrDecl_6; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_htElementIDAttrDecl_6() { return &___htElementIDAttrDecl_6; }
	inline void set_htElementIDAttrDecl_6(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___htElementIDAttrDecl_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___htElementIDAttrDecl_6), (void*)value);
	}

	inline static int32_t get_offset_of_schemaInfo_7() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___schemaInfo_7)); }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * get_schemaInfo_7() const { return ___schemaInfo_7; }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 ** get_address_of_schemaInfo_7() { return &___schemaInfo_7; }
	inline void set_schemaInfo_7(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * value)
	{
		___schemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___schemaInfo_7), (void*)value);
	}

	inline static int32_t get_offset_of_schemas_8() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___schemas_8)); }
	inline XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * get_schemas_8() const { return ___schemas_8; }
	inline XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F ** get_address_of_schemas_8() { return &___schemas_8; }
	inline void set_schemas_8(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * value)
	{
		___schemas_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___schemas_8), (void*)value);
	}

	inline static int32_t get_offset_of_reportValidity_9() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___reportValidity_9)); }
	inline bool get_reportValidity_9() const { return ___reportValidity_9; }
	inline bool* get_address_of_reportValidity_9() { return &___reportValidity_9; }
	inline void set_reportValidity_9(bool value)
	{
		___reportValidity_9 = value;
	}

	inline static int32_t get_offset_of_actualLoadingStatus_10() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___actualLoadingStatus_10)); }
	inline bool get_actualLoadingStatus_10() const { return ___actualLoadingStatus_10; }
	inline bool* get_address_of_actualLoadingStatus_10() { return &___actualLoadingStatus_10; }
	inline void set_actualLoadingStatus_10(bool value)
	{
		___actualLoadingStatus_10 = value;
	}

	inline static int32_t get_offset_of_onNodeInsertingDelegate_11() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___onNodeInsertingDelegate_11)); }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * get_onNodeInsertingDelegate_11() const { return ___onNodeInsertingDelegate_11; }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 ** get_address_of_onNodeInsertingDelegate_11() { return &___onNodeInsertingDelegate_11; }
	inline void set_onNodeInsertingDelegate_11(XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * value)
	{
		___onNodeInsertingDelegate_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onNodeInsertingDelegate_11), (void*)value);
	}

	inline static int32_t get_offset_of_onNodeInsertedDelegate_12() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___onNodeInsertedDelegate_12)); }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * get_onNodeInsertedDelegate_12() const { return ___onNodeInsertedDelegate_12; }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 ** get_address_of_onNodeInsertedDelegate_12() { return &___onNodeInsertedDelegate_12; }
	inline void set_onNodeInsertedDelegate_12(XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * value)
	{
		___onNodeInsertedDelegate_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onNodeInsertedDelegate_12), (void*)value);
	}

	inline static int32_t get_offset_of_onNodeRemovingDelegate_13() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___onNodeRemovingDelegate_13)); }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * get_onNodeRemovingDelegate_13() const { return ___onNodeRemovingDelegate_13; }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 ** get_address_of_onNodeRemovingDelegate_13() { return &___onNodeRemovingDelegate_13; }
	inline void set_onNodeRemovingDelegate_13(XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * value)
	{
		___onNodeRemovingDelegate_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onNodeRemovingDelegate_13), (void*)value);
	}

	inline static int32_t get_offset_of_onNodeRemovedDelegate_14() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___onNodeRemovedDelegate_14)); }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * get_onNodeRemovedDelegate_14() const { return ___onNodeRemovedDelegate_14; }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 ** get_address_of_onNodeRemovedDelegate_14() { return &___onNodeRemovedDelegate_14; }
	inline void set_onNodeRemovedDelegate_14(XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * value)
	{
		___onNodeRemovedDelegate_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onNodeRemovedDelegate_14), (void*)value);
	}

	inline static int32_t get_offset_of_onNodeChangingDelegate_15() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___onNodeChangingDelegate_15)); }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * get_onNodeChangingDelegate_15() const { return ___onNodeChangingDelegate_15; }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 ** get_address_of_onNodeChangingDelegate_15() { return &___onNodeChangingDelegate_15; }
	inline void set_onNodeChangingDelegate_15(XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * value)
	{
		___onNodeChangingDelegate_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onNodeChangingDelegate_15), (void*)value);
	}

	inline static int32_t get_offset_of_onNodeChangedDelegate_16() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___onNodeChangedDelegate_16)); }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * get_onNodeChangedDelegate_16() const { return ___onNodeChangedDelegate_16; }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 ** get_address_of_onNodeChangedDelegate_16() { return &___onNodeChangedDelegate_16; }
	inline void set_onNodeChangedDelegate_16(XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * value)
	{
		___onNodeChangedDelegate_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onNodeChangedDelegate_16), (void*)value);
	}

	inline static int32_t get_offset_of_fEntRefNodesPresent_17() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___fEntRefNodesPresent_17)); }
	inline bool get_fEntRefNodesPresent_17() const { return ___fEntRefNodesPresent_17; }
	inline bool* get_address_of_fEntRefNodesPresent_17() { return &___fEntRefNodesPresent_17; }
	inline void set_fEntRefNodesPresent_17(bool value)
	{
		___fEntRefNodesPresent_17 = value;
	}

	inline static int32_t get_offset_of_fCDataNodesPresent_18() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___fCDataNodesPresent_18)); }
	inline bool get_fCDataNodesPresent_18() const { return ___fCDataNodesPresent_18; }
	inline bool* get_address_of_fCDataNodesPresent_18() { return &___fCDataNodesPresent_18; }
	inline void set_fCDataNodesPresent_18(bool value)
	{
		___fCDataNodesPresent_18 = value;
	}

	inline static int32_t get_offset_of_preserveWhitespace_19() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___preserveWhitespace_19)); }
	inline bool get_preserveWhitespace_19() const { return ___preserveWhitespace_19; }
	inline bool* get_address_of_preserveWhitespace_19() { return &___preserveWhitespace_19; }
	inline void set_preserveWhitespace_19(bool value)
	{
		___preserveWhitespace_19 = value;
	}

	inline static int32_t get_offset_of_isLoading_20() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___isLoading_20)); }
	inline bool get_isLoading_20() const { return ___isLoading_20; }
	inline bool* get_address_of_isLoading_20() { return &___isLoading_20; }
	inline void set_isLoading_20(bool value)
	{
		___isLoading_20 = value;
	}

	inline static int32_t get_offset_of_strDocumentName_21() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strDocumentName_21)); }
	inline String_t* get_strDocumentName_21() const { return ___strDocumentName_21; }
	inline String_t** get_address_of_strDocumentName_21() { return &___strDocumentName_21; }
	inline void set_strDocumentName_21(String_t* value)
	{
		___strDocumentName_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strDocumentName_21), (void*)value);
	}

	inline static int32_t get_offset_of_strDocumentFragmentName_22() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strDocumentFragmentName_22)); }
	inline String_t* get_strDocumentFragmentName_22() const { return ___strDocumentFragmentName_22; }
	inline String_t** get_address_of_strDocumentFragmentName_22() { return &___strDocumentFragmentName_22; }
	inline void set_strDocumentFragmentName_22(String_t* value)
	{
		___strDocumentFragmentName_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strDocumentFragmentName_22), (void*)value);
	}

	inline static int32_t get_offset_of_strCommentName_23() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strCommentName_23)); }
	inline String_t* get_strCommentName_23() const { return ___strCommentName_23; }
	inline String_t** get_address_of_strCommentName_23() { return &___strCommentName_23; }
	inline void set_strCommentName_23(String_t* value)
	{
		___strCommentName_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strCommentName_23), (void*)value);
	}

	inline static int32_t get_offset_of_strTextName_24() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strTextName_24)); }
	inline String_t* get_strTextName_24() const { return ___strTextName_24; }
	inline String_t** get_address_of_strTextName_24() { return &___strTextName_24; }
	inline void set_strTextName_24(String_t* value)
	{
		___strTextName_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strTextName_24), (void*)value);
	}

	inline static int32_t get_offset_of_strCDataSectionName_25() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strCDataSectionName_25)); }
	inline String_t* get_strCDataSectionName_25() const { return ___strCDataSectionName_25; }
	inline String_t** get_address_of_strCDataSectionName_25() { return &___strCDataSectionName_25; }
	inline void set_strCDataSectionName_25(String_t* value)
	{
		___strCDataSectionName_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strCDataSectionName_25), (void*)value);
	}

	inline static int32_t get_offset_of_strEntityName_26() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strEntityName_26)); }
	inline String_t* get_strEntityName_26() const { return ___strEntityName_26; }
	inline String_t** get_address_of_strEntityName_26() { return &___strEntityName_26; }
	inline void set_strEntityName_26(String_t* value)
	{
		___strEntityName_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strEntityName_26), (void*)value);
	}

	inline static int32_t get_offset_of_strID_27() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strID_27)); }
	inline String_t* get_strID_27() const { return ___strID_27; }
	inline String_t** get_address_of_strID_27() { return &___strID_27; }
	inline void set_strID_27(String_t* value)
	{
		___strID_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strID_27), (void*)value);
	}

	inline static int32_t get_offset_of_strXmlns_28() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strXmlns_28)); }
	inline String_t* get_strXmlns_28() const { return ___strXmlns_28; }
	inline String_t** get_address_of_strXmlns_28() { return &___strXmlns_28; }
	inline void set_strXmlns_28(String_t* value)
	{
		___strXmlns_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strXmlns_28), (void*)value);
	}

	inline static int32_t get_offset_of_strXml_29() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strXml_29)); }
	inline String_t* get_strXml_29() const { return ___strXml_29; }
	inline String_t** get_address_of_strXml_29() { return &___strXml_29; }
	inline void set_strXml_29(String_t* value)
	{
		___strXml_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strXml_29), (void*)value);
	}

	inline static int32_t get_offset_of_strSpace_30() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strSpace_30)); }
	inline String_t* get_strSpace_30() const { return ___strSpace_30; }
	inline String_t** get_address_of_strSpace_30() { return &___strSpace_30; }
	inline void set_strSpace_30(String_t* value)
	{
		___strSpace_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strSpace_30), (void*)value);
	}

	inline static int32_t get_offset_of_strLang_31() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strLang_31)); }
	inline String_t* get_strLang_31() const { return ___strLang_31; }
	inline String_t** get_address_of_strLang_31() { return &___strLang_31; }
	inline void set_strLang_31(String_t* value)
	{
		___strLang_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strLang_31), (void*)value);
	}

	inline static int32_t get_offset_of_strEmpty_32() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strEmpty_32)); }
	inline String_t* get_strEmpty_32() const { return ___strEmpty_32; }
	inline String_t** get_address_of_strEmpty_32() { return &___strEmpty_32; }
	inline void set_strEmpty_32(String_t* value)
	{
		___strEmpty_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strEmpty_32), (void*)value);
	}

	inline static int32_t get_offset_of_strNonSignificantWhitespaceName_33() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strNonSignificantWhitespaceName_33)); }
	inline String_t* get_strNonSignificantWhitespaceName_33() const { return ___strNonSignificantWhitespaceName_33; }
	inline String_t** get_address_of_strNonSignificantWhitespaceName_33() { return &___strNonSignificantWhitespaceName_33; }
	inline void set_strNonSignificantWhitespaceName_33(String_t* value)
	{
		___strNonSignificantWhitespaceName_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strNonSignificantWhitespaceName_33), (void*)value);
	}

	inline static int32_t get_offset_of_strSignificantWhitespaceName_34() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strSignificantWhitespaceName_34)); }
	inline String_t* get_strSignificantWhitespaceName_34() const { return ___strSignificantWhitespaceName_34; }
	inline String_t** get_address_of_strSignificantWhitespaceName_34() { return &___strSignificantWhitespaceName_34; }
	inline void set_strSignificantWhitespaceName_34(String_t* value)
	{
		___strSignificantWhitespaceName_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strSignificantWhitespaceName_34), (void*)value);
	}

	inline static int32_t get_offset_of_strReservedXmlns_35() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strReservedXmlns_35)); }
	inline String_t* get_strReservedXmlns_35() const { return ___strReservedXmlns_35; }
	inline String_t** get_address_of_strReservedXmlns_35() { return &___strReservedXmlns_35; }
	inline void set_strReservedXmlns_35(String_t* value)
	{
		___strReservedXmlns_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strReservedXmlns_35), (void*)value);
	}

	inline static int32_t get_offset_of_strReservedXml_36() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strReservedXml_36)); }
	inline String_t* get_strReservedXml_36() const { return ___strReservedXml_36; }
	inline String_t** get_address_of_strReservedXml_36() { return &___strReservedXml_36; }
	inline void set_strReservedXml_36(String_t* value)
	{
		___strReservedXml_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strReservedXml_36), (void*)value);
	}

	inline static int32_t get_offset_of_baseURI_37() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___baseURI_37)); }
	inline String_t* get_baseURI_37() const { return ___baseURI_37; }
	inline String_t** get_address_of_baseURI_37() { return &___baseURI_37; }
	inline void set_baseURI_37(String_t* value)
	{
		___baseURI_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___baseURI_37), (void*)value);
	}

	inline static int32_t get_offset_of_resolver_38() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___resolver_38)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_resolver_38() const { return ___resolver_38; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_resolver_38() { return &___resolver_38; }
	inline void set_resolver_38(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___resolver_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resolver_38), (void*)value);
	}

	inline static int32_t get_offset_of_bSetResolver_39() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___bSetResolver_39)); }
	inline bool get_bSetResolver_39() const { return ___bSetResolver_39; }
	inline bool* get_address_of_bSetResolver_39() { return &___bSetResolver_39; }
	inline void set_bSetResolver_39(bool value)
	{
		___bSetResolver_39 = value;
	}

	inline static int32_t get_offset_of_objLock_40() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___objLock_40)); }
	inline RuntimeObject * get_objLock_40() const { return ___objLock_40; }
	inline RuntimeObject ** get_address_of_objLock_40() { return &___objLock_40; }
	inline void set_objLock_40(RuntimeObject * value)
	{
		___objLock_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objLock_40), (void*)value);
	}
};

struct XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields
{
public:
	// System.Xml.EmptyEnumerator System.Xml.XmlDocument::EmptyEnumerator
	EmptyEnumerator_t7702FAD652CDD694DC3D136712CA48C48FFB0DC3 * ___EmptyEnumerator_41;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::NotKnownSchemaInfo
	RuntimeObject* ___NotKnownSchemaInfo_42;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::ValidSchemaInfo
	RuntimeObject* ___ValidSchemaInfo_43;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::InvalidSchemaInfo
	RuntimeObject* ___InvalidSchemaInfo_44;

public:
	inline static int32_t get_offset_of_EmptyEnumerator_41() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields, ___EmptyEnumerator_41)); }
	inline EmptyEnumerator_t7702FAD652CDD694DC3D136712CA48C48FFB0DC3 * get_EmptyEnumerator_41() const { return ___EmptyEnumerator_41; }
	inline EmptyEnumerator_t7702FAD652CDD694DC3D136712CA48C48FFB0DC3 ** get_address_of_EmptyEnumerator_41() { return &___EmptyEnumerator_41; }
	inline void set_EmptyEnumerator_41(EmptyEnumerator_t7702FAD652CDD694DC3D136712CA48C48FFB0DC3 * value)
	{
		___EmptyEnumerator_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyEnumerator_41), (void*)value);
	}

	inline static int32_t get_offset_of_NotKnownSchemaInfo_42() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields, ___NotKnownSchemaInfo_42)); }
	inline RuntimeObject* get_NotKnownSchemaInfo_42() const { return ___NotKnownSchemaInfo_42; }
	inline RuntimeObject** get_address_of_NotKnownSchemaInfo_42() { return &___NotKnownSchemaInfo_42; }
	inline void set_NotKnownSchemaInfo_42(RuntimeObject* value)
	{
		___NotKnownSchemaInfo_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NotKnownSchemaInfo_42), (void*)value);
	}

	inline static int32_t get_offset_of_ValidSchemaInfo_43() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields, ___ValidSchemaInfo_43)); }
	inline RuntimeObject* get_ValidSchemaInfo_43() const { return ___ValidSchemaInfo_43; }
	inline RuntimeObject** get_address_of_ValidSchemaInfo_43() { return &___ValidSchemaInfo_43; }
	inline void set_ValidSchemaInfo_43(RuntimeObject* value)
	{
		___ValidSchemaInfo_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ValidSchemaInfo_43), (void*)value);
	}

	inline static int32_t get_offset_of_InvalidSchemaInfo_44() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields, ___InvalidSchemaInfo_44)); }
	inline RuntimeObject* get_InvalidSchemaInfo_44() const { return ___InvalidSchemaInfo_44; }
	inline RuntimeObject** get_address_of_InvalidSchemaInfo_44() { return &___InvalidSchemaInfo_44; }
	inline void set_InvalidSchemaInfo_44(RuntimeObject* value)
	{
		___InvalidSchemaInfo_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InvalidSchemaInfo_44), (void*)value);
	}
};


// System.Xml.XmlLinkedNode
struct  XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E  : public XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::next
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * ___next_1;

public:
	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E, ___next_1)); }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * get_next_1() const { return ___next_1; }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___next_1), (void*)value);
	}
};


// System.Xml.XmlNamedNodeMap_SmallXmlNodeList
struct  SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E 
{
public:
	// System.Object System.Xml.XmlNamedNodeMap_SmallXmlNodeList::field
	RuntimeObject * ___field_0;

public:
	inline static int32_t get_offset_of_field_0() { return static_cast<int32_t>(offsetof(SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E, ___field_0)); }
	inline RuntimeObject * get_field_0() const { return ___field_0; }
	inline RuntimeObject ** get_address_of_field_0() { return &___field_0; }
	inline void set_field_0(RuntimeObject * value)
	{
		___field_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___field_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Xml.XmlNamedNodeMap/SmallXmlNodeList
struct SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E_marshaled_pinvoke
{
	Il2CppIUnknown* ___field_0;
};
// Native definition for COM marshalling of System.Xml.XmlNamedNodeMap/SmallXmlNodeList
struct SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E_marshaled_com
{
	Il2CppIUnknown* ___field_0;
};

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// GameAnalyticsSDK.GAProgressionStatus
struct  GAProgressionStatus_tBA172450460B29A032AA13C714D6A2CADD010E62 
{
public:
	// System.Int32 GameAnalyticsSDK.GAProgressionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GAProgressionStatus_tBA172450460B29A032AA13C714D6A2CADD010E62, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Collections.Generic.Dictionary`2_Enumerator<System.Object,System.Object>
struct  Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::dictionary
	Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::version
	int32_t ___version_1;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::index
	int32_t ___index_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::current
	KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE  ___current_3;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::getEnumeratorRetType
	int32_t ___getEnumeratorRetType_4;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB, ___dictionary_0)); }
	inline Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB, ___current_3)); }
	inline KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___key_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___value_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_getEnumeratorRetType_4() { return static_cast<int32_t>(offsetof(Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB, ___getEnumeratorRetType_4)); }
	inline int32_t get_getEnumeratorRetType_4() const { return ___getEnumeratorRetType_4; }
	inline int32_t* get_address_of_getEnumeratorRetType_4() { return &___getEnumeratorRetType_4; }
	inline void set_getEnumeratorRetType_4(int32_t value)
	{
		___getEnumeratorRetType_4 = value;
	}
};


// System.Collections.Generic.Dictionary`2_Enumerator<System.String,System.String>
struct  Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::dictionary
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::version
	int32_t ___version_1;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::index
	int32_t ___index_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::current
	KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  ___current_3;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::getEnumeratorRetType
	int32_t ___getEnumeratorRetType_4;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3, ___dictionary_0)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3, ___current_3)); }
	inline KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___key_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___value_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_getEnumeratorRetType_4() { return static_cast<int32_t>(offsetof(Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3, ___getEnumeratorRetType_4)); }
	inline int32_t get_getEnumeratorRetType_4() const { return ___getEnumeratorRetType_4; }
	inline int32_t* get_address_of_getEnumeratorRetType_4() { return &___getEnumeratorRetType_4; }
	inline void set_getEnumeratorRetType_4(int32_t value)
	{
		___getEnumeratorRetType_4 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Xml.ConformanceLevel
struct  ConformanceLevel_t42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConformanceLevel_t42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.NamespaceHandling
struct  NamespaceHandling_tECE89F8A3E0CF300C5A18E41FBC65494C7EB3618 
{
public:
	// System.Int32 System.Xml.NamespaceHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NamespaceHandling_tECE89F8A3E0CF300C5A18E41FBC65494C7EB3618, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.NewLineHandling
struct  NewLineHandling_t35189DCBC088E592125C4F2FF4EDD3DB9F594543 
{
public:
	// System.Int32 System.Xml.NewLineHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NewLineHandling_t35189DCBC088E592125C4F2FF4EDD3DB9F594543, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.TriState
struct  TriState_t7F02F6CD4AE210F0DC6B2C03291967B54D0D77B0 
{
public:
	// System.Int32 System.Xml.TriState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TriState_t7F02F6CD4AE210F0DC6B2C03291967B54D0D77B0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.XmlElement
struct  XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC  : public XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E
{
public:
	// System.Xml.XmlName System.Xml.XmlElement::name
	XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * ___name_2;
	// System.Xml.XmlAttributeCollection System.Xml.XmlElement::attributes
	XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E * ___attributes_3;
	// System.Xml.XmlLinkedNode System.Xml.XmlElement::lastChild
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * ___lastChild_4;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC, ___name_2)); }
	inline XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * get_name_2() const { return ___name_2; }
	inline XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 ** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_2), (void*)value);
	}

	inline static int32_t get_offset_of_attributes_3() { return static_cast<int32_t>(offsetof(XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC, ___attributes_3)); }
	inline XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E * get_attributes_3() const { return ___attributes_3; }
	inline XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E ** get_address_of_attributes_3() { return &___attributes_3; }
	inline void set_attributes_3(XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E * value)
	{
		___attributes_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___attributes_3), (void*)value);
	}

	inline static int32_t get_offset_of_lastChild_4() { return static_cast<int32_t>(offsetof(XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC, ___lastChild_4)); }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * get_lastChild_4() const { return ___lastChild_4; }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E ** get_address_of_lastChild_4() { return &___lastChild_4; }
	inline void set_lastChild_4(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * value)
	{
		___lastChild_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastChild_4), (void*)value);
	}
};


// System.Xml.XmlNamedNodeMap
struct  XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNamedNodeMap::parent
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___parent_0;
	// System.Xml.XmlNamedNodeMap_SmallXmlNodeList System.Xml.XmlNamedNodeMap::nodes
	SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E  ___nodes_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31, ___parent_0)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_parent_0() const { return ___parent_0; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_0), (void*)value);
	}

	inline static int32_t get_offset_of_nodes_1() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31, ___nodes_1)); }
	inline SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E  get_nodes_1() const { return ___nodes_1; }
	inline SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E * get_address_of_nodes_1() { return &___nodes_1; }
	inline void set_nodes_1(SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E  value)
	{
		___nodes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___nodes_1))->___field_0), (void*)NULL);
	}
};


// System.Xml.XmlOutputMethod
struct  XmlOutputMethod_tF47F7732ECF902DFC04BAB9F9B7025F94DA862D4 
{
public:
	// System.Int32 System.Xml.XmlOutputMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlOutputMethod_tF47F7732ECF902DFC04BAB9F9B7025F94DA862D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Xml.XmlStandalone
struct  XmlStandalone_tD77DD6044609EA966459D821754EEE0BED37B39B 
{
public:
	// System.Int32 System.Xml.XmlStandalone::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlStandalone_tD77DD6044609EA966459D821754EEE0BED37B39B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.GameState_State
struct  State_t5DD173073C26C7FB5DF445E9A4C5F5EC9C97601D 
{
public:
	// System.Int32 UnityEngine.GameState_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t5DD173073C26C7FB5DF445E9A4C5F5EC9C97601D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_DesignEvent
struct  DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82  : public GameAnalyticsEvent_tC785053EBE27E395C98E2111584454333DD866D2
{
public:
	// System.String Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_DesignEvent::eventName
	String_t* ___eventName_0;
	// System.Nullable`1<System.Single> Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_DesignEvent::eventValue
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___eventValue_1;

public:
	inline static int32_t get_offset_of_eventName_0() { return static_cast<int32_t>(offsetof(DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82, ___eventName_0)); }
	inline String_t* get_eventName_0() const { return ___eventName_0; }
	inline String_t** get_address_of_eventName_0() { return &___eventName_0; }
	inline void set_eventName_0(String_t* value)
	{
		___eventName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventName_0), (void*)value);
	}

	inline static int32_t get_offset_of_eventValue_1() { return static_cast<int32_t>(offsetof(DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82, ___eventValue_1)); }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  get_eventValue_1() const { return ___eventValue_1; }
	inline Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * get_address_of_eventValue_1() { return &___eventValue_1; }
	inline void set_eventValue_1(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  value)
	{
		___eventValue_1 = value;
	}
};


// Voodoo.Sauce.Internal.VoodooLogLevel
struct  VoodooLogLevel_t3198716FA4C0E19431BA68178019372280AA402F 
{
public:
	// System.Int32 Voodoo.Sauce.Internal.VoodooLogLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VoodooLogLevel_t3198716FA4C0E19431BA68178019372280AA402F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Xml.XmlAttributeCollection
struct  XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E  : public XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31
{
public:

public:
};


// System.Xml.XmlWriterSettings
struct  XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.XmlWriterSettings::useAsync
	bool ___useAsync_0;
	// System.Text.Encoding System.Xml.XmlWriterSettings::encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_1;
	// System.Boolean System.Xml.XmlWriterSettings::omitXmlDecl
	bool ___omitXmlDecl_2;
	// System.Xml.NewLineHandling System.Xml.XmlWriterSettings::newLineHandling
	int32_t ___newLineHandling_3;
	// System.String System.Xml.XmlWriterSettings::newLineChars
	String_t* ___newLineChars_4;
	// System.Xml.TriState System.Xml.XmlWriterSettings::indent
	int32_t ___indent_5;
	// System.String System.Xml.XmlWriterSettings::indentChars
	String_t* ___indentChars_6;
	// System.Boolean System.Xml.XmlWriterSettings::newLineOnAttributes
	bool ___newLineOnAttributes_7;
	// System.Boolean System.Xml.XmlWriterSettings::closeOutput
	bool ___closeOutput_8;
	// System.Xml.NamespaceHandling System.Xml.XmlWriterSettings::namespaceHandling
	int32_t ___namespaceHandling_9;
	// System.Xml.ConformanceLevel System.Xml.XmlWriterSettings::conformanceLevel
	int32_t ___conformanceLevel_10;
	// System.Boolean System.Xml.XmlWriterSettings::checkCharacters
	bool ___checkCharacters_11;
	// System.Boolean System.Xml.XmlWriterSettings::writeEndDocumentOnClose
	bool ___writeEndDocumentOnClose_12;
	// System.Xml.XmlOutputMethod System.Xml.XmlWriterSettings::outputMethod
	int32_t ___outputMethod_13;
	// System.Collections.Generic.List`1<System.Xml.XmlQualifiedName> System.Xml.XmlWriterSettings::cdataSections
	List_1_tCAF7C69A9EA5F50BF5587DD8E7657B89C6866626 * ___cdataSections_14;
	// System.Boolean System.Xml.XmlWriterSettings::doNotEscapeUriAttributes
	bool ___doNotEscapeUriAttributes_15;
	// System.Boolean System.Xml.XmlWriterSettings::mergeCDataSections
	bool ___mergeCDataSections_16;
	// System.String System.Xml.XmlWriterSettings::mediaType
	String_t* ___mediaType_17;
	// System.String System.Xml.XmlWriterSettings::docTypeSystem
	String_t* ___docTypeSystem_18;
	// System.String System.Xml.XmlWriterSettings::docTypePublic
	String_t* ___docTypePublic_19;
	// System.Xml.XmlStandalone System.Xml.XmlWriterSettings::standalone
	int32_t ___standalone_20;
	// System.Boolean System.Xml.XmlWriterSettings::autoXmlDecl
	bool ___autoXmlDecl_21;
	// System.Boolean System.Xml.XmlWriterSettings::isReadOnly
	bool ___isReadOnly_22;

public:
	inline static int32_t get_offset_of_useAsync_0() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___useAsync_0)); }
	inline bool get_useAsync_0() const { return ___useAsync_0; }
	inline bool* get_address_of_useAsync_0() { return &___useAsync_0; }
	inline void set_useAsync_0(bool value)
	{
		___useAsync_0 = value;
	}

	inline static int32_t get_offset_of_encoding_1() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___encoding_1)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_encoding_1() const { return ___encoding_1; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_encoding_1() { return &___encoding_1; }
	inline void set_encoding_1(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___encoding_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encoding_1), (void*)value);
	}

	inline static int32_t get_offset_of_omitXmlDecl_2() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___omitXmlDecl_2)); }
	inline bool get_omitXmlDecl_2() const { return ___omitXmlDecl_2; }
	inline bool* get_address_of_omitXmlDecl_2() { return &___omitXmlDecl_2; }
	inline void set_omitXmlDecl_2(bool value)
	{
		___omitXmlDecl_2 = value;
	}

	inline static int32_t get_offset_of_newLineHandling_3() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___newLineHandling_3)); }
	inline int32_t get_newLineHandling_3() const { return ___newLineHandling_3; }
	inline int32_t* get_address_of_newLineHandling_3() { return &___newLineHandling_3; }
	inline void set_newLineHandling_3(int32_t value)
	{
		___newLineHandling_3 = value;
	}

	inline static int32_t get_offset_of_newLineChars_4() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___newLineChars_4)); }
	inline String_t* get_newLineChars_4() const { return ___newLineChars_4; }
	inline String_t** get_address_of_newLineChars_4() { return &___newLineChars_4; }
	inline void set_newLineChars_4(String_t* value)
	{
		___newLineChars_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___newLineChars_4), (void*)value);
	}

	inline static int32_t get_offset_of_indent_5() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___indent_5)); }
	inline int32_t get_indent_5() const { return ___indent_5; }
	inline int32_t* get_address_of_indent_5() { return &___indent_5; }
	inline void set_indent_5(int32_t value)
	{
		___indent_5 = value;
	}

	inline static int32_t get_offset_of_indentChars_6() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___indentChars_6)); }
	inline String_t* get_indentChars_6() const { return ___indentChars_6; }
	inline String_t** get_address_of_indentChars_6() { return &___indentChars_6; }
	inline void set_indentChars_6(String_t* value)
	{
		___indentChars_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___indentChars_6), (void*)value);
	}

	inline static int32_t get_offset_of_newLineOnAttributes_7() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___newLineOnAttributes_7)); }
	inline bool get_newLineOnAttributes_7() const { return ___newLineOnAttributes_7; }
	inline bool* get_address_of_newLineOnAttributes_7() { return &___newLineOnAttributes_7; }
	inline void set_newLineOnAttributes_7(bool value)
	{
		___newLineOnAttributes_7 = value;
	}

	inline static int32_t get_offset_of_closeOutput_8() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___closeOutput_8)); }
	inline bool get_closeOutput_8() const { return ___closeOutput_8; }
	inline bool* get_address_of_closeOutput_8() { return &___closeOutput_8; }
	inline void set_closeOutput_8(bool value)
	{
		___closeOutput_8 = value;
	}

	inline static int32_t get_offset_of_namespaceHandling_9() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___namespaceHandling_9)); }
	inline int32_t get_namespaceHandling_9() const { return ___namespaceHandling_9; }
	inline int32_t* get_address_of_namespaceHandling_9() { return &___namespaceHandling_9; }
	inline void set_namespaceHandling_9(int32_t value)
	{
		___namespaceHandling_9 = value;
	}

	inline static int32_t get_offset_of_conformanceLevel_10() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___conformanceLevel_10)); }
	inline int32_t get_conformanceLevel_10() const { return ___conformanceLevel_10; }
	inline int32_t* get_address_of_conformanceLevel_10() { return &___conformanceLevel_10; }
	inline void set_conformanceLevel_10(int32_t value)
	{
		___conformanceLevel_10 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_11() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___checkCharacters_11)); }
	inline bool get_checkCharacters_11() const { return ___checkCharacters_11; }
	inline bool* get_address_of_checkCharacters_11() { return &___checkCharacters_11; }
	inline void set_checkCharacters_11(bool value)
	{
		___checkCharacters_11 = value;
	}

	inline static int32_t get_offset_of_writeEndDocumentOnClose_12() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___writeEndDocumentOnClose_12)); }
	inline bool get_writeEndDocumentOnClose_12() const { return ___writeEndDocumentOnClose_12; }
	inline bool* get_address_of_writeEndDocumentOnClose_12() { return &___writeEndDocumentOnClose_12; }
	inline void set_writeEndDocumentOnClose_12(bool value)
	{
		___writeEndDocumentOnClose_12 = value;
	}

	inline static int32_t get_offset_of_outputMethod_13() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___outputMethod_13)); }
	inline int32_t get_outputMethod_13() const { return ___outputMethod_13; }
	inline int32_t* get_address_of_outputMethod_13() { return &___outputMethod_13; }
	inline void set_outputMethod_13(int32_t value)
	{
		___outputMethod_13 = value;
	}

	inline static int32_t get_offset_of_cdataSections_14() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___cdataSections_14)); }
	inline List_1_tCAF7C69A9EA5F50BF5587DD8E7657B89C6866626 * get_cdataSections_14() const { return ___cdataSections_14; }
	inline List_1_tCAF7C69A9EA5F50BF5587DD8E7657B89C6866626 ** get_address_of_cdataSections_14() { return &___cdataSections_14; }
	inline void set_cdataSections_14(List_1_tCAF7C69A9EA5F50BF5587DD8E7657B89C6866626 * value)
	{
		___cdataSections_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cdataSections_14), (void*)value);
	}

	inline static int32_t get_offset_of_doNotEscapeUriAttributes_15() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___doNotEscapeUriAttributes_15)); }
	inline bool get_doNotEscapeUriAttributes_15() const { return ___doNotEscapeUriAttributes_15; }
	inline bool* get_address_of_doNotEscapeUriAttributes_15() { return &___doNotEscapeUriAttributes_15; }
	inline void set_doNotEscapeUriAttributes_15(bool value)
	{
		___doNotEscapeUriAttributes_15 = value;
	}

	inline static int32_t get_offset_of_mergeCDataSections_16() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___mergeCDataSections_16)); }
	inline bool get_mergeCDataSections_16() const { return ___mergeCDataSections_16; }
	inline bool* get_address_of_mergeCDataSections_16() { return &___mergeCDataSections_16; }
	inline void set_mergeCDataSections_16(bool value)
	{
		___mergeCDataSections_16 = value;
	}

	inline static int32_t get_offset_of_mediaType_17() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___mediaType_17)); }
	inline String_t* get_mediaType_17() const { return ___mediaType_17; }
	inline String_t** get_address_of_mediaType_17() { return &___mediaType_17; }
	inline void set_mediaType_17(String_t* value)
	{
		___mediaType_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mediaType_17), (void*)value);
	}

	inline static int32_t get_offset_of_docTypeSystem_18() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___docTypeSystem_18)); }
	inline String_t* get_docTypeSystem_18() const { return ___docTypeSystem_18; }
	inline String_t** get_address_of_docTypeSystem_18() { return &___docTypeSystem_18; }
	inline void set_docTypeSystem_18(String_t* value)
	{
		___docTypeSystem_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___docTypeSystem_18), (void*)value);
	}

	inline static int32_t get_offset_of_docTypePublic_19() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___docTypePublic_19)); }
	inline String_t* get_docTypePublic_19() const { return ___docTypePublic_19; }
	inline String_t** get_address_of_docTypePublic_19() { return &___docTypePublic_19; }
	inline void set_docTypePublic_19(String_t* value)
	{
		___docTypePublic_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___docTypePublic_19), (void*)value);
	}

	inline static int32_t get_offset_of_standalone_20() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___standalone_20)); }
	inline int32_t get_standalone_20() const { return ___standalone_20; }
	inline int32_t* get_address_of_standalone_20() { return &___standalone_20; }
	inline void set_standalone_20(int32_t value)
	{
		___standalone_20 = value;
	}

	inline static int32_t get_offset_of_autoXmlDecl_21() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___autoXmlDecl_21)); }
	inline bool get_autoXmlDecl_21() const { return ___autoXmlDecl_21; }
	inline bool* get_address_of_autoXmlDecl_21() { return &___autoXmlDecl_21; }
	inline void set_autoXmlDecl_21(bool value)
	{
		___autoXmlDecl_21 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_22() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___isReadOnly_22)); }
	inline bool get_isReadOnly_22() const { return ___isReadOnly_22; }
	inline bool* get_address_of_isReadOnly_22() { return &___isReadOnly_22; }
	inline void set_isReadOnly_22(bool value)
	{
		___isReadOnly_22 = value;
	}
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};

// Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_ProgressEvent
struct  ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3  : public GameAnalyticsEvent_tC785053EBE27E395C98E2111584454333DD866D2
{
public:
	// GameAnalyticsSDK.GAProgressionStatus Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_ProgressEvent::status
	int32_t ___status_0;
	// System.String Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_ProgressEvent::progress
	String_t* ___progress_1;
	// System.Nullable`1<System.Int32> Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_ProgressEvent::score
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___score_2;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3, ___status_0)); }
	inline int32_t get_status_0() const { return ___status_0; }
	inline int32_t* get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(int32_t value)
	{
		___status_0 = value;
	}

	inline static int32_t get_offset_of_progress_1() { return static_cast<int32_t>(offsetof(ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3, ___progress_1)); }
	inline String_t* get_progress_1() const { return ___progress_1; }
	inline String_t** get_address_of_progress_1() { return &___progress_1; }
	inline void set_progress_1(String_t* value)
	{
		___progress_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___progress_1), (void*)value);
	}

	inline static int32_t get_offset_of_score_2() { return static_cast<int32_t>(offsetof(ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3, ___score_2)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_score_2() const { return ___score_2; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_score_2() { return &___score_2; }
	inline void set_score_2(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___score_2 = value;
	}
};


// Voodoo.Sauce.Internal.VoodooLog
struct  VoodooLog_t3E10EB33560DF558C5ADC45DF8FFF5ADCFFF9A24  : public RuntimeObject
{
public:

public:
};

struct VoodooLog_t3E10EB33560DF558C5ADC45DF8FFF5ADCFFF9A24_StaticFields
{
public:
	// Voodoo.Sauce.Internal.VoodooLogLevel Voodoo.Sauce.Internal.VoodooLog::_logLevel
	int32_t ____logLevel_0;

public:
	inline static int32_t get_offset_of__logLevel_0() { return static_cast<int32_t>(offsetof(VoodooLog_t3E10EB33560DF558C5ADC45DF8FFF5ADCFFF9A24_StaticFields, ____logLevel_0)); }
	inline int32_t get__logLevel_0() const { return ____logLevel_0; }
	inline int32_t* get_address_of__logLevel_0() { return &____logLevel_0; }
	inline void set__logLevel_0(int32_t value)
	{
		____logLevel_0 = value;
	}
};


// System.Action`1<System.String>
struct  Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`2<System.String,System.Single>
struct  Action_2_tD053D0BAA29F99F68652212454226A67BE949793  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`4<System.Boolean,System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct  Action_4_tCEEFFACC1F7C2A50B1B6A47E17A88F7AC26C5AEB  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// Voodoo.Sauce.Internal.TinySauceSettings
struct  TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String Voodoo.Sauce.Internal.TinySauceSettings::gameAnalyticsIosGameKey
	String_t* ___gameAnalyticsIosGameKey_5;
	// System.String Voodoo.Sauce.Internal.TinySauceSettings::gameAnalyticsIosSecretKey
	String_t* ___gameAnalyticsIosSecretKey_6;
	// System.String Voodoo.Sauce.Internal.TinySauceSettings::gameAnalyticsAndroidGameKey
	String_t* ___gameAnalyticsAndroidGameKey_7;
	// System.String Voodoo.Sauce.Internal.TinySauceSettings::gameAnalyticsAndroidSecretKey
	String_t* ___gameAnalyticsAndroidSecretKey_8;
	// System.String Voodoo.Sauce.Internal.TinySauceSettings::facebookAppId
	String_t* ___facebookAppId_9;

public:
	inline static int32_t get_offset_of_gameAnalyticsIosGameKey_5() { return static_cast<int32_t>(offsetof(TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B, ___gameAnalyticsIosGameKey_5)); }
	inline String_t* get_gameAnalyticsIosGameKey_5() const { return ___gameAnalyticsIosGameKey_5; }
	inline String_t** get_address_of_gameAnalyticsIosGameKey_5() { return &___gameAnalyticsIosGameKey_5; }
	inline void set_gameAnalyticsIosGameKey_5(String_t* value)
	{
		___gameAnalyticsIosGameKey_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameAnalyticsIosGameKey_5), (void*)value);
	}

	inline static int32_t get_offset_of_gameAnalyticsIosSecretKey_6() { return static_cast<int32_t>(offsetof(TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B, ___gameAnalyticsIosSecretKey_6)); }
	inline String_t* get_gameAnalyticsIosSecretKey_6() const { return ___gameAnalyticsIosSecretKey_6; }
	inline String_t** get_address_of_gameAnalyticsIosSecretKey_6() { return &___gameAnalyticsIosSecretKey_6; }
	inline void set_gameAnalyticsIosSecretKey_6(String_t* value)
	{
		___gameAnalyticsIosSecretKey_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameAnalyticsIosSecretKey_6), (void*)value);
	}

	inline static int32_t get_offset_of_gameAnalyticsAndroidGameKey_7() { return static_cast<int32_t>(offsetof(TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B, ___gameAnalyticsAndroidGameKey_7)); }
	inline String_t* get_gameAnalyticsAndroidGameKey_7() const { return ___gameAnalyticsAndroidGameKey_7; }
	inline String_t** get_address_of_gameAnalyticsAndroidGameKey_7() { return &___gameAnalyticsAndroidGameKey_7; }
	inline void set_gameAnalyticsAndroidGameKey_7(String_t* value)
	{
		___gameAnalyticsAndroidGameKey_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameAnalyticsAndroidGameKey_7), (void*)value);
	}

	inline static int32_t get_offset_of_gameAnalyticsAndroidSecretKey_8() { return static_cast<int32_t>(offsetof(TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B, ___gameAnalyticsAndroidSecretKey_8)); }
	inline String_t* get_gameAnalyticsAndroidSecretKey_8() const { return ___gameAnalyticsAndroidSecretKey_8; }
	inline String_t** get_address_of_gameAnalyticsAndroidSecretKey_8() { return &___gameAnalyticsAndroidSecretKey_8; }
	inline void set_gameAnalyticsAndroidSecretKey_8(String_t* value)
	{
		___gameAnalyticsAndroidSecretKey_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameAnalyticsAndroidSecretKey_8), (void*)value);
	}

	inline static int32_t get_offset_of_facebookAppId_9() { return static_cast<int32_t>(offsetof(TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B, ___facebookAppId_9)); }
	inline String_t* get_facebookAppId_9() const { return ___facebookAppId_9; }
	inline String_t** get_address_of_facebookAppId_9() { return &___facebookAppId_9; }
	inline void set_facebookAppId_9(String_t* value)
	{
		___facebookAppId_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___facebookAppId_9), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// GameAnalyticsSDK.GameAnalytics
struct  GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E_StaticFields
{
public:
	// GameAnalyticsSDK.Setup.Settings GameAnalyticsSDK.GameAnalytics::_settings
	Settings_tAF7F5B61F6B519B10CED42C18502A4ED0DD3FE49 * ____settings_4;
	// GameAnalyticsSDK.GameAnalytics GameAnalyticsSDK.GameAnalytics::_instance
	GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E * ____instance_5;
	// System.Boolean GameAnalyticsSDK.GameAnalytics::_hasInitializeBeenCalled
	bool ____hasInitializeBeenCalled_6;
	// System.Action GameAnalyticsSDK.GameAnalytics::OnRemoteConfigsUpdatedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnRemoteConfigsUpdatedEvent_7;

public:
	inline static int32_t get_offset_of__settings_4() { return static_cast<int32_t>(offsetof(GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E_StaticFields, ____settings_4)); }
	inline Settings_tAF7F5B61F6B519B10CED42C18502A4ED0DD3FE49 * get__settings_4() const { return ____settings_4; }
	inline Settings_tAF7F5B61F6B519B10CED42C18502A4ED0DD3FE49 ** get_address_of__settings_4() { return &____settings_4; }
	inline void set__settings_4(Settings_tAF7F5B61F6B519B10CED42C18502A4ED0DD3FE49 * value)
	{
		____settings_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____settings_4), (void*)value);
	}

	inline static int32_t get_offset_of__instance_5() { return static_cast<int32_t>(offsetof(GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E_StaticFields, ____instance_5)); }
	inline GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E * get__instance_5() const { return ____instance_5; }
	inline GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E ** get_address_of__instance_5() { return &____instance_5; }
	inline void set__instance_5(GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E * value)
	{
		____instance_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_5), (void*)value);
	}

	inline static int32_t get_offset_of__hasInitializeBeenCalled_6() { return static_cast<int32_t>(offsetof(GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E_StaticFields, ____hasInitializeBeenCalled_6)); }
	inline bool get__hasInitializeBeenCalled_6() const { return ____hasInitializeBeenCalled_6; }
	inline bool* get_address_of__hasInitializeBeenCalled_6() { return &____hasInitializeBeenCalled_6; }
	inline void set__hasInitializeBeenCalled_6(bool value)
	{
		____hasInitializeBeenCalled_6 = value;
	}

	inline static int32_t get_offset_of_OnRemoteConfigsUpdatedEvent_7() { return static_cast<int32_t>(offsetof(GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E_StaticFields, ___OnRemoteConfigsUpdatedEvent_7)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnRemoteConfigsUpdatedEvent_7() const { return ___OnRemoteConfigsUpdatedEvent_7; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnRemoteConfigsUpdatedEvent_7() { return &___OnRemoteConfigsUpdatedEvent_7; }
	inline void set_OnRemoteConfigsUpdatedEvent_7(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnRemoteConfigsUpdatedEvent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnRemoteConfigsUpdatedEvent_7), (void*)value);
	}
};


// Singleton`1<GameManager>
struct  Singleton_1_t79F2C980C6E97D53B76A1F21A959510D24A8A3FB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Singleton`1::dontDestroy
	bool ___dontDestroy_4;

public:
	inline static int32_t get_offset_of_dontDestroy_4() { return static_cast<int32_t>(offsetof(Singleton_1_t79F2C980C6E97D53B76A1F21A959510D24A8A3FB, ___dontDestroy_4)); }
	inline bool get_dontDestroy_4() const { return ___dontDestroy_4; }
	inline bool* get_address_of_dontDestroy_4() { return &___dontDestroy_4; }
	inline void set_dontDestroy_4(bool value)
	{
		___dontDestroy_4 = value;
	}
};

struct Singleton_1_t79F2C980C6E97D53B76A1F21A959510D24A8A3FB_StaticFields
{
public:
	// T Singleton`1::m_Instance
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___m_Instance_5;

public:
	inline static int32_t get_offset_of_m_Instance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t79F2C980C6E97D53B76A1F21A959510D24A8A3FB_StaticFields, ___m_Instance_5)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_m_Instance_5() const { return ___m_Instance_5; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_m_Instance_5() { return &___m_Instance_5; }
	inline void set_m_Instance_5(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___m_Instance_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Instance_5), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// Voodoo.Sauce.Internal.TinySauceBehaviour
struct  TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Voodoo.Sauce.Internal.TinySauceSettings Voodoo.Sauce.Internal.TinySauceBehaviour::_sauceSettings
	TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B * ____sauceSettings_6;

public:
	inline static int32_t get_offset_of__sauceSettings_6() { return static_cast<int32_t>(offsetof(TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5, ____sauceSettings_6)); }
	inline TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B * get__sauceSettings_6() const { return ____sauceSettings_6; }
	inline TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B ** get_address_of__sauceSettings_6() { return &____sauceSettings_6; }
	inline void set__sauceSettings_6(TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B * value)
	{
		____sauceSettings_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sauceSettings_6), (void*)value);
	}
};

struct TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5_StaticFields
{
public:
	// Voodoo.Sauce.Internal.TinySauceBehaviour Voodoo.Sauce.Internal.TinySauceBehaviour::_instance
	TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5 * ____instance_5;

public:
	inline static int32_t get_offset_of__instance_5() { return static_cast<int32_t>(offsetof(TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5_StaticFields, ____instance_5)); }
	inline TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5 * get__instance_5() const { return ____instance_5; }
	inline TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5 ** get_address_of__instance_5() { return &____instance_5; }
	inline void set__instance_5(TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5 * value)
	{
		____instance_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_5), (void*)value);
	}
};


// Winpopup
struct  Winpopup_t3BEDB5A4227E29A985A517FB8F2214ABA2396F0C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Winpopup::scoreLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___scoreLabel_4;
	// UnityEngine.UI.Text Winpopup::billLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___billLabel_5;
	// UnityEngine.GameObject Winpopup::wapper
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___wapper_6;

public:
	inline static int32_t get_offset_of_scoreLabel_4() { return static_cast<int32_t>(offsetof(Winpopup_t3BEDB5A4227E29A985A517FB8F2214ABA2396F0C, ___scoreLabel_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_scoreLabel_4() const { return ___scoreLabel_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_scoreLabel_4() { return &___scoreLabel_4; }
	inline void set_scoreLabel_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___scoreLabel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___scoreLabel_4), (void*)value);
	}

	inline static int32_t get_offset_of_billLabel_5() { return static_cast<int32_t>(offsetof(Winpopup_t3BEDB5A4227E29A985A517FB8F2214ABA2396F0C, ___billLabel_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_billLabel_5() const { return ___billLabel_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_billLabel_5() { return &___billLabel_5; }
	inline void set_billLabel_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___billLabel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___billLabel_5), (void*)value);
	}

	inline static int32_t get_offset_of_wapper_6() { return static_cast<int32_t>(offsetof(Winpopup_t3BEDB5A4227E29A985A517FB8F2214ABA2396F0C, ___wapper_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_wapper_6() const { return ___wapper_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_wapper_6() { return &___wapper_6; }
	inline void set_wapper_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___wapper_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wapper_6), (void*)value);
	}
};


// GameManager
struct  GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89  : public Singleton_1_t79F2C980C6E97D53B76A1F21A959510D24A8A3FB
{
public:
	// System.Boolean GameManager::resetlevel
	bool ___resetlevel_6;
	// System.Int32 GameManager::beginLevel
	int32_t ___beginLevel_7;
	// System.Int32 GameManager::beginMoney
	int32_t ___beginMoney_8;
	// UnityEngine.GameObject GameManager::enemy
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___enemy_9;
	// UnityEngine.GameObject GameManager::comboEnemy
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___comboEnemy_10;
	// BlockSpawnController GameManager::blockSpawnController
	BlockSpawnController_tA7EE1271EC33CDCE82BF8A6E8C6B225A02BAE516 * ___blockSpawnController_11;
	// UnityEngine.GameState_State GameManager::gameState
	int32_t ___gameState_12;
	// UnityEngine.GameObject GameManager::tapScreen
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___tapScreen_13;
	// Winpopup GameManager::winPopup
	Winpopup_t3BEDB5A4227E29A985A517FB8F2214ABA2396F0C * ___winPopup_14;
	// Losepopup GameManager::losepopup
	Losepopup_t8B2116C321F683CF632AF47B4D4405FB71E1FFBA * ___losepopup_15;
	// System.Single GameManager::percentSpawnMoney
	float ___percentSpawnMoney_16;
	// System.Int32 GameManager::billLevel
	int32_t ___billLevel_17;
	// System.Int32 GameManager::maxBlockLevel
	int32_t ___maxBlockLevel_18;
	// System.Single GameManager::distanceWithLastEnemy
	float ___distanceWithLastEnemy_19;
	// System.Boolean GameManager::autoBoss
	bool ___autoBoss_20;
	// UnityEngine.GameObject GameManager::bossPrefabs
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bossPrefabs_21;
	// UnityEngine.GameObject GameManager::bossPoint
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bossPoint_22;
	// UnityEngine.GameObject GameManager::bossFightWarning
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bossFightWarning_23;
	// UnityEngine.Transform GameManager::canvas
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___canvas_24;
	// UnityEngine.GameObject GameManager::hpEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___hpEffect_25;
	// FinishLinePosition GameManager::finishLinePosition
	FinishLinePosition_tC83CE07F7971D2D016843ED2F59E6E9C5008D6FE * ___finishLinePosition_26;
	// UnityEngine.Vector3 GameManager::totalEnemy
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___totalEnemy_27;
	// LevelController GameManager::levelController
	LevelController_tF5B6CBA811D10D3EEAC896B8FCFF852C3D58930D * ___levelController_28;
	// ScoreController GameManager::scoreController
	ScoreController_t922A5B08EFA00C15E3A48C8AD2C0729E9B2D5207 * ___scoreController_29;
	// GUIManager GameManager::gUIManager
	GUIManager_tDFF7E793464A0B061DBB5565BC73EC9457BA75BB * ___gUIManager_30;

public:
	inline static int32_t get_offset_of_resetlevel_6() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___resetlevel_6)); }
	inline bool get_resetlevel_6() const { return ___resetlevel_6; }
	inline bool* get_address_of_resetlevel_6() { return &___resetlevel_6; }
	inline void set_resetlevel_6(bool value)
	{
		___resetlevel_6 = value;
	}

	inline static int32_t get_offset_of_beginLevel_7() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___beginLevel_7)); }
	inline int32_t get_beginLevel_7() const { return ___beginLevel_7; }
	inline int32_t* get_address_of_beginLevel_7() { return &___beginLevel_7; }
	inline void set_beginLevel_7(int32_t value)
	{
		___beginLevel_7 = value;
	}

	inline static int32_t get_offset_of_beginMoney_8() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___beginMoney_8)); }
	inline int32_t get_beginMoney_8() const { return ___beginMoney_8; }
	inline int32_t* get_address_of_beginMoney_8() { return &___beginMoney_8; }
	inline void set_beginMoney_8(int32_t value)
	{
		___beginMoney_8 = value;
	}

	inline static int32_t get_offset_of_enemy_9() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___enemy_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_enemy_9() const { return ___enemy_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_enemy_9() { return &___enemy_9; }
	inline void set_enemy_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___enemy_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enemy_9), (void*)value);
	}

	inline static int32_t get_offset_of_comboEnemy_10() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___comboEnemy_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_comboEnemy_10() const { return ___comboEnemy_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_comboEnemy_10() { return &___comboEnemy_10; }
	inline void set_comboEnemy_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___comboEnemy_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comboEnemy_10), (void*)value);
	}

	inline static int32_t get_offset_of_blockSpawnController_11() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___blockSpawnController_11)); }
	inline BlockSpawnController_tA7EE1271EC33CDCE82BF8A6E8C6B225A02BAE516 * get_blockSpawnController_11() const { return ___blockSpawnController_11; }
	inline BlockSpawnController_tA7EE1271EC33CDCE82BF8A6E8C6B225A02BAE516 ** get_address_of_blockSpawnController_11() { return &___blockSpawnController_11; }
	inline void set_blockSpawnController_11(BlockSpawnController_tA7EE1271EC33CDCE82BF8A6E8C6B225A02BAE516 * value)
	{
		___blockSpawnController_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blockSpawnController_11), (void*)value);
	}

	inline static int32_t get_offset_of_gameState_12() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___gameState_12)); }
	inline int32_t get_gameState_12() const { return ___gameState_12; }
	inline int32_t* get_address_of_gameState_12() { return &___gameState_12; }
	inline void set_gameState_12(int32_t value)
	{
		___gameState_12 = value;
	}

	inline static int32_t get_offset_of_tapScreen_13() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___tapScreen_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_tapScreen_13() const { return ___tapScreen_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_tapScreen_13() { return &___tapScreen_13; }
	inline void set_tapScreen_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___tapScreen_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tapScreen_13), (void*)value);
	}

	inline static int32_t get_offset_of_winPopup_14() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___winPopup_14)); }
	inline Winpopup_t3BEDB5A4227E29A985A517FB8F2214ABA2396F0C * get_winPopup_14() const { return ___winPopup_14; }
	inline Winpopup_t3BEDB5A4227E29A985A517FB8F2214ABA2396F0C ** get_address_of_winPopup_14() { return &___winPopup_14; }
	inline void set_winPopup_14(Winpopup_t3BEDB5A4227E29A985A517FB8F2214ABA2396F0C * value)
	{
		___winPopup_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___winPopup_14), (void*)value);
	}

	inline static int32_t get_offset_of_losepopup_15() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___losepopup_15)); }
	inline Losepopup_t8B2116C321F683CF632AF47B4D4405FB71E1FFBA * get_losepopup_15() const { return ___losepopup_15; }
	inline Losepopup_t8B2116C321F683CF632AF47B4D4405FB71E1FFBA ** get_address_of_losepopup_15() { return &___losepopup_15; }
	inline void set_losepopup_15(Losepopup_t8B2116C321F683CF632AF47B4D4405FB71E1FFBA * value)
	{
		___losepopup_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___losepopup_15), (void*)value);
	}

	inline static int32_t get_offset_of_percentSpawnMoney_16() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___percentSpawnMoney_16)); }
	inline float get_percentSpawnMoney_16() const { return ___percentSpawnMoney_16; }
	inline float* get_address_of_percentSpawnMoney_16() { return &___percentSpawnMoney_16; }
	inline void set_percentSpawnMoney_16(float value)
	{
		___percentSpawnMoney_16 = value;
	}

	inline static int32_t get_offset_of_billLevel_17() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___billLevel_17)); }
	inline int32_t get_billLevel_17() const { return ___billLevel_17; }
	inline int32_t* get_address_of_billLevel_17() { return &___billLevel_17; }
	inline void set_billLevel_17(int32_t value)
	{
		___billLevel_17 = value;
	}

	inline static int32_t get_offset_of_maxBlockLevel_18() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___maxBlockLevel_18)); }
	inline int32_t get_maxBlockLevel_18() const { return ___maxBlockLevel_18; }
	inline int32_t* get_address_of_maxBlockLevel_18() { return &___maxBlockLevel_18; }
	inline void set_maxBlockLevel_18(int32_t value)
	{
		___maxBlockLevel_18 = value;
	}

	inline static int32_t get_offset_of_distanceWithLastEnemy_19() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___distanceWithLastEnemy_19)); }
	inline float get_distanceWithLastEnemy_19() const { return ___distanceWithLastEnemy_19; }
	inline float* get_address_of_distanceWithLastEnemy_19() { return &___distanceWithLastEnemy_19; }
	inline void set_distanceWithLastEnemy_19(float value)
	{
		___distanceWithLastEnemy_19 = value;
	}

	inline static int32_t get_offset_of_autoBoss_20() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___autoBoss_20)); }
	inline bool get_autoBoss_20() const { return ___autoBoss_20; }
	inline bool* get_address_of_autoBoss_20() { return &___autoBoss_20; }
	inline void set_autoBoss_20(bool value)
	{
		___autoBoss_20 = value;
	}

	inline static int32_t get_offset_of_bossPrefabs_21() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___bossPrefabs_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bossPrefabs_21() const { return ___bossPrefabs_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bossPrefabs_21() { return &___bossPrefabs_21; }
	inline void set_bossPrefabs_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bossPrefabs_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bossPrefabs_21), (void*)value);
	}

	inline static int32_t get_offset_of_bossPoint_22() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___bossPoint_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bossPoint_22() const { return ___bossPoint_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bossPoint_22() { return &___bossPoint_22; }
	inline void set_bossPoint_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bossPoint_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bossPoint_22), (void*)value);
	}

	inline static int32_t get_offset_of_bossFightWarning_23() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___bossFightWarning_23)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bossFightWarning_23() const { return ___bossFightWarning_23; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bossFightWarning_23() { return &___bossFightWarning_23; }
	inline void set_bossFightWarning_23(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bossFightWarning_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bossFightWarning_23), (void*)value);
	}

	inline static int32_t get_offset_of_canvas_24() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___canvas_24)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_canvas_24() const { return ___canvas_24; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_canvas_24() { return &___canvas_24; }
	inline void set_canvas_24(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___canvas_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvas_24), (void*)value);
	}

	inline static int32_t get_offset_of_hpEffect_25() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___hpEffect_25)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_hpEffect_25() const { return ___hpEffect_25; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_hpEffect_25() { return &___hpEffect_25; }
	inline void set_hpEffect_25(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___hpEffect_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hpEffect_25), (void*)value);
	}

	inline static int32_t get_offset_of_finishLinePosition_26() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___finishLinePosition_26)); }
	inline FinishLinePosition_tC83CE07F7971D2D016843ED2F59E6E9C5008D6FE * get_finishLinePosition_26() const { return ___finishLinePosition_26; }
	inline FinishLinePosition_tC83CE07F7971D2D016843ED2F59E6E9C5008D6FE ** get_address_of_finishLinePosition_26() { return &___finishLinePosition_26; }
	inline void set_finishLinePosition_26(FinishLinePosition_tC83CE07F7971D2D016843ED2F59E6E9C5008D6FE * value)
	{
		___finishLinePosition_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___finishLinePosition_26), (void*)value);
	}

	inline static int32_t get_offset_of_totalEnemy_27() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___totalEnemy_27)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_totalEnemy_27() const { return ___totalEnemy_27; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_totalEnemy_27() { return &___totalEnemy_27; }
	inline void set_totalEnemy_27(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___totalEnemy_27 = value;
	}

	inline static int32_t get_offset_of_levelController_28() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___levelController_28)); }
	inline LevelController_tF5B6CBA811D10D3EEAC896B8FCFF852C3D58930D * get_levelController_28() const { return ___levelController_28; }
	inline LevelController_tF5B6CBA811D10D3EEAC896B8FCFF852C3D58930D ** get_address_of_levelController_28() { return &___levelController_28; }
	inline void set_levelController_28(LevelController_tF5B6CBA811D10D3EEAC896B8FCFF852C3D58930D * value)
	{
		___levelController_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___levelController_28), (void*)value);
	}

	inline static int32_t get_offset_of_scoreController_29() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___scoreController_29)); }
	inline ScoreController_t922A5B08EFA00C15E3A48C8AD2C0729E9B2D5207 * get_scoreController_29() const { return ___scoreController_29; }
	inline ScoreController_t922A5B08EFA00C15E3A48C8AD2C0729E9B2D5207 ** get_address_of_scoreController_29() { return &___scoreController_29; }
	inline void set_scoreController_29(ScoreController_t922A5B08EFA00C15E3A48C8AD2C0729E9B2D5207 * value)
	{
		___scoreController_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___scoreController_29), (void*)value);
	}

	inline static int32_t get_offset_of_gUIManager_30() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___gUIManager_30)); }
	inline GUIManager_tDFF7E793464A0B061DBB5565BC73EC9457BA75BB * get_gUIManager_30() const { return ___gUIManager_30; }
	inline GUIManager_tDFF7E793464A0B061DBB5565BC73EC9457BA75BB ** get_address_of_gUIManager_30() { return &___gUIManager_30; }
	inline void set_gUIManager_30(GUIManager_tDFF7E793464A0B061DBB5565BC73EC9457BA75BB * value)
	{
		___gUIManager_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gUIManager_30), (void*)value);
	}
};


// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_11;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_12;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_13;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_14;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_18;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_CachedMesh_21;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_CachedUvs_22;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_23;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_11() const { return ___m_RectTransform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_11() { return &___m_RectTransform_11; }
	inline void set_m_RectTransform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_12)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_12() const { return ___m_CanvasRenderer_12; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_12() { return &___m_CanvasRenderer_12; }
	inline void set_m_CanvasRenderer_12(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_13)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_13() const { return ___m_Canvas_13; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_13() { return &___m_Canvas_13; }
	inline void set_m_Canvas_13(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_14)); }
	inline bool get_m_VertsDirty_14() const { return ___m_VertsDirty_14; }
	inline bool* get_address_of_m_VertsDirty_14() { return &___m_VertsDirty_14; }
	inline void set_m_VertsDirty_14(bool value)
	{
		___m_VertsDirty_14 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_15)); }
	inline bool get_m_MaterialDirty_15() const { return ___m_MaterialDirty_15; }
	inline bool* get_address_of_m_MaterialDirty_15() { return &___m_MaterialDirty_15; }
	inline void set_m_MaterialDirty_15(bool value)
	{
		___m_MaterialDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_16() const { return ___m_OnDirtyLayoutCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_16() { return &___m_OnDirtyLayoutCallback_16; }
	inline void set_m_OnDirtyLayoutCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_17)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_17() const { return ___m_OnDirtyVertsCallback_17; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_17() { return &___m_OnDirtyVertsCallback_17; }
	inline void set_m_OnDirtyVertsCallback_17(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_18)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_18() const { return ___m_OnDirtyMaterialCallback_18; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_18() { return &___m_OnDirtyMaterialCallback_18; }
	inline void set_m_OnDirtyMaterialCallback_18(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_21() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedMesh_21)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_CachedMesh_21() const { return ___m_CachedMesh_21; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_CachedMesh_21() { return &___m_CachedMesh_21; }
	inline void set_m_CachedMesh_21(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_CachedMesh_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_22() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedUvs_22)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_CachedUvs_22() const { return ___m_CachedUvs_22; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_CachedUvs_22() { return &___m_CachedUvs_22; }
	inline void set_m_CachedUvs_22(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_CachedUvs_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_23() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_23)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_23() const { return ___m_ColorTweenRunner_23; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_23() { return &___m_ColorTweenRunner_23; }
	inline void set_m_ColorTweenRunner_23(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_24(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_24 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_19;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_20;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_19)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_19() const { return ___s_Mesh_19; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_19() { return &___s_Mesh_19; }
	inline void set_s_Mesh_19(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_19), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_20)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_20() const { return ___s_VertexHelper_20; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_20() { return &___s_VertexHelper_20; }
	inline void set_s_VertexHelper_20(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_20), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_25;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_26;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_27;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_30;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_31;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_32;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_33;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_34;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_25)); }
	inline bool get_m_ShouldRecalculateStencil_25() const { return ___m_ShouldRecalculateStencil_25; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_25() { return &___m_ShouldRecalculateStencil_25; }
	inline void set_m_ShouldRecalculateStencil_25(bool value)
	{
		___m_ShouldRecalculateStencil_25 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_26)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_26() const { return ___m_MaskMaterial_26; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_26() { return &___m_MaskMaterial_26; }
	inline void set_m_MaskMaterial_26(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_27)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_27() const { return ___m_ParentMask_27; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_27() { return &___m_ParentMask_27; }
	inline void set_m_ParentMask_27(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_28)); }
	inline bool get_m_Maskable_28() const { return ___m_Maskable_28; }
	inline bool* get_address_of_m_Maskable_28() { return &___m_Maskable_28; }
	inline void set_m_Maskable_28(bool value)
	{
		___m_Maskable_28 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IsMaskingGraphic_29)); }
	inline bool get_m_IsMaskingGraphic_29() const { return ___m_IsMaskingGraphic_29; }
	inline bool* get_address_of_m_IsMaskingGraphic_29() { return &___m_IsMaskingGraphic_29; }
	inline void set_m_IsMaskingGraphic_29(bool value)
	{
		___m_IsMaskingGraphic_29 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_30)); }
	inline bool get_m_IncludeForMasking_30() const { return ___m_IncludeForMasking_30; }
	inline bool* get_address_of_m_IncludeForMasking_30() { return &___m_IncludeForMasking_30; }
	inline void set_m_IncludeForMasking_30(bool value)
	{
		___m_IncludeForMasking_30 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_31)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_31() const { return ___m_OnCullStateChanged_31; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_31() { return &___m_OnCullStateChanged_31; }
	inline void set_m_OnCullStateChanged_31(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_32)); }
	inline bool get_m_ShouldRecalculate_32() const { return ___m_ShouldRecalculate_32; }
	inline bool* get_address_of_m_ShouldRecalculate_32() { return &___m_ShouldRecalculate_32; }
	inline void set_m_ShouldRecalculate_32(bool value)
	{
		___m_ShouldRecalculate_32 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_33)); }
	inline int32_t get_m_StencilValue_33() const { return ___m_StencilValue_33; }
	inline int32_t* get_address_of_m_StencilValue_33() { return &___m_StencilValue_33; }
	inline void set_m_StencilValue_33(int32_t value)
	{
		___m_StencilValue_33 = value;
	}

	inline static int32_t get_offset_of_m_Corners_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_34)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_34() const { return ___m_Corners_34; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_34() { return &___m_Corners_34; }
	inline void set_m_Corners_34(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_34), (void*)value);
	}
};


// UnityEngine.UI.Text
struct  Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * ___m_FontData_35;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_36;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCache_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCacheForLayout_38;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_40;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ___m_TempVerts_41;

public:
	inline static int32_t get_offset_of_m_FontData_35() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_FontData_35)); }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * get_m_FontData_35() const { return ___m_FontData_35; }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 ** get_address_of_m_FontData_35() { return &___m_FontData_35; }
	inline void set_m_FontData_35(FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * value)
	{
		___m_FontData_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_36() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_Text_36)); }
	inline String_t* get_m_Text_36() const { return ___m_Text_36; }
	inline String_t** get_address_of_m_Text_36() { return &___m_Text_36; }
	inline void set_m_Text_36(String_t* value)
	{
		___m_Text_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_37() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCache_37)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCache_37() const { return ___m_TextCache_37; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCache_37() { return &___m_TextCache_37; }
	inline void set_m_TextCache_37(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCache_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_38() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCacheForLayout_38)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCacheForLayout_38() const { return ___m_TextCacheForLayout_38; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCacheForLayout_38() { return &___m_TextCacheForLayout_38; }
	inline void set_m_TextCacheForLayout_38(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCacheForLayout_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_40() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_DisableFontTextureRebuiltCallback_40)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_40() const { return ___m_DisableFontTextureRebuiltCallback_40; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_40() { return &___m_DisableFontTextureRebuiltCallback_40; }
	inline void set_m_DisableFontTextureRebuiltCallback_40(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_40 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_41() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TempVerts_41)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get_m_TempVerts_41() const { return ___m_TempVerts_41; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of_m_TempVerts_41() { return &___m_TempVerts_41; }
	inline void set_m_TempVerts_41(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		___m_TempVerts_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_41), (void*)value);
	}
};

struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultText_39;

public:
	inline static int32_t get_offset_of_s_DefaultText_39() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields, ___s_DefaultText_39)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultText_39() const { return ___s_DefaultText_39; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultText_39() { return &___s_DefaultText_39; }
	inline void set_s_DefaultText_39(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultText_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_39), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Action`4<System.Boolean,System.Single,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_4__ctor_m10BD64BBDD5C62DD26DF7624107C3C881F7E8020_gshared (Action_4_tF771E40FB7247CDC261F257C3C983FD4D3A42FE9 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Action`2<System.Object,System.Single>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_m48959D2BB23E9FA6236CD4B23534E051150D5A23_gshared (Action_2_t69121CC132451DFD4D151CC9FEEFC91909A937A0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.Int32>::.ctor(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Nullable_1__ctor_m11F9C228CFDF836DDFCD7880C09CB4098AB9D7F2_gshared (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.Single>::.ctor(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Nullable_1__ctor_mAB819EE77BD7E38C1E0E494C307D52F42DB259AE_gshared (Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * __this, float ___value0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Queue`1<System.Object>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t Queue_1_get_Count_m0CE0B6919A09EFFBB1EBA5B5DFEF50E4F8A89CFA_gshared_inline (Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<System.Object>::Dequeue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Queue_1_Dequeue_m6013DB8A542ACA15F662B6832ED389BB061EFEDE_gshared (Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<System.Object>::Enqueue(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Queue_1_Enqueue_m12D1C0BBE742C2537335B7E2B71F7E42A421A6FD_gshared (Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m5D8E761EC92CF92D391744DE422D5E24DE57123E_gshared (const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Queue_1_Clear_m489F423C3AF50477F337CB5470AC6337F2469096_gshared (Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Queue_1__ctor_m57D20E9B6532A644845C835306D5BCBCD3163964_gshared (Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0 * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.Single>::get_HasValue()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m6C7E30F1E2D85F0A4AB37F0F6685E37607F26231_gshared_inline (Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * __this, const RuntimeMethod* method);
// !0 System.Nullable`1<System.Single>::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Nullable_1_get_Value_m8ED77F1776BBC65874AF9D0ED769FF7B6B918DA2_gshared (Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_mB664E2C41CADA8413EF8842E6601B8C696A7CE15_gshared_inline (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * __this, const RuntimeMethod* method);
// !0 System.Nullable`1<System.Int32>::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Nullable_1_get_Value_mA8BB683CA6A8C5BF448A737FB5A2AF63C730B3E5_gshared (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Resources_Load_TisRuntimeObject_m312D167000593C478994C1F4C93930C5DAED66D3_gshared (String_t* ___path0, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB  Dictionary_2_GetEnumerator_mF1CF1D13F3E70C6D20D96D9AC88E44454E4C0053_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE  Enumerator_get_Current_m5B32A9FC8294CB723DCD1171744B32E1775B6318_gshared_inline (Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Key_m3AA5875E6F038F027D9B80929300B746525F9D65_gshared_inline (KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Value_m8C7B882C4D425535288FAAD08EAF11D289A43AEC_gshared_inline (KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m9B9FB07EC2C1D82E921C9316A4E0901C933BBF6C_gshared (Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mE363888280B72ED50538416C060EF9FC94B3BB00_gshared (Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB * __this, const RuntimeMethod* method);
// T Singleton`1<System.Object>::get_instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Singleton_1_get_instance_m65C237D0A37A2DA93028768AF573858DD6B36EED_gshared (const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::RegisterEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsProvider_RegisterEvents_mDE6B88888D1EADBC9A28A94FA3ADB62F3D822388 (const RuntimeMethod* method);
// System.Boolean Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::Initialize(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameAnalyticsWrapper_Initialize_m6A2269DECEC4E5C5D8107255BBF78CE06DA96FE5 (bool ___consent0, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::UnregisterEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsProvider_UnregisterEvents_mC334E0CD3E15B9E6775C12989F863BD09A2A50F8 (const RuntimeMethod* method);
// System.Void System.Action`1<System.String>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::add_OnGameStartedEvent(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnalyticsManager_add_OnGameStartedEvent_m2DB92954CA3ACF9DAB2DA17C23D3616BB969683E (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method);
// System.Void System.Action`4<System.Boolean,System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::.ctor(System.Object,System.IntPtr)
inline void Action_4__ctor_m71C0D7B7176F00D40BEF875619B60EB87DF02BE6 (Action_4_tCEEFFACC1F7C2A50B1B6A47E17A88F7AC26C5AEB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_4_tCEEFFACC1F7C2A50B1B6A47E17A88F7AC26C5AEB *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_4__ctor_m10BD64BBDD5C62DD26DF7624107C3C881F7E8020_gshared)(__this, ___object0, ___method1, method);
}
// System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::add_OnGameFinishedEvent(System.Action`4<System.Boolean,System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnalyticsManager_add_OnGameFinishedEvent_mD245F0F432F9603FFD7DF7CC2D416E56A308D9B9 (Action_4_tCEEFFACC1F7C2A50B1B6A47E17A88F7AC26C5AEB * ___value0, const RuntimeMethod* method);
// System.Void System.Action`2<System.String,System.Single>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m24FAA059B2074E94759D35E159056E5D30671E04 (Action_2_tD053D0BAA29F99F68652212454226A67BE949793 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tD053D0BAA29F99F68652212454226A67BE949793 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_2__ctor_m48959D2BB23E9FA6236CD4B23534E051150D5A23_gshared)(__this, ___object0, ___method1, method);
}
// System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::add_OnTrackCustomValueEvent(System.Action`2<System.String,System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnalyticsManager_add_OnTrackCustomValueEvent_mCA155B97B760B99E6211B88B1ADD56DA51C4DA82 (Action_2_tD053D0BAA29F99F68652212454226A67BE949793 * ___value0, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::add_OnTrackCustomEvent(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnalyticsManager_add_OnTrackCustomEvent_m92DD176430D5C061E516428635A3549E25B936D9 (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::remove_OnGameStartedEvent(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnalyticsManager_remove_OnGameStartedEvent_m41D0E1C7DC0A0EA4B0C2962D7817373BDD442F9C (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::remove_OnGameFinishedEvent(System.Action`4<System.Boolean,System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnalyticsManager_remove_OnGameFinishedEvent_mAC210AC5EEA8AE878F279C59F4B7C945CAE17AC5 (Action_4_tCEEFFACC1F7C2A50B1B6A47E17A88F7AC26C5AEB * ___value0, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::remove_OnTrackCustomValueEvent(System.Action`2<System.String,System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnalyticsManager_remove_OnTrackCustomValueEvent_mF597715E58A5EBE15D1928AEAF8FB8F3A902784F (Action_2_tD053D0BAA29F99F68652212454226A67BE949793 * ___value0, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::remove_OnTrackCustomEvent(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnalyticsManager_remove_OnTrackCustomEvent_m8823BAF4923C42C1202DFC4835923343684F1C9B (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::TrackProgressEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.Nullable`1<System.Int32>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsWrapper_TrackProgressEvent_m7DE043963ECAC5503F3118CCDD85775EA66B0664 (int32_t ___status0, String_t* ___progress1, Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___score2, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.Int32>::.ctor(!0)
inline void Nullable_1__ctor_m11F9C228CFDF836DDFCD7880C09CB4098AB9D7F2 (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB *, int32_t, const RuntimeMethod*))Nullable_1__ctor_m11F9C228CFDF836DDFCD7880C09CB4098AB9D7F2_gshared)(__this, ___value0, method);
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::TrackDesignEvent(System.String,System.Nullable`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsWrapper_TrackDesignEvent_mEFBC717B6DC026CB9A3CA0A3B9EE55B576D15040 (String_t* ___eventName0, Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___eventValue1, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.Single>::.ctor(!0)
inline void Nullable_1__ctor_mAB819EE77BD7E38C1E0E494C307D52F42DB259AE (Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * __this, float ___value0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 *, float, const RuntimeMethod*))Nullable_1__ctor_mAB819EE77BD7E38C1E0E494C307D52F42DB259AE_gshared)(__this, ___value0, method);
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::Disable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsWrapper_Disable_m60C77A631514F58F9C58B4001B27DC46A3D5D22C (const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::InstantiateGameAnalytics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsWrapper_InstantiateGameAnalytics_mD26002F3EBDA3080C19C03B6ACFCDBF428A6D199 (const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Queue`1<Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/GameAnalyticsEvent>::get_Count()
inline int32_t Queue_1_get_Count_mDA340F16177C88272289E749C4E60CE81F4784B7_inline (Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 *, const RuntimeMethod*))Queue_1_get_Count_m0CE0B6919A09EFFBB1EBA5B5DFEF50E4F8A89CFA_gshared_inline)(__this, method);
}
// System.String System.String::Concat(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.VoodooLog::Log(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoodooLog_Log_mA7C46EE6B665A3BCC85F83F844B24039AEE02D71 (String_t* ___tag0, String_t* ___message1, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/GameAnalyticsEvent>::Dequeue()
inline GameAnalyticsEvent_tC785053EBE27E395C98E2111584454333DD866D2 * Queue_1_Dequeue_mF79FD12769126B693964FFFAC56583A24767CFC5 (Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * __this, const RuntimeMethod* method)
{
	return ((  GameAnalyticsEvent_tC785053EBE27E395C98E2111584454333DD866D2 * (*) (Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 *, const RuntimeMethod*))Queue_1_Dequeue_m6013DB8A542ACA15F662B6832ED389BB061EFEDE_gshared)(__this, method);
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/ProgressEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressEvent__ctor_mF526A5CB445B100C5226C85B54C2269C2AA35F1E (ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/GameAnalyticsEvent>::Enqueue(!0)
inline void Queue_1_Enqueue_m983F9917753FF76DDDB83133E87A476CFCB0474F (Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * __this, GameAnalyticsEvent_tC785053EBE27E395C98E2111584454333DD866D2 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 *, GameAnalyticsEvent_tC785053EBE27E395C98E2111584454333DD866D2 *, const RuntimeMethod*))Queue_1_Enqueue_m12D1C0BBE742C2537335B7E2B71F7E42A421A6FD_gshared)(__this, ___item0, method);
}
// System.String System.String::Concat(System.Object,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC (RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/DesignEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DesignEvent__ctor_m3FD5746BB7040BD5E5597A05FAC28EA1A2C1DA3D (DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<GameAnalyticsSDK.GameAnalytics>()
inline GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E * Object_FindObjectOfType_TisGameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E_m756FB0FECAAC6808A917DF06536CB78C8FBCEDDC (const RuntimeMethod* method)
{
	return ((  GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E * (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m5D8E761EC92CF92D391744DE422D5E24DE57123E_gshared)(method);
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, String_t* ___name0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<GameAnalyticsSDK.GameAnalytics>()
inline GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E * GameObject_AddComponent_TisGameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E_m3EFE312F9182AD6A1F6D1400B06C364312A74864 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m80EDFEAC4927F588A7A702F81524EDBFA8603FE2_gshared)(__this, method);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void GameAnalyticsSDK.GameAnalytics::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalytics_Initialize_mD34FBE312428CEDD5DAC3FD0ED1234D9A4339C52 (const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/GameAnalyticsEvent>::Clear()
inline void Queue_1_Clear_mFA93B7441B1DE6E0C9B424A6B1F7F397FFE1027C (Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * __this, const RuntimeMethod* method)
{
	((  void (*) (Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 *, const RuntimeMethod*))Queue_1_Clear_m489F423C3AF50477F337CB5470AC6337F2469096_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Queue`1<Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/GameAnalyticsEvent>::.ctor()
inline void Queue_1__ctor_mD8195A92FD232CFFF98EB6FC64127CE36F972241 (Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * __this, const RuntimeMethod* method)
{
	((  void (*) (Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 *, const RuntimeMethod*))Queue_1__ctor_m57D20E9B6532A644845C835306D5BCBCD3163964_gshared)(__this, method);
}
// System.Boolean System.Nullable`1<System.Single>::get_HasValue()
inline bool Nullable_1_get_HasValue_m6C7E30F1E2D85F0A4AB37F0F6685E37607F26231_inline (Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 *, const RuntimeMethod*))Nullable_1_get_HasValue_m6C7E30F1E2D85F0A4AB37F0F6685E37607F26231_gshared_inline)(__this, method);
}
// !0 System.Nullable`1<System.Single>::get_Value()
inline float Nullable_1_get_Value_m8ED77F1776BBC65874AF9D0ED769FF7B6B918DA2 (Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * __this, const RuntimeMethod* method)
{
	return ((  float (*) (Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 *, const RuntimeMethod*))Nullable_1_get_Value_m8ED77F1776BBC65874AF9D0ED769FF7B6B918DA2_gshared)(__this, method);
}
// System.Void GameAnalyticsSDK.GameAnalytics::NewDesignEvent(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalytics_NewDesignEvent_mEF36424787F728DF2FE0209B114E222974A27039 (String_t* ___eventName0, float ___eventValue1, const RuntimeMethod* method);
// System.Void GameAnalyticsSDK.GameAnalytics::NewDesignEvent(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalytics_NewDesignEvent_mA0F014656A8DE873F7282DF2E2DA07F6FAD601B8 (String_t* ___eventName0, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/GameAnalyticsEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsEvent__ctor_m74986D7CC5BEF02A2DE9B161D30C35F8B6FAA72E (GameAnalyticsEvent_tC785053EBE27E395C98E2111584454333DD866D2 * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
inline bool Nullable_1_get_HasValue_mB664E2C41CADA8413EF8842E6601B8C696A7CE15_inline (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB *, const RuntimeMethod*))Nullable_1_get_HasValue_mB664E2C41CADA8413EF8842E6601B8C696A7CE15_gshared_inline)(__this, method);
}
// !0 System.Nullable`1<System.Int32>::get_Value()
inline int32_t Nullable_1_get_Value_mA8BB683CA6A8C5BF448A737FB5A2AF63C730B3E5 (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB *, const RuntimeMethod*))Nullable_1_get_Value_mA8BB683CA6A8C5BF448A737FB5A2AF63C730B3E5_gshared)(__this, method);
}
// System.Void GameAnalyticsSDK.GameAnalytics::NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalytics_NewProgressionEvent_mA689664EDAEB841FE2CC133595029D2316B74339 (int32_t ___progressionStatus0, String_t* ___progression011, int32_t ___score2, const RuntimeMethod* method);
// System.Void GameAnalyticsSDK.GameAnalytics::NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalytics_NewProgressionEvent_m6946828FEB1C33A578CDCB9F731AAB49121A353F (int32_t ___progressionStatus0, String_t* ___progression011, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_root()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Transform_get_root_m101A8B5C2CC6D868B6B66EEDBD5336FC1EB5DDD6 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void System.Exception::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m89BADFF36C3B170013878726E07729D51AA9FBE0 (Exception_t * __this, String_t* ___message0, const RuntimeMethod* method);
// Voodoo.Sauce.Internal.TinySauceSettings Voodoo.Sauce.Internal.TinySauceSettings::Load()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B * TinySauceSettings_Load_m6B57575CD43B33BAE0FDDDC7CEDCCC3CF850C747 (const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___target0, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.VoodooLog::Initialize(Voodoo.Sauce.Internal.VoodooLogLevel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoodooLog_Initialize_m9C4412D348DE823CC4535F0327D0985EC68B5B74 (int32_t ___level0, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.TinySauceBehaviour::InitAnalytics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TinySauceBehaviour_InitAnalytics_m0E97CA2D3FA96886CD3E6FB732B13084D73C90FA (TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5 * __this, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::Initialize(Voodoo.Sauce.Internal.TinySauceSettings,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnalyticsManager_Initialize_mFFF61A4BE10567C5D6E20DD7A4051B17E9FD5D9A (TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B * ___sauceSettings0, bool ___consent1, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::OnApplicationResume()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnalyticsManager_OnApplicationResume_m7C345F057B8BC930D89FE15CEDD83D6A5F650FB8 (const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<Voodoo.Sauce.Internal.TinySauceSettings>(System.String)
inline TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B * Resources_Load_TisTinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B_m5A64B8561C0240EE2D968668B0B3A97CFC58B058 (String_t* ___path0, const RuntimeMethod* method)
{
	return ((  TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B * (*) (String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m312D167000593C478994C1F4C93930C5DAED66D3_gshared)(___path0, method);
}
// System.Void UnityEngine.ScriptableObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B (ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734 * __this, const RuntimeMethod* method);
// System.Xml.XmlDocument Voodoo.Sauce.Internal.Utils.ManifestUtils::LoadFromFile(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ManifestUtils_LoadFromFile_m6D0336D0716E4CBF02268B16E0D7099939C301D8 (String_t* ___manifestPath0, const RuntimeMethod* method);
// System.Xml.XmlDocument Voodoo.Sauce.Internal.Utils.ManifestUtils::Add(System.Xml.XmlDocument,System.Xml.XmlDocument)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ManifestUtils_Add_mBFB87E1A709BF3A7EBD8A6D26F6B4FCBBBB66675 (XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___sourceDocument0, XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___destDocument1, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Utils.ManifestUtils::SaveDocumentInFile(System.String,System.Xml.XmlDocument)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManifestUtils_SaveDocumentInFile_m7BB668EEBB671FCC3F827635800D4EA483725B57 (String_t* ___manifestPath0, XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___document1, const RuntimeMethod* method);
// System.Xml.XmlDocument Voodoo.Sauce.Internal.Utils.ManifestUtils::LoadFromString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ManifestUtils_LoadFromString_m49896DCFF05217BCE4279ACF6CDE7FC7B607825F (String_t* ___manifestContent0, const RuntimeMethod* method);
// System.Xml.XmlNode Voodoo.Sauce.Internal.Utils.ManifestUtils::FindChildNode(System.Xml.XmlNode,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ManifestUtils_FindChildNode_mEF66F9E3B4965C85BA9B80EB7DAE77F621F0A1CF (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___parent0, String_t* ___childName1, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,System.String>::GetEnumerator()
inline Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3  Dictionary_2_GetEnumerator_m3378B4792B81EF81397CB9D9A761BD7149BD27F5 (Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3  (*) (Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC *, const RuntimeMethod*))Dictionary_2_GetEnumerator_mF1CF1D13F3E70C6D20D96D9AC88E44454E4C0053_gshared)(__this, method);
}
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_Current()
inline KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  Enumerator_get_Current_mBEC9B470213860581893E0F197CAAE657B8B6C69_inline (Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 * __this, const RuntimeMethod* method)
{
	return ((  KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  (*) (Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *, const RuntimeMethod*))Enumerator_get_Current_m5B32A9FC8294CB723DCD1171744B32E1775B6318_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Key()
inline String_t* KeyValuePair_2_get_Key_m434E29A1251E81B5A2124466105823011C462BF2_inline (KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m3AA5875E6F038F027D9B80929300B746525F9D65_gshared_inline)(__this, method);
}
// System.Xml.XmlAttribute Voodoo.Sauce.Internal.Utils.ManifestUtils::FindAttribute(System.Collections.IEnumerable,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * ManifestUtils_FindAttribute_mADD12182712B3FF205596CDA738DE551C111C7A2 (RuntimeObject* ___parent0, String_t* ___name1, const RuntimeMethod* method);
// System.String[] System.String::Split(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* String_Split_m13262358217AD2C119FD1B9733C3C0289D608512 (String_t* __this, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___separator0, const RuntimeMethod* method);
// System.Xml.XmlAttribute System.Xml.XmlAttributeCollection::Append(System.Xml.XmlAttribute)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * XmlAttributeCollection_Append_m106AD7C9DC091EE15A02BF846539AF63D5FB6137 (XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E * __this, XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * ___node0, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Value()
inline String_t* KeyValuePair_2_get_Value_mEAF4B15DEEAC6EB29683A5C6569F0F50B4DEBDA2_inline (KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m8C7B882C4D425535288FAAD08EAF11D289A43AEC_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::MoveNext()
inline bool Enumerator_MoveNext_m6E6A22A8620F5A5582BB67E367BE5086D7D895A6 (Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *, const RuntimeMethod*))Enumerator_MoveNext_m9B9FB07EC2C1D82E921C9316A4E0901C933BBF6C_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::Dispose()
inline void Enumerator_Dispose_m16C0E963A012498CD27422B463DB327BA4C7A321 (Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *, const RuntimeMethod*))Enumerator_Dispose_mE363888280B72ED50538416C060EF9FC94B3BB00_gshared)(__this, method);
}
// System.Boolean System.IO.File::Exists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool File_Exists_m6B9BDD8EEB33D744EB0590DD27BC0152FAFBD1FB (String_t* ___path0, const RuntimeMethod* method);
// System.String System.IO.File::ReadAllText(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* File_ReadAllText_m404A1BE4C87AC3C7B9C0B07469CDC44DE52817FF (String_t* ___path0, const RuntimeMethod* method);
// System.String System.String::Replace(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Replace_m970DFB0A280952FA7D3BA20AB7A8FB9F80CF6470 (String_t* __this, String_t* ___oldValue0, String_t* ___newValue1, const RuntimeMethod* method);
// System.Void System.IO.File::WriteAllText(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void File_WriteAllText_m7BA355E5631C6A3E3D3378D6101EF65E72A45F0A (String_t* ___path0, String_t* ___contents1, const RuntimeMethod* method);
// System.Xml.XmlElement System.Xml.XmlDocument::get_DocumentElement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * XmlDocument_get_DocumentElement_m4834A8AB3916884BA14431A2E084C2C8A76E2EDA (XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * __this, const RuntimeMethod* method);
// System.Xml.XmlNode Voodoo.Sauce.Internal.Utils.ManifestUtils::FindChildNode(System.Xml.XmlNode,System.Xml.XmlNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ManifestUtils_FindChildNode_m8CAAAC87A37AFCCEAB570F078FEDF556683973F3 (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___parent0, XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___child1, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.Utils.ManifestUtils::AddChildNode(System.Xml.XmlDocument,System.Xml.XmlNode,System.Xml.XmlNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManifestUtils_AddChildNode_m0774E2B100503D9E06CB49A9CC70542AB8467B1D (XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___document0, XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___parent1, XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___node2, const RuntimeMethod* method);
// System.Void System.Xml.XmlDocument::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlDocument__ctor_mB8BC8FA423C0BB144B91D00FA93246B8D0CFA490 (XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * __this, const RuntimeMethod* method);
// System.Boolean Voodoo.Sauce.Internal.Utils.ManifestUtils::FindElementWithAndroidName(System.Xml.XmlNode,System.Xml.XmlNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ManifestUtils_FindElementWithAndroidName_m040EF519E28229C677A7625260255A813176EC39 (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___parent0, XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___child1, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Boolean System.String::Equals(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Equals_m9C4D78DFA0979504FE31429B64A4C26DF48020D1 (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// System.String Voodoo.Sauce.Internal.Utils.ManifestUtils::GetAndroidElementName(System.Xml.XmlNode,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ManifestUtils_GetAndroidElementName_mC68A244A12CA5110AE421509EC1D90B18F30B050 (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___node0, String_t* ___namespaceOfPrefix1, const RuntimeMethod* method);
// System.Void System.Xml.XmlWriterSettings::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriterSettings__ctor_m5EA6551DB71068AFEAEB8AA92E684F3F82CF9017 (XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * __this, const RuntimeMethod* method);
// System.Void System.Xml.XmlWriterSettings::set_Indent(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriterSettings_set_Indent_m4B6DC1D7D5183258ADAF18CD4C23090BC9933F9A (XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Xml.XmlWriterSettings::set_IndentChars(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriterSettings_set_IndentChars_m3526849AD9EC3996EA5D35AB330E674D0BCE4338 (XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Xml.XmlWriterSettings::set_NewLineChars(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriterSettings_set_NewLineChars_mB9F47DA8097C733C10DCFEAE2E3CE6C9D2321C6D (XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Xml.XmlWriterSettings::set_NewLineHandling(System.Xml.NewLineHandling)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlWriterSettings_set_NewLineHandling_mD8C993B04A8A8518EAD20E85A2E528BD65F522C8 (XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Xml.XmlWriter System.Xml.XmlWriter::Create(System.String,System.Xml.XmlWriterSettings)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * XmlWriter_Create_mB11A06263B3E7A4B91EC026CEDF9D828C4424050 (String_t* ___outputFileName0, XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * ___settings1, const RuntimeMethod* method);
// System.Void Voodoo.Sauce.Internal.VoodooLog::EnableLogs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoodooLog_EnableLogs_mCF301D8664674A872023E1673BD6A4A65D989B46 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Application::get_isEditor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Application_get_isEditor_m347E6EE16E5109EF613C83ED98DB1EC6E3EF5E26 (const RuntimeMethod* method);
// System.String Voodoo.Sauce.Internal.VoodooLog::Format(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* VoodooLog_Format_m3B92528E7DEBA4AE65FE81F97571702522A85DFE (String_t* ___tag0, String_t* ___message1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.DateTime System.DateTime::get_Now()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  DateTime_get_Now_mB464D30F15C97069F92C1F910DCDDC3DFCC7F7D2 (const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865 (String_t* ___format0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// UnityEngine.ILogger UnityEngine.Debug::get_unityLogger()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Debug_get_unityLogger_mFA75EC397E067D09FD66D56B4E7692C3FCC3E960 (const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m19325298DBC61AAC016C16F7B3CF97A8A3DEA34A (String_t* ___format0, RuntimeObject * ___arg01, RuntimeObject * ___arg12, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA (String_t* ___format0, RuntimeObject * ___arg01, const RuntimeMethod* method);
// T Singleton`1<GameManager>::get_instance()
inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * Singleton_1_get_instance_m0D5DA7AAAEF2F4368826A7C6C88F129C79B6EE9B (const RuntimeMethod* method)
{
	return ((  GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * (*) (const RuntimeMethod*))Singleton_1_get_instance_m65C237D0A37A2DA93028768AF573858DD6B36EED_gshared)(method);
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m318D2B42E0FCAF179BF86D6C2353B38A58089BAD (String_t* ___key0, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_SetInt_mBF4101DF829B4738CCC293E1C2D173AEE45EFE62 (String_t* ___key0, int32_t ___value1, const RuntimeMethod* method);
// System.Void GameManager::NextLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_NextLevel_mF6C956ECA09992B9D36B913E2273B39C076B71F2 (GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager_<>c__DisplayClass17_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass17_0__ctor_mFCDB860B35FCAAD8E9866D940D1B1FBF817BE80C (U3CU3Ec__DisplayClass17_0_t5B0D56F5953B463EFAEE9D14CBA56AEF599C09D1 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager_<>c__DisplayClass17_0::<Initialize>b__0(Voodoo.Sauce.Internal.Analytics.IAnalyticsProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass17_0_U3CInitializeU3Eb__0_mEC0C58D2E555B5F9A7A2BA7BDA7D27F292CDD1A7 (U3CU3Ec__DisplayClass17_0_t5B0D56F5953B463EFAEE9D14CBA56AEF599C09D1 * __this, RuntimeObject* ___provider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass17_0_U3CInitializeU3Eb__0_mEC0C58D2E555B5F9A7A2BA7BDA7D27F292CDD1A7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _analyticsProviders.ForEach(provider => provider.Initialize( consent));
		RuntimeObject* L_0 = ___provider0;
		bool L_1 = __this->get_consent_0();
		NullCheck(L_0);
		InterfaceActionInvoker1< bool >::Invoke(0 /* System.Void Voodoo.Sauce.Internal.Analytics.IAnalyticsProvider::Initialize(System.Boolean) */, IAnalyticsProvider_t068EAC8917C2BBC1AF8AD85E21D41F2B23EBA931_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsProvider__ctor_m649CA095AC71C702042C742F7CA5A53F34CE8180 (GameAnalyticsProvider_tB512F4FF8602E4C3A071276A4AF633834E91EB20 * __this, const RuntimeMethod* method)
{
	{
		// internal GameAnalyticsProvider()
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// RegisterEvents();
		GameAnalyticsProvider_RegisterEvents_mDE6B88888D1EADBC9A28A94FA3ADB62F3D822388(/*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::Initialize(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsProvider_Initialize_m294805F45E57906DEE3B2E7633EF71BAC8F14354 (GameAnalyticsProvider_tB512F4FF8602E4C3A071276A4AF633834E91EB20 * __this, bool ___consent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameAnalyticsProvider_Initialize_m294805F45E57906DEE3B2E7633EF71BAC8F14354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!GameAnalyticsWrapper.Initialize(consent)) {
		bool L_0 = ___consent0;
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		bool L_1 = GameAnalyticsWrapper_Initialize_m6A2269DECEC4E5C5D8107255BBF78CE06DA96FE5(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		// UnregisterEvents();
		GameAnalyticsProvider_UnregisterEvents_mC334E0CD3E15B9E6775C12989F863BD09A2A50F8(/*hidden argument*/NULL);
	}

IL_000d:
	{
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::RegisterEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsProvider_RegisterEvents_mDE6B88888D1EADBC9A28A94FA3ADB62F3D822388 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameAnalyticsProvider_RegisterEvents_mDE6B88888D1EADBC9A28A94FA3ADB62F3D822388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AnalyticsManager.OnGameStartedEvent += OnGameStarted;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_0 = (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)il2cpp_codegen_object_new(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var);
		Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B(L_0, NULL, (intptr_t)((intptr_t)GameAnalyticsProvider_OnGameStarted_mEAC6338759F4E680D93D78C630694211CCA4401E_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(AnalyticsManager_t3183DE15E51166BDEC995DB2C8952B1950C82148_il2cpp_TypeInfo_var);
		AnalyticsManager_add_OnGameStartedEvent_m2DB92954CA3ACF9DAB2DA17C23D3616BB969683E(L_0, /*hidden argument*/NULL);
		// AnalyticsManager.OnGameFinishedEvent += OnGameFinished;
		Action_4_tCEEFFACC1F7C2A50B1B6A47E17A88F7AC26C5AEB * L_1 = (Action_4_tCEEFFACC1F7C2A50B1B6A47E17A88F7AC26C5AEB *)il2cpp_codegen_object_new(Action_4_tCEEFFACC1F7C2A50B1B6A47E17A88F7AC26C5AEB_il2cpp_TypeInfo_var);
		Action_4__ctor_m71C0D7B7176F00D40BEF875619B60EB87DF02BE6(L_1, NULL, (intptr_t)((intptr_t)GameAnalyticsProvider_OnGameFinished_m9F975CB121DDE865EDDDF875B70FD83136B2A9D8_RuntimeMethod_var), /*hidden argument*/Action_4__ctor_m71C0D7B7176F00D40BEF875619B60EB87DF02BE6_RuntimeMethod_var);
		AnalyticsManager_add_OnGameFinishedEvent_mD245F0F432F9603FFD7DF7CC2D416E56A308D9B9(L_1, /*hidden argument*/NULL);
		// AnalyticsManager.OnTrackCustomValueEvent += TrackCustomEvent;
		Action_2_tD053D0BAA29F99F68652212454226A67BE949793 * L_2 = (Action_2_tD053D0BAA29F99F68652212454226A67BE949793 *)il2cpp_codegen_object_new(Action_2_tD053D0BAA29F99F68652212454226A67BE949793_il2cpp_TypeInfo_var);
		Action_2__ctor_m24FAA059B2074E94759D35E159056E5D30671E04(L_2, NULL, (intptr_t)((intptr_t)GameAnalyticsProvider_TrackCustomEvent_m06AEFC1C2384CDF19C60A23DCFB227387598565C_RuntimeMethod_var), /*hidden argument*/Action_2__ctor_m24FAA059B2074E94759D35E159056E5D30671E04_RuntimeMethod_var);
		AnalyticsManager_add_OnTrackCustomValueEvent_mCA155B97B760B99E6211B88B1ADD56DA51C4DA82(L_2, /*hidden argument*/NULL);
		// AnalyticsManager.OnTrackCustomEvent += TrackCustomEvent;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_3 = (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)il2cpp_codegen_object_new(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var);
		Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B(L_3, NULL, (intptr_t)((intptr_t)GameAnalyticsProvider_TrackCustomEvent_mB625F71BC4E98012C3393805C0DBAB9FD5F2C66A_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B_RuntimeMethod_var);
		AnalyticsManager_add_OnTrackCustomEvent_m92DD176430D5C061E516428635A3549E25B936D9(L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::UnregisterEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsProvider_UnregisterEvents_mC334E0CD3E15B9E6775C12989F863BD09A2A50F8 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameAnalyticsProvider_UnregisterEvents_mC334E0CD3E15B9E6775C12989F863BD09A2A50F8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AnalyticsManager.OnGameStartedEvent -= OnGameStarted;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_0 = (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)il2cpp_codegen_object_new(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var);
		Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B(L_0, NULL, (intptr_t)((intptr_t)GameAnalyticsProvider_OnGameStarted_mEAC6338759F4E680D93D78C630694211CCA4401E_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(AnalyticsManager_t3183DE15E51166BDEC995DB2C8952B1950C82148_il2cpp_TypeInfo_var);
		AnalyticsManager_remove_OnGameStartedEvent_m41D0E1C7DC0A0EA4B0C2962D7817373BDD442F9C(L_0, /*hidden argument*/NULL);
		// AnalyticsManager.OnGameFinishedEvent -= OnGameFinished;
		Action_4_tCEEFFACC1F7C2A50B1B6A47E17A88F7AC26C5AEB * L_1 = (Action_4_tCEEFFACC1F7C2A50B1B6A47E17A88F7AC26C5AEB *)il2cpp_codegen_object_new(Action_4_tCEEFFACC1F7C2A50B1B6A47E17A88F7AC26C5AEB_il2cpp_TypeInfo_var);
		Action_4__ctor_m71C0D7B7176F00D40BEF875619B60EB87DF02BE6(L_1, NULL, (intptr_t)((intptr_t)GameAnalyticsProvider_OnGameFinished_m9F975CB121DDE865EDDDF875B70FD83136B2A9D8_RuntimeMethod_var), /*hidden argument*/Action_4__ctor_m71C0D7B7176F00D40BEF875619B60EB87DF02BE6_RuntimeMethod_var);
		AnalyticsManager_remove_OnGameFinishedEvent_mAC210AC5EEA8AE878F279C59F4B7C945CAE17AC5(L_1, /*hidden argument*/NULL);
		// AnalyticsManager.OnTrackCustomValueEvent -= TrackCustomEvent;
		Action_2_tD053D0BAA29F99F68652212454226A67BE949793 * L_2 = (Action_2_tD053D0BAA29F99F68652212454226A67BE949793 *)il2cpp_codegen_object_new(Action_2_tD053D0BAA29F99F68652212454226A67BE949793_il2cpp_TypeInfo_var);
		Action_2__ctor_m24FAA059B2074E94759D35E159056E5D30671E04(L_2, NULL, (intptr_t)((intptr_t)GameAnalyticsProvider_TrackCustomEvent_m06AEFC1C2384CDF19C60A23DCFB227387598565C_RuntimeMethod_var), /*hidden argument*/Action_2__ctor_m24FAA059B2074E94759D35E159056E5D30671E04_RuntimeMethod_var);
		AnalyticsManager_remove_OnTrackCustomValueEvent_mF597715E58A5EBE15D1928AEAF8FB8F3A902784F(L_2, /*hidden argument*/NULL);
		// AnalyticsManager.OnTrackCustomEvent -= TrackCustomEvent;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_3 = (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)il2cpp_codegen_object_new(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var);
		Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B(L_3, NULL, (intptr_t)((intptr_t)GameAnalyticsProvider_TrackCustomEvent_mB625F71BC4E98012C3393805C0DBAB9FD5F2C66A_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B_RuntimeMethod_var);
		AnalyticsManager_remove_OnTrackCustomEvent_m8823BAF4923C42C1202DFC4835923343684F1C9B(L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::OnGameStarted(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsProvider_OnGameStarted_mEAC6338759F4E680D93D78C630694211CCA4401E (String_t* ___levelNumber0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameAnalyticsProvider_OnGameStarted_mEAC6338759F4E680D93D78C630694211CCA4401E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// GameAnalyticsWrapper.TrackProgressEvent(GAProgressionStatus.Start, levelNumber, null);
		String_t* L_0 = ___levelNumber0;
		il2cpp_codegen_initobj((&V_0), sizeof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB ));
		Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		GameAnalyticsWrapper_TrackProgressEvent_m7DE043963ECAC5503F3118CCDD85775EA66B0664(1, L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::OnGameFinished(System.Boolean,System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsProvider_OnGameFinished_m9F975CB121DDE865EDDDF875B70FD83136B2A9D8 (bool ___levelComplete0, float ___score1, String_t* ___levelNumber2, Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___eventProperties3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameAnalyticsProvider_OnGameFinished_m9F975CB121DDE865EDDDF875B70FD83136B2A9D8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		// GameAnalyticsWrapper.TrackProgressEvent(levelComplete ? GAProgressionStatus.Complete : GAProgressionStatus.Fail, levelNumber, (int) score);
		bool L_0 = ___levelComplete0;
		if (L_0)
		{
			goto IL_0006;
		}
	}
	{
		G_B3_0 = 3;
		goto IL_0007;
	}

IL_0006:
	{
		G_B3_0 = 2;
	}

IL_0007:
	{
		String_t* L_1 = ___levelNumber2;
		float L_2 = ___score1;
		Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Nullable_1__ctor_m11F9C228CFDF836DDFCD7880C09CB4098AB9D7F2((&L_3), (((int32_t)((int32_t)L_2))), /*hidden argument*/Nullable_1__ctor_m11F9C228CFDF836DDFCD7880C09CB4098AB9D7F2_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		GameAnalyticsWrapper_TrackProgressEvent_m7DE043963ECAC5503F3118CCDD85775EA66B0664(G_B3_0, L_1, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::TrackCustomEvent(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsProvider_TrackCustomEvent_mB625F71BC4E98012C3393805C0DBAB9FD5F2C66A (String_t* ___eventName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameAnalyticsProvider_TrackCustomEvent_mB625F71BC4E98012C3393805C0DBAB9FD5F2C66A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// GameAnalyticsWrapper.TrackDesignEvent(eventName, null);
		String_t* L_0 = ___eventName0;
		il2cpp_codegen_initobj((&V_0), sizeof(Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 ));
		Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		GameAnalyticsWrapper_TrackDesignEvent_mEFBC717B6DC026CB9A3CA0A3B9EE55B576D15040(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::TrackCustomEvent(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsProvider_TrackCustomEvent_m06AEFC1C2384CDF19C60A23DCFB227387598565C (String_t* ___eventName0, float ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameAnalyticsProvider_TrackCustomEvent_m06AEFC1C2384CDF19C60A23DCFB227387598565C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GameAnalyticsWrapper.TrackDesignEvent(eventName, value);
		String_t* L_0 = ___eventName0;
		float L_1 = ___value1;
		Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Nullable_1__ctor_mAB819EE77BD7E38C1E0E494C307D52F42DB259AE((&L_2), L_1, /*hidden argument*/Nullable_1__ctor_mAB819EE77BD7E38C1E0E494C307D52F42DB259AE_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		GameAnalyticsWrapper_TrackDesignEvent_mEFBC717B6DC026CB9A3CA0A3B9EE55B576D15040(L_0, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::Initialize(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameAnalyticsWrapper_Initialize_m6A2269DECEC4E5C5D8107255BBF78CE06DA96FE5 (bool ___consent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameAnalyticsWrapper_Initialize_m6A2269DECEC4E5C5D8107255BBF78CE06DA96FE5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!consent) {
		bool L_0 = ___consent0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// Disable();
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		GameAnalyticsWrapper_Disable_m60C77A631514F58F9C58B4001B27DC46A3D5D22C(/*hidden argument*/NULL);
		// return _isInitialized;
		bool L_1 = ((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->get__isInitialized_1();
		return L_1;
	}

IL_000e:
	{
		// InstantiateGameAnalytics();
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		GameAnalyticsWrapper_InstantiateGameAnalytics_mD26002F3EBDA3080C19C03B6ACFCDBF428A6D199(/*hidden argument*/NULL);
		// VoodooLog.Log(TAG, "GameAnalytics initialized, tracking pending events: " + QueuedEvents.Count);
		Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * L_2 = ((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->get_QueuedEvents_3();
		NullCheck(L_2);
		int32_t L_3 = Queue_1_get_Count_mDA340F16177C88272289E749C4E60CE81F4784B7_inline(L_2, /*hidden argument*/Queue_1_get_Count_mDA340F16177C88272289E749C4E60CE81F4784B7_RuntimeMethod_var);
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralA5A3766367B4512FF38BBC68AA06EDC7CA9B95C0, L_5, /*hidden argument*/NULL);
		VoodooLog_Log_mA7C46EE6B665A3BCC85F83F844B24039AEE02D71(_stringLiteralD3C3F362DE50006549F1D2418450180E59B63353, L_6, /*hidden argument*/NULL);
		goto IL_0047;
	}

IL_0038:
	{
		// QueuedEvents.Dequeue().Track();
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * L_7 = ((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->get_QueuedEvents_3();
		NullCheck(L_7);
		GameAnalyticsEvent_tC785053EBE27E395C98E2111584454333DD866D2 * L_8 = Queue_1_Dequeue_mF79FD12769126B693964FFFAC56583A24767CFC5(L_7, /*hidden argument*/Queue_1_Dequeue_mF79FD12769126B693964FFFAC56583A24767CFC5_RuntimeMethod_var);
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(4 /* System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/GameAnalyticsEvent::Track() */, L_8);
	}

IL_0047:
	{
		// while (QueuedEvents.Count > 0) {
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * L_9 = ((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->get_QueuedEvents_3();
		NullCheck(L_9);
		int32_t L_10 = Queue_1_get_Count_mDA340F16177C88272289E749C4E60CE81F4784B7_inline(L_9, /*hidden argument*/Queue_1_get_Count_mDA340F16177C88272289E749C4E60CE81F4784B7_RuntimeMethod_var);
		if ((((int32_t)L_10) > ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		// _isInitialized = true;
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->set__isInitialized_1((bool)1);
		// return _isInitialized;
		bool L_11 = ((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->get__isInitialized_1();
		return L_11;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::TrackProgressEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.Nullable`1<System.Int32>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsWrapper_TrackProgressEvent_m7DE043963ECAC5503F3118CCDD85775EA66B0664 (int32_t ___status0, String_t* ___progress1, Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___score2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameAnalyticsWrapper_TrackProgressEvent_m7DE043963ECAC5503F3118CCDD85775EA66B0664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3 * V_0 = NULL;
	{
		// if (_isDisabled) return;
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		bool L_0 = ((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->get__isDisabled_2();
		if (!L_0)
		{
			goto IL_0008;
		}
	}
	{
		// if (_isDisabled) return;
		return;
	}

IL_0008:
	{
		// var progressEvent = new ProgressEvent {
		//     status = status,
		//     progress = progress,
		//     score = score
		// };
		ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3 * L_1 = (ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3 *)il2cpp_codegen_object_new(ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3_il2cpp_TypeInfo_var);
		ProgressEvent__ctor_mF526A5CB445B100C5226C85B54C2269C2AA35F1E(L_1, /*hidden argument*/NULL);
		ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3 * L_2 = L_1;
		int32_t L_3 = ___status0;
		NullCheck(L_2);
		L_2->set_status_0(L_3);
		ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3 * L_4 = L_2;
		String_t* L_5 = ___progress1;
		NullCheck(L_4);
		L_4->set_progress_1(L_5);
		ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3 * L_6 = L_4;
		Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  L_7 = ___score2;
		NullCheck(L_6);
		L_6->set_score_2(L_7);
		V_0 = L_6;
		// if (!_isInitialized) {
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		bool L_8 = ((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->get__isInitialized_1();
		if (L_8)
		{
			goto IL_0050;
		}
	}
	{
		// VoodooLog.Log(TAG, "GameAnalytics NOT initialized queuing event..." + status);
		int32_t L_9 = ___status0;
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = Box(GAProgressionStatus_tBA172450460B29A032AA13C714D6A2CADD010E62_il2cpp_TypeInfo_var, &L_10);
		String_t* L_12 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral1D92DDEB89F9C2D23B93C5046B3EF3A6C20A635F, L_11, /*hidden argument*/NULL);
		VoodooLog_Log_mA7C46EE6B665A3BCC85F83F844B24039AEE02D71(_stringLiteralD3C3F362DE50006549F1D2418450180E59B63353, L_12, /*hidden argument*/NULL);
		// QueuedEvents.Enqueue(progressEvent);
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * L_13 = ((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->get_QueuedEvents_3();
		ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3 * L_14 = V_0;
		NullCheck(L_13);
		Queue_1_Enqueue_m983F9917753FF76DDDB83133E87A476CFCB0474F(L_13, L_14, /*hidden argument*/Queue_1_Enqueue_m983F9917753FF76DDDB83133E87A476CFCB0474F_RuntimeMethod_var);
		// } else {
		return;
	}

IL_0050:
	{
		// VoodooLog.Log(TAG, "Sending event " + status + " to GameAnalytics");
		int32_t L_15 = ___status0;
		int32_t L_16 = L_15;
		RuntimeObject * L_17 = Box(GAProgressionStatus_tBA172450460B29A032AA13C714D6A2CADD010E62_il2cpp_TypeInfo_var, &L_16);
		String_t* L_18 = String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC(_stringLiteral4FDCDF9A6AD02645E48F4C62332CACDFBFCC6BF5, L_17, _stringLiteralACB138D47B861B40670D08FD59CB04F3A5D13FD9, /*hidden argument*/NULL);
		VoodooLog_Log_mA7C46EE6B665A3BCC85F83F844B24039AEE02D71(_stringLiteralD3C3F362DE50006549F1D2418450180E59B63353, L_18, /*hidden argument*/NULL);
		// progressEvent.Track();
		ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3 * L_19 = V_0;
		NullCheck(L_19);
		VirtActionInvoker0::Invoke(4 /* System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/GameAnalyticsEvent::Track() */, L_19);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::TrackDesignEvent(System.String,System.Nullable`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsWrapper_TrackDesignEvent_mEFBC717B6DC026CB9A3CA0A3B9EE55B576D15040 (String_t* ___eventName0, Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  ___eventValue1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameAnalyticsWrapper_TrackDesignEvent_mEFBC717B6DC026CB9A3CA0A3B9EE55B576D15040_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82 * V_0 = NULL;
	{
		// if (_isDisabled) return;
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		bool L_0 = ((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->get__isDisabled_2();
		if (!L_0)
		{
			goto IL_0008;
		}
	}
	{
		// if (_isDisabled) return;
		return;
	}

IL_0008:
	{
		// var designEvent = new DesignEvent {
		//     eventName = eventName,
		//     eventValue = eventValue
		// };
		DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82 * L_1 = (DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82 *)il2cpp_codegen_object_new(DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82_il2cpp_TypeInfo_var);
		DesignEvent__ctor_m3FD5746BB7040BD5E5597A05FAC28EA1A2C1DA3D(L_1, /*hidden argument*/NULL);
		DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82 * L_2 = L_1;
		String_t* L_3 = ___eventName0;
		NullCheck(L_2);
		L_2->set_eventName_0(L_3);
		DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82 * L_4 = L_2;
		Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0  L_5 = ___eventValue1;
		NullCheck(L_4);
		L_4->set_eventValue_1(L_5);
		V_0 = L_4;
		// if (!_isInitialized) {
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		bool L_6 = ((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->get__isInitialized_1();
		if (L_6)
		{
			goto IL_0044;
		}
	}
	{
		// VoodooLog.Log(TAG, "GameAnalytics NOT initialized queuing event..." + eventName);
		String_t* L_7 = ___eventName0;
		String_t* L_8 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral1D92DDEB89F9C2D23B93C5046B3EF3A6C20A635F, L_7, /*hidden argument*/NULL);
		VoodooLog_Log_mA7C46EE6B665A3BCC85F83F844B24039AEE02D71(_stringLiteralD3C3F362DE50006549F1D2418450180E59B63353, L_8, /*hidden argument*/NULL);
		// QueuedEvents.Enqueue(designEvent);
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * L_9 = ((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->get_QueuedEvents_3();
		DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82 * L_10 = V_0;
		NullCheck(L_9);
		Queue_1_Enqueue_m983F9917753FF76DDDB83133E87A476CFCB0474F(L_9, L_10, /*hidden argument*/Queue_1_Enqueue_m983F9917753FF76DDDB83133E87A476CFCB0474F_RuntimeMethod_var);
		// } else {
		return;
	}

IL_0044:
	{
		// VoodooLog.Log(TAG, "Sending event " + eventName + " to GameAnalytics");
		String_t* L_11 = ___eventName0;
		String_t* L_12 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteral4FDCDF9A6AD02645E48F4C62332CACDFBFCC6BF5, L_11, _stringLiteralACB138D47B861B40670D08FD59CB04F3A5D13FD9, /*hidden argument*/NULL);
		VoodooLog_Log_mA7C46EE6B665A3BCC85F83F844B24039AEE02D71(_stringLiteralD3C3F362DE50006549F1D2418450180E59B63353, L_12, /*hidden argument*/NULL);
		// designEvent.Track();
		DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82 * L_13 = V_0;
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(4 /* System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper/GameAnalyticsEvent::Track() */, L_13);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::InstantiateGameAnalytics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsWrapper_InstantiateGameAnalytics_mD26002F3EBDA3080C19C03B6ACFCDBF428A6D199 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameAnalyticsWrapper_InstantiateGameAnalytics_mD26002F3EBDA3080C19C03B6ACFCDBF428A6D199_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E * V_0 = NULL;
	{
		// var gameAnalyticsComponent = Object.FindObjectOfType<GameAnalytics>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E * L_0 = Object_FindObjectOfType_TisGameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E_m756FB0FECAAC6808A917DF06536CB78C8FBCEDDC(/*hidden argument*/Object_FindObjectOfType_TisGameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E_m756FB0FECAAC6808A917DF06536CB78C8FBCEDDC_RuntimeMethod_var);
		V_0 = L_0;
		// if (gameAnalyticsComponent == null) {
		GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E * L_1 = V_0;
		bool L_2 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		// var gameAnalyticsGameObject = new GameObject("GameAnalytics");
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mBB454E679AD9CF0B84D3609A01E6A9753ACF4686(L_3, _stringLiteral8BE4551C194CDBAEA1CA023939C560EA448E1CB4, /*hidden argument*/NULL);
		// gameAnalyticsGameObject.AddComponent<GameAnalytics>();
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = L_3;
		NullCheck(L_4);
		GameObject_AddComponent_TisGameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E_m3EFE312F9182AD6A1F6D1400B06C364312A74864(L_4, /*hidden argument*/GameObject_AddComponent_TisGameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E_m3EFE312F9182AD6A1F6D1400B06C364312A74864_RuntimeMethod_var);
		// gameAnalyticsGameObject.SetActive(true);
		NullCheck(L_4);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_4, (bool)1, /*hidden argument*/NULL);
		// } else {
		goto IL_0038;
	}

IL_0028:
	{
		// gameAnalyticsComponent.gameObject.name = "GameAnalytics";
		GameAnalytics_tEDF8BEE04FB40B85302A6353D2E5547739E84A5E * L_5 = V_0;
		NullCheck(L_5);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_6, _stringLiteral8BE4551C194CDBAEA1CA023939C560EA448E1CB4, /*hidden argument*/NULL);
	}

IL_0038:
	{
		// GameAnalytics.Initialize();
		GameAnalytics_Initialize_mD34FBE312428CEDD5DAC3FD0ED1234D9A4339C52(/*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::Disable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsWrapper_Disable_m60C77A631514F58F9C58B4001B27DC46A3D5D22C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameAnalyticsWrapper_Disable_m60C77A631514F58F9C58B4001B27DC46A3D5D22C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// VoodooLog.Log(TAG, "Disabling GameAnalytics No User Consent.");
		VoodooLog_Log_mA7C46EE6B665A3BCC85F83F844B24039AEE02D71(_stringLiteralD3C3F362DE50006549F1D2418450180E59B63353, _stringLiteralE59BD5CB77309247AE077BC6D900A334ED2E99CC, /*hidden argument*/NULL);
		// _isDisabled = true;
		IL2CPP_RUNTIME_CLASS_INIT(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var);
		((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->set__isDisabled_2((bool)1);
		// QueuedEvents.Clear();
		Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * L_0 = ((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->get_QueuedEvents_3();
		NullCheck(L_0);
		Queue_1_Clear_mFA93B7441B1DE6E0C9B424A6B1F7F397FFE1027C(L_0, /*hidden argument*/Queue_1_Clear_mFA93B7441B1DE6E0C9B424A6B1F7F397FFE1027C_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsWrapper__cctor_m53D4076D48F07C37FAFF4D0FCB728A7B238DFA51 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameAnalyticsWrapper__cctor_m53D4076D48F07C37FAFF4D0FCB728A7B238DFA51_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static readonly Queue<GameAnalyticsEvent> QueuedEvents = new Queue<GameAnalyticsEvent>();
		Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 * L_0 = (Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3 *)il2cpp_codegen_object_new(Queue_1_tA30A498D7163FDD9D8EE5210F2F309932A685AE3_il2cpp_TypeInfo_var);
		Queue_1__ctor_mD8195A92FD232CFFF98EB6FC64127CE36F972241(L_0, /*hidden argument*/Queue_1__ctor_mD8195A92FD232CFFF98EB6FC64127CE36F972241_RuntimeMethod_var);
		((GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_StaticFields*)il2cpp_codegen_static_fields_for(GameAnalyticsWrapper_t4652350C6A1F7EE70118DB849EFB1BAF3908B8BC_il2cpp_TypeInfo_var))->set_QueuedEvents_3(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_DesignEvent::Track()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DesignEvent_Track_mEAC9146E85F410568B20984DC4D56AFF9E940D66 (DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DesignEvent_Track_mEAC9146E85F410568B20984DC4D56AFF9E940D66_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (eventValue != null) {
		Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * L_0 = __this->get_address_of_eventValue_1();
		bool L_1 = Nullable_1_get_HasValue_m6C7E30F1E2D85F0A4AB37F0F6685E37607F26231_inline((Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 *)L_0, /*hidden argument*/Nullable_1_get_HasValue_m6C7E30F1E2D85F0A4AB37F0F6685E37607F26231_RuntimeMethod_var);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		// GameAnalytics.NewDesignEvent(eventName, (float)eventValue);
		String_t* L_2 = __this->get_eventName_0();
		Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * L_3 = __this->get_address_of_eventValue_1();
		float L_4 = Nullable_1_get_Value_m8ED77F1776BBC65874AF9D0ED769FF7B6B918DA2((Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 *)L_3, /*hidden argument*/Nullable_1_get_Value_m8ED77F1776BBC65874AF9D0ED769FF7B6B918DA2_RuntimeMethod_var);
		GameAnalytics_NewDesignEvent_mEF36424787F728DF2FE0209B114E222974A27039(L_2, (((float)((float)L_4))), /*hidden argument*/NULL);
		// } else {
		return;
	}

IL_0025:
	{
		// GameAnalytics.NewDesignEvent(eventName);
		String_t* L_5 = __this->get_eventName_0();
		GameAnalytics_NewDesignEvent_mA0F014656A8DE873F7282DF2E2DA07F6FAD601B8(L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_DesignEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DesignEvent__ctor_m3FD5746BB7040BD5E5597A05FAC28EA1A2C1DA3D (DesignEvent_t0E3F3082056B4CB89CECDBCE7EE4329134452D82 * __this, const RuntimeMethod* method)
{
	{
		GameAnalyticsEvent__ctor_m74986D7CC5BEF02A2DE9B161D30C35F8B6FAA72E(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_GameAnalyticsEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameAnalyticsEvent__ctor_m74986D7CC5BEF02A2DE9B161D30C35F8B6FAA72E (GameAnalyticsEvent_tC785053EBE27E395C98E2111584454333DD866D2 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_ProgressEvent::Track()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressEvent_Track_m756CD0C4E2F7DDA8217449F7B1470385323B9322 (ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ProgressEvent_Track_m756CD0C4E2F7DDA8217449F7B1470385323B9322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (score != null) {
		Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * L_0 = __this->get_address_of_score_2();
		bool L_1 = Nullable_1_get_HasValue_mB664E2C41CADA8413EF8842E6601B8C696A7CE15_inline((Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB *)L_0, /*hidden argument*/Nullable_1_get_HasValue_mB664E2C41CADA8413EF8842E6601B8C696A7CE15_RuntimeMethod_var);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		// GameAnalytics.NewProgressionEvent(status, progress, (int) score);
		int32_t L_2 = __this->get_status_0();
		String_t* L_3 = __this->get_progress_1();
		Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * L_4 = __this->get_address_of_score_2();
		int32_t L_5 = Nullable_1_get_Value_mA8BB683CA6A8C5BF448A737FB5A2AF63C730B3E5((Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB *)L_4, /*hidden argument*/Nullable_1_get_Value_mA8BB683CA6A8C5BF448A737FB5A2AF63C730B3E5_RuntimeMethod_var);
		GameAnalytics_NewProgressionEvent_mA689664EDAEB841FE2CC133595029D2316B74339(L_2, L_3, L_5, /*hidden argument*/NULL);
		// } else {
		return;
	}

IL_002a:
	{
		// GameAnalytics.NewProgressionEvent(status, progress);
		int32_t L_6 = __this->get_status_0();
		String_t* L_7 = __this->get_progress_1();
		GameAnalytics_NewProgressionEvent_m6946828FEB1C33A578CDCB9F731AAB49121A353F(L_6, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_ProgressEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressEvent__ctor_mF526A5CB445B100C5226C85B54C2269C2AA35F1E (ProgressEvent_t969C9D9FFE457E90A555506A4F88ACECBA73EEC3 * __this, const RuntimeMethod* method)
{
	{
		GameAnalyticsEvent__ctor_m74986D7CC5BEF02A2DE9B161D30C35F8B6FAA72E(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Voodoo.Sauce.Internal.TinySauceBehaviour::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TinySauceBehaviour_Awake_mFAAA01A24AFF626349B4700FE195B4249FB2CCF2 (TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TinySauceBehaviour_Awake_mFAAA01A24AFF626349B4700FE195B4249FB2CCF2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (transform != transform.root)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = Transform_get_root_m101A8B5C2CC6D868B6B66EEDBD5336FC1EB5DDD6(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		// throw new Exception("TinySauce prefab HAS to be at the ROOT level!");
		Exception_t * L_4 = (Exception_t *)il2cpp_codegen_object_new(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m89BADFF36C3B170013878726E07729D51AA9FBE0(L_4, _stringLiteral097ECD89364746904BE8F4DCBC24E564C943F7F8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, TinySauceBehaviour_Awake_mFAAA01A24AFF626349B4700FE195B4249FB2CCF2_RuntimeMethod_var);
	}

IL_0023:
	{
		// _sauceSettings = TinySauceSettings.Load();
		TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B * L_5 = TinySauceSettings_Load_m6B57575CD43B33BAE0FDDDC7CEDCCC3CF850C747(/*hidden argument*/NULL);
		__this->set__sauceSettings_6(L_5);
		// if (_sauceSettings == null)
		TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B * L_6 = __this->get__sauceSettings_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_6, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0047;
		}
	}
	{
		// throw new Exception("Can't find TinySauce sauceSettings file.");
		Exception_t * L_8 = (Exception_t *)il2cpp_codegen_object_new(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m89BADFF36C3B170013878726E07729D51AA9FBE0(L_8, _stringLiteral4D8E850DA5E1602C6FE45219DAAD6E932B115613, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, TinySauceBehaviour_Awake_mFAAA01A24AFF626349B4700FE195B4249FB2CCF2_RuntimeMethod_var);
	}

IL_0047:
	{
		// if (_instance != null) {
		TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5 * L_9 = ((TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5_StaticFields*)il2cpp_codegen_static_fields_for(TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5_il2cpp_TypeInfo_var))->get__instance_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_9, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0060;
		}
	}
	{
		// Destroy(gameObject);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_11, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0060:
	{
		// _instance = this;
		((TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5_StaticFields*)il2cpp_codegen_static_fields_for(TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5_il2cpp_TypeInfo_var))->set__instance_5(__this);
		// DontDestroyOnLoad(this);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m4DC90770AD6084E4B1B8489C6B41205DC020C207(__this, /*hidden argument*/NULL);
		// VoodooLog.Initialize(VoodooLogLevel.WARNING);
		VoodooLog_Initialize_m9C4412D348DE823CC4535F0327D0985EC68B5B74(1, /*hidden argument*/NULL);
		// InitAnalytics();
		TinySauceBehaviour_InitAnalytics_m0E97CA2D3FA96886CD3E6FB732B13084D73C90FA(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.TinySauceBehaviour::InitAnalytics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TinySauceBehaviour_InitAnalytics_m0E97CA2D3FA96886CD3E6FB732B13084D73C90FA (TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TinySauceBehaviour_InitAnalytics_m0E97CA2D3FA96886CD3E6FB732B13084D73C90FA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// VoodooLog.Log(TAG, "Initializing Analytics");
		VoodooLog_Log_mA7C46EE6B665A3BCC85F83F844B24039AEE02D71(_stringLiteral9C96764F709FCE75AFDF092580F09418CB63AB15, _stringLiteral70D6D05A55F416E3D28E1C5CB4D517D7495F4E19, /*hidden argument*/NULL);
		// AnalyticsManager.Initialize(_sauceSettings, true);
		TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B * L_0 = __this->get__sauceSettings_6();
		IL2CPP_RUNTIME_CLASS_INIT(AnalyticsManager_t3183DE15E51166BDEC995DB2C8952B1950C82148_il2cpp_TypeInfo_var);
		AnalyticsManager_Initialize_mFFF61A4BE10567C5D6E20DD7A4051B17E9FD5D9A(L_0, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.TinySauceBehaviour::OnApplicationPause(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TinySauceBehaviour_OnApplicationPause_m6EA76A02E0EB2780ECE18C572E36F494375C792F (TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5 * __this, bool ___pauseStatus0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TinySauceBehaviour_OnApplicationPause_m6EA76A02E0EB2780ECE18C572E36F494375C792F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!pauseStatus) {
		bool L_0 = ___pauseStatus0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		// AnalyticsManager.OnApplicationResume();
		IL2CPP_RUNTIME_CLASS_INIT(AnalyticsManager_t3183DE15E51166BDEC995DB2C8952B1950C82148_il2cpp_TypeInfo_var);
		AnalyticsManager_OnApplicationResume_m7C345F057B8BC930D89FE15CEDD83D6A5F650FB8(/*hidden argument*/NULL);
	}

IL_0008:
	{
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.TinySauceBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TinySauceBehaviour__ctor_m0E88FCBC2D30637F5A566100037FC4894F0D8B96 (TinySauceBehaviour_tD5E3841D1FD07E529DD8701B8B506AEA357A0FF5 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Voodoo.Sauce.Internal.TinySauceSettings Voodoo.Sauce.Internal.TinySauceSettings::Load()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B * TinySauceSettings_Load_m6B57575CD43B33BAE0FDDDC7CEDCCC3CF850C747 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TinySauceSettings_Load_m6B57575CD43B33BAE0FDDDC7CEDCCC3CF850C747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static TinySauceSettings Load() => Resources.Load<TinySauceSettings>(SETTING_RESOURCES_PATH);
		TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B * L_0 = Resources_Load_TisTinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B_m5A64B8561C0240EE2D968668B0B3A97CFC58B058(_stringLiteralB015E89A88FE62554D403ED2FC90E599C38D3D82, /*hidden argument*/Resources_Load_TisTinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B_m5A64B8561C0240EE2D968668B0B3A97CFC58B058_RuntimeMethod_var);
		return L_0;
	}
}
// System.Void Voodoo.Sauce.Internal.TinySauceSettings::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TinySauceSettings__ctor_m17B2849E172B818BD924A5473B9E47CD800D3837 (TinySauceSettings_t5B1AB8363F02075E1278E8B9812E26DCAA13F64B * __this, const RuntimeMethod* method)
{
	{
		ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Voodoo.Sauce.Internal.Utils.ManifestUtils::MergeManifestsFiles(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ManifestUtils_MergeManifestsFiles_m486A18A2433548D7340240185A7B2C415A284131 (String_t* ___manifestSourcePath0, String_t* ___manifestDestPath1, const RuntimeMethod* method)
{
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * V_0 = NULL;
	{
		// XmlDocument resultDocument = Add(LoadFromFile(manifestSourcePath), LoadFromFile(manifestDestPath));
		String_t* L_0 = ___manifestSourcePath0;
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_1 = ManifestUtils_LoadFromFile_m6D0336D0716E4CBF02268B16E0D7099939C301D8(L_0, /*hidden argument*/NULL);
		String_t* L_2 = ___manifestDestPath1;
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_3 = ManifestUtils_LoadFromFile_m6D0336D0716E4CBF02268B16E0D7099939C301D8(L_2, /*hidden argument*/NULL);
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_4 = ManifestUtils_Add_mBFB87E1A709BF3A7EBD8A6D26F6B4FCBBBB66675(L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// if (resultDocument == null) {
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0017;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0017:
	{
		// SaveDocumentInFile(manifestDestPath, resultDocument);
		String_t* L_6 = ___manifestDestPath1;
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_7 = V_0;
		ManifestUtils_SaveDocumentInFile_m7BB668EEBB671FCC3F827635800D4EA483725B57(L_6, L_7, /*hidden argument*/NULL);
		// return true;
		return (bool)1;
	}
}
// System.String Voodoo.Sauce.Internal.Utils.ManifestUtils::SetApplicationAttributes(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ManifestUtils_SetApplicationAttributes_mD8E4D1597CCD5EB79D87947EB02AC5B781B9A759 (String_t* ___manifestContent0, Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___keysValues1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManifestUtils_SetApplicationAttributes_mD8E4D1597CCD5EB79D87947EB02AC5B781B9A759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * V_0 = NULL;
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * V_1 = NULL;
	Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3  V_2;
	memset((&V_2), 0, sizeof(V_2));
	KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  V_3;
	memset((&V_3), 0, sizeof(V_3));
	XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * V_4 = NULL;
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* V_5 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E * G_B5_0 = NULL;
	XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E * G_B4_0 = NULL;
	{
		// XmlDocument xmlDocument = LoadFromString(manifestContent);
		String_t* L_0 = ___manifestContent0;
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_1 = ManifestUtils_LoadFromString_m49896DCFF05217BCE4279ACF6CDE7FC7B607825F(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// XmlNode manifestNode = FindChildNode(xmlDocument, "manifest");
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_2 = V_0;
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_3 = ManifestUtils_FindChildNode_mEF66F9E3B4965C85BA9B80EB7DAE77F621F0A1CF(L_2, _stringLiteralC5BEEF2DE776A9EB64B6A612ABEAC7157E1E00A4, /*hidden argument*/NULL);
		// XmlNode applicationNode = FindChildNode(manifestNode, "application");
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_4 = ManifestUtils_FindChildNode_mEF66F9E3B4965C85BA9B80EB7DAE77F621F0A1CF(L_3, _stringLiteralD2005CC206CCBFDEDF2BE43A200CB050C538BDB5, /*hidden argument*/NULL);
		V_1 = L_4;
		// foreach (KeyValuePair<string, string> pair in keysValues) {
		Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * L_5 = ___keysValues1;
		NullCheck(L_5);
		Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3  L_6 = Dictionary_2_GetEnumerator_m3378B4792B81EF81397CB9D9A761BD7149BD27F5(L_5, /*hidden argument*/Dictionary_2_GetEnumerator_m3378B4792B81EF81397CB9D9A761BD7149BD27F5_RuntimeMethod_var);
		V_2 = L_6;
	}

IL_0024:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0092;
		}

IL_0026:
		{
			// foreach (KeyValuePair<string, string> pair in keysValues) {
			KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  L_7 = Enumerator_get_Current_mBEC9B470213860581893E0F197CAAE657B8B6C69_inline((Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *)(&V_2), /*hidden argument*/Enumerator_get_Current_mBEC9B470213860581893E0F197CAAE657B8B6C69_RuntimeMethod_var);
			V_3 = L_7;
			// XmlAttribute xmlAttribute = FindAttribute(applicationNode.Attributes, pair.Key);
			XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_8 = V_1;
			NullCheck(L_8);
			XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E * L_9 = VirtFuncInvoker0< XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E * >::Invoke(14 /* System.Xml.XmlAttributeCollection System.Xml.XmlNode::get_Attributes() */, L_8);
			String_t* L_10 = KeyValuePair_2_get_Key_m434E29A1251E81B5A2124466105823011C462BF2_inline((KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 *)(&V_3), /*hidden argument*/KeyValuePair_2_get_Key_m434E29A1251E81B5A2124466105823011C462BF2_RuntimeMethod_var);
			XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * L_11 = ManifestUtils_FindAttribute_mADD12182712B3FF205596CDA738DE551C111C7A2(L_9, L_10, /*hidden argument*/NULL);
			V_4 = L_11;
			// if (xmlAttribute == null) {
			XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * L_12 = V_4;
			if (L_12)
			{
				goto IL_0084;
			}
		}

IL_0046:
		{
			// string[] keys = pair.Key.Split(':');
			String_t* L_13 = KeyValuePair_2_get_Key_m434E29A1251E81B5A2124466105823011C462BF2_inline((KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 *)(&V_3), /*hidden argument*/KeyValuePair_2_get_Key_m434E29A1251E81B5A2124466105823011C462BF2_RuntimeMethod_var);
			CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_14 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)1);
			CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_15 = L_14;
			NullCheck(L_15);
			(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)58));
			NullCheck(L_13);
			StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_16 = String_Split_m13262358217AD2C119FD1B9733C3C0289D608512(L_13, L_15, /*hidden argument*/NULL);
			V_5 = L_16;
			// xmlAttribute = xmlDocument.CreateAttribute(keys[0], keys[1], null);
			XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_17 = V_0;
			StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_18 = V_5;
			NullCheck(L_18);
			int32_t L_19 = 0;
			String_t* L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
			StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_21 = V_5;
			NullCheck(L_21);
			int32_t L_22 = 1;
			String_t* L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
			NullCheck(L_17);
			XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * L_24 = VirtFuncInvoker3< XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA *, String_t*, String_t*, String_t* >::Invoke(62 /* System.Xml.XmlAttribute System.Xml.XmlDocument::CreateAttribute(System.String,System.String,System.String) */, L_17, L_20, L_23, (String_t*)NULL);
			V_4 = L_24;
			// applicationNode.Attributes?.Append(xmlAttribute);
			XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_25 = V_1;
			NullCheck(L_25);
			XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E * L_26 = VirtFuncInvoker0< XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E * >::Invoke(14 /* System.Xml.XmlAttributeCollection System.Xml.XmlNode::get_Attributes() */, L_25);
			XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E * L_27 = L_26;
			G_B4_0 = L_27;
			if (L_27)
			{
				G_B5_0 = L_27;
				goto IL_007c;
			}
		}

IL_0079:
		{
			goto IL_0084;
		}

IL_007c:
		{
			XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * L_28 = V_4;
			NullCheck(G_B5_0);
			XmlAttributeCollection_Append_m106AD7C9DC091EE15A02BF846539AF63D5FB6137(G_B5_0, L_28, /*hidden argument*/NULL);
		}

IL_0084:
		{
			// xmlAttribute.Value = pair.Value;
			XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * L_29 = V_4;
			String_t* L_30 = KeyValuePair_2_get_Value_mEAF4B15DEEAC6EB29683A5C6569F0F50B4DEBDA2_inline((KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 *)(&V_3), /*hidden argument*/KeyValuePair_2_get_Value_mEAF4B15DEEAC6EB29683A5C6569F0F50B4DEBDA2_RuntimeMethod_var);
			NullCheck(L_29);
			VirtActionInvoker1< String_t* >::Invoke(8 /* System.Void System.Xml.XmlNode::set_Value(System.String) */, L_29, L_30);
		}

IL_0092:
		{
			// foreach (KeyValuePair<string, string> pair in keysValues) {
			bool L_31 = Enumerator_MoveNext_m6E6A22A8620F5A5582BB67E367BE5086D7D895A6((Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m6E6A22A8620F5A5582BB67E367BE5086D7D895A6_RuntimeMethod_var);
			if (L_31)
			{
				goto IL_0026;
			}
		}

IL_009b:
		{
			IL2CPP_LEAVE(0xAB, FINALLY_009d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_009d;
	}

FINALLY_009d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m16C0E963A012498CD27422B463DB327BA4C7A321((Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *)(&V_2), /*hidden argument*/Enumerator_Dispose_m16C0E963A012498CD27422B463DB327BA4C7A321_RuntimeMethod_var);
		IL2CPP_END_FINALLY(157)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(157)
	{
		IL2CPP_JUMP_TBL(0xAB, IL_00ab)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00ab:
	{
		// return xmlDocument.OuterXml;
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_32 = V_0;
		NullCheck(L_32);
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(35 /* System.String System.Xml.XmlNode::get_OuterXml() */, L_32);
		return L_33;
	}
}
// System.Boolean Voodoo.Sauce.Internal.Utils.ManifestUtils::ReplaceKeys(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ManifestUtils_ReplaceKeys_mC82D1EA16D1BB27023A9C83F66B8C59971A0CFD1 (String_t* ___manifestPath0, Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___keysValues1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManifestUtils_ReplaceKeys_mC82D1EA16D1BB27023A9C83F66B8C59971A0CFD1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3  V_1;
	memset((&V_1), 0, sizeof(V_1));
	KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// if (File.Exists(manifestPath)) {
		String_t* L_0 = ___manifestPath0;
		bool L_1 = File_Exists_m6B9BDD8EEB33D744EB0590DD27BC0152FAFBD1FB(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0057;
		}
	}
	{
		// string manifestContent = File.ReadAllText(manifestPath);
		String_t* L_2 = ___manifestPath0;
		String_t* L_3 = File_ReadAllText_m404A1BE4C87AC3C7B9C0B07469CDC44DE52817FF(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// foreach (KeyValuePair<string, string> pair in keysValues) {
		Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * L_4 = ___keysValues1;
		NullCheck(L_4);
		Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3  L_5 = Dictionary_2_GetEnumerator_m3378B4792B81EF81397CB9D9A761BD7149BD27F5(L_4, /*hidden argument*/Dictionary_2_GetEnumerator_m3378B4792B81EF81397CB9D9A761BD7149BD27F5_RuntimeMethod_var);
		V_1 = L_5;
	}

IL_0016:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0035;
		}

IL_0018:
		{
			// foreach (KeyValuePair<string, string> pair in keysValues) {
			KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  L_6 = Enumerator_get_Current_mBEC9B470213860581893E0F197CAAE657B8B6C69_inline((Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *)(&V_1), /*hidden argument*/Enumerator_get_Current_mBEC9B470213860581893E0F197CAAE657B8B6C69_RuntimeMethod_var);
			V_2 = L_6;
			// manifestContent = manifestContent.Replace(pair.Key, pair.Value);
			String_t* L_7 = V_0;
			String_t* L_8 = KeyValuePair_2_get_Key_m434E29A1251E81B5A2124466105823011C462BF2_inline((KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 *)(&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m434E29A1251E81B5A2124466105823011C462BF2_RuntimeMethod_var);
			String_t* L_9 = KeyValuePair_2_get_Value_mEAF4B15DEEAC6EB29683A5C6569F0F50B4DEBDA2_inline((KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 *)(&V_2), /*hidden argument*/KeyValuePair_2_get_Value_mEAF4B15DEEAC6EB29683A5C6569F0F50B4DEBDA2_RuntimeMethod_var);
			NullCheck(L_7);
			String_t* L_10 = String_Replace_m970DFB0A280952FA7D3BA20AB7A8FB9F80CF6470(L_7, L_8, L_9, /*hidden argument*/NULL);
			V_0 = L_10;
		}

IL_0035:
		{
			// foreach (KeyValuePair<string, string> pair in keysValues) {
			bool L_11 = Enumerator_MoveNext_m6E6A22A8620F5A5582BB67E367BE5086D7D895A6((Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m6E6A22A8620F5A5582BB67E367BE5086D7D895A6_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0018;
			}
		}

IL_003e:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m16C0E963A012498CD27422B463DB327BA4C7A321((Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *)(&V_1), /*hidden argument*/Enumerator_Dispose_m16C0E963A012498CD27422B463DB327BA4C7A321_RuntimeMethod_var);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_004e:
	{
		// File.WriteAllText(manifestPath, manifestContent);
		String_t* L_12 = ___manifestPath0;
		String_t* L_13 = V_0;
		File_WriteAllText_m7BA355E5631C6A3E3D3378D6101EF65E72A45F0A(L_12, L_13, /*hidden argument*/NULL);
		// return true;
		return (bool)1;
	}

IL_0057:
	{
		// return false;
		return (bool)0;
	}
}
// System.Xml.XmlDocument Voodoo.Sauce.Internal.Utils.ManifestUtils::Add(System.Xml.XmlDocument,System.Xml.XmlDocument)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ManifestUtils_Add_mBFB87E1A709BF3A7EBD8A6D26F6B4FCBBBB66675 (XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___sourceDocument0, XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___destDocument1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManifestUtils_Add_mBFB87E1A709BF3A7EBD8A6D26F6B4FCBBBB66675_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * V_1 = NULL;
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * V_4 = NULL;
	RuntimeObject* V_5 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * G_B3_0 = NULL;
	XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * G_B7_0 = NULL;
	{
		// if (sourceDocument?.DocumentElement == null || destDocument?.DocumentElement == null) {
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_0 = ___sourceDocument0;
		if (L_0)
		{
			goto IL_0006;
		}
	}
	{
		G_B3_0 = ((XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC *)(NULL));
		goto IL_000c;
	}

IL_0006:
	{
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_1 = ___sourceDocument0;
		NullCheck(L_1);
		XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * L_2 = XmlDocument_get_DocumentElement_m4834A8AB3916884BA14431A2E084C2C8A76E2EDA(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_000c:
	{
		if (!G_B3_0)
		{
			goto IL_001c;
		}
	}
	{
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_3 = ___destDocument1;
		if (L_3)
		{
			goto IL_0014;
		}
	}
	{
		G_B7_0 = ((XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC *)(NULL));
		goto IL_001a;
	}

IL_0014:
	{
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_4 = ___destDocument1;
		NullCheck(L_4);
		XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * L_5 = XmlDocument_get_DocumentElement_m4834A8AB3916884BA14431A2E084C2C8A76E2EDA(L_4, /*hidden argument*/NULL);
		G_B7_0 = L_5;
	}

IL_001a:
	{
		if (G_B7_0)
		{
			goto IL_001e;
		}
	}

IL_001c:
	{
		// return null;
		return (XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 *)NULL;
	}

IL_001e:
	{
		// foreach (XmlNode nodeSource in sourceDocument.DocumentElement.ChildNodes) {
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_6 = ___sourceDocument0;
		NullCheck(L_6);
		XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * L_7 = XmlDocument_get_DocumentElement_m4834A8AB3916884BA14431A2E084C2C8A76E2EDA(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		XmlNodeList_t6A2162EDB563F1707F00C5156460E1073244C8E7 * L_8 = VirtFuncInvoker0< XmlNodeList_t6A2162EDB563F1707F00C5156460E1073244C8E7 * >::Invoke(11 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_7);
		NullCheck(L_8);
		RuntimeObject* L_9 = VirtFuncInvoker0< RuntimeObject* >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_8);
		V_0 = L_9;
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_009c;
		}

IL_0031:
		{
			// foreach (XmlNode nodeSource in sourceDocument.DocumentElement.ChildNodes) {
			RuntimeObject* L_10 = V_0;
			NullCheck(L_10);
			RuntimeObject * L_11 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_10);
			V_1 = ((XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB *)CastclassClass((RuntimeObject*)L_11, XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB_il2cpp_TypeInfo_var));
			// if (nodeSource.HasChildNodes) {
			XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_12 = V_1;
			NullCheck(L_12);
			bool L_13 = VirtFuncInvoker0< bool >::Invoke(26 /* System.Boolean System.Xml.XmlNode::get_HasChildNodes() */, L_12);
			if (!L_13)
			{
				goto IL_0094;
			}
		}

IL_0045:
		{
			// XmlNode destNode = FindChildNode(destDocument.DocumentElement, nodeSource);
			XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_14 = ___destDocument1;
			NullCheck(L_14);
			XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * L_15 = XmlDocument_get_DocumentElement_m4834A8AB3916884BA14431A2E084C2C8A76E2EDA(L_14, /*hidden argument*/NULL);
			XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_16 = V_1;
			XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_17 = ManifestUtils_FindChildNode_m8CAAAC87A37AFCCEAB570F078FEDF556683973F3(L_15, L_16, /*hidden argument*/NULL);
			V_2 = L_17;
			// foreach (XmlNode node in nodeSource.ChildNodes) {
			XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_18 = V_1;
			NullCheck(L_18);
			XmlNodeList_t6A2162EDB563F1707F00C5156460E1073244C8E7 * L_19 = VirtFuncInvoker0< XmlNodeList_t6A2162EDB563F1707F00C5156460E1073244C8E7 * >::Invoke(11 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_18);
			NullCheck(L_19);
			RuntimeObject* L_20 = VirtFuncInvoker0< RuntimeObject* >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_19);
			V_3 = L_20;
		}

IL_005e:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0076;
			}

IL_0060:
			{
				// foreach (XmlNode node in nodeSource.ChildNodes) {
				RuntimeObject* L_21 = V_3;
				NullCheck(L_21);
				RuntimeObject * L_22 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_21);
				V_4 = ((XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB *)CastclassClass((RuntimeObject*)L_22, XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB_il2cpp_TypeInfo_var));
				// AddChildNode(destDocument, destNode, node);
				XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_23 = ___destDocument1;
				XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_24 = V_2;
				XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_25 = V_4;
				ManifestUtils_AddChildNode_m0774E2B100503D9E06CB49A9CC70542AB8467B1D(L_23, L_24, L_25, /*hidden argument*/NULL);
			}

IL_0076:
			{
				// foreach (XmlNode node in nodeSource.ChildNodes) {
				RuntimeObject* L_26 = V_3;
				NullCheck(L_26);
				bool L_27 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_26);
				if (L_27)
				{
					goto IL_0060;
				}
			}

IL_007e:
			{
				IL2CPP_LEAVE(0x9C, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			{
				RuntimeObject* L_28 = V_3;
				V_5 = ((RuntimeObject*)IsInst((RuntimeObject*)L_28, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var));
				RuntimeObject* L_29 = V_5;
				if (!L_29)
				{
					goto IL_0093;
				}
			}

IL_008c:
			{
				RuntimeObject* L_30 = V_5;
				NullCheck(L_30);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_30);
			}

IL_0093:
			{
				IL2CPP_END_FINALLY(128)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x9C, IL_009c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0094:
		{
			// AddChildNode(destDocument, destDocument, nodeSource);
			XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_31 = ___destDocument1;
			XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_32 = ___destDocument1;
			XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_33 = V_1;
			ManifestUtils_AddChildNode_m0774E2B100503D9E06CB49A9CC70542AB8467B1D(L_31, L_32, L_33, /*hidden argument*/NULL);
		}

IL_009c:
		{
			// foreach (XmlNode nodeSource in sourceDocument.DocumentElement.ChildNodes) {
			RuntimeObject* L_34 = V_0;
			NullCheck(L_34);
			bool L_35 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_34);
			if (L_35)
			{
				goto IL_0031;
			}
		}

IL_00a4:
		{
			IL2CPP_LEAVE(0xBA, FINALLY_00a6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00a6;
	}

FINALLY_00a6:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_36 = V_0;
			V_5 = ((RuntimeObject*)IsInst((RuntimeObject*)L_36, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var));
			RuntimeObject* L_37 = V_5;
			if (!L_37)
			{
				goto IL_00b9;
			}
		}

IL_00b2:
		{
			RuntimeObject* L_38 = V_5;
			NullCheck(L_38);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_38);
		}

IL_00b9:
		{
			IL2CPP_END_FINALLY(166)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(166)
	{
		IL2CPP_JUMP_TBL(0xBA, IL_00ba)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00ba:
	{
		// return destDocument;
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_39 = ___destDocument1;
		return L_39;
	}
}
// System.Xml.XmlDocument Voodoo.Sauce.Internal.Utils.ManifestUtils::LoadFromFile(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ManifestUtils_LoadFromFile_m6D0336D0716E4CBF02268B16E0D7099939C301D8 (String_t* ___manifestPath0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManifestUtils_LoadFromFile_m6D0336D0716E4CBF02268B16E0D7099939C301D8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * V_0 = NULL;
	{
		// XmlDocument document = null;
		V_0 = (XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 *)NULL;
		// if (File.Exists(manifestPath)) {
		String_t* L_0 = ___manifestPath0;
		bool L_1 = File_Exists_m6B9BDD8EEB33D744EB0590DD27BC0152FAFBD1FB(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		// document = new XmlDocument();
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_2 = (XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 *)il2cpp_codegen_object_new(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_il2cpp_TypeInfo_var);
		XmlDocument__ctor_mB8BC8FA423C0BB144B91D00FA93246B8D0CFA490(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		// document.Load(manifestPath);
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_3 = V_0;
		String_t* L_4 = ___manifestPath0;
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void System.Xml.XmlDocument::Load(System.String) */, L_3, L_4);
	}

IL_0017:
	{
		// return document;
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_5 = V_0;
		return L_5;
	}
}
// System.Xml.XmlDocument Voodoo.Sauce.Internal.Utils.ManifestUtils::LoadFromString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ManifestUtils_LoadFromString_m49896DCFF05217BCE4279ACF6CDE7FC7B607825F (String_t* ___manifestContent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManifestUtils_LoadFromString_m49896DCFF05217BCE4279ACF6CDE7FC7B607825F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var document = new XmlDocument();
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_0 = (XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 *)il2cpp_codegen_object_new(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_il2cpp_TypeInfo_var);
		XmlDocument__ctor_mB8BC8FA423C0BB144B91D00FA93246B8D0CFA490(L_0, /*hidden argument*/NULL);
		// document.LoadXml(manifestContent);
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_1 = L_0;
		String_t* L_2 = ___manifestContent0;
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(67 /* System.Void System.Xml.XmlDocument::LoadXml(System.String) */, L_1, L_2);
		// return document;
		return L_1;
	}
}
// System.Void Voodoo.Sauce.Internal.Utils.ManifestUtils::AddChildNode(System.Xml.XmlDocument,System.Xml.XmlNode,System.Xml.XmlNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManifestUtils_AddChildNode_m0774E2B100503D9E06CB49A9CC70542AB8467B1D (XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___document0, XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___parent1, XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___node2, const RuntimeMethod* method)
{
	{
		// if (parent != null && node != null && !FindElementWithAndroidName(parent, node)) {
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_0 = ___parent1;
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_1 = ___node2;
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_2 = ___parent1;
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_3 = ___node2;
		bool L_4 = ManifestUtils_FindElementWithAndroidName_m040EF519E28229C677A7625260255A813176EC39(L_2, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_001e;
		}
	}
	{
		// parent.AppendChild(document.ImportNode(node, true));
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_5 = ___parent1;
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_6 = ___document0;
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_7 = ___node2;
		NullCheck(L_6);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_8 = VirtFuncInvoker2< XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB *, XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB *, bool >::Invoke(61 /* System.Xml.XmlNode System.Xml.XmlDocument::ImportNode(System.Xml.XmlNode,System.Boolean) */, L_6, L_7, (bool)1);
		NullCheck(L_5);
		VirtFuncInvoker1< XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB *, XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * >::Invoke(22 /* System.Xml.XmlNode System.Xml.XmlNode::AppendChild(System.Xml.XmlNode) */, L_5, L_8);
	}

IL_001e:
	{
		// }
		return;
	}
}
// System.Xml.XmlNode Voodoo.Sauce.Internal.Utils.ManifestUtils::FindChildNode(System.Xml.XmlNode,System.Xml.XmlNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ManifestUtils_FindChildNode_m8CAAAC87A37AFCCEAB570F078FEDF556683973F3 (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___parent0, XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___child1, const RuntimeMethod* method)
{
	{
		// private static XmlNode FindChildNode(XmlNode parent, XmlNode child) => FindChildNode(parent, child.Name);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_0 = ___parent0;
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_1 = ___child1;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Xml.XmlNode::get_Name() */, L_1);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_3 = ManifestUtils_FindChildNode_mEF66F9E3B4965C85BA9B80EB7DAE77F621F0A1CF(L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Xml.XmlAttribute Voodoo.Sauce.Internal.Utils.ManifestUtils::FindAttribute(System.Collections.IEnumerable,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * ManifestUtils_FindAttribute_mADD12182712B3FF205596CDA738DE551C111C7A2 (RuntimeObject* ___parent0, String_t* ___name1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManifestUtils_FindAttribute_mADD12182712B3FF205596CDA738DE551C111C7A2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * V_1 = NULL;
	XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// foreach (XmlAttribute attribute in parent) {
		RuntimeObject* L_0 = ___parent0;
		NullCheck(L_0);
		RuntimeObject* L_1 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_tD74549CEA1AA48E768382B94FEACBB07E2E3FA2C_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0027;
		}

IL_0009:
		{
			// foreach (XmlAttribute attribute in parent) {
			RuntimeObject* L_2 = V_0;
			NullCheck(L_2);
			RuntimeObject * L_3 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_2);
			V_1 = ((XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA *)CastclassClass((RuntimeObject*)L_3, XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA_il2cpp_TypeInfo_var));
			// if (attribute.Name == name) {
			XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * L_4 = V_1;
			NullCheck(L_4);
			String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Xml.XmlNode::get_Name() */, L_4);
			String_t* L_6 = ___name1;
			bool L_7 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_5, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0027;
			}
		}

IL_0023:
		{
			// return attribute;
			XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * L_8 = V_1;
			V_2 = L_8;
			IL2CPP_LEAVE(0x44, FINALLY_0031);
		}

IL_0027:
		{
			// foreach (XmlAttribute attribute in parent) {
			RuntimeObject* L_9 = V_0;
			NullCheck(L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_0009;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x42, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_11 = V_0;
			V_3 = ((RuntimeObject*)IsInst((RuntimeObject*)L_11, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var));
			RuntimeObject* L_12 = V_3;
			if (!L_12)
			{
				goto IL_0041;
			}
		}

IL_003b:
		{
			RuntimeObject* L_13 = V_3;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_13);
		}

IL_0041:
		{
			IL2CPP_END_FINALLY(49)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0042:
	{
		// return null;
		return (XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA *)NULL;
	}

IL_0044:
	{
		// }
		XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA * L_14 = V_2;
		return L_14;
	}
}
// System.Xml.XmlNode Voodoo.Sauce.Internal.Utils.ManifestUtils::FindChildNode(System.Xml.XmlNode,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ManifestUtils_FindChildNode_mEF66F9E3B4965C85BA9B80EB7DAE77F621F0A1CF (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___parent0, String_t* ___childName1, const RuntimeMethod* method)
{
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * V_0 = NULL;
	{
		// XmlNode node = parent.FirstChild;
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_0 = ___parent0;
		NullCheck(L_0);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_1 = VirtFuncInvoker0< XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * >::Invoke(16 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_0);
		V_0 = L_1;
		goto IL_0020;
	}

IL_0009:
	{
		// if (node.Name.Equals(childName)) {
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Xml.XmlNode::get_Name() */, L_2);
		String_t* L_4 = ___childName1;
		NullCheck(L_3);
		bool L_5 = String_Equals_m9C4D78DFA0979504FE31429B64A4C26DF48020D1(L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0019;
		}
	}
	{
		// return node;
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_6 = V_0;
		return L_6;
	}

IL_0019:
	{
		// node = node.NextSibling;
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_7 = V_0;
		NullCheck(L_7);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_8 = VirtFuncInvoker0< XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * >::Invoke(13 /* System.Xml.XmlNode System.Xml.XmlNode::get_NextSibling() */, L_7);
		V_0 = L_8;
	}

IL_0020:
	{
		// while (node != null) {
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_9 = V_0;
		if (L_9)
		{
			goto IL_0009;
		}
	}
	{
		// return null;
		return (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB *)NULL;
	}
}
// System.Boolean Voodoo.Sauce.Internal.Utils.ManifestUtils::FindElementWithAndroidName(System.Xml.XmlNode,System.Xml.XmlNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ManifestUtils_FindElementWithAndroidName_m040EF519E28229C677A7625260255A813176EC39 (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___parent0, XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___child1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManifestUtils_FindElementWithAndroidName_m040EF519E28229C677A7625260255A813176EC39_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * V_2 = NULL;
	{
		// string namespaceOfPrefix = parent.GetNamespaceOfPrefix("android");
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_0 = ___parent0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(41 /* System.String System.Xml.XmlNode::GetNamespaceOfPrefix(System.String) */, L_0, _stringLiteralE4BBE5B7A4C1EB55652965AEE885DD59BD2EE7F4);
		V_0 = L_1;
		// string childName = GetAndroidElementName(child, namespaceOfPrefix);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_2 = ___child1;
		String_t* L_3 = V_0;
		String_t* L_4 = ManifestUtils_GetAndroidElementName_mC68A244A12CA5110AE421509EC1D90B18F30B050(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		// if (childName != null) {
		String_t* L_5 = V_1;
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		// XmlNode node = parent.FirstChild;
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_6 = ___parent0;
		NullCheck(L_6);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_7 = VirtFuncInvoker0< XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * >::Invoke(16 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_6);
		V_2 = L_7;
		goto IL_0038;
	}

IL_0020:
	{
		// if (GetAndroidElementName(node, namespaceOfPrefix) == childName) {
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_8 = V_2;
		String_t* L_9 = V_0;
		String_t* L_10 = ManifestUtils_GetAndroidElementName_mC68A244A12CA5110AE421509EC1D90B18F30B050(L_8, L_9, /*hidden argument*/NULL);
		String_t* L_11 = V_1;
		bool L_12 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0031;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0031:
	{
		// node = node.NextSibling;
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_13 = V_2;
		NullCheck(L_13);
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_14 = VirtFuncInvoker0< XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * >::Invoke(13 /* System.Xml.XmlNode System.Xml.XmlNode::get_NextSibling() */, L_13);
		V_2 = L_14;
	}

IL_0038:
	{
		// while (node != null) {
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_15 = V_2;
		if (L_15)
		{
			goto IL_0020;
		}
	}

IL_003b:
	{
		// return false;
		return (bool)0;
	}
}
// System.String Voodoo.Sauce.Internal.Utils.ManifestUtils::GetAndroidElementName(System.Xml.XmlNode,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ManifestUtils_GetAndroidElementName_mC68A244A12CA5110AE421509EC1D90B18F30B050 (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___node0, String_t* ___namespaceOfPrefix1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManifestUtils_GetAndroidElementName_mC68A244A12CA5110AE421509EC1D90B18F30B050_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * V_0 = NULL;
	{
		// node is XmlElement element ? element.GetAttribute("name", namespaceOfPrefix) : null;
		XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * L_0 = ___node0;
		XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * L_1 = ((XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC *)IsInstClass((RuntimeObject*)L_0, XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC_il2cpp_TypeInfo_var));
		V_0 = L_1;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000c:
	{
		XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * L_2 = V_0;
		String_t* L_3 = ___namespaceOfPrefix1;
		NullCheck(L_2);
		String_t* L_4 = VirtFuncInvoker2< String_t*, String_t*, String_t* >::Invoke(51 /* System.String System.Xml.XmlElement::GetAttribute(System.String,System.String) */, L_2, _stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, L_3);
		return L_4;
	}
}
// System.Void Voodoo.Sauce.Internal.Utils.ManifestUtils::SaveDocumentInFile(System.String,System.Xml.XmlDocument)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManifestUtils_SaveDocumentInFile_m7BB668EEBB671FCC3F827635800D4EA483725B57 (String_t* ___manifestPath0, XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___document1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ManifestUtils_SaveDocumentInFile_m7BB668EEBB671FCC3F827635800D4EA483725B57_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * V_0 = NULL;
	XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// var set = new XmlWriterSettings {
		//     Indent = true,
		//     IndentChars = "  ",
		//     NewLineChars = "\r\n",
		//     NewLineHandling = NewLineHandling.Replace
		// };
		XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * L_0 = (XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 *)il2cpp_codegen_object_new(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3_il2cpp_TypeInfo_var);
		XmlWriterSettings__ctor_m5EA6551DB71068AFEAEB8AA92E684F3F82CF9017(L_0, /*hidden argument*/NULL);
		XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * L_1 = L_0;
		NullCheck(L_1);
		XmlWriterSettings_set_Indent_m4B6DC1D7D5183258ADAF18CD4C23090BC9933F9A(L_1, (bool)1, /*hidden argument*/NULL);
		XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * L_2 = L_1;
		NullCheck(L_2);
		XmlWriterSettings_set_IndentChars_m3526849AD9EC3996EA5D35AB330E674D0BCE4338(L_2, _stringLiteral099600A10A944114AAC406D136B625FB416DD779, /*hidden argument*/NULL);
		XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * L_3 = L_2;
		NullCheck(L_3);
		XmlWriterSettings_set_NewLineChars_mB9F47DA8097C733C10DCFEAE2E3CE6C9D2321C6D(L_3, _stringLiteralBA8AB5A0280B953AA97435FF8946CBCBB2755A27, /*hidden argument*/NULL);
		XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * L_4 = L_3;
		NullCheck(L_4);
		XmlWriterSettings_set_NewLineHandling_mD8C993B04A8A8518EAD20E85A2E528BD65F522C8(L_4, 0, /*hidden argument*/NULL);
		V_0 = L_4;
		// using (var xmlWriter = XmlWriter.Create(manifestPath, set)) {
		String_t* L_5 = ___manifestPath0;
		XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * L_6 = V_0;
		XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * L_7 = XmlWriter_Create_mB11A06263B3E7A4B91EC026CEDF9D828C4424050(L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
	}

IL_0032:
	try
	{ // begin try (depth: 1)
		// document.Save(xmlWriter);
		XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * L_8 = ___document1;
		XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * L_9 = V_1;
		NullCheck(L_8);
		VirtActionInvoker1< XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * >::Invoke(68 /* System.Void System.Xml.XmlDocument::Save(System.Xml.XmlWriter) */, L_8, L_9);
		// }
		IL2CPP_LEAVE(0x45, FINALLY_003b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		{
			XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * L_10 = V_1;
			if (!L_10)
			{
				goto IL_0044;
			}
		}

IL_003e:
		{
			XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * L_11 = V_1;
			NullCheck(L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_11);
		}

IL_0044:
		{
			IL2CPP_END_FINALLY(59)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0045:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Voodoo.Sauce.Internal.VoodooLog::Initialize(Voodoo.Sauce.Internal.VoodooLogLevel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoodooLog_Initialize_m9C4412D348DE823CC4535F0327D0985EC68B5B74 (int32_t ___level0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoodooLog_Initialize_m9C4412D348DE823CC4535F0327D0985EC68B5B74_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _logLevel = level;
		int32_t L_0 = ___level0;
		((VoodooLog_t3E10EB33560DF558C5ADC45DF8FFF5ADCFFF9A24_StaticFields*)il2cpp_codegen_static_fields_for(VoodooLog_t3E10EB33560DF558C5ADC45DF8FFF5ADCFFF9A24_il2cpp_TypeInfo_var))->set__logLevel_0(L_0);
		// EnableLogs();
		VoodooLog_EnableLogs_mCF301D8664674A872023E1673BD6A4A65D989B46(/*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.VoodooLog::Log(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoodooLog_Log_mA7C46EE6B665A3BCC85F83F844B24039AEE02D71 (String_t* ___tag0, String_t* ___message1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoodooLog_Log_mA7C46EE6B665A3BCC85F83F844B24039AEE02D71_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor || _logLevel >= VoodooLogLevel.DEBUG)
		bool L_0 = Application_get_isEditor_m347E6EE16E5109EF613C83ED98DB1EC6E3EF5E26(/*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ((VoodooLog_t3E10EB33560DF558C5ADC45DF8FFF5ADCFFF9A24_StaticFields*)il2cpp_codegen_static_fields_for(VoodooLog_t3E10EB33560DF558C5ADC45DF8FFF5ADCFFF9A24_il2cpp_TypeInfo_var))->get__logLevel_0();
		if ((((int32_t)L_1) < ((int32_t)2)))
		{
			goto IL_001b;
		}
	}

IL_000f:
	{
		// Debug.Log(Format(tag, message));
		String_t* L_2 = ___tag0;
		String_t* L_3 = ___message1;
		String_t* L_4 = VoodooLog_Format_m3B92528E7DEBA4AE65FE81F97571702522A85DFE(L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_4, /*hidden argument*/NULL);
	}

IL_001b:
	{
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.VoodooLog::LogE(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoodooLog_LogE_mE4E1FC2258F9C025F5E965B9D6FE302F8346ED69 (String_t* ___tag0, String_t* ___message1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoodooLog_LogE_mE4E1FC2258F9C025F5E965B9D6FE302F8346ED69_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor || _logLevel >= VoodooLogLevel.ERROR)
		bool L_0 = Application_get_isEditor_m347E6EE16E5109EF613C83ED98DB1EC6E3EF5E26(/*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ((VoodooLog_t3E10EB33560DF558C5ADC45DF8FFF5ADCFFF9A24_StaticFields*)il2cpp_codegen_static_fields_for(VoodooLog_t3E10EB33560DF558C5ADC45DF8FFF5ADCFFF9A24_il2cpp_TypeInfo_var))->get__logLevel_0();
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_001b;
		}
	}

IL_000f:
	{
		// Debug.LogError(Format(tag, message));
		String_t* L_2 = ___tag0;
		String_t* L_3 = ___message1;
		String_t* L_4 = VoodooLog_Format_m3B92528E7DEBA4AE65FE81F97571702522A85DFE(L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(L_4, /*hidden argument*/NULL);
	}

IL_001b:
	{
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.VoodooLog::LogW(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoodooLog_LogW_m1B8171158AD9DCD531714EA008F3EE5F6115ED74 (String_t* ___tag0, String_t* ___message1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoodooLog_LogW_m1B8171158AD9DCD531714EA008F3EE5F6115ED74_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!Application.isEditor || _logLevel >= VoodooLogLevel.WARNING)
		bool L_0 = Application_get_isEditor_m347E6EE16E5109EF613C83ED98DB1EC6E3EF5E26(/*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ((VoodooLog_t3E10EB33560DF558C5ADC45DF8FFF5ADCFFF9A24_StaticFields*)il2cpp_codegen_static_fields_for(VoodooLog_t3E10EB33560DF558C5ADC45DF8FFF5ADCFFF9A24_il2cpp_TypeInfo_var))->get__logLevel_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001b;
		}
	}

IL_000f:
	{
		// Debug.LogWarning(Format(tag, message));
		String_t* L_2 = ___tag0;
		String_t* L_3 = ___message1;
		String_t* L_4 = VoodooLog_Format_m3B92528E7DEBA4AE65FE81F97571702522A85DFE(L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(L_4, /*hidden argument*/NULL);
	}

IL_001b:
	{
		// }
		return;
	}
}
// System.String Voodoo.Sauce.Internal.VoodooLog::Format(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* VoodooLog_Format_m3B92528E7DEBA4AE65FE81F97571702522A85DFE (String_t* ___tag0, String_t* ___message1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoodooLog_Format_m3B92528E7DEBA4AE65FE81F97571702522A85DFE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static string Format(string tag, string message) => $"{DateTime.Now} - {TAG}/{tag}: {message}";
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_il2cpp_TypeInfo_var);
		DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  L_2 = DateTime_get_Now_mB464D30F15C97069F92C1F910DCDDC3DFCC7F7D2(/*hidden argument*/NULL);
		DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  L_3 = L_2;
		RuntimeObject * L_4 = Box(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral9C96764F709FCE75AFDF092580F09418CB63AB15);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)_stringLiteral9C96764F709FCE75AFDF092580F09418CB63AB15);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_6 = L_5;
		String_t* L_7 = ___tag0;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_8 = L_6;
		String_t* L_9 = ___message1;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_9);
		String_t* L_10 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral19E4C1BBB9DCB1CCCF84033DF6077C233FAA301C, L_8, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void Voodoo.Sauce.Internal.VoodooLog::DisableLogs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoodooLog_DisableLogs_mB02FC2E65F7485E3AC4F142F6550E5CC21A90B69 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoodooLog_DisableLogs_mB02FC2E65F7485E3AC4F142F6550E5CC21A90B69_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.unityLogger.logEnabled = false;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_mFA75EC397E067D09FD66D56B4E7692C3FCC3E960(/*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UnityEngine.ILogger::set_logEnabled(System.Boolean) */, ILogger_t572B66532D8EB6E76240476A788384A26D70866F_il2cpp_TypeInfo_var, L_0, (bool)0);
		// }
		return;
	}
}
// System.Void Voodoo.Sauce.Internal.VoodooLog::EnableLogs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoodooLog_EnableLogs_mCF301D8664674A872023E1673BD6A4A65D989B46 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoodooLog_EnableLogs_mCF301D8664674A872023E1673BD6A4A65D989B46_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.unityLogger.logEnabled = true;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_mFA75EC397E067D09FD66D56B4E7692C3FCC3E960(/*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UnityEngine.ILogger::set_logEnabled(System.Boolean) */, ILogger_t572B66532D8EB6E76240476A788384A26D70866F_il2cpp_TypeInfo_var, L_0, (bool)1);
		// }
		return;
	}
}
// System.Boolean Voodoo.Sauce.Internal.VoodooLog::IsLogsEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool VoodooLog_IsLogsEnabled_mD4375E25A3B9DDFDF46A87CEA0E869977AAE749B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoodooLog_IsLogsEnabled_mD4375E25A3B9DDFDF46A87CEA0E869977AAE749B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static bool IsLogsEnabled() => Debug.unityLogger.logEnabled;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = Debug_get_unityLogger_mFA75EC397E067D09FD66D56B4E7692C3FCC3E960(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean UnityEngine.ILogger::get_logEnabled() */, ILogger_t572B66532D8EB6E76240476A788384A26D70866F_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Winpopup::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Winpopup_Awake_mB4049D084B9D7FD1A69267D9FE3051DDBC5FF066 (Winpopup_t3BEDB5A4227E29A985A517FB8F2214ABA2396F0C * __this, const RuntimeMethod* method)
{
	{
		// wapper.SetActive(false);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_wapper_6();
		NullCheck(L_0);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_0, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Winpopup::Visible(UnityEngine.Vector2,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Winpopup_Visible_m1A79AFA1415DE6AFA54D3736B3E030C85C67588D (Winpopup_t3BEDB5A4227E29A985A517FB8F2214ABA2396F0C * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___kill0, int32_t ___bill1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Winpopup_Visible_m1A79AFA1415DE6AFA54D3736B3E030C85C67588D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// wapper.SetActive(true);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_wapper_6();
		NullCheck(L_0);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_0, (bool)1, /*hidden argument*/NULL);
		// scoreLabel.text = string.Format("{0}/{1}", kill.x, kill.y);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_1 = __this->get_scoreLabel_4();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = ___kill0;
		float L_3 = L_2.get_x_0();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_4);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_6 = ___kill0;
		float L_7 = L_6.get_y_1();
		float L_8 = L_7;
		RuntimeObject * L_9 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_8);
		String_t* L_10 = String_Format_m19325298DBC61AAC016C16F7B3CF97A8A3DEA34A(_stringLiteralEAEF99A09E561A86004BAEB87A80BB4CFCE8CE67, L_5, L_9, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_10);
		// billLabel.text = string.Format("+{0}", bill);
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_11 = __this->get_billLabel_5();
		int32_t L_12 = ___bill1;
		int32_t L_13 = L_12;
		RuntimeObject * L_14 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_13);
		String_t* L_15 = String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA(_stringLiteral1C9C5631B79CAE61BFD7B812E162914C6B04C5FB, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_15);
		// }
		return;
	}
}
// System.Void Winpopup::NextLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Winpopup_NextLevel_m32266E8FE87DC639A4A06D85480C43F2806B7FC7 (Winpopup_t3BEDB5A4227E29A985A517FB8F2214ABA2396F0C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Winpopup_NextLevel_m32266E8FE87DC639A4A06D85480C43F2806B7FC7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("Next level");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteralD7525D28A8645F1ED44D717CC1E92F39E9D6EB3E, /*hidden argument*/NULL);
		// if (GameManager.instance.autoBoss || PlayerPrefs.GetInt("Level") % 5 == 0)
		GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * L_0 = Singleton_1_get_instance_m0D5DA7AAAEF2F4368826A7C6C88F129C79B6EE9B(/*hidden argument*/Singleton_1_get_instance_m0D5DA7AAAEF2F4368826A7C6C88F129C79B6EE9B_RuntimeMethod_var);
		NullCheck(L_0);
		bool L_1 = L_0->get_autoBoss_20();
		if (L_1)
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_2 = PlayerPrefs_GetInt_m318D2B42E0FCAF179BF86D6C2353B38A58089BAD(_stringLiteral7C7F5D049FAD2569721D446C4A811F9BD5DA5393, /*hidden argument*/NULL);
		if (((int32_t)((int32_t)L_2%(int32_t)5)))
		{
			goto IL_003a;
		}
	}

IL_0024:
	{
		// PlayerPrefs.SetInt("Map", PlayerPrefs.GetInt("Map") + 1);
		int32_t L_3 = PlayerPrefs_GetInt_m318D2B42E0FCAF179BF86D6C2353B38A58089BAD(_stringLiteralAB478F3EFC840EEBAB919DFF1B9512286F70C10C, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_mBF4101DF829B4738CCC293E1C2D173AEE45EFE62(_stringLiteralAB478F3EFC840EEBAB919DFF1B9512286F70C10C, ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)), /*hidden argument*/NULL);
	}

IL_003a:
	{
		// GameManager.instance.NextLevel();
		GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * L_4 = Singleton_1_get_instance_m0D5DA7AAAEF2F4368826A7C6C88F129C79B6EE9B(/*hidden argument*/Singleton_1_get_instance_m0D5DA7AAAEF2F4368826A7C6C88F129C79B6EE9B_RuntimeMethod_var);
		NullCheck(L_4);
		GameManager_NextLevel_mF6C956ECA09992B9D36B913E2273B39C076B71F2(L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Winpopup::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Winpopup__ctor_m9820769F6988324A8E39A6349C08978C8C06FF62 (Winpopup_t3BEDB5A4227E29A985A517FB8F2214ABA2396F0C * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t Queue_1_get_Count_m0CE0B6919A09EFFBB1EBA5B5DFEF50E4F8A89CFA_gshared_inline (Queue_1_tCC0C12E9ABD1C1421DEDD8C737F1A87C67ACC8F0 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m6C7E30F1E2D85F0A4AB37F0F6685E37607F26231_gshared_inline (Nullable_1_tE4EDC8D5ED2772A911F67696644E6C77FA716DC0 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_mB664E2C41CADA8413EF8842E6601B8C696A7CE15_gshared_inline (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE  Enumerator_get_Current_m5B32A9FC8294CB723DCD1171744B32E1775B6318_gshared_inline (Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB * __this, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE  L_0 = (KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE )__this->get_current_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Key_m3AA5875E6F038F027D9B80929300B746525F9D65_gshared_inline (KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Value_m8C7B882C4D425535288FAAD08EAF11D289A43AEC_gshared_inline (KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
