﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void FlyCameraBehaviour::Start()
extern void FlyCameraBehaviour_Start_mD5F03875DAC326070CF9F7961AAB5299D31E4E0A (void);
// 0x00000002 System.Void FlyCameraBehaviour::Update()
extern void FlyCameraBehaviour_Update_mAF7B0CD72BF8ED54A0CB605956CB05E018174434 (void);
// 0x00000003 System.Void FlyCameraBehaviour::.ctor()
extern void FlyCameraBehaviour__ctor_m4E5C91887D435DD045D9D9ED416325789BE586B0 (void);
// 0x00000004 System.Void BaseCharacter::BeAttack(UnityEngine.BulletAttack)
extern void BaseCharacter_BeAttack_mAFE4638099EAB407E585F2E4815EA8065E2EBCC0 (void);
// 0x00000005 System.Collections.IEnumerator BaseCharacter::Hit()
extern void BaseCharacter_Hit_m54604A801C42CBEAA4E302B222B601D03CF500CC (void);
// 0x00000006 System.Collections.IEnumerator BaseCharacter::Die()
extern void BaseCharacter_Die_m04BB50CED114B10465F1DBD123CABDF1650E84FC (void);
// 0x00000007 System.Void BaseCharacter::.ctor()
extern void BaseCharacter__ctor_m2AC0EED18CBC80E7736FAE3FB1A0C805C3DD27BC (void);
// 0x00000008 System.Void ComboEnemyController::Awake()
extern void ComboEnemyController_Awake_m83309054411D23A79E360D4ADA203DAB499167B1 (void);
// 0x00000009 System.Void ComboEnemyController::BeAttack()
extern void ComboEnemyController_BeAttack_mF06C7CF1F95FD2D432CA29DDDC781A8DDEADB9E1 (void);
// 0x0000000A System.Void ComboEnemyController::.ctor()
extern void ComboEnemyController__ctor_m365805A374A6039F2BBC6B42A23D77687463243D (void);
// 0x0000000B System.Void BossController::Awake()
extern void BossController_Awake_mEA7B3F03478FE1035E5D1CFEA0EE8F09FD736995 (void);
// 0x0000000C System.Void BossController::Start()
extern void BossController_Start_mE31D8F5F0F3B8AC1AC51D52700FDDA343595465D (void);
// 0x0000000D System.Void BossController::Update()
extern void BossController_Update_mF0D7102428C7E3CF059FECD6A33B4D69B43DA045 (void);
// 0x0000000E System.Void BossController::FixedUpdate()
extern void BossController_FixedUpdate_m4B86DA9FB846643BFC351DE8D920908505B99538 (void);
// 0x0000000F System.Void BossController::SpawnMoney()
extern void BossController_SpawnMoney_mD00D66AFC5730469D77014B6A9D7711F4B8208D7 (void);
// 0x00000010 System.Void BossController::OnTriggerEnter(UnityEngine.Collider)
extern void BossController_OnTriggerEnter_m7A18EBED9F1A80698AA4978DA08493D3134CFBB0 (void);
// 0x00000011 System.Void BossController::.ctor()
extern void BossController__ctor_m972067AE5DABD9A9EF17BF56DC497D5BAB15B772 (void);
// 0x00000012 System.Void FootBoss::Start()
extern void FootBoss_Start_m7C04B963FE801A1C32FD16154866AD7F483E331D (void);
// 0x00000013 System.Void FootBoss::Update()
extern void FootBoss_Update_m844F55F6C735E8035A673497A1106449414EA9C4 (void);
// 0x00000014 System.Void FootBoss::.ctor()
extern void FootBoss__ctor_m5A2684CC64C8058D1DAF30F7BF4BF9561E6F66CB (void);
// 0x00000015 System.Void ResizeColider::Start()
extern void ResizeColider_Start_mC85D80CD9D24D882BEAFDB1CC00E599B6A59991D (void);
// 0x00000016 System.Void ResizeColider::Update()
extern void ResizeColider_Update_mF30C2561F2F9E9B35A5361146078093398239850 (void);
// 0x00000017 System.Void ResizeColider::.ctor()
extern void ResizeColider__ctor_m6ABF8D653C6C3F407C7556ADAD7EBB9C41845143 (void);
// 0x00000018 System.Void DieState::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
extern void DieState_OnStateMachineEnter_m95E274B3BF7012F35BE1A6AE38DE6573085D94E2 (void);
// 0x00000019 System.Void DieState::OnStateMachineExit(UnityEngine.Animator,System.Int32)
extern void DieState_OnStateMachineExit_mA522AC28AD5A0829064FA6F60EB4A097A2169756 (void);
// 0x0000001A System.Void DieState::.ctor()
extern void DieState__ctor_m7BCD83358DCB7951D2790FDFE1FCF3B5F3F9C15E (void);
// 0x0000001B System.Void EnemyController::Awake()
extern void EnemyController_Awake_m88AE34A0729F468C299B20EF870C5BDB490144F0 (void);
// 0x0000001C System.Void EnemyController::Start()
extern void EnemyController_Start_m098A57DECB6D20D8A8CF100D55F3814A033BEDD0 (void);
// 0x0000001D System.Void EnemyController::Update()
extern void EnemyController_Update_mB42FF5202BE5953ED1E726A86BF4B61F19F8BB3B (void);
// 0x0000001E System.Void EnemyController::OnTriggerEnter(UnityEngine.Collider)
extern void EnemyController_OnTriggerEnter_m9AC93CB8FA65CCD0733581A726E3C92D7715B9F8 (void);
// 0x0000001F System.Void EnemyController::RandomSpawnMoney()
extern void EnemyController_RandomSpawnMoney_m79B33C54446666FD9B8E3ECA50DEFD353E07B7DE (void);
// 0x00000020 System.Void EnemyController::BeAttack(UnityEngine.BulletAttack)
extern void EnemyController_BeAttack_m61C66E448E4E6E7C53A6F86B81B5AA8FFCDD1067 (void);
// 0x00000021 System.Void EnemyController::.ctor()
extern void EnemyController__ctor_m76A2014F0A5962202971DC4401046D10B7A6FF53 (void);
// 0x00000022 System.Void BillBoard::Awake()
extern void BillBoard_Awake_mD497D82339F65DD1BB5CA57613181BC9B137FA6E (void);
// 0x00000023 System.Void BillBoard::LateUpdate()
extern void BillBoard_LateUpdate_m944542FEC57F6337840522F2E34AAABDE1F5FF05 (void);
// 0x00000024 System.Void BillBoard::.ctor()
extern void BillBoard__ctor_m4B75AB6699E2B247FC60C56E09154E2AACDAD6FD (void);
// 0x00000025 System.Void DieStatePlayer::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
extern void DieStatePlayer_OnStateMachineEnter_m9962C36B18ABCB69DA30C480DC2B6F3451C85350 (void);
// 0x00000026 System.Void DieStatePlayer::OnStateMachineExit(UnityEngine.Animator,System.Int32)
extern void DieStatePlayer_OnStateMachineExit_m05036B316B00DE69D657C04DF509EF5CA5D37991 (void);
// 0x00000027 System.Void DieStatePlayer::.ctor()
extern void DieStatePlayer__ctor_m29FF16A7FB896613F785B07DBC81240439E8933E (void);
// 0x00000028 System.Void HearthBar::SetMaxHearth()
extern void HearthBar_SetMaxHearth_m93FE7B1CF1391CD5A628DCE8EDFC0332D5296035 (void);
// 0x00000029 System.Void HearthBar::SetHearth(System.Single)
extern void HearthBar_SetHearth_m4D121923ACF5CEF8AD08F7827211D2D717CC7E51 (void);
// 0x0000002A System.Void HearthBar::.ctor()
extern void HearthBar__ctor_mDB543348EE500E438723F483D42D5AB7B3D4EC1F (void);
// 0x0000002B System.Void PlayerController::Start()
extern void PlayerController_Start_mC0C9B9461D0BDAC48EC43715818A4BA63C4F45EF (void);
// 0x0000002C System.Void PlayerController::Update()
extern void PlayerController_Update_m38903EF1C8F12B9388303741F8040EE26C33DC33 (void);
// 0x0000002D System.Void PlayerController::FixedUpdate()
extern void PlayerController_FixedUpdate_m914EA3E3CCE4DF6AEB2E78317FFC1D507DACEBDE (void);
// 0x0000002E System.Void PlayerController::WinGame()
extern void PlayerController_WinGame_m19B8CF2287B77BCED011B003A937D999F8F0BAD9 (void);
// 0x0000002F System.Void PlayerController::GameOver()
extern void PlayerController_GameOver_mDC8366D3AA34C7CF8E5FD702A40958732F8E0310 (void);
// 0x00000030 System.Void PlayerController::EarnMoney(Money)
extern void PlayerController_EarnMoney_mCF97FB39C370D904F81585D5126CB3B835EC7D21 (void);
// 0x00000031 System.Void PlayerController::OnTriggerEnter(UnityEngine.Collider)
extern void PlayerController_OnTriggerEnter_m2F6A24BACBA236DC01D4689BD68C270C29607E44 (void);
// 0x00000032 System.Void PlayerController::OnCollisionEnter(UnityEngine.Collision)
extern void PlayerController_OnCollisionEnter_mC64545E3CBFFED17020D4B52BB65359240FA581D (void);
// 0x00000033 System.Void PlayerController::SelectedGun(UnityEngine.Gun)
extern void PlayerController_SelectedGun_mF02792CDB46FD6FA983CFE0C9CCE5EF10924FD15 (void);
// 0x00000034 System.Void PlayerController::StartGame()
extern void PlayerController_StartGame_m17B78A00C65313CC3417B68DAB9DA210B5BB7158 (void);
// 0x00000035 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_m648E40092E37395F4307411E855445886113CD60 (void);
// 0x00000036 System.Void PlayerMoving::Start()
extern void PlayerMoving_Start_mD4760421AF2A736192DBBBE7C9D30A2724ED928E (void);
// 0x00000037 System.Void PlayerMoving::Update()
extern void PlayerMoving_Update_m212584CDC636944C30790CDA935C4A9407D4B6D5 (void);
// 0x00000038 System.Void PlayerMoving::FixedUpdate()
extern void PlayerMoving_FixedUpdate_m1F513EDC0DEB19C6581AB6F2735A90A7A10911EF (void);
// 0x00000039 System.Void PlayerMoving::.ctor()
extern void PlayerMoving__ctor_m66082909DE78580A5F872605E84D17F07ACA7855 (void);
// 0x0000003A System.Collections.Generic.List`1<UnityEngine.Transform> BlockSpawnController::GetSpawnPositionByBlock(System.Int32,System.Int32,System.Int32)
extern void BlockSpawnController_GetSpawnPositionByBlock_m4D8811ADA2D5272A7C0E99E101CBBBD281F93C79 (void);
// 0x0000003B System.Boolean BlockSpawnController::IsFar(System.Collections.Generic.List`1<UnityEngine.Transform>,UnityEngine.Transform)
extern void BlockSpawnController_IsFar_m8EB99CC1D75774EAE6BFFE4B72FCB50469E443D3 (void);
// 0x0000003C System.Boolean BlockSpawnController::IsChoose()
extern void BlockSpawnController_IsChoose_mB3794E5F6EF8D3FBF2003618424CDC8B66CB0A52 (void);
// 0x0000003D System.Void BlockSpawnController::.ctor()
extern void BlockSpawnController__ctor_m1FEABEDD39BD490BA3B38EB1DC844DB1FFC7A201 (void);
// 0x0000003E System.Void GUIManager::Start()
extern void GUIManager_Start_mE7B72986FCB050249234403FFC183D8D65426E2C (void);
// 0x0000003F System.Void GUIManager::ShowGUI(System.String)
extern void GUIManager_ShowGUI_mC2422C853AEC7AD9FC28FFB719CFFA379AB59DB3 (void);
// 0x00000040 System.Void GUIManager::.ctor()
extern void GUIManager__ctor_m6EF2692AB15663AE9F7F3C97197A77B763306D57 (void);
// 0x00000041 System.Void LevelController::Start()
extern void LevelController_Start_m717BD7EE0EDB69590F76AC78193EA9233DFCCFF6 (void);
// 0x00000042 System.Void LevelController::SetLevel(System.Int32)
extern void LevelController_SetLevel_mF0369952038B38BA24BC07361B8C222D6CCC967A (void);
// 0x00000043 System.Void LevelController::SetProgressLevel(System.Single)
extern void LevelController_SetProgressLevel_m38895D11578A4B7130E83E664203186580EF9017 (void);
// 0x00000044 System.Void LevelController::SetStep(System.Int32)
extern void LevelController_SetStep_mD711F9A3CA6460DE7B37EC2F763F824BC48AA19C (void);
// 0x00000045 System.Void LevelController::.ctor()
extern void LevelController__ctor_m2EE9AEC5CE53E6D07C0AE13875CA5B6C1BB91AB7 (void);
// 0x00000046 System.Void MapController::Awake()
extern void MapController_Awake_m5AD7E95093E9291FCB3D073D6E9F63F7EF83191A (void);
// 0x00000047 System.Void MapController::ActiveMapByNumber(System.Int32)
extern void MapController_ActiveMapByNumber_m01DEACF1105EDAAC907728F028FF15984346DA8F (void);
// 0x00000048 System.Void MapController::.ctor()
extern void MapController__ctor_m1D5CBB76A4B511669448FBE0AEA15B4120FEC11D (void);
// 0x00000049 System.Void ScoreController::Start()
extern void ScoreController_Start_mF4A294D5E420788B89914F345C07BC2EE8B2D01F (void);
// 0x0000004A System.Void ScoreController::SetMoney(System.Int32)
extern void ScoreController_SetMoney_m2822A9FD42CFB3D1E9BFE71F5734B6757EFA824D (void);
// 0x0000004B System.Void ScoreController::CollectMoney(Money)
extern void ScoreController_CollectMoney_m1AC51A34557358C15558EC1FAAEC591E8B72731C (void);
// 0x0000004C System.Collections.IEnumerator ScoreController::GotoIcon(UnityEngine.Transform,UnityEngine.Transform)
extern void ScoreController_GotoIcon_mC90FC47ACE641CABF896ECA5D202C268FDACA6BF (void);
// 0x0000004D System.Void ScoreController::.ctor()
extern void ScoreController__ctor_mF696E9AEDE1200519DF2EEF752C5C1E0810EF649 (void);
// 0x0000004E System.Void SettingController::Start()
extern void SettingController_Start_mFF2A2A98C7658D187B675AF0AC32E4BA37394F6B (void);
// 0x0000004F System.Void SettingController::ChangeValueToggle(System.Boolean)
extern void SettingController_ChangeValueToggle_mD3AEC69AB18D86A6C8E7EC1E0CBA461DF6461C15 (void);
// 0x00000050 System.Void SettingController::BackToScreen()
extern void SettingController_BackToScreen_m38DC988A7C92DDA4BFB2EEF9B2CA25D2E4F438F6 (void);
// 0x00000051 System.Void SettingController::.ctor()
extern void SettingController__ctor_mA8AA8CB0AF797CBECDB0FFB2AFE2BF6A1318D3E8 (void);
// 0x00000052 System.Void SettingController::<Start>b__2_0(System.Boolean)
extern void SettingController_U3CStartU3Eb__2_0_m7C984BBC2A77BB52422AE0D09F8232E887AD75B7 (void);
// 0x00000053 System.Void ShopController::Awake()
extern void ShopController_Awake_m0BABF499639BABDBE3A020F6C633ADB32732F0FE (void);
// 0x00000054 System.Void ShopController::Start()
extern void ShopController_Start_m5145579322746B255CD3FA74A7AC147193CBD177 (void);
// 0x00000055 System.Void ShopController::Back()
extern void ShopController_Back_m43C87163BA25CE96850883E3736DE260C75CF60E (void);
// 0x00000056 System.Void ShopController::SelectItem(ShopGunItem,UnityEngine.Gun)
extern void ShopController_SelectItem_mB28DC20388B91381CCAE3F249973A8C51BB9CAA4 (void);
// 0x00000057 System.Void ShopController::BuyItem(ShopGunItem,UnityEngine.Gun)
extern void ShopController_BuyItem_mE0591DBE9C24538E08A9B720CF8E526123670B21 (void);
// 0x00000058 System.Void ShopController::RefreshShop()
extern void ShopController_RefreshShop_m530DDD0A7FE4BCDA539AA9A3423F49023C71FA65 (void);
// 0x00000059 ShopGunItem ShopController::GetShopItemByName(System.String)
extern void ShopController_GetShopItemByName_mEDECE62D4C6F365CF9F6CBE96611E66B061B953F (void);
// 0x0000005A System.Void ShopController::.ctor()
extern void ShopController__ctor_mFA8709B7E257556CF9A2D80B2048DC59331BE291 (void);
// 0x0000005B System.Void SoundController::Awake()
extern void SoundController_Awake_m7D4556CC74ED46B01483640F957997A2F5BC903D (void);
// 0x0000005C System.Void SoundController::Play(System.String,System.Single)
extern void SoundController_Play_m87ACE746BE8079D0554CBFE330575CE5221ADE0F (void);
// 0x0000005D System.Void SoundController::.ctor()
extern void SoundController__ctor_m6D8FCBBD8F2A1B7AC1D0D87211405ED5CEE72EDB (void);
// 0x0000005E System.Void CameraFollow::Awake()
extern void CameraFollow_Awake_m01D7A81EF199D2F15E835CC795A9DA324EE127A2 (void);
// 0x0000005F System.Void CameraFollow::FixedUpdate()
extern void CameraFollow_FixedUpdate_mBF9AC25722C9FF8A0251A308BEC67DC528150756 (void);
// 0x00000060 System.Void CameraFollow::.ctor()
extern void CameraFollow__ctor_m27AF37B0C19243374F9376EBB2C40A2F605DB16E (void);
// 0x00000061 System.Void CameraShake::Update()
extern void CameraShake_Update_m39B56B99762CE6FA09C4205F478154F15C52A38A (void);
// 0x00000062 System.Void CameraShake::InduceStress(System.Single)
extern void CameraShake_InduceStress_m7FEF6744880B524AEDF4B49B393C28599BD2F233 (void);
// 0x00000063 System.Void CameraShake::.ctor()
extern void CameraShake__ctor_m8960CA715C5646BD0F1325FAAA552854EC3A9F84 (void);
// 0x00000064 System.Void ControlShopButton::Update()
extern void ControlShopButton_Update_mD1DEDF92ACE1CEAF73C217866C3A1A5510E441B7 (void);
// 0x00000065 System.Void ControlShopButton::.ctor()
extern void ControlShopButton__ctor_m2327166314835D13F833D45FFCEDE8AA2E5B16AA (void);
// 0x00000066 System.Void FinishLinePosition::Start()
extern void FinishLinePosition_Start_mFF95489D0ABDF0FF0BEADF637276A8B6706E17AC (void);
// 0x00000067 System.Void FinishLinePosition::ShowFinishLine()
extern void FinishLinePosition_ShowFinishLine_m15DD6F03E791A3D16BC421A82ED45B31300691E7 (void);
// 0x00000068 System.Collections.IEnumerator FinishLinePosition::PingpongFinishLine()
extern void FinishLinePosition_PingpongFinishLine_m8FE4DEB636ACC62918E8ADE93A30C54AA6C4D754 (void);
// 0x00000069 System.Void FinishLinePosition::.ctor()
extern void FinishLinePosition__ctor_m25792EA6D6BB9595C0150139088BEC1BC816ECC3 (void);
// 0x0000006A System.Void Firework::OnTriggerEnter(UnityEngine.Collider)
extern void Firework_OnTriggerEnter_mDEACCA5B9A344986334E4709C6E3BF4005B06569 (void);
// 0x0000006B System.Void Firework::.ctor()
extern void Firework__ctor_m1674CEEEFEAA38CD6685C90CCF4A34F6D8832774 (void);
// 0x0000006C System.Void GunPreviewBehaviour::Update()
extern void GunPreviewBehaviour_Update_m09F6FE8F7C2D5C52B9DBB84DE650EACC423901DB (void);
// 0x0000006D System.Void GunPreviewBehaviour::.ctor()
extern void GunPreviewBehaviour__ctor_mB4BB9151146B8E8629B4A5140620453AAC281D82 (void);
// 0x0000006E System.Void HPEffectState::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void HPEffectState_OnStateExit_m5AFF644C0ABEB21D495910A0228688A20541176F (void);
// 0x0000006F System.Void HPEffectState::.ctor()
extern void HPEffectState__ctor_m81432E17061233B2A1E4DEFDE5FBA4D10C846989 (void);
// 0x00000070 System.Void Losepopup::Awake()
extern void Losepopup_Awake_mDCF2BF073DF5F40068664876BB0660FB1C17F250 (void);
// 0x00000071 System.Void Losepopup::Visible()
extern void Losepopup_Visible_m17BF95189BE6D0768E94B492F0A2AAA85A8ABDAC (void);
// 0x00000072 System.Void Losepopup::Retry()
extern void Losepopup_Retry_mF76B438A63DADAC68D43ADC3FADA22BC5CC20C67 (void);
// 0x00000073 System.Void Losepopup::.ctor()
extern void Losepopup__ctor_m25B39748C9FD45EFCE2DA23DA205327EDE15F485 (void);
// 0x00000074 System.Void RedDotBuyGun::Awake()
extern void RedDotBuyGun_Awake_mB9913ABE7F95976B30376452E85F6536AA688C0E (void);
// 0x00000075 System.Void RedDotBuyGun::SetVisibleDot()
extern void RedDotBuyGun_SetVisibleDot_m62A22690099C5813D6A1FE057B10D545A1481267 (void);
// 0x00000076 System.Void RedDotBuyGun::.ctor()
extern void RedDotBuyGun__ctor_mD0CA736AFBE52C3CD74156EA3DE3D885B32093BC (void);
// 0x00000077 System.Void ResourceManager::Start()
extern void ResourceManager_Start_mE850CB356A63B42B45634184F0AA97024B1A0551 (void);
// 0x00000078 System.Void ResourceManager::Update()
extern void ResourceManager_Update_mB5D3DD122FE4428573E273F5A534D4A3F99D43E9 (void);
// 0x00000079 System.Void ResourceManager::DetectInput()
extern void ResourceManager_DetectInput_mA7FA4B61B501FEBCF43546D5A6E4F000D509CA72 (void);
// 0x0000007A System.Boolean ResourceManager::MouseDetected()
extern void ResourceManager_MouseDetected_m1B4EFB095B30136627BAF0EA82EDF7057A4F44A0 (void);
// 0x0000007B System.Boolean ResourceManager::TouchDetected()
extern void ResourceManager_TouchDetected_m5A37AA077F1F33F019F987DDE6929E26FA1CBF34 (void);
// 0x0000007C System.Void ResourceManager::RayCast3D(UnityEngine.Ray)
extern void ResourceManager_RayCast3D_m071CFBF59DE0FBD3ECD05625A618D841F75B3A7A (void);
// 0x0000007D System.Void ResourceManager::CollectResource()
extern void ResourceManager_CollectResource_m83A4572E62DD6845FE8EA22DC8E5E4F38381D412 (void);
// 0x0000007E System.Void ResourceManager::.ctor()
extern void ResourceManager__ctor_m30280EBBE503FC7B00E72E0D17FE5C772B4383FB (void);
// 0x0000007F System.Void Vibrator::Vibrate(Vibrator_Size)
extern void Vibrator_Vibrate_m0DBCEF0AFBEB77D8FFB27E78064446BFAD553466 (void);
// 0x00000080 System.Collections.IEnumerator Vibrator::Cancel(Vibrator_Size)
extern void Vibrator_Cancel_m6D5C50E05A6E29CA47F9C45409842F10D9F9D9DD (void);
// 0x00000081 System.Boolean Vibrator::IsAndroid()
extern void Vibrator_IsAndroid_m970EB8F3BFB2AA98A3A9312FB7D0972A4F415D07 (void);
// 0x00000082 System.Void Vibrator::StartCoroutine(System.Collections.IEnumerator)
extern void Vibrator_StartCoroutine_m020CEDB1B1F80648B7EA9B63509EB52250351DB6 (void);
// 0x00000083 System.Void Winpopup::Awake()
extern void Winpopup_Awake_mB4049D084B9D7FD1A69267D9FE3051DDBC5FF066 (void);
// 0x00000084 System.Void Winpopup::Visible(UnityEngine.Vector2,System.Int32)
extern void Winpopup_Visible_m1A79AFA1415DE6AFA54D3736B3E030C85C67588D (void);
// 0x00000085 System.Void Winpopup::NextLevel()
extern void Winpopup_NextLevel_m32266E8FE87DC639A4A06D85480C43F2806B7FC7 (void);
// 0x00000086 System.Void Winpopup::.ctor()
extern void Winpopup__ctor_m9820769F6988324A8E39A6349C08978C8C06FF62 (void);
// 0x00000087 System.Void GameManager::Awake()
extern void GameManager_Awake_mE60F41F3186E80B2BAB293918745366D18508C0F (void);
// 0x00000088 System.Void GameManager::Start()
extern void GameManager_Start_mD77CCDBF1DA8EC5C3AE7ED955DE4E7F54B79C88E (void);
// 0x00000089 System.Void GameManager::OnStartGame()
extern void GameManager_OnStartGame_mC704DB9B0B4D020A5EB1CA056CF7C451B65E3B22 (void);
// 0x0000008A System.Void GameManager::OpenSettingModal()
extern void GameManager_OpenSettingModal_mAB539847E383C79305AD6BA349B32A3FCCF5E104 (void);
// 0x0000008B System.Void GameManager::OpenShopPanel()
extern void GameManager_OpenShopPanel_m8ADDDD7BB6ECF75F1B68CF6004E964EA49AC1FAC (void);
// 0x0000008C System.Void GameManager::CloseShop()
extern void GameManager_CloseShop_mB6B07F87CDB49666DDD64BD71D6B86612D7F9379 (void);
// 0x0000008D System.Void GameManager::PlayerDie()
extern void GameManager_PlayerDie_mA7289673C8FEE00849AE786E0B9D203485ACDDAB (void);
// 0x0000008E System.Void GameManager::FinishLevel()
extern void GameManager_FinishLevel_mA0148AD6040ADCA0B9E4E32E12C013790CAE9129 (void);
// 0x0000008F System.Void GameManager::FinishEarlyLevel()
extern void GameManager_FinishEarlyLevel_mE2AFC6C92026DD5AAAD3C3A0DCBE5CB8ED5B84E3 (void);
// 0x00000090 System.Void GameManager::NextLevel()
extern void GameManager_NextLevel_mF6C956ECA09992B9D36B913E2273B39C076B71F2 (void);
// 0x00000091 System.Void GameManager::EnemyDie()
extern void GameManager_EnemyDie_mB7532C7B1C48FCA8DF49A73BE5E801C9CC823B01 (void);
// 0x00000092 System.Void GameManager::BossDie()
extern void GameManager_BossDie_m0DAD0350B8FC6F2C10CAE0DE17BEE05AE67DB96E (void);
// 0x00000093 System.Void GameManager::EarnMoney(System.Int32)
extern void GameManager_EarnMoney_mFE2FDFEA7988052729AFA7D64532CAF6E85FE5E5 (void);
// 0x00000094 System.Void GameManager::EarnMoney(Money)
extern void GameManager_EarnMoney_mC0F5E515019966641AB8113DB179BBEC03BEE5B2 (void);
// 0x00000095 System.Void GameManager::FightBoss()
extern void GameManager_FightBoss_m4B4496B11CEA2E7EEA425EDE473488B548EB1E2C (void);
// 0x00000096 System.Void GameManager::EnemyBeAttach(System.Int32)
extern void GameManager_EnemyBeAttach_m29814C9FBE71A9421DD23C7E0A49F64E2F93E699 (void);
// 0x00000097 System.Void GameManager::IncrementEnemy()
extern void GameManager_IncrementEnemy_m33474A553E437E9F90E22A0FF1F7FF1DE3199DD7 (void);
// 0x00000098 System.Collections.IEnumerator GameManager::PingWarning()
extern void GameManager_PingWarning_m1F780B5BB20AEB348A97641601BE7560D7A725F1 (void);
// 0x00000099 System.Void GameManager::.ctor()
extern void GameManager__ctor_mF7F1107D38DE91EB8A57C1C3BB1A932C50CD9693 (void);
// 0x0000009A System.Void BaseGunController::Start()
extern void BaseGunController_Start_m26D375003C4AC0D8FD72A1A265EE7F14D6C0AA06 (void);
// 0x0000009B System.Void BaseGunController::Update()
extern void BaseGunController_Update_mA1828106F06A043F2AAB7C359CE2572E226C9307 (void);
// 0x0000009C System.Void BaseGunController::SpawnVFX()
// 0x0000009D System.Void BaseGunController::.ctor()
extern void BaseGunController__ctor_mBA3CAC22BA4040C824F973DD318A60E51BF3AA1D (void);
// 0x0000009E System.Void BossGunController::Start()
extern void BossGunController_Start_mAEE91505DB06B20552B3A596A006E89F3DA578AA (void);
// 0x0000009F System.Void BossGunController::SpawnVFX()
extern void BossGunController_SpawnVFX_mFA7B327A0C9701E7C3E4F057B64C68668496CD61 (void);
// 0x000000A0 System.Collections.IEnumerator BossGunController::BossFight(System.Int32)
extern void BossGunController_BossFight_mF62AE5D455CC4F9A99DE56571ADAF01D0C07AD5C (void);
// 0x000000A1 System.Void BossGunController::Shoot()
extern void BossGunController_Shoot_m78AD993F76A7E6A96EE3315F0A347F28F8B55070 (void);
// 0x000000A2 System.Void BossGunController::.ctor()
extern void BossGunController__ctor_mBDAA83E836C55E2885FC9F204087E86412ED82FC (void);
// 0x000000A3 System.Collections.IEnumerator BaseBullet::Start()
extern void BaseBullet_Start_mDCCF822701BABE995CAAC44183BA8CFAAA251764 (void);
// 0x000000A4 System.Void BaseBullet::Update()
extern void BaseBullet_Update_mCBE9F62B8DDB81CFE10338FCC0A12268580D0D0B (void);
// 0x000000A5 System.Void BaseBullet::SetupBullet(System.Single,System.Single)
extern void BaseBullet_SetupBullet_m057BE05673905E808EFCDE956231A755D50AE469 (void);
// 0x000000A6 System.Void BaseBullet::SetupBullet(System.Single,System.Single,System.Int32)
extern void BaseBullet_SetupBullet_mD41D24128A9D5DAEF3D77BF464C28032EB9C0B11 (void);
// 0x000000A7 System.Void BaseBullet::OnTriggerEnter(UnityEngine.Collider)
extern void BaseBullet_OnTriggerEnter_m92B4B94859144799A7C0746517FC48C25F8D338E (void);
// 0x000000A8 System.Void BaseBullet::.ctor()
extern void BaseBullet__ctor_mAF6F15CFCB1ECF48D950BD4FAD77E5D1BAA89E5E (void);
// 0x000000A9 System.Collections.IEnumerator Bullet::Start()
extern void Bullet_Start_m27CD1C7B5B3C9EF7862D40F5F167E9B3B11EA81B (void);
// 0x000000AA System.Void Bullet::.ctor()
extern void Bullet__ctor_mFBA1E7297C133AE06ADA2EF2BA04567AD213A9D4 (void);
// 0x000000AB System.Collections.IEnumerator BulletAutoDestroy::Start()
extern void BulletAutoDestroy_Start_mCF8E562165E4638DD8A8593735BB85CAF5257B65 (void);
// 0x000000AC System.Void BulletAutoDestroy::.ctor()
extern void BulletAutoDestroy__ctor_mEB77369F9E2C134B9AF8B9404543810B9B721BC8 (void);
// 0x000000AD System.Collections.IEnumerator BulletBoss::Start()
extern void BulletBoss_Start_m5222DCFBD2C9B4C1DDE13043F02DC164FBE0F678 (void);
// 0x000000AE System.Void BulletBoss::.ctor()
extern void BulletBoss__ctor_mC08116DD356C329FFABB1D87FE79246DBD863B97 (void);
// 0x000000AF System.Void EnemyGunController::SpawnVFX()
extern void EnemyGunController_SpawnVFX_m2B4B7A6D276A2BF5EBC6745DD106AEC00F16311C (void);
// 0x000000B0 System.Void EnemyGunController::.ctor()
extern void EnemyGunController__ctor_m82B8AAF36D09E1C8E7006466065A2641380BD2BC (void);
// 0x000000B1 System.Void PlayerGunController::SpawnVFX()
extern void PlayerGunController_SpawnVFX_m475250EDD17CF1B14DA66F46BCBFFA2C9651916B (void);
// 0x000000B2 System.Void PlayerGunController::SetGunValue(UnityEngine.Gun)
extern void PlayerGunController_SetGunValue_mA68D8C687D455AA454B9E9E110061FABACBE2EF4 (void);
// 0x000000B3 System.Void PlayerGunController::.ctor()
extern void PlayerGunController__ctor_m34D8F3C4BC3B433E2357B5A05FB27DC62560C8B9 (void);
// 0x000000B4 System.Void ShopGunItem::Awake()
extern void ShopGunItem_Awake_m680779B042C2E90A2A582BB68089D722A801CF3C (void);
// 0x000000B5 System.Void ShopGunItem::Start()
extern void ShopGunItem_Start_m315EADBCE010ED49783340B49E35519073D23FDC (void);
// 0x000000B6 System.Void ShopGunItem::DetectItem()
extern void ShopGunItem_DetectItem_m5A14E56B58B33BC1FF8524273E2DF3AC23D715CB (void);
// 0x000000B7 System.Void ShopGunItem::SetStateItem()
extern void ShopGunItem_SetStateItem_m0C253E4348E641B2F496B23B9E15F13E8BB3F4AB (void);
// 0x000000B8 System.Void ShopGunItem::.ctor()
extern void ShopGunItem__ctor_mDBBAA2333BD9CC2C8B51BE3646C571512D942240 (void);
// 0x000000B9 System.Void ShopGunItem::<Start>b__5_0()
extern void ShopGunItem_U3CStartU3Eb__5_0_m1E5DF1C5662C5719E965245857363E03E6253ACF (void);
// 0x000000BA System.Void ShopGunItem::<Start>b__5_1()
extern void ShopGunItem_U3CStartU3Eb__5_1_mB62A0067CDCCF32E3505F8D44187447D2B3B37B7 (void);
// 0x000000BB System.Void Barel::Update()
extern void Barel_Update_m49ECB146394C2B0309FEF964EB517BC867F40B6E (void);
// 0x000000BC System.Void Barel::OnTriggerEnter(UnityEngine.Collider)
extern void Barel_OnTriggerEnter_m1B487FF2F4C0B18F6B1E510D85F50C0EC5341681 (void);
// 0x000000BD System.Void Barel::.ctor()
extern void Barel__ctor_m18516609D99D382904DA9699581C7869E271583D (void);
// 0x000000BE System.Void BaseObserver::OnTriggerEnter(UnityEngine.Collider)
extern void BaseObserver_OnTriggerEnter_m49AE93BA8C4B7CB64A1FBE5C85B90EC54EA8E5AB (void);
// 0x000000BF System.Void BaseObserver::.ctor()
extern void BaseObserver__ctor_m0D300FC92BD5676D2E3950534BD377ABB098CD8B (void);
// 0x000000C0 System.Void Crate::Update()
extern void Crate_Update_mC589F67907AE3D98186803873973832249D8336D (void);
// 0x000000C1 System.Void Crate::OnTriggerEnter(UnityEngine.Collider)
extern void Crate_OnTriggerEnter_m6A24A8E8D867235FFF63ACC88C4E0A199D3BBE44 (void);
// 0x000000C2 System.Void Crate::.ctor()
extern void Crate__ctor_mE06B10DE1316EEEFA7451AAFA0DB28B4945A0BD2 (void);
// 0x000000C3 System.Void Money::Update()
extern void Money_Update_mBB594F917756D9128F9E24A14FF31AC213A7FDDF (void);
// 0x000000C4 System.Void Money::OnTriggerEnter(UnityEngine.Collider)
extern void Money_OnTriggerEnter_m890A35FDD3F1BFBA3B99B67091947C086FD220C0 (void);
// 0x000000C5 System.Void Money::OnTriggerStay(UnityEngine.Collider)
extern void Money_OnTriggerStay_m52B65EE001C63E2808665A327E08B64BF9C34116 (void);
// 0x000000C6 System.Void Money::SetValueBill(System.Int32)
extern void Money_SetValueBill_m4B70706187C46FE6689B7E3A2DCB028DBEBE398C (void);
// 0x000000C7 System.Void Money::.ctor()
extern void Money__ctor_mD69086E9FA0B1F09BC8D102B7B02A04B51ECD716 (void);
// 0x000000C8 T Singleton`1::get_instance()
// 0x000000C9 System.Void Singleton`1::Awake()
// 0x000000CA System.Void Singleton`1::.ctor()
// 0x000000CB System.Void GAMopubIntegration::ListenForImpressions(System.Action`1<System.String>)
extern void GAMopubIntegration_ListenForImpressions_mD66781CBAAD09D59461D0C987F69981D88D61303 (void);
// 0x000000CC System.Void GAMopubIntegration::.ctor()
extern void GAMopubIntegration__ctor_m298C59618B1C557FB5F52E0E3D950C23BEE77F46 (void);
// 0x000000CD System.Void TinySauce::OnGameStarted(System.String)
extern void TinySauce_OnGameStarted_mBDBEAC3AA051B96C51605149C820E7B5D6080655 (void);
// 0x000000CE System.Void TinySauce::OnGameFinished(System.Single)
extern void TinySauce_OnGameFinished_m9FB59E27A476401E9E415EB1E2C80F578CD09C46 (void);
// 0x000000CF System.Void TinySauce::OnGameFinished(System.String,System.Single)
extern void TinySauce_OnGameFinished_m71622801396A3CD8588DB046C7C2288E4025F821 (void);
// 0x000000D0 System.Void TinySauce::OnGameFinished(System.String,System.Boolean,System.Single)
extern void TinySauce_OnGameFinished_mBFCBC99B6A8521D0EC482708746605FF3DD31E5B (void);
// 0x000000D1 System.Void TinySauce::TrackCustomEvent(System.String)
extern void TinySauce_TrackCustomEvent_mBD460F784CAF35BB642211C4ADF2A41AF6214487 (void);
// 0x000000D2 System.Void TinySauce::TrackCustomEvent(System.String,System.Single)
extern void TinySauce_TrackCustomEvent_mB24087301BA08BD977A957A729F12C9AE4C22691 (void);
// 0x000000D3 GameAnalyticsSDK.Setup.Settings GameAnalyticsSDK.GameAnalytics::get_SettingsGA()
extern void GameAnalytics_get_SettingsGA_mCAFF4A8EC0F3E46C746EC483505838E656569FA8 (void);
// 0x000000D4 System.Void GameAnalyticsSDK.GameAnalytics::set_SettingsGA(GameAnalyticsSDK.Setup.Settings)
extern void GameAnalytics_set_SettingsGA_mA7BBCB88ED0C5491D00AF3B065009D3DCA474F25 (void);
// 0x000000D5 System.Void GameAnalyticsSDK.GameAnalytics::OnEnable()
extern void GameAnalytics_OnEnable_m071A686DFE8C22F0696F8776865A3E74B02802EB (void);
// 0x000000D6 System.Void GameAnalyticsSDK.GameAnalytics::OnDisable()
extern void GameAnalytics_OnDisable_m9FFCE859B095AC7F55F2B9A0187F8DBD825FB86B (void);
// 0x000000D7 System.Void GameAnalyticsSDK.GameAnalytics::Awake()
extern void GameAnalytics_Awake_mB074269E53A413A84CECF5A8C74EE40B899266E9 (void);
// 0x000000D8 System.Void GameAnalyticsSDK.GameAnalytics::OnDestroy()
extern void GameAnalytics_OnDestroy_m5D1722C5EAC1F42ABBC1DA7489D7D30021221749 (void);
// 0x000000D9 System.Void GameAnalyticsSDK.GameAnalytics::OnApplicationQuit()
extern void GameAnalytics_OnApplicationQuit_m094E0FFD31EC7EEBE13E21F28EB77752DD9E6618 (void);
// 0x000000DA System.Void GameAnalyticsSDK.GameAnalytics::InitAPI()
extern void GameAnalytics_InitAPI_mB619C7BB48F828E656304606D6D83ECFA6C13386 (void);
// 0x000000DB System.Void GameAnalyticsSDK.GameAnalytics::InternalInitialize()
extern void GameAnalytics_InternalInitialize_m04C1EA37B3A4FC8F27DFE83F12541F4CFFB42E57 (void);
// 0x000000DC System.Void GameAnalyticsSDK.GameAnalytics::Initialize()
extern void GameAnalytics_Initialize_mD34FBE312428CEDD5DAC3FD0ED1234D9A4339C52 (void);
// 0x000000DD System.Void GameAnalyticsSDK.GameAnalytics::NewBusinessEvent(System.String,System.Int32,System.String,System.String,System.String)
extern void GameAnalytics_NewBusinessEvent_m48DD2E73C17BB4D8E22391616F63A424D3886DE2 (void);
// 0x000000DE System.Void GameAnalyticsSDK.GameAnalytics::NewBusinessEventIOS(System.String,System.Int32,System.String,System.String,System.String,System.String)
extern void GameAnalytics_NewBusinessEventIOS_mE8D2C53603C5C6A048FF963F4B6F43D0DD4BC69F (void);
// 0x000000DF System.Void GameAnalyticsSDK.GameAnalytics::NewBusinessEventIOSAutoFetchReceipt(System.String,System.Int32,System.String,System.String,System.String)
extern void GameAnalytics_NewBusinessEventIOSAutoFetchReceipt_m833D7727A25A14E3821D959D969EEDDF0BFF458E (void);
// 0x000000E0 System.Void GameAnalyticsSDK.GameAnalytics::NewDesignEvent(System.String)
extern void GameAnalytics_NewDesignEvent_mA0F014656A8DE873F7282DF2E2DA07F6FAD601B8 (void);
// 0x000000E1 System.Void GameAnalyticsSDK.GameAnalytics::NewDesignEvent(System.String,System.Single)
extern void GameAnalytics_NewDesignEvent_mEF36424787F728DF2FE0209B114E222974A27039 (void);
// 0x000000E2 System.Void GameAnalyticsSDK.GameAnalytics::NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus,System.String)
extern void GameAnalytics_NewProgressionEvent_m6946828FEB1C33A578CDCB9F731AAB49121A353F (void);
// 0x000000E3 System.Void GameAnalyticsSDK.GameAnalytics::NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.String)
extern void GameAnalytics_NewProgressionEvent_m2F24D38D91C73AB66AAFCA865F9CB157EF7EE45D (void);
// 0x000000E4 System.Void GameAnalyticsSDK.GameAnalytics::NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.String,System.String)
extern void GameAnalytics_NewProgressionEvent_mC89146B9B7344E90BE2F49A691521BD486C2D4F6 (void);
// 0x000000E5 System.Void GameAnalyticsSDK.GameAnalytics::NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.Int32)
extern void GameAnalytics_NewProgressionEvent_mA689664EDAEB841FE2CC133595029D2316B74339 (void);
// 0x000000E6 System.Void GameAnalyticsSDK.GameAnalytics::NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.String,System.Int32)
extern void GameAnalytics_NewProgressionEvent_m1BFADBDA2DAA8DC9041808C3757E597F0B4BEE13 (void);
// 0x000000E7 System.Void GameAnalyticsSDK.GameAnalytics::NewProgressionEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.String,System.String,System.Int32)
extern void GameAnalytics_NewProgressionEvent_mD697E2064CBBF4306889162F7094E7E7784D339B (void);
// 0x000000E8 System.Void GameAnalyticsSDK.GameAnalytics::NewResourceEvent(GameAnalyticsSDK.GAResourceFlowType,System.String,System.Single,System.String,System.String)
extern void GameAnalytics_NewResourceEvent_mFE8F1D00B3C14F78D4B76CFA40474FD026C549A8 (void);
// 0x000000E9 System.Void GameAnalyticsSDK.GameAnalytics::NewErrorEvent(GameAnalyticsSDK.GAErrorSeverity,System.String)
extern void GameAnalytics_NewErrorEvent_mFE9F68F20AE5A5C82D1768F1671E443949489364 (void);
// 0x000000EA System.Void GameAnalyticsSDK.GameAnalytics::NewAdEvent(GameAnalyticsSDK.GAAdAction,GameAnalyticsSDK.GAAdType,System.String,System.String,System.Int64)
extern void GameAnalytics_NewAdEvent_mBE879EA4FA9D35E6B1B526DE94CFCB13DF012F2D (void);
// 0x000000EB System.Void GameAnalyticsSDK.GameAnalytics::NewAdEvent(GameAnalyticsSDK.GAAdAction,GameAnalyticsSDK.GAAdType,System.String,System.String,GameAnalyticsSDK.GAAdError)
extern void GameAnalytics_NewAdEvent_m08BEC9267AA5E80B28917C49D82C56BDB8D6C123 (void);
// 0x000000EC System.Void GameAnalyticsSDK.GameAnalytics::NewAdEvent(GameAnalyticsSDK.GAAdAction,GameAnalyticsSDK.GAAdType,System.String,System.String)
extern void GameAnalytics_NewAdEvent_mFD9D91653EFABC4A96873846D088F36DAC89A2DD (void);
// 0x000000ED System.Void GameAnalyticsSDK.GameAnalytics::SetCustomId(System.String)
extern void GameAnalytics_SetCustomId_m4DB8288F2E96CC9EFC63CCA651E42C0B97069D91 (void);
// 0x000000EE System.Void GameAnalyticsSDK.GameAnalytics::SetEnabledManualSessionHandling(System.Boolean)
extern void GameAnalytics_SetEnabledManualSessionHandling_mBBEBE37CEABE38F38FBD95DE37F97247D8EA4CE6 (void);
// 0x000000EF System.Void GameAnalyticsSDK.GameAnalytics::SetEnabledEventSubmission(System.Boolean)
extern void GameAnalytics_SetEnabledEventSubmission_m2889A9B72FF0614D67EAAE23AB4D4E01E151741A (void);
// 0x000000F0 System.Void GameAnalyticsSDK.GameAnalytics::StartSession()
extern void GameAnalytics_StartSession_m6914518259F6DDC722BE25FE34D14F24CFABEDE0 (void);
// 0x000000F1 System.Void GameAnalyticsSDK.GameAnalytics::EndSession()
extern void GameAnalytics_EndSession_m48B3DFBBC15C0C2741D7892A85FE76D4906E164E (void);
// 0x000000F2 System.Void GameAnalyticsSDK.GameAnalytics::SetCustomDimension01(System.String)
extern void GameAnalytics_SetCustomDimension01_m1CEC6213E2AD7AC33ED04068A45C3B647B963530 (void);
// 0x000000F3 System.Void GameAnalyticsSDK.GameAnalytics::SetCustomDimension02(System.String)
extern void GameAnalytics_SetCustomDimension02_mF47995B2C7590BAB559FD80B742FB05E55355E44 (void);
// 0x000000F4 System.Void GameAnalyticsSDK.GameAnalytics::SetCustomDimension03(System.String)
extern void GameAnalytics_SetCustomDimension03_m95974AC0DCF3A52472B20C4ED0F50BF1C2B075FE (void);
// 0x000000F5 System.Void GameAnalyticsSDK.GameAnalytics::add_OnRemoteConfigsUpdatedEvent(System.Action)
extern void GameAnalytics_add_OnRemoteConfigsUpdatedEvent_m512F31B3B78EBF245354E3F1AC53DC7DB366EF5B (void);
// 0x000000F6 System.Void GameAnalyticsSDK.GameAnalytics::remove_OnRemoteConfigsUpdatedEvent(System.Action)
extern void GameAnalytics_remove_OnRemoteConfigsUpdatedEvent_m1933A49ED802C97FA79102535BD8EAE460A57F47 (void);
// 0x000000F7 System.Void GameAnalyticsSDK.GameAnalytics::OnRemoteConfigsUpdated()
extern void GameAnalytics_OnRemoteConfigsUpdated_mA8F148C8514C84613B18863D8BF533A4D5F0D549 (void);
// 0x000000F8 System.Void GameAnalyticsSDK.GameAnalytics::RemoteConfigsUpdated()
extern void GameAnalytics_RemoteConfigsUpdated_mC7C245A598C040B36A6BF625883641B68E6110A3 (void);
// 0x000000F9 System.String GameAnalyticsSDK.GameAnalytics::GetRemoteConfigsValueAsString(System.String)
extern void GameAnalytics_GetRemoteConfigsValueAsString_mD96ACDD18E0561B363F6D6C8F18814515AF1CC31 (void);
// 0x000000FA System.String GameAnalyticsSDK.GameAnalytics::GetRemoteConfigsValueAsString(System.String,System.String)
extern void GameAnalytics_GetRemoteConfigsValueAsString_m4E0E4C9653D9768B74109F412C6A2062092EAC90 (void);
// 0x000000FB System.Boolean GameAnalyticsSDK.GameAnalytics::IsRemoteConfigsReady()
extern void GameAnalytics_IsRemoteConfigsReady_m064AC9F7A0E7FFFFD05B06344EEFB2DE3F94187C (void);
// 0x000000FC System.String GameAnalyticsSDK.GameAnalytics::GetRemoteConfigsContentAsString()
extern void GameAnalytics_GetRemoteConfigsContentAsString_mBC50EB193D05F0A5E901DF14BC5B3EE26B6EA786 (void);
// 0x000000FD System.String GameAnalyticsSDK.GameAnalytics::GetABTestingId()
extern void GameAnalytics_GetABTestingId_mFC77C1556D51A140E65D0AAD9A6132E46561C4CB (void);
// 0x000000FE System.String GameAnalyticsSDK.GameAnalytics::GetABTestingVariantId()
extern void GameAnalytics_GetABTestingVariantId_mB0D7D90532C98949F61FC2301FDF249E9B16BD0D (void);
// 0x000000FF System.Void GameAnalyticsSDK.GameAnalytics::SubscribeMoPubImpressions()
extern void GameAnalytics_SubscribeMoPubImpressions_m5A77A2558D37ACE8E04F2CE9E94F4DC61E565821 (void);
// 0x00000100 System.Void GameAnalyticsSDK.GameAnalytics::StartTimer(System.String)
extern void GameAnalytics_StartTimer_m30CC464D3FAE9848B4000CFCB7D9F05852BD83D9 (void);
// 0x00000101 System.Void GameAnalyticsSDK.GameAnalytics::PauseTimer(System.String)
extern void GameAnalytics_PauseTimer_m311D0A17F1FA844FE2F3A02FE6618FCAF6E303D0 (void);
// 0x00000102 System.Void GameAnalyticsSDK.GameAnalytics::ResumeTimer(System.String)
extern void GameAnalytics_ResumeTimer_m09F560FFB1D2FA550D7D3D0E8D389E0A7CFEC8F3 (void);
// 0x00000103 System.Int64 GameAnalyticsSDK.GameAnalytics::StopTimer(System.String)
extern void GameAnalytics_StopTimer_m6723A5845A33E30DED376BD9801D1B846B5C48BE (void);
// 0x00000104 System.String GameAnalyticsSDK.GameAnalytics::GetUnityVersion()
extern void GameAnalytics_GetUnityVersion_m35C794B4A79C906922B19F07BAF875893DC2B4A8 (void);
// 0x00000105 System.Int32 GameAnalyticsSDK.GameAnalytics::GetPlatformIndex()
extern void GameAnalytics_GetPlatformIndex_mD6A1E752CD0045B564B9C8B46FC074B35880F630 (void);
// 0x00000106 System.Void GameAnalyticsSDK.GameAnalytics::SetBuildAllPlatforms(System.String)
extern void GameAnalytics_SetBuildAllPlatforms_m079A42CA8E6208D1A243FDE6BF949FF08342F87C (void);
// 0x00000107 System.Void GameAnalyticsSDK.GameAnalytics::.ctor()
extern void GameAnalytics__ctor_m0E78CEF8FDA5540BE58221BA5B2619ED182DD5AC (void);
// 0x00000108 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetAvailableCustomDimensions01(System.String)
extern void GA_Wrapper_SetAvailableCustomDimensions01_m49226B72676D4A316ACCB6B49AC01E67B7D18E35 (void);
// 0x00000109 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetAvailableCustomDimensions02(System.String)
extern void GA_Wrapper_SetAvailableCustomDimensions02_mA84F6096DFD803E7642FD4F252D2009D3F3E31F9 (void);
// 0x0000010A System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetAvailableCustomDimensions03(System.String)
extern void GA_Wrapper_SetAvailableCustomDimensions03_m28C56480CDD3890A28060F5474E9E784B14DE4F8 (void);
// 0x0000010B System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetAvailableResourceCurrencies(System.String)
extern void GA_Wrapper_SetAvailableResourceCurrencies_m1A75453DCE5B4DDADF6CCEB7C9D663B5041CD202 (void);
// 0x0000010C System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetAvailableResourceItemTypes(System.String)
extern void GA_Wrapper_SetAvailableResourceItemTypes_mE57F26C669230ABFB3543D65B63ECF14BA299BFB (void);
// 0x0000010D System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetUnitySdkVersion(System.String)
extern void GA_Wrapper_SetUnitySdkVersion_mF73410E2221DED07BA469FCCAD1156320EFBE2F2 (void);
// 0x0000010E System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetUnityEngineVersion(System.String)
extern void GA_Wrapper_SetUnityEngineVersion_mA39EDABE836FBD738EF9AF854303F812B6EB7C41 (void);
// 0x0000010F System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetBuild(System.String)
extern void GA_Wrapper_SetBuild_mDE4539D365AD9CAC3FA58E929213FBE9A24A54C2 (void);
// 0x00000110 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetCustomUserId(System.String)
extern void GA_Wrapper_SetCustomUserId_m9093B4AECF1BBA391F4FD37C19660178FB457D08 (void);
// 0x00000111 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetEnabledManualSessionHandling(System.Boolean)
extern void GA_Wrapper_SetEnabledManualSessionHandling_mF8981E6D63AB1B92BE5EFA19F8C5FD12942B7D23 (void);
// 0x00000112 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetEnabledEventSubmission(System.Boolean)
extern void GA_Wrapper_SetEnabledEventSubmission_m3864DA950A56CA4CFAC4E389703E7F36AC04C26C (void);
// 0x00000113 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetAutoDetectAppVersion(System.Boolean)
extern void GA_Wrapper_SetAutoDetectAppVersion_m4C267049A1B829DE80AC1EB483C2F107785852C9 (void);
// 0x00000114 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::StartSession()
extern void GA_Wrapper_StartSession_m96E51C8838738FE69E6EF2455793067F7568F0DE (void);
// 0x00000115 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::EndSession()
extern void GA_Wrapper_EndSession_mBFBC5629B6C9A8B23863243F0D3943A5DC2622C8 (void);
// 0x00000116 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::Initialize(System.String,System.String)
extern void GA_Wrapper_Initialize_mF32F62DA19DCDD83E87EAF4056119193A5E3224F (void);
// 0x00000117 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetCustomDimension01(System.String)
extern void GA_Wrapper_SetCustomDimension01_mCFFCB1F233EBADAAF8A64BE093EA7D02840F2D2A (void);
// 0x00000118 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetCustomDimension02(System.String)
extern void GA_Wrapper_SetCustomDimension02_m1945A1A027B97EDA5B2FA7A7295E6180C95466A9 (void);
// 0x00000119 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetCustomDimension03(System.String)
extern void GA_Wrapper_SetCustomDimension03_mC3081F7772D8E40AAFE570D9DBF25816690DF186 (void);
// 0x0000011A System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::AddBusinessEvent(System.String,System.Int32,System.String,System.String,System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Wrapper_AddBusinessEvent_mF63AC5327C541D6E0E94B6B969EC926B30C094CD (void);
// 0x0000011B System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::AddBusinessEventAndAutoFetchReceipt(System.String,System.Int32,System.String,System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Wrapper_AddBusinessEventAndAutoFetchReceipt_m4C1CE54B14044A4FEED94368B15666C567B17D05 (void);
// 0x0000011C System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::AddResourceEvent(GameAnalyticsSDK.GAResourceFlowType,System.String,System.Single,System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Wrapper_AddResourceEvent_mDC824665294BA78AE458873F50819E22772194A0 (void);
// 0x0000011D System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::AddProgressionEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Wrapper_AddProgressionEvent_mEEEFE6F3DEAAECA7149DCA928AB6EF7772EDF25F (void);
// 0x0000011E System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::AddProgressionEventWithScore(GameAnalyticsSDK.GAProgressionStatus,System.String,System.String,System.String,System.Int32,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Wrapper_AddProgressionEventWithScore_m79C70A858DA23457F92201E3101A68755B5EBB06 (void);
// 0x0000011F System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::AddDesignEvent(System.String,System.Single,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Wrapper_AddDesignEvent_m0B4F894A199FF19DB4AA340B49724D50FBADC815 (void);
// 0x00000120 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::AddDesignEvent(System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Wrapper_AddDesignEvent_m2A6381DB281AFFACF8FE17BB79D57CB5060FAA1F (void);
// 0x00000121 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::AddErrorEvent(GameAnalyticsSDK.GAErrorSeverity,System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Wrapper_AddErrorEvent_mB8CE6DFFF292AE95E7C4F92EDC4B33F54D1E4833 (void);
// 0x00000122 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::AddAdEventWithDuration(GameAnalyticsSDK.GAAdAction,GameAnalyticsSDK.GAAdType,System.String,System.String,System.Int64)
extern void GA_Wrapper_AddAdEventWithDuration_m2FCCC0B17CFA287EEF422DBA69376716BAE45170 (void);
// 0x00000123 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::AddAdEventWithReason(GameAnalyticsSDK.GAAdAction,GameAnalyticsSDK.GAAdType,System.String,System.String,GameAnalyticsSDK.GAAdError)
extern void GA_Wrapper_AddAdEventWithReason_m0306BC0F90965B39557DCF95C435C71E552B3D71 (void);
// 0x00000124 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::AddAdEvent(GameAnalyticsSDK.GAAdAction,GameAnalyticsSDK.GAAdType,System.String,System.String)
extern void GA_Wrapper_AddAdEvent_m818905A1569B1D466EA9CF9C80B1DE0A70F7FB2A (void);
// 0x00000125 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetInfoLog(System.Boolean)
extern void GA_Wrapper_SetInfoLog_m6F095E7ABF2C0BC938A06D5D5443AF76BEE7570A (void);
// 0x00000126 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SetVerboseLog(System.Boolean)
extern void GA_Wrapper_SetVerboseLog_mD191337555FF27FDC53813EF4AD72146EFBB2792 (void);
// 0x00000127 System.String GameAnalyticsSDK.Wrapper.GA_Wrapper::GetRemoteConfigsValueAsString(System.String,System.String)
extern void GA_Wrapper_GetRemoteConfigsValueAsString_m5F5D6BC1B3D423AEF86E656AEF4E06EBD1979866 (void);
// 0x00000128 System.Boolean GameAnalyticsSDK.Wrapper.GA_Wrapper::IsRemoteConfigsReady()
extern void GA_Wrapper_IsRemoteConfigsReady_m7767F545B020516ABCB62B206E81F04664B5C175 (void);
// 0x00000129 System.String GameAnalyticsSDK.Wrapper.GA_Wrapper::GetRemoteConfigsContentAsString()
extern void GA_Wrapper_GetRemoteConfigsContentAsString_m63745240E6A730FE913EA6D4FAADFCEA50E84DA8 (void);
// 0x0000012A System.String GameAnalyticsSDK.Wrapper.GA_Wrapper::GetABTestingId()
extern void GA_Wrapper_GetABTestingId_mD97A2483C9062AA2C965FDD603B9B4ECBA998718 (void);
// 0x0000012B System.String GameAnalyticsSDK.Wrapper.GA_Wrapper::GetABTestingVariantId()
extern void GA_Wrapper_GetABTestingVariantId_m1BFB815F0E4AE0464A446B50E593E55BBEABD20A (void);
// 0x0000012C System.String GameAnalyticsSDK.Wrapper.GA_Wrapper::DictionaryToJsonString(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Wrapper_DictionaryToJsonString_mFE250BCB996E6F72B76E0DA60DEB376709CA578A (void);
// 0x0000012D System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::SubscribeMoPubImpressions()
extern void GA_Wrapper_SubscribeMoPubImpressions_mCF0DA2849B3FD21CAB7D681C80A36ABEA97B8A16 (void);
// 0x0000012E System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::StartTimer(System.String)
extern void GA_Wrapper_StartTimer_m4BE2B8B3A232E05F0BE26785F2F7280BFBE8527E (void);
// 0x0000012F System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::PauseTimer(System.String)
extern void GA_Wrapper_PauseTimer_mEBFFE4FBF92625156552278F545FF4E4F7BD2275 (void);
// 0x00000130 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::ResumeTimer(System.String)
extern void GA_Wrapper_ResumeTimer_m7F1E247E742FF5F478C495A364C055955E85D4C3 (void);
// 0x00000131 System.Int64 GameAnalyticsSDK.Wrapper.GA_Wrapper::StopTimer(System.String)
extern void GA_Wrapper_StopTimer_m47AB83EFDFD5526E9B96EE0B505C5A048E4C3FE5 (void);
// 0x00000132 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::configureAvailableCustomDimensions01(System.String)
extern void GA_Wrapper_configureAvailableCustomDimensions01_m2201795FEA65EB2F040D06084B708D1DC46CB1A5 (void);
// 0x00000133 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::configureAvailableCustomDimensions02(System.String)
extern void GA_Wrapper_configureAvailableCustomDimensions02_m283315D2811A7FB46F7DF0B050A154DEBC5335CB (void);
// 0x00000134 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::configureAvailableCustomDimensions03(System.String)
extern void GA_Wrapper_configureAvailableCustomDimensions03_mBFE26A31BBAA35345BF15E7EB161DE48C4FDB62D (void);
// 0x00000135 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::configureAvailableResourceCurrencies(System.String)
extern void GA_Wrapper_configureAvailableResourceCurrencies_m88A05D54BD004F8C3B55592F67589DA8167D1E00 (void);
// 0x00000136 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::configureAvailableResourceItemTypes(System.String)
extern void GA_Wrapper_configureAvailableResourceItemTypes_mA13D8043D108C76BE2152C265F3E8C07D3330878 (void);
// 0x00000137 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::configureSdkGameEngineVersion(System.String)
extern void GA_Wrapper_configureSdkGameEngineVersion_m35AC3C364EC431788377710DC202501F0D5D164D (void);
// 0x00000138 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::configureGameEngineVersion(System.String)
extern void GA_Wrapper_configureGameEngineVersion_m1CCF8F625BC52F6239EBD959F81134DC4DAF7A28 (void);
// 0x00000139 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::configureBuild(System.String)
extern void GA_Wrapper_configureBuild_m6DB9826BF4C87B4DDBE18BB73A7FEA16B1AE46FA (void);
// 0x0000013A System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::configureUserId(System.String)
extern void GA_Wrapper_configureUserId_m0608B6B8E813B3D43A3E034E0AB06726F8A3F849 (void);
// 0x0000013B System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::configureAutoDetectAppVersion(System.Boolean)
extern void GA_Wrapper_configureAutoDetectAppVersion_m3A32F68A36BDED5384B424226C794E846608B339 (void);
// 0x0000013C System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::initialize(System.String,System.String)
extern void GA_Wrapper_initialize_mE7EA700D00EA07D5CBD98CBAAADC400BD9E9418D (void);
// 0x0000013D System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::setCustomDimension01(System.String)
extern void GA_Wrapper_setCustomDimension01_mB671735540B49EED976610899B5357A5FBFD4305 (void);
// 0x0000013E System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::setCustomDimension02(System.String)
extern void GA_Wrapper_setCustomDimension02_mCCFD40EFF7DC146EFDE106C9F7B179B6FF534251 (void);
// 0x0000013F System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::setCustomDimension03(System.String)
extern void GA_Wrapper_setCustomDimension03_m551223E5880600B2213D67EAB42A9CAFC8442818 (void);
// 0x00000140 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::addBusinessEvent(System.String,System.Int32,System.String,System.String,System.String,System.String,System.String)
extern void GA_Wrapper_addBusinessEvent_m4E34AE6D51EF26F6E04140834418874998B81E67 (void);
// 0x00000141 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::addBusinessEventAndAutoFetchReceipt(System.String,System.Int32,System.String,System.String,System.String,System.String)
extern void GA_Wrapper_addBusinessEventAndAutoFetchReceipt_m5D8C0B19387CAD3A087E5992327AE337EB55DC2E (void);
// 0x00000142 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::addResourceEvent(System.Int32,System.String,System.Single,System.String,System.String,System.String)
extern void GA_Wrapper_addResourceEvent_m741BA78E5201D49AE9A9C237863B337A6293A0EB (void);
// 0x00000143 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::addProgressionEvent(System.Int32,System.String,System.String,System.String,System.String)
extern void GA_Wrapper_addProgressionEvent_m4C5C71B6CA8BAF636FC42ECD6C73816C82D33D1D (void);
// 0x00000144 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::addProgressionEventWithScore(System.Int32,System.String,System.String,System.String,System.Int32,System.String)
extern void GA_Wrapper_addProgressionEventWithScore_mA8672F6A5FCA155242F35326E73662A6473610C7 (void);
// 0x00000145 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::addDesignEvent(System.String,System.String)
extern void GA_Wrapper_addDesignEvent_m0F61AD1A2473129C965DD538A74B317C1826ECF3 (void);
// 0x00000146 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::addDesignEventWithValue(System.String,System.Single,System.String)
extern void GA_Wrapper_addDesignEventWithValue_m0F114C2A873C9D22507357CD1F7AF02C9487F396 (void);
// 0x00000147 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::addErrorEvent(System.Int32,System.String,System.String)
extern void GA_Wrapper_addErrorEvent_m3863F9CAE4DDCC5DDA30EDD247BAC56457881359 (void);
// 0x00000148 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::addAdEventWithDuration(System.Int32,System.Int32,System.String,System.String,System.Int64)
extern void GA_Wrapper_addAdEventWithDuration_m5C51B7153B83D5F0E63076058ABBD6A831A5D060 (void);
// 0x00000149 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::addAdEventWithReason(System.Int32,System.Int32,System.String,System.String,System.Int32)
extern void GA_Wrapper_addAdEventWithReason_m4693C2D4F5022E1791E2ABB59FBEB27546553B31 (void);
// 0x0000014A System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::addAdEvent(System.Int32,System.Int32,System.String,System.String)
extern void GA_Wrapper_addAdEvent_mDCFA2FE2C6FADE21D75D5C74882B980C674BC74C (void);
// 0x0000014B System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::addImpressionEvent(System.String,System.String)
extern void GA_Wrapper_addImpressionEvent_m8D107EFAD33B608EC7EB9B6B1F81485AD3EC5D98 (void);
// 0x0000014C System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::setEnabledInfoLog(System.Boolean)
extern void GA_Wrapper_setEnabledInfoLog_mBBEFB5A46E527721907563B0F81C858F13A4E29A (void);
// 0x0000014D System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::setEnabledVerboseLog(System.Boolean)
extern void GA_Wrapper_setEnabledVerboseLog_m375C9E881C2EADB10A8EF8FC314A3D9B06898B0C (void);
// 0x0000014E System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::setManualSessionHandling(System.Boolean)
extern void GA_Wrapper_setManualSessionHandling_mD95B2B97D7B250DAA1DC8C52413F8314125BDF14 (void);
// 0x0000014F System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::setEventSubmission(System.Boolean)
extern void GA_Wrapper_setEventSubmission_m2383E511BB0B5006B87B4EE5C163553746302732 (void);
// 0x00000150 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::gameAnalyticsStartSession()
extern void GA_Wrapper_gameAnalyticsStartSession_mF3917115845892455B7D1BEC7D2E796A1191AC4A (void);
// 0x00000151 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::gameAnalyticsEndSession()
extern void GA_Wrapper_gameAnalyticsEndSession_mF8FE2CA5F1ADA7DCE87A8A1EECDC1D0E7D534CBB (void);
// 0x00000152 System.String GameAnalyticsSDK.Wrapper.GA_Wrapper::getRemoteConfigsValueAsString(System.String,System.String)
extern void GA_Wrapper_getRemoteConfigsValueAsString_m43C2DDE3F337C2323FB4DDDC973FBD3624190D14 (void);
// 0x00000153 System.Boolean GameAnalyticsSDK.Wrapper.GA_Wrapper::isRemoteConfigsReady()
extern void GA_Wrapper_isRemoteConfigsReady_mBA324FB96FAE61B5B2BB4535F7434B0473D2B015 (void);
// 0x00000154 System.String GameAnalyticsSDK.Wrapper.GA_Wrapper::getRemoteConfigsContentAsString()
extern void GA_Wrapper_getRemoteConfigsContentAsString_m7A3F9B990174D6EC6624AE07BF98F752005387AC (void);
// 0x00000155 System.String GameAnalyticsSDK.Wrapper.GA_Wrapper::getABTestingId()
extern void GA_Wrapper_getABTestingId_mC1D8073809041C7CA2ABD012600475100F6DD03D (void);
// 0x00000156 System.String GameAnalyticsSDK.Wrapper.GA_Wrapper::getABTestingVariantId()
extern void GA_Wrapper_getABTestingVariantId_mBA50F987FFCD55E533252CE4FF8A97A7330805B0 (void);
// 0x00000157 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::startTimer(System.String)
extern void GA_Wrapper_startTimer_m04AF910C4442C18721B7E683DE6E03FAD7B1941A (void);
// 0x00000158 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::pauseTimer(System.String)
extern void GA_Wrapper_pauseTimer_m242C0C032CD8BF3D2DD0DA2A690EBE4B2AA960D8 (void);
// 0x00000159 System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::resumeTimer(System.String)
extern void GA_Wrapper_resumeTimer_mE173112639D1CAFFF96E03B63B196E7B11C8E7FC (void);
// 0x0000015A System.Int64 GameAnalyticsSDK.Wrapper.GA_Wrapper::stopTimer(System.String)
extern void GA_Wrapper_stopTimer_m7E0218272ADF8CBC815FF12C8ACDFD8899A35979 (void);
// 0x0000015B System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::subscribeMoPubImpressions()
extern void GA_Wrapper_subscribeMoPubImpressions_mE83B0EC38551E511B7B4289D7AA42C24EF61582F (void);
// 0x0000015C System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::ImpressionHandler(System.String)
extern void GA_Wrapper_ImpressionHandler_m13E386DFB9F943B8E4ADF84CF022161A09735EBF (void);
// 0x0000015D System.Void GameAnalyticsSDK.Wrapper.GA_Wrapper::.ctor()
extern void GA_Wrapper__ctor_m19545E7D36FD7EF37D3FAB7B96A00106BA110AF9 (void);
// 0x0000015E System.Object GameAnalyticsSDK.Utilities.GA_MiniJSON::Deserialize(System.String)
extern void GA_MiniJSON_Deserialize_m94CD95ACCB8707F15E65B527040F38105ADED1B7 (void);
// 0x0000015F System.String GameAnalyticsSDK.Utilities.GA_MiniJSON::Serialize(System.Object)
extern void GA_MiniJSON_Serialize_m3AEA946EB4FD81E71C4989DAB12E20777D4C44B4 (void);
// 0x00000160 System.Void GameAnalyticsSDK.Utilities.GA_MiniJSON::.ctor()
extern void GA_MiniJSON__ctor_m5C3A474EA564FDA0371996ED03066A5B0E59AD2E (void);
// 0x00000161 System.Void GameAnalyticsSDK.State.GAState::Init()
extern void GAState_Init_mAF4D2301C4FD7FFD247BE60560FE7F369AD5A135 (void);
// 0x00000162 System.Boolean GameAnalyticsSDK.State.GAState::ListContainsString(System.Collections.Generic.List`1<System.String>,System.String)
extern void GAState_ListContainsString_mADB9A0E0980737021B99E6F9DEDCED2D787482FA (void);
// 0x00000163 System.Boolean GameAnalyticsSDK.State.GAState::IsManualSessionHandlingEnabled()
extern void GAState_IsManualSessionHandlingEnabled_mA97CF570ABE9A569D185A22F868C77CEC3011074 (void);
// 0x00000164 System.Boolean GameAnalyticsSDK.State.GAState::HasAvailableResourceCurrency(System.String)
extern void GAState_HasAvailableResourceCurrency_m7B18CAA90BF7B42142729C70BF66FE08776AEF97 (void);
// 0x00000165 System.Boolean GameAnalyticsSDK.State.GAState::HasAvailableResourceItemType(System.String)
extern void GAState_HasAvailableResourceItemType_m7DBC3CAC6E13671D1F8A78E0E5D071CAAD727DEC (void);
// 0x00000166 System.Boolean GameAnalyticsSDK.State.GAState::HasAvailableCustomDimensions01(System.String)
extern void GAState_HasAvailableCustomDimensions01_m0EB9E901216824580B0BCA8152FD468066D46AA7 (void);
// 0x00000167 System.Boolean GameAnalyticsSDK.State.GAState::HasAvailableCustomDimensions02(System.String)
extern void GAState_HasAvailableCustomDimensions02_m8E0BA177C69FB399A03ECDF5ADFBDEF1E4E83590 (void);
// 0x00000168 System.Boolean GameAnalyticsSDK.State.GAState::HasAvailableCustomDimensions03(System.String)
extern void GAState_HasAvailableCustomDimensions03_mCAB90F3AA79C5FA306A5D55D727C021DE8755DAA (void);
// 0x00000169 System.Void GameAnalyticsSDK.Setup.Settings::SetCustomUserID(System.String)
extern void Settings_SetCustomUserID_m1DAB095E8930ECB2921C5126B64010AA02647000 (void);
// 0x0000016A System.Void GameAnalyticsSDK.Setup.Settings::RemovePlatformAtIndex(System.Int32)
extern void Settings_RemovePlatformAtIndex_mFCF5D691A6CF21DDEAA77613C205277704F3521B (void);
// 0x0000016B System.Void GameAnalyticsSDK.Setup.Settings::AddPlatform(UnityEngine.RuntimePlatform)
extern void Settings_AddPlatform_m634DA5908D5DE08BB3D292161268153A219B024A (void);
// 0x0000016C System.String[] GameAnalyticsSDK.Setup.Settings::GetAvailablePlatforms()
extern void Settings_GetAvailablePlatforms_m45D43B36F53F04AF60ACAF8CE467BBE9EAED4AF6 (void);
// 0x0000016D System.Boolean GameAnalyticsSDK.Setup.Settings::IsGameKeyValid(System.Int32,System.String)
extern void Settings_IsGameKeyValid_m0F6348C1B68D0E5B29E986FDDC4A9A59778DAB09 (void);
// 0x0000016E System.Boolean GameAnalyticsSDK.Setup.Settings::IsSecretKeyValid(System.Int32,System.String)
extern void Settings_IsSecretKeyValid_m5E9A73E8CD944D769EB0070CFF9C2029C55E2759 (void);
// 0x0000016F System.Void GameAnalyticsSDK.Setup.Settings::UpdateGameKey(System.Int32,System.String)
extern void Settings_UpdateGameKey_m412823F3A86356F48F186A3ECFE3BC108D6BC2F0 (void);
// 0x00000170 System.Void GameAnalyticsSDK.Setup.Settings::UpdateSecretKey(System.Int32,System.String)
extern void Settings_UpdateSecretKey_m43D6A554BA7BB3899A859A1332745D7C679FF8EA (void);
// 0x00000171 System.String GameAnalyticsSDK.Setup.Settings::GetGameKey(System.Int32)
extern void Settings_GetGameKey_m1B6690326EE7FF236F71F82E87A1DD88CC1C1100 (void);
// 0x00000172 System.String GameAnalyticsSDK.Setup.Settings::GetSecretKey(System.Int32)
extern void Settings_GetSecretKey_m6F6E1881171A2E1F0E5472AF1AF23700FC7F10C0 (void);
// 0x00000173 System.Void GameAnalyticsSDK.Setup.Settings::SetCustomArea(System.String)
extern void Settings_SetCustomArea_mD66A8D31E012B2944973EB54FC544168BEDD4B08 (void);
// 0x00000174 System.Void GameAnalyticsSDK.Setup.Settings::SetKeys(System.String,System.String)
extern void Settings_SetKeys_m413E0ADFCDE90525929A8700E4B352748858CDD0 (void);
// 0x00000175 System.Void GameAnalyticsSDK.Setup.Settings::.ctor()
extern void Settings__ctor_mD348605211EA0147657EFDA462B03E21230FC7BE (void);
// 0x00000176 System.Void GameAnalyticsSDK.Setup.Settings::.cctor()
extern void Settings__cctor_mE297E9AA997520D04F63BC250A7B7B957B2DD098 (void);
// 0x00000177 System.String GameAnalyticsSDK.Setup.Organization::get_Name()
extern void Organization_get_Name_m279E143F49217DC785781A9337C5ECF1DDC3ABE6 (void);
// 0x00000178 System.Void GameAnalyticsSDK.Setup.Organization::set_Name(System.String)
extern void Organization_set_Name_m83DF5003B40FBA8DDC4A240B2E3693B05C1EBC10 (void);
// 0x00000179 System.String GameAnalyticsSDK.Setup.Organization::get_ID()
extern void Organization_get_ID_mD7C05E4703E9748261E8C3F0926405102E6CCADE (void);
// 0x0000017A System.Void GameAnalyticsSDK.Setup.Organization::set_ID(System.String)
extern void Organization_set_ID_mC3A097F532C20EF8F3E736716C2172CED9F6558E (void);
// 0x0000017B System.Collections.Generic.List`1<GameAnalyticsSDK.Setup.Studio> GameAnalyticsSDK.Setup.Organization::get_Studios()
extern void Organization_get_Studios_m6DB282D03656B16BF4CBF3C563BEB6D3977E9B84 (void);
// 0x0000017C System.Void GameAnalyticsSDK.Setup.Organization::set_Studios(System.Collections.Generic.List`1<GameAnalyticsSDK.Setup.Studio>)
extern void Organization_set_Studios_m3F27B87BB3970C7911F58FC5E2C4684BCE44B30B (void);
// 0x0000017D System.Void GameAnalyticsSDK.Setup.Organization::.ctor(System.String,System.String)
extern void Organization__ctor_m3589A2B5F25476CD5834F8BCC7682898D34A92CB (void);
// 0x0000017E System.String[] GameAnalyticsSDK.Setup.Organization::GetOrganizationNames(System.Collections.Generic.List`1<GameAnalyticsSDK.Setup.Organization>,System.Boolean)
extern void Organization_GetOrganizationNames_m3F721B0E0AB3FF174F57D9708C34DD203D509B4F (void);
// 0x0000017F System.String GameAnalyticsSDK.Setup.Studio::get_Name()
extern void Studio_get_Name_m9C38B3CD0BB49C3574F8D330A0AE75B097A10D56 (void);
// 0x00000180 System.Void GameAnalyticsSDK.Setup.Studio::set_Name(System.String)
extern void Studio_set_Name_mC2B8B607628B1890C8C53865A710D0AC439F3C84 (void);
// 0x00000181 System.String GameAnalyticsSDK.Setup.Studio::get_ID()
extern void Studio_get_ID_mED3314E2F75CE2634364713724D1BE310C78B448 (void);
// 0x00000182 System.Void GameAnalyticsSDK.Setup.Studio::set_ID(System.String)
extern void Studio_set_ID_m60CB3D539BBBC2F28A2081BF3F0A98B49EAC1586 (void);
// 0x00000183 System.String GameAnalyticsSDK.Setup.Studio::get_OrganizationID()
extern void Studio_get_OrganizationID_mD780092B6DBD6CBD110A053A861EA5619D734235 (void);
// 0x00000184 System.Void GameAnalyticsSDK.Setup.Studio::set_OrganizationID(System.String)
extern void Studio_set_OrganizationID_m1E2C735C9B8D045C7ECCA42780FC3560F64E0D8D (void);
// 0x00000185 System.Collections.Generic.List`1<GameAnalyticsSDK.Setup.Game> GameAnalyticsSDK.Setup.Studio::get_Games()
extern void Studio_get_Games_mC820F41524477421D0079FBD83A61F7961133AE7 (void);
// 0x00000186 System.Void GameAnalyticsSDK.Setup.Studio::set_Games(System.Collections.Generic.List`1<GameAnalyticsSDK.Setup.Game>)
extern void Studio_set_Games_m12820045A18B4AC14E5D3B796A57DE26FDB6A390 (void);
// 0x00000187 System.Void GameAnalyticsSDK.Setup.Studio::.ctor(System.String,System.String,System.String,System.Collections.Generic.List`1<GameAnalyticsSDK.Setup.Game>)
extern void Studio__ctor_mEED52D47755729201C2D6CB13A99E795CCDE6E3D (void);
// 0x00000188 System.String[] GameAnalyticsSDK.Setup.Studio::GetStudioNames(System.Collections.Generic.List`1<GameAnalyticsSDK.Setup.Studio>,System.Boolean)
extern void Studio_GetStudioNames_mB39A53EAFE0D941CC58CF87A4D48A7B2F4EB48C9 (void);
// 0x00000189 System.String[] GameAnalyticsSDK.Setup.Studio::GetGameNames(System.Int32,System.Collections.Generic.List`1<GameAnalyticsSDK.Setup.Studio>)
extern void Studio_GetGameNames_m01EB13121F696288EFA878CD000CCCE7A1E61CD8 (void);
// 0x0000018A System.String GameAnalyticsSDK.Setup.Game::get_Name()
extern void Game_get_Name_m03D31A24CE65EA926646E4433920863EB069CA51 (void);
// 0x0000018B System.Void GameAnalyticsSDK.Setup.Game::set_Name(System.String)
extern void Game_set_Name_m6493C376BC904499578834BAC51D422CC54D0F69 (void);
// 0x0000018C System.Int32 GameAnalyticsSDK.Setup.Game::get_ID()
extern void Game_get_ID_mC82557B878105F51F707E99FF1A03DF8C3A0FF17 (void);
// 0x0000018D System.Void GameAnalyticsSDK.Setup.Game::set_ID(System.Int32)
extern void Game_set_ID_mD0FC95C16C349C673AC1DBAD7AB6FCFCF37D8DD1 (void);
// 0x0000018E System.String GameAnalyticsSDK.Setup.Game::get_GameKey()
extern void Game_get_GameKey_m3E6F700AB988ED71EFA49FFAA7B2220713814A9C (void);
// 0x0000018F System.Void GameAnalyticsSDK.Setup.Game::set_GameKey(System.String)
extern void Game_set_GameKey_mF0C84B1943CFA6FF686554E5A0E5B01AEFBED3DB (void);
// 0x00000190 System.String GameAnalyticsSDK.Setup.Game::get_SecretKey()
extern void Game_get_SecretKey_m6F2AECEC7AD8C39404A4D46CC1272E5557CCD488 (void);
// 0x00000191 System.Void GameAnalyticsSDK.Setup.Game::set_SecretKey(System.String)
extern void Game_set_SecretKey_mF39915667EC0C739C743ABADF5A8451DB490E531 (void);
// 0x00000192 System.Void GameAnalyticsSDK.Setup.Game::.ctor(System.String,System.Int32,System.String,System.String)
extern void Game__ctor_m802F2A46F86F61DBEB64CFC6824146F6C4C84A03 (void);
// 0x00000193 System.Void GameAnalyticsSDK.Events.GA_Ads::NewEvent(GameAnalyticsSDK.GAAdAction,GameAnalyticsSDK.GAAdType,System.String,System.String,System.Int64)
extern void GA_Ads_NewEvent_mE9E5A9CD4FDDC15FA2846855B35B3C9521A3AC54 (void);
// 0x00000194 System.Void GameAnalyticsSDK.Events.GA_Ads::NewEvent(GameAnalyticsSDK.GAAdAction,GameAnalyticsSDK.GAAdType,System.String,System.String,GameAnalyticsSDK.GAAdError)
extern void GA_Ads_NewEvent_m9CFC13BC346529012BAB2CC23ADF022D77D1AEB6 (void);
// 0x00000195 System.Void GameAnalyticsSDK.Events.GA_Ads::NewEvent(GameAnalyticsSDK.GAAdAction,GameAnalyticsSDK.GAAdType,System.String,System.String)
extern void GA_Ads_NewEvent_m5F9DD89A371D9A3008513EEE3477A7E255583226 (void);
// 0x00000196 System.Void GameAnalyticsSDK.Events.GA_Business::NewEvent(System.String,System.Int32,System.String,System.String,System.String,System.String,System.Boolean,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Business_NewEvent_m8DD67C87CF1AD6BCE4D2A2AA987E34FBF16E969A (void);
// 0x00000197 System.Void GameAnalyticsSDK.Events.GA_Business::NewEvent(System.String,System.Int32,System.String,System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Business_NewEvent_m4A9E8EDEEAC83C6A6A263520805268871370C28A (void);
// 0x00000198 System.Void GameAnalyticsSDK.Events.GA_Debug::HandleLog(System.String,System.String,UnityEngine.LogType)
extern void GA_Debug_HandleLog_mE694DF9C6C50D86BD3BD3EF9ADA1E8D49BB6E6EA (void);
// 0x00000199 System.Void GameAnalyticsSDK.Events.GA_Debug::SubmitError(System.String,UnityEngine.LogType)
extern void GA_Debug_SubmitError_mA4013C0234BA3B134764F8B172D190BAE1C27C12 (void);
// 0x0000019A System.Void GameAnalyticsSDK.Events.GA_Debug::EnabledLog()
extern void GA_Debug_EnabledLog_m62102A6AE3527EA07F61732C29D7D41C98B6A558 (void);
// 0x0000019B System.Void GameAnalyticsSDK.Events.GA_Debug::.cctor()
extern void GA_Debug__cctor_mB8DC7261179EA484102FBEEE4BC76EBA01E270CB (void);
// 0x0000019C System.Void GameAnalyticsSDK.Events.GA_Design::NewEvent(System.String,System.Single,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Design_NewEvent_m0681AE097D570D727CBA861F97E1D622A19AABBA (void);
// 0x0000019D System.Void GameAnalyticsSDK.Events.GA_Design::NewEvent(System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Design_NewEvent_mE9688AC97B578FDE675C532C5A0409F2FAB67E78 (void);
// 0x0000019E System.Void GameAnalyticsSDK.Events.GA_Design::CreateNewEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Design_CreateNewEvent_m2CA912870B54A47E3624213556A8BB28FE6ACCDF (void);
// 0x0000019F System.Void GameAnalyticsSDK.Events.GA_Error::NewEvent(GameAnalyticsSDK.GAErrorSeverity,System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Error_NewEvent_mBF6CCDD3DDBA879AB22FD69E183C4666DBE2958B (void);
// 0x000001A0 System.Void GameAnalyticsSDK.Events.GA_Error::CreateNewEvent(GameAnalyticsSDK.GAErrorSeverity,System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Error_CreateNewEvent_m7A3E2D6A8AFDED5896C0B830497D8B5B76C68D66 (void);
// 0x000001A1 System.Void GameAnalyticsSDK.Events.GA_Progression::NewEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Progression_NewEvent_m3D69390A5DD9040131E56165FE2299B534C57368 (void);
// 0x000001A2 System.Void GameAnalyticsSDK.Events.GA_Progression::NewEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Progression_NewEvent_mAC7703988CFDF13A6D8FFB1F1018C228CB502305 (void);
// 0x000001A3 System.Void GameAnalyticsSDK.Events.GA_Progression::NewEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Progression_NewEvent_mE7251147E816C4DC954B4BB1C86820B5CD1C4FB9 (void);
// 0x000001A4 System.Void GameAnalyticsSDK.Events.GA_Progression::NewEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.Int32,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Progression_NewEvent_m89279B921396C02B3901272FBD362EAAEC119831 (void);
// 0x000001A5 System.Void GameAnalyticsSDK.Events.GA_Progression::NewEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.String,System.Int32,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Progression_NewEvent_m457B486D4843F9C046F568C096775EE8D3C320ED (void);
// 0x000001A6 System.Void GameAnalyticsSDK.Events.GA_Progression::NewEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.String,System.String,System.Int32,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Progression_NewEvent_m7492D3124C3666B060D06B18D784679F166C1AE7 (void);
// 0x000001A7 System.Void GameAnalyticsSDK.Events.GA_Progression::CreateEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.String,System.String,System.Nullable`1<System.Int32>,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Progression_CreateEvent_mF8CA9105B01C4AB3950B3E75B4BD361AC81B4DE5 (void);
// 0x000001A8 System.Void GameAnalyticsSDK.Events.GA_Resource::NewEvent(GameAnalyticsSDK.GAResourceFlowType,System.String,System.Single,System.String,System.String,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern void GA_Resource_NewEvent_mAB92EAE1B0F94AEA77820026D1273D211C0AB19B (void);
// 0x000001A9 System.Void GameAnalyticsSDK.Events.GA_Setup::SetAvailableCustomDimensions01(System.Collections.Generic.List`1<System.String>)
extern void GA_Setup_SetAvailableCustomDimensions01_mDD38BA20BA0F2D3F6BE0A9A78965AEDA6F3EE48D (void);
// 0x000001AA System.Void GameAnalyticsSDK.Events.GA_Setup::SetAvailableCustomDimensions02(System.Collections.Generic.List`1<System.String>)
extern void GA_Setup_SetAvailableCustomDimensions02_m3F6DBCDA8E25BA6E138F675F28B2CBB8B87C5D64 (void);
// 0x000001AB System.Void GameAnalyticsSDK.Events.GA_Setup::SetAvailableCustomDimensions03(System.Collections.Generic.List`1<System.String>)
extern void GA_Setup_SetAvailableCustomDimensions03_m814310F3D8C97D1D2F6E63D6F1EC755C931DE7C5 (void);
// 0x000001AC System.Void GameAnalyticsSDK.Events.GA_Setup::SetAvailableResourceCurrencies(System.Collections.Generic.List`1<System.String>)
extern void GA_Setup_SetAvailableResourceCurrencies_m0A340DC639662C84D04F60BDCC99A0076958BB35 (void);
// 0x000001AD System.Void GameAnalyticsSDK.Events.GA_Setup::SetAvailableResourceItemTypes(System.Collections.Generic.List`1<System.String>)
extern void GA_Setup_SetAvailableResourceItemTypes_m76764261864B821C3537B2971FE94C3C64D62ED1 (void);
// 0x000001AE System.Void GameAnalyticsSDK.Events.GA_Setup::SetInfoLog(System.Boolean)
extern void GA_Setup_SetInfoLog_m9F9DD51F7FB24A2DDE0AB166FE6151C422588248 (void);
// 0x000001AF System.Void GameAnalyticsSDK.Events.GA_Setup::SetVerboseLog(System.Boolean)
extern void GA_Setup_SetVerboseLog_m0DB3E2B4085A94C4FCBA9D789F2724E57309B560 (void);
// 0x000001B0 System.Void GameAnalyticsSDK.Events.GA_Setup::SetCustomDimension01(System.String)
extern void GA_Setup_SetCustomDimension01_m50ED8A54CDA84B5F07FE4DE2F856DE48251DC762 (void);
// 0x000001B1 System.Void GameAnalyticsSDK.Events.GA_Setup::SetCustomDimension02(System.String)
extern void GA_Setup_SetCustomDimension02_m37F7B159CB7CD012153763E2432BDE33609B9348 (void);
// 0x000001B2 System.Void GameAnalyticsSDK.Events.GA_Setup::SetCustomDimension03(System.String)
extern void GA_Setup_SetCustomDimension03_mDDE3A128E8ADAF9388C6B4D2C200BCE493137013 (void);
// 0x000001B3 System.Void GameAnalyticsSDK.Events.GA_SpecialEvents::Start()
extern void GA_SpecialEvents_Start_mF0CCF7223C003AAA275D2CD1417490D753D39A1C (void);
// 0x000001B4 System.Collections.IEnumerator GameAnalyticsSDK.Events.GA_SpecialEvents::SubmitFPSRoutine()
extern void GA_SpecialEvents_SubmitFPSRoutine_mB7ED0FECE94E26541C76004A5AF746E12BB4CA23 (void);
// 0x000001B5 System.Collections.IEnumerator GameAnalyticsSDK.Events.GA_SpecialEvents::CheckCriticalFPSRoutine()
extern void GA_SpecialEvents_CheckCriticalFPSRoutine_mD7D9FE93AF20F2C17F8197F9C9D2AC5A87A6292C (void);
// 0x000001B6 System.Void GameAnalyticsSDK.Events.GA_SpecialEvents::Update()
extern void GA_SpecialEvents_Update_m4DB853C10E28CE664D6D577247FCBFA5EDB51F34 (void);
// 0x000001B7 System.Void GameAnalyticsSDK.Events.GA_SpecialEvents::SubmitFPS()
extern void GA_SpecialEvents_SubmitFPS_m4BB21A0BC71E9BDBD2D30F9DAE46340672D7AB45 (void);
// 0x000001B8 System.Void GameAnalyticsSDK.Events.GA_SpecialEvents::CheckCriticalFPS()
extern void GA_SpecialEvents_CheckCriticalFPS_mAAAF2395C79FAEAE2CAFF64D2A849A1A70FD215E (void);
// 0x000001B9 System.Void GameAnalyticsSDK.Events.GA_SpecialEvents::.ctor()
extern void GA_SpecialEvents__ctor_mDBA1A33FC309E558151F4B469D7C04CD7B808140 (void);
// 0x000001BA System.Void GameAnalyticsSDK.Events.GA_SpecialEvents::.cctor()
extern void GA_SpecialEvents__cctor_m4B4688F83329CE504DD70643BDBB8E85F1651D1A (void);
// 0x000001BB System.Boolean GameAnalyticsSDK.Validators.GAValidator::StringMatch(System.String,System.String)
extern void GAValidator_StringMatch_mA357E4C8EF025495D522DE696F671A8CD0CBF41C (void);
// 0x000001BC System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateBusinessEvent(System.String,System.Int32,System.String,System.String,System.String)
extern void GAValidator_ValidateBusinessEvent_m73C2CD6E99E6BC169C71DA9ABA72B9E4EA2F61D7 (void);
// 0x000001BD System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateResourceEvent(GameAnalyticsSDK.GAResourceFlowType,System.String,System.Single,System.String,System.String)
extern void GAValidator_ValidateResourceEvent_m9D6719DF515F00A84DCD9C13C44D7AE2C8E3316E (void);
// 0x000001BE System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateProgressionEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.String,System.String)
extern void GAValidator_ValidateProgressionEvent_m37BD46F9638FF5D3F06DBB574E0C42CD601A9897 (void);
// 0x000001BF System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateDesignEvent(System.String)
extern void GAValidator_ValidateDesignEvent_m7354B2B20A7917AA5D641611A9061181256BB003 (void);
// 0x000001C0 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateErrorEvent(GameAnalyticsSDK.GAErrorSeverity,System.String)
extern void GAValidator_ValidateErrorEvent_m979DC06FE0B01857B85D19E1191FA0AA5447B7DD (void);
// 0x000001C1 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateAdEvent(GameAnalyticsSDK.GAAdAction,GameAnalyticsSDK.GAAdType,System.String,System.String)
extern void GAValidator_ValidateAdEvent_m2C8A9673537D14386B48D2C99FA777CF3F0BF096 (void);
// 0x000001C2 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateSdkErrorEvent(System.String,System.String,GameAnalyticsSDK.GAErrorSeverity)
extern void GAValidator_ValidateSdkErrorEvent_m57F71173AB1D60C8CA6657228F419C025D8CA114 (void);
// 0x000001C3 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateKeys(System.String,System.String)
extern void GAValidator_ValidateKeys_mF3DC8DC6E3DEFE05CB91A4C18874ABF1B1A46541 (void);
// 0x000001C4 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateCurrency(System.String)
extern void GAValidator_ValidateCurrency_m4AA7647E62CAF5E71C97405D941E45D77557D79B (void);
// 0x000001C5 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateEventPartLength(System.String,System.Boolean)
extern void GAValidator_ValidateEventPartLength_m3257ECC3162EB9C7331F3C727E6D02BC3A111245 (void);
// 0x000001C6 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateEventPartCharacters(System.String)
extern void GAValidator_ValidateEventPartCharacters_mD7BD8FF487DA788F144C4DF42F21D6A50F033F6B (void);
// 0x000001C7 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateEventIdLength(System.String)
extern void GAValidator_ValidateEventIdLength_m222C2216765B4655CD0ECE098AED4292A0075480 (void);
// 0x000001C8 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateEventIdCharacters(System.String)
extern void GAValidator_ValidateEventIdCharacters_mBDDAB1AAC84BFE0567EC799AC3C96B0DE7CCC599 (void);
// 0x000001C9 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateBuild(System.String)
extern void GAValidator_ValidateBuild_m6D1D70A38A270EFFBA517CDCFF1287A9F2BAA99A (void);
// 0x000001CA System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateUserId(System.String)
extern void GAValidator_ValidateUserId_m7992B6F93172799E3C0FA41E123E2462FD76108B (void);
// 0x000001CB System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateShortString(System.String,System.Boolean)
extern void GAValidator_ValidateShortString_m64E44353AE7525A1A0AF735D110A84BDED4AF310 (void);
// 0x000001CC System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateString(System.String,System.Boolean)
extern void GAValidator_ValidateString_m762049135FEA5B6439A4D2B5C221E86476B08267 (void);
// 0x000001CD System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateLongString(System.String,System.Boolean)
extern void GAValidator_ValidateLongString_m66BA7E43A5FE0CA029FF62FB6D0D5AC0EC41404B (void);
// 0x000001CE System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateConnectionType(System.String)
extern void GAValidator_ValidateConnectionType_m8797FECAC6703D6378334AE1952A32F1E11434E9 (void);
// 0x000001CF System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateCustomDimensions(System.String[])
extern void GAValidator_ValidateCustomDimensions_mB3D912D85F952C2D3DDE597088BA0CBC8456906D (void);
// 0x000001D0 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateResourceCurrencies(System.String[])
extern void GAValidator_ValidateResourceCurrencies_mA6CC78155D70696B4148F5BDE72B83AC95FD1EB4 (void);
// 0x000001D1 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateResourceItemTypes(System.String[])
extern void GAValidator_ValidateResourceItemTypes_mF2A140147E85A19318A8BF5F0DE519FC6E8282BF (void);
// 0x000001D2 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateDimension01(System.String)
extern void GAValidator_ValidateDimension01_mC47008DEFAC01910F1F8D050D79223B7585BAD9D (void);
// 0x000001D3 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateDimension02(System.String)
extern void GAValidator_ValidateDimension02_m44ABDDE77100EF7BB62A3EE043ADA5160CC3F390 (void);
// 0x000001D4 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateDimension03(System.String)
extern void GAValidator_ValidateDimension03_mE15B6F02F34B4969AE3367FE74D4AD59E4FFA0AD (void);
// 0x000001D5 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateArrayOfStrings(System.Int64,System.Int64,System.Boolean,System.String,System.String[])
extern void GAValidator_ValidateArrayOfStrings_m218B0989ABE5AA287D9D26C20212CB1E655DE3DD (void);
// 0x000001D6 System.Boolean GameAnalyticsSDK.Validators.GAValidator::ValidateClientTs(System.Int64)
extern void GAValidator_ValidateClientTs_m5F737880925FB1ECD73F944508CA05A4B1F0BD69 (void);
// 0x000001D7 System.Void Voodoo.Sauce.Internal.VoodooLog::Initialize(Voodoo.Sauce.Internal.VoodooLogLevel)
extern void VoodooLog_Initialize_m9C4412D348DE823CC4535F0327D0985EC68B5B74 (void);
// 0x000001D8 System.Void Voodoo.Sauce.Internal.VoodooLog::Log(System.String,System.String)
extern void VoodooLog_Log_mA7C46EE6B665A3BCC85F83F844B24039AEE02D71 (void);
// 0x000001D9 System.Void Voodoo.Sauce.Internal.VoodooLog::LogE(System.String,System.String)
extern void VoodooLog_LogE_mE4E1FC2258F9C025F5E965B9D6FE302F8346ED69 (void);
// 0x000001DA System.Void Voodoo.Sauce.Internal.VoodooLog::LogW(System.String,System.String)
extern void VoodooLog_LogW_m1B8171158AD9DCD531714EA008F3EE5F6115ED74 (void);
// 0x000001DB System.String Voodoo.Sauce.Internal.VoodooLog::Format(System.String,System.String)
extern void VoodooLog_Format_m3B92528E7DEBA4AE65FE81F97571702522A85DFE (void);
// 0x000001DC System.Void Voodoo.Sauce.Internal.VoodooLog::DisableLogs()
extern void VoodooLog_DisableLogs_mB02FC2E65F7485E3AC4F142F6550E5CC21A90B69 (void);
// 0x000001DD System.Void Voodoo.Sauce.Internal.VoodooLog::EnableLogs()
extern void VoodooLog_EnableLogs_mCF301D8664674A872023E1673BD6A4A65D989B46 (void);
// 0x000001DE System.Boolean Voodoo.Sauce.Internal.VoodooLog::IsLogsEnabled()
extern void VoodooLog_IsLogsEnabled_mD4375E25A3B9DDFDF46A87CEA0E869977AAE749B (void);
// 0x000001DF System.Void Voodoo.Sauce.Internal.TinySauceBehaviour::Awake()
extern void TinySauceBehaviour_Awake_mFAAA01A24AFF626349B4700FE195B4249FB2CCF2 (void);
// 0x000001E0 System.Void Voodoo.Sauce.Internal.TinySauceBehaviour::InitAnalytics()
extern void TinySauceBehaviour_InitAnalytics_m0E97CA2D3FA96886CD3E6FB732B13084D73C90FA (void);
// 0x000001E1 System.Void Voodoo.Sauce.Internal.TinySauceBehaviour::OnApplicationPause(System.Boolean)
extern void TinySauceBehaviour_OnApplicationPause_m6EA76A02E0EB2780ECE18C572E36F494375C792F (void);
// 0x000001E2 System.Void Voodoo.Sauce.Internal.TinySauceBehaviour::.ctor()
extern void TinySauceBehaviour__ctor_m0E88FCBC2D30637F5A566100037FC4894F0D8B96 (void);
// 0x000001E3 Voodoo.Sauce.Internal.TinySauceSettings Voodoo.Sauce.Internal.TinySauceSettings::Load()
extern void TinySauceSettings_Load_m6B57575CD43B33BAE0FDDDC7CEDCCC3CF850C747 (void);
// 0x000001E4 System.Void Voodoo.Sauce.Internal.TinySauceSettings::.ctor()
extern void TinySauceSettings__ctor_m17B2849E172B818BD924A5473B9E47CD800D3837 (void);
// 0x000001E5 System.Boolean Voodoo.Sauce.Internal.Utils.ManifestUtils::MergeManifestsFiles(System.String,System.String)
extern void ManifestUtils_MergeManifestsFiles_m486A18A2433548D7340240185A7B2C415A284131 (void);
// 0x000001E6 System.String Voodoo.Sauce.Internal.Utils.ManifestUtils::SetApplicationAttributes(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void ManifestUtils_SetApplicationAttributes_mD8E4D1597CCD5EB79D87947EB02AC5B781B9A759 (void);
// 0x000001E7 System.Boolean Voodoo.Sauce.Internal.Utils.ManifestUtils::ReplaceKeys(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void ManifestUtils_ReplaceKeys_mC82D1EA16D1BB27023A9C83F66B8C59971A0CFD1 (void);
// 0x000001E8 System.Xml.XmlDocument Voodoo.Sauce.Internal.Utils.ManifestUtils::Add(System.Xml.XmlDocument,System.Xml.XmlDocument)
extern void ManifestUtils_Add_mBFB87E1A709BF3A7EBD8A6D26F6B4FCBBBB66675 (void);
// 0x000001E9 System.Xml.XmlDocument Voodoo.Sauce.Internal.Utils.ManifestUtils::LoadFromFile(System.String)
extern void ManifestUtils_LoadFromFile_m6D0336D0716E4CBF02268B16E0D7099939C301D8 (void);
// 0x000001EA System.Xml.XmlDocument Voodoo.Sauce.Internal.Utils.ManifestUtils::LoadFromString(System.String)
extern void ManifestUtils_LoadFromString_m49896DCFF05217BCE4279ACF6CDE7FC7B607825F (void);
// 0x000001EB System.Void Voodoo.Sauce.Internal.Utils.ManifestUtils::AddChildNode(System.Xml.XmlDocument,System.Xml.XmlNode,System.Xml.XmlNode)
extern void ManifestUtils_AddChildNode_m0774E2B100503D9E06CB49A9CC70542AB8467B1D (void);
// 0x000001EC System.Xml.XmlNode Voodoo.Sauce.Internal.Utils.ManifestUtils::FindChildNode(System.Xml.XmlNode,System.Xml.XmlNode)
extern void ManifestUtils_FindChildNode_m8CAAAC87A37AFCCEAB570F078FEDF556683973F3 (void);
// 0x000001ED System.Xml.XmlAttribute Voodoo.Sauce.Internal.Utils.ManifestUtils::FindAttribute(System.Collections.IEnumerable,System.String)
extern void ManifestUtils_FindAttribute_mADD12182712B3FF205596CDA738DE551C111C7A2 (void);
// 0x000001EE System.Xml.XmlNode Voodoo.Sauce.Internal.Utils.ManifestUtils::FindChildNode(System.Xml.XmlNode,System.String)
extern void ManifestUtils_FindChildNode_mEF66F9E3B4965C85BA9B80EB7DAE77F621F0A1CF (void);
// 0x000001EF System.Boolean Voodoo.Sauce.Internal.Utils.ManifestUtils::FindElementWithAndroidName(System.Xml.XmlNode,System.Xml.XmlNode)
extern void ManifestUtils_FindElementWithAndroidName_m040EF519E28229C677A7625260255A813176EC39 (void);
// 0x000001F0 System.String Voodoo.Sauce.Internal.Utils.ManifestUtils::GetAndroidElementName(System.Xml.XmlNode,System.String)
extern void ManifestUtils_GetAndroidElementName_mC68A244A12CA5110AE421509EC1D90B18F30B050 (void);
// 0x000001F1 System.Void Voodoo.Sauce.Internal.Utils.ManifestUtils::SaveDocumentInFile(System.String,System.Xml.XmlDocument)
extern void ManifestUtils_SaveDocumentInFile_m7BB668EEBB671FCC3F827635800D4EA483725B57 (void);
// 0x000001F2 System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::add_OnGameStartedEvent(System.Action`1<System.String>)
extern void AnalyticsManager_add_OnGameStartedEvent_m2DB92954CA3ACF9DAB2DA17C23D3616BB969683E (void);
// 0x000001F3 System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::remove_OnGameStartedEvent(System.Action`1<System.String>)
extern void AnalyticsManager_remove_OnGameStartedEvent_m41D0E1C7DC0A0EA4B0C2962D7817373BDD442F9C (void);
// 0x000001F4 System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::add_OnGameFinishedEvent(System.Action`4<System.Boolean,System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern void AnalyticsManager_add_OnGameFinishedEvent_mD245F0F432F9603FFD7DF7CC2D416E56A308D9B9 (void);
// 0x000001F5 System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::remove_OnGameFinishedEvent(System.Action`4<System.Boolean,System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>)
extern void AnalyticsManager_remove_OnGameFinishedEvent_mAC210AC5EEA8AE878F279C59F4B7C945CAE17AC5 (void);
// 0x000001F6 System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::add_OnTrackCustomValueEvent(System.Action`2<System.String,System.Single>)
extern void AnalyticsManager_add_OnTrackCustomValueEvent_mCA155B97B760B99E6211B88B1ADD56DA51C4DA82 (void);
// 0x000001F7 System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::remove_OnTrackCustomValueEvent(System.Action`2<System.String,System.Single>)
extern void AnalyticsManager_remove_OnTrackCustomValueEvent_mF597715E58A5EBE15D1928AEAF8FB8F3A902784F (void);
// 0x000001F8 System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::add_OnTrackCustomEvent(System.Action`1<System.String>)
extern void AnalyticsManager_add_OnTrackCustomEvent_m92DD176430D5C061E516428635A3549E25B936D9 (void);
// 0x000001F9 System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::remove_OnTrackCustomEvent(System.Action`1<System.String>)
extern void AnalyticsManager_remove_OnTrackCustomEvent_m8823BAF4923C42C1202DFC4835923343684F1C9B (void);
// 0x000001FA System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::add_OnApplicationResumeEvent(System.Action)
extern void AnalyticsManager_add_OnApplicationResumeEvent_mC1ECC28471F5B3D75CFE4EE9D03B7B1E2A63CBEA (void);
// 0x000001FB System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::remove_OnApplicationResumeEvent(System.Action)
extern void AnalyticsManager_remove_OnApplicationResumeEvent_m3AE3CF3CABE9B1B9E874B684659EE452940842E1 (void);
// 0x000001FC System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::Initialize(Voodoo.Sauce.Internal.TinySauceSettings,System.Boolean)
extern void AnalyticsManager_Initialize_mFFF61A4BE10567C5D6E20DD7A4051B17E9FD5D9A (void);
// 0x000001FD System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::OnGameStarted(System.String)
extern void AnalyticsManager_OnGameStarted_m6D59D3F362521E742EAFD0AAE7525F289BD80B51 (void);
// 0x000001FE System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::OnGameFinished(System.Boolean,System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AnalyticsManager_OnGameFinished_m35F8AF37C05A8370BC19981E2D991BDC9EDB21A8 (void);
// 0x000001FF System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::TrackCustomEvent(System.String)
extern void AnalyticsManager_TrackCustomEvent_mEE1FAF7E9B09562D95CC12E0235D8CB3F5C04A72 (void);
// 0x00000200 System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::TrackCustomEvent(System.String,System.Single)
extern void AnalyticsManager_TrackCustomEvent_m3E65453D0935B6D21742C2455C515E54435F2238 (void);
// 0x00000201 System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::OnApplicationResume()
extern void AnalyticsManager_OnApplicationResume_m7C345F057B8BC930D89FE15CEDD83D6A5F650FB8 (void);
// 0x00000202 System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager::.cctor()
extern void AnalyticsManager__cctor_m48CCE10D4A28C4ECA3B091E7BF705533734329DA (void);
// 0x00000203 System.Void Voodoo.Sauce.Internal.Analytics.IAnalyticsProvider::Initialize(System.Boolean)
// 0x00000204 System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::.ctor()
extern void GameAnalyticsProvider__ctor_m649CA095AC71C702042C742F7CA5A53F34CE8180 (void);
// 0x00000205 System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::Initialize(System.Boolean)
extern void GameAnalyticsProvider_Initialize_m294805F45E57906DEE3B2E7633EF71BAC8F14354 (void);
// 0x00000206 System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::RegisterEvents()
extern void GameAnalyticsProvider_RegisterEvents_mDE6B88888D1EADBC9A28A94FA3ADB62F3D822388 (void);
// 0x00000207 System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::UnregisterEvents()
extern void GameAnalyticsProvider_UnregisterEvents_mC334E0CD3E15B9E6775C12989F863BD09A2A50F8 (void);
// 0x00000208 System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::OnGameStarted(System.String)
extern void GameAnalyticsProvider_OnGameStarted_mEAC6338759F4E680D93D78C630694211CCA4401E (void);
// 0x00000209 System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::OnGameFinished(System.Boolean,System.Single,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void GameAnalyticsProvider_OnGameFinished_m9F975CB121DDE865EDDDF875B70FD83136B2A9D8 (void);
// 0x0000020A System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::TrackCustomEvent(System.String)
extern void GameAnalyticsProvider_TrackCustomEvent_mB625F71BC4E98012C3393805C0DBAB9FD5F2C66A (void);
// 0x0000020B System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsProvider::TrackCustomEvent(System.String,System.Single)
extern void GameAnalyticsProvider_TrackCustomEvent_m06AEFC1C2384CDF19C60A23DCFB227387598565C (void);
// 0x0000020C System.Boolean Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::Initialize(System.Boolean)
extern void GameAnalyticsWrapper_Initialize_m6A2269DECEC4E5C5D8107255BBF78CE06DA96FE5 (void);
// 0x0000020D System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::TrackProgressEvent(GameAnalyticsSDK.GAProgressionStatus,System.String,System.Nullable`1<System.Int32>)
extern void GameAnalyticsWrapper_TrackProgressEvent_m7DE043963ECAC5503F3118CCDD85775EA66B0664 (void);
// 0x0000020E System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::TrackDesignEvent(System.String,System.Nullable`1<System.Single>)
extern void GameAnalyticsWrapper_TrackDesignEvent_mEFBC717B6DC026CB9A3CA0A3B9EE55B576D15040 (void);
// 0x0000020F System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::InstantiateGameAnalytics()
extern void GameAnalyticsWrapper_InstantiateGameAnalytics_mD26002F3EBDA3080C19C03B6ACFCDBF428A6D199 (void);
// 0x00000210 System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::Disable()
extern void GameAnalyticsWrapper_Disable_m60C77A631514F58F9C58B4001B27DC46A3D5D22C (void);
// 0x00000211 System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper::.cctor()
extern void GameAnalyticsWrapper__cctor_m53D4076D48F07C37FAFF4D0FCB728A7B238DFA51 (void);
// 0x00000212 System.Void UnityEngine.GameState::.ctor()
extern void GameState__ctor_m8DA1777D5848F1B7AF01F10AB51A89D6EB690B4B (void);
// 0x00000213 System.Void UnityEngine.StepLevel::.ctor()
extern void StepLevel__ctor_m26E21E51BD2F5A085B2DD452890F961806C78553 (void);
// 0x00000214 System.Void UnityEngine.BulletAttack::.ctor(UnityEngine.Vector3,System.Int32)
extern void BulletAttack__ctor_m3777F5A99C7C59CE9B1D6CDC42CC1A9072EF1A97 (void);
// 0x00000215 System.Void UnityEngine.Gun::.ctor()
extern void Gun__ctor_m6D10398582A319E054D4C5B2E76D9C4BB50FD695 (void);
// 0x00000216 System.Void UnityEngine.Sound::.ctor()
extern void Sound__ctor_m2ADC59C98B7151620EA490E2745463A5D6603ED2 (void);
// 0x00000217 System.Void BaseCharacter_<Hit>d__8::.ctor(System.Int32)
extern void U3CHitU3Ed__8__ctor_m2E2CA499B29C0C2B2F28C33674D87C13B50FF806 (void);
// 0x00000218 System.Void BaseCharacter_<Hit>d__8::System.IDisposable.Dispose()
extern void U3CHitU3Ed__8_System_IDisposable_Dispose_mF101BF3A30D55FC252BC4DAEF2AAF41B84D94A5B (void);
// 0x00000219 System.Boolean BaseCharacter_<Hit>d__8::MoveNext()
extern void U3CHitU3Ed__8_MoveNext_mCD1E534D906D8A80461E1CA6E9F5DECE64020293 (void);
// 0x0000021A System.Object BaseCharacter_<Hit>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m798551BB8388864FEEA3870E13A0A2132D33617C (void);
// 0x0000021B System.Void BaseCharacter_<Hit>d__8::System.Collections.IEnumerator.Reset()
extern void U3CHitU3Ed__8_System_Collections_IEnumerator_Reset_mE5795F4E77068893A9866EE941EE1DE9C1FDBB47 (void);
// 0x0000021C System.Object BaseCharacter_<Hit>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CHitU3Ed__8_System_Collections_IEnumerator_get_Current_mA47D1BA386A499F8CB3E84EC7A112557E2EB6EBE (void);
// 0x0000021D System.Void BaseCharacter_<Die>d__9::.ctor(System.Int32)
extern void U3CDieU3Ed__9__ctor_mF934CF8A2EB8CD524608CA3C418C711A7867CC66 (void);
// 0x0000021E System.Void BaseCharacter_<Die>d__9::System.IDisposable.Dispose()
extern void U3CDieU3Ed__9_System_IDisposable_Dispose_m07840CC29ABF13C986E8F2D6486073E755D0C04E (void);
// 0x0000021F System.Boolean BaseCharacter_<Die>d__9::MoveNext()
extern void U3CDieU3Ed__9_MoveNext_m12176FB55FDD2BD4AC2CDA4C54C4F13365F75A24 (void);
// 0x00000220 System.Object BaseCharacter_<Die>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDieU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88871930AA0A5E8C775394D2693BB47A24AD982D (void);
// 0x00000221 System.Void BaseCharacter_<Die>d__9::System.Collections.IEnumerator.Reset()
extern void U3CDieU3Ed__9_System_Collections_IEnumerator_Reset_m3FE9C7925EFD40688BB870ED8D38C4BBD1324C03 (void);
// 0x00000222 System.Object BaseCharacter_<Die>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CDieU3Ed__9_System_Collections_IEnumerator_get_Current_m9479A448D0CD68F167C0D6CD7DE26B0759ACC0CF (void);
// 0x00000223 System.Void ScoreController_<GotoIcon>d__9::.ctor(System.Int32)
extern void U3CGotoIconU3Ed__9__ctor_mF6540977B0C142CE29B6FB9B48259B26CA310DBB (void);
// 0x00000224 System.Void ScoreController_<GotoIcon>d__9::System.IDisposable.Dispose()
extern void U3CGotoIconU3Ed__9_System_IDisposable_Dispose_mF2C58AECBAF42A2942BD19B1E9BDBD4845CF8DEB (void);
// 0x00000225 System.Boolean ScoreController_<GotoIcon>d__9::MoveNext()
extern void U3CGotoIconU3Ed__9_MoveNext_m7FF741F1A325DA0008C47DBC10378BD472417CBF (void);
// 0x00000226 System.Object ScoreController_<GotoIcon>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGotoIconU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF0FC9B1BC42011E21A401CC06728091D3E18473 (void);
// 0x00000227 System.Void ScoreController_<GotoIcon>d__9::System.Collections.IEnumerator.Reset()
extern void U3CGotoIconU3Ed__9_System_Collections_IEnumerator_Reset_m0B70AC0B521DE9063E71CD4CB7B974D2059F2835 (void);
// 0x00000228 System.Object ScoreController_<GotoIcon>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CGotoIconU3Ed__9_System_Collections_IEnumerator_get_Current_m306B27D9962BA73B8F17EB45E3A3F1E62E6D526C (void);
// 0x00000229 System.Void SoundController_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m3E2A097E435942C67F76BF494E18317C76D8C2B2 (void);
// 0x0000022A System.Boolean SoundController_<>c__DisplayClass2_0::<Play>b__0(UnityEngine.Sound)
extern void U3CU3Ec__DisplayClass2_0_U3CPlayU3Eb__0_m397A095DF92C801AED5DD3D7387EEE6A5CD40910 (void);
// 0x0000022B System.Void FinishLinePosition_<PingpongFinishLine>d__6::.ctor(System.Int32)
extern void U3CPingpongFinishLineU3Ed__6__ctor_mA1C50F6FFEC349CA633D75A6E89227C22780DFCB (void);
// 0x0000022C System.Void FinishLinePosition_<PingpongFinishLine>d__6::System.IDisposable.Dispose()
extern void U3CPingpongFinishLineU3Ed__6_System_IDisposable_Dispose_m1AA0F696FCFA4492536A6F86E451314C1CA8140E (void);
// 0x0000022D System.Boolean FinishLinePosition_<PingpongFinishLine>d__6::MoveNext()
extern void U3CPingpongFinishLineU3Ed__6_MoveNext_m414FC4BCCE7F4A8D39AC88327733BEB388FA7076 (void);
// 0x0000022E System.Object FinishLinePosition_<PingpongFinishLine>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPingpongFinishLineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C2114CFB31D90D532397D95880C346C78322045 (void);
// 0x0000022F System.Void FinishLinePosition_<PingpongFinishLine>d__6::System.Collections.IEnumerator.Reset()
extern void U3CPingpongFinishLineU3Ed__6_System_Collections_IEnumerator_Reset_m31CA2CC3C7B260D511063BC93636603EC3085AED (void);
// 0x00000230 System.Object FinishLinePosition_<PingpongFinishLine>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CPingpongFinishLineU3Ed__6_System_Collections_IEnumerator_get_Current_mEE21B950B1FA88555505738BF8521F0BF765983E (void);
// 0x00000231 System.Void Vibrator_<Cancel>d__6::.ctor(System.Int32)
extern void U3CCancelU3Ed__6__ctor_m8A9D78A5A24DE8D8D57E919FFDC0BBFF27A4C626 (void);
// 0x00000232 System.Void Vibrator_<Cancel>d__6::System.IDisposable.Dispose()
extern void U3CCancelU3Ed__6_System_IDisposable_Dispose_m9A0E056A2C4AC1D78FD7607B30B3EC70EE97D1BB (void);
// 0x00000233 System.Boolean Vibrator_<Cancel>d__6::MoveNext()
extern void U3CCancelU3Ed__6_MoveNext_mF0821233C774A16953299453667ACBD5CF46689C (void);
// 0x00000234 System.Object Vibrator_<Cancel>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCancelU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mADEA1A845BA81B998D01926D53593F1910E95E02 (void);
// 0x00000235 System.Void Vibrator_<Cancel>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCancelU3Ed__6_System_Collections_IEnumerator_Reset_m55911FE421A635EDB1BF223D063938E66FEBDB3C (void);
// 0x00000236 System.Object Vibrator_<Cancel>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCancelU3Ed__6_System_Collections_IEnumerator_get_Current_mB65BC877A2FBF94D80E52DC38EB2C382BDBF11DC (void);
// 0x00000237 System.Void GameManager_<PingWarning>d__42::.ctor(System.Int32)
extern void U3CPingWarningU3Ed__42__ctor_mA0B7CA28B1F7FBDFF68557DB4168D3F2D897974C (void);
// 0x00000238 System.Void GameManager_<PingWarning>d__42::System.IDisposable.Dispose()
extern void U3CPingWarningU3Ed__42_System_IDisposable_Dispose_m8C7F07D5E8A7BAF846334DC8B4239CFD0F90DAC8 (void);
// 0x00000239 System.Boolean GameManager_<PingWarning>d__42::MoveNext()
extern void U3CPingWarningU3Ed__42_MoveNext_m2602F2C84AD68CB7918E16D8F0B45593D74ED77B (void);
// 0x0000023A System.Object GameManager_<PingWarning>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPingWarningU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3D79B026EB49146712786370033D7FE02A3CE602 (void);
// 0x0000023B System.Void GameManager_<PingWarning>d__42::System.Collections.IEnumerator.Reset()
extern void U3CPingWarningU3Ed__42_System_Collections_IEnumerator_Reset_m46305774EAE7525D01EC79D21E80EA395B8CE422 (void);
// 0x0000023C System.Object GameManager_<PingWarning>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CPingWarningU3Ed__42_System_Collections_IEnumerator_get_Current_mE7E64143D6CD100BCA742DBD9435C52BB159F2BD (void);
// 0x0000023D System.Void BossGunController_<BossFight>d__4::.ctor(System.Int32)
extern void U3CBossFightU3Ed__4__ctor_mA5D7724309E846D4E280F59F410C653674C943CC (void);
// 0x0000023E System.Void BossGunController_<BossFight>d__4::System.IDisposable.Dispose()
extern void U3CBossFightU3Ed__4_System_IDisposable_Dispose_mBCAF3B312C9EB44916496A4BB4BCADB4D86BC490 (void);
// 0x0000023F System.Boolean BossGunController_<BossFight>d__4::MoveNext()
extern void U3CBossFightU3Ed__4_MoveNext_m793C392AEE1C3EF966E72AE164A80707B85F1A1C (void);
// 0x00000240 System.Object BossGunController_<BossFight>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBossFightU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m703481914D909722E5447B427404C8A5E3B38BED (void);
// 0x00000241 System.Void BossGunController_<BossFight>d__4::System.Collections.IEnumerator.Reset()
extern void U3CBossFightU3Ed__4_System_Collections_IEnumerator_Reset_m57418F64079A821CCDA63406454C7B16729A63DF (void);
// 0x00000242 System.Object BossGunController_<BossFight>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CBossFightU3Ed__4_System_Collections_IEnumerator_get_Current_mB459A871EDA1B08BB12269ED87DA6B8CE92373F2 (void);
// 0x00000243 System.Void BaseBullet_<Start>d__5::.ctor(System.Int32)
extern void U3CStartU3Ed__5__ctor_mF2961383233177CAFFCCA6D847CEC747807F0489 (void);
// 0x00000244 System.Void BaseBullet_<Start>d__5::System.IDisposable.Dispose()
extern void U3CStartU3Ed__5_System_IDisposable_Dispose_m941CBBE9C658E8F68415E14E8C36BACA17394B59 (void);
// 0x00000245 System.Boolean BaseBullet_<Start>d__5::MoveNext()
extern void U3CStartU3Ed__5_MoveNext_mBCE88E052FA9C8BD91EE16321ECAC6A08EBE6918 (void);
// 0x00000246 System.Object BaseBullet_<Start>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFAD7091AA1A4AE96130C0957C9D867FA134B5995 (void);
// 0x00000247 System.Void BaseBullet_<Start>d__5::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__5_System_Collections_IEnumerator_Reset_m0AA880D00287E8A03A252B7C8400716AEDA4CE48 (void);
// 0x00000248 System.Object BaseBullet_<Start>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__5_System_Collections_IEnumerator_get_Current_m36E5719D03288E89813E3AFDD6638494C6E1840F (void);
// 0x00000249 System.Void BulletAutoDestroy_<Start>d__1::.ctor(System.Int32)
extern void U3CStartU3Ed__1__ctor_m798D636CCF04E07DB9557D36BF3F01362DBE2CD6 (void);
// 0x0000024A System.Void BulletAutoDestroy_<Start>d__1::System.IDisposable.Dispose()
extern void U3CStartU3Ed__1_System_IDisposable_Dispose_m4E32BEB5EA7E5B42E3425AA81E5CFC373DB731EA (void);
// 0x0000024B System.Boolean BulletAutoDestroy_<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m93192AB72557B282BD46F135D5FFB5F1EB8E6FFA (void);
// 0x0000024C System.Object BulletAutoDestroy_<Start>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC9DC5BC8114331194AD89E3D41E1B106278C638C (void);
// 0x0000024D System.Void BulletAutoDestroy_<Start>d__1::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_mDD3CED12E0CD28F02232972A21D92AC8AE1B1FDD (void);
// 0x0000024E System.Object BulletAutoDestroy_<Start>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m5D60D96E86210508BBFD426C0D815F4A97A11E21 (void);
// 0x0000024F System.Boolean GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_m7FD116D792B6BA36448E35A51612D36562E90527 (void);
// 0x00000250 System.Void GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::.ctor(System.String)
extern void Parser__ctor_m5B351AF5ABA2A87278A6352C66122EE60E4BCE13 (void);
// 0x00000251 System.Object GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::Parse(System.String)
extern void Parser_Parse_mEA4E3C744BCC561E81195B809B0AC857416F3072 (void);
// 0x00000252 System.Void GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::Dispose()
extern void Parser_Dispose_m487491A4438771227B6630A48574B30B52279AC9 (void);
// 0x00000253 System.Collections.Generic.Dictionary`2<System.String,System.Object> GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::ParseObject()
extern void Parser_ParseObject_m6BC97A0DDE81FBC437E225CAC841641AC0C3623D (void);
// 0x00000254 System.Collections.Generic.List`1<System.Object> GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::ParseArray()
extern void Parser_ParseArray_mD25D8E36291A19139F54EDAE23CA9A484E9AF06C (void);
// 0x00000255 System.Object GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::ParseValue()
extern void Parser_ParseValue_mBD8A7D064E636C48DAAAFAB2367F806A235CCC17 (void);
// 0x00000256 System.Object GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::ParseByToken(GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser_TOKEN)
extern void Parser_ParseByToken_m1264B18AC165C9130808F52DDEE72B2C701B667F (void);
// 0x00000257 System.String GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::ParseString()
extern void Parser_ParseString_mD6827D047119E0C597D701A6FA8AC1AEDC248827 (void);
// 0x00000258 System.Object GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::ParseNumber()
extern void Parser_ParseNumber_mD619A21BEED602342A177FA160C0137284125984 (void);
// 0x00000259 System.Void GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::EatWhitespace()
extern void Parser_EatWhitespace_mDCEC0B86CA61D28F43ADF2B96E9093C9A7F64F1D (void);
// 0x0000025A System.Char GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::get_PeekChar()
extern void Parser_get_PeekChar_mE7F60F0949AE0D6979981A7E406DA132A36A5F71 (void);
// 0x0000025B System.Char GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::get_NextChar()
extern void Parser_get_NextChar_m6E2FDD55FCA3C4872266C299811C91BE69D01039 (void);
// 0x0000025C System.String GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::get_NextWord()
extern void Parser_get_NextWord_mBB267293F9D244522179C189155DE54D75F86EA5 (void);
// 0x0000025D GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser_TOKEN GameAnalyticsSDK.Utilities.GA_MiniJSON_Parser::get_NextToken()
extern void Parser_get_NextToken_m18434DF3642CA5D73174669069B195EABAA4D7F6 (void);
// 0x0000025E System.Void GameAnalyticsSDK.Utilities.GA_MiniJSON_Serializer::.ctor()
extern void Serializer__ctor_m94982AC291A8D796ED90138D15C6E6574E69CEE7 (void);
// 0x0000025F System.String GameAnalyticsSDK.Utilities.GA_MiniJSON_Serializer::Serialize(System.Object)
extern void Serializer_Serialize_m2942C1314A02CE414959A572F6270A2FF60FCB72 (void);
// 0x00000260 System.Void GameAnalyticsSDK.Utilities.GA_MiniJSON_Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_mEFF93C2569039C69E02F9C836CA4899ADAD22F90 (void);
// 0x00000261 System.Void GameAnalyticsSDK.Utilities.GA_MiniJSON_Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_mC6A2EA88EED97E459BEAC12FCED66A3E1DF54290 (void);
// 0x00000262 System.Void GameAnalyticsSDK.Utilities.GA_MiniJSON_Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_m6DD81E55FB7E11A7D8B00201A0D5BB3BEEC47CFE (void);
// 0x00000263 System.Void GameAnalyticsSDK.Utilities.GA_MiniJSON_Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_mA7658C4CD540D609A5B5FBFFF1978EEF1EACC059 (void);
// 0x00000264 System.Void GameAnalyticsSDK.Utilities.GA_MiniJSON_Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_m0815A226A5F03D5F9642E190963E99ADD5DE3F82 (void);
// 0x00000265 System.Void GameAnalyticsSDK.Events.GA_SpecialEvents_<SubmitFPSRoutine>d__6::.ctor(System.Int32)
extern void U3CSubmitFPSRoutineU3Ed__6__ctor_m094B86B76E427C5B78588C7897440BB3B3D508F2 (void);
// 0x00000266 System.Void GameAnalyticsSDK.Events.GA_SpecialEvents_<SubmitFPSRoutine>d__6::System.IDisposable.Dispose()
extern void U3CSubmitFPSRoutineU3Ed__6_System_IDisposable_Dispose_m322DCD72816F3E60A11D5845B914A0FD1871A764 (void);
// 0x00000267 System.Boolean GameAnalyticsSDK.Events.GA_SpecialEvents_<SubmitFPSRoutine>d__6::MoveNext()
extern void U3CSubmitFPSRoutineU3Ed__6_MoveNext_m227A93E5EC1D455A2717B58AFD91C0592A0BA49C (void);
// 0x00000268 System.Object GameAnalyticsSDK.Events.GA_SpecialEvents_<SubmitFPSRoutine>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSubmitFPSRoutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D31F3D4A34A2E322ACA7A96C2660C90D9127A9B (void);
// 0x00000269 System.Void GameAnalyticsSDK.Events.GA_SpecialEvents_<SubmitFPSRoutine>d__6::System.Collections.IEnumerator.Reset()
extern void U3CSubmitFPSRoutineU3Ed__6_System_Collections_IEnumerator_Reset_m4850E811C8FB070EB3702003C06053805C845994 (void);
// 0x0000026A System.Object GameAnalyticsSDK.Events.GA_SpecialEvents_<SubmitFPSRoutine>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CSubmitFPSRoutineU3Ed__6_System_Collections_IEnumerator_get_Current_m0F55918C9A41E97F9B1216A387579BE5474924D2 (void);
// 0x0000026B System.Void GameAnalyticsSDK.Events.GA_SpecialEvents_<CheckCriticalFPSRoutine>d__7::.ctor(System.Int32)
extern void U3CCheckCriticalFPSRoutineU3Ed__7__ctor_m66D0372FAD02852947FB9DF749EE1E2192671A9E (void);
// 0x0000026C System.Void GameAnalyticsSDK.Events.GA_SpecialEvents_<CheckCriticalFPSRoutine>d__7::System.IDisposable.Dispose()
extern void U3CCheckCriticalFPSRoutineU3Ed__7_System_IDisposable_Dispose_m38ACF4A21A1FD5A57F31BFD33758946B77CCFAD6 (void);
// 0x0000026D System.Boolean GameAnalyticsSDK.Events.GA_SpecialEvents_<CheckCriticalFPSRoutine>d__7::MoveNext()
extern void U3CCheckCriticalFPSRoutineU3Ed__7_MoveNext_m1FFA8E50B4763BA86121C98399CC052C4F74C5A0 (void);
// 0x0000026E System.Object GameAnalyticsSDK.Events.GA_SpecialEvents_<CheckCriticalFPSRoutine>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckCriticalFPSRoutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7701B52E9326DE5D24AD72122548B785F13184E0 (void);
// 0x0000026F System.Void GameAnalyticsSDK.Events.GA_SpecialEvents_<CheckCriticalFPSRoutine>d__7::System.Collections.IEnumerator.Reset()
extern void U3CCheckCriticalFPSRoutineU3Ed__7_System_Collections_IEnumerator_Reset_m2203B84052ACE97F985E4405A365D1803625D5B5 (void);
// 0x00000270 System.Object GameAnalyticsSDK.Events.GA_SpecialEvents_<CheckCriticalFPSRoutine>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CCheckCriticalFPSRoutineU3Ed__7_System_Collections_IEnumerator_get_Current_mED4075309B99F41EA2249CEF351640075A223223 (void);
// 0x00000271 System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager_<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_mFCDB860B35FCAAD8E9866D940D1B1FBF817BE80C (void);
// 0x00000272 System.Void Voodoo.Sauce.Internal.Analytics.AnalyticsManager_<>c__DisplayClass17_0::<Initialize>b__0(Voodoo.Sauce.Internal.Analytics.IAnalyticsProvider)
extern void U3CU3Ec__DisplayClass17_0_U3CInitializeU3Eb__0_mEC0C58D2E555B5F9A7A2BA7BDA7D27F292CDD1A7 (void);
// 0x00000273 System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_GameAnalyticsEvent::Track()
// 0x00000274 System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_GameAnalyticsEvent::.ctor()
extern void GameAnalyticsEvent__ctor_m74986D7CC5BEF02A2DE9B161D30C35F8B6FAA72E (void);
// 0x00000275 System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_ProgressEvent::Track()
extern void ProgressEvent_Track_m756CD0C4E2F7DDA8217449F7B1470385323B9322 (void);
// 0x00000276 System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_ProgressEvent::.ctor()
extern void ProgressEvent__ctor_mF526A5CB445B100C5226C85B54C2269C2AA35F1E (void);
// 0x00000277 System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_DesignEvent::Track()
extern void DesignEvent_Track_mEAC9146E85F410568B20984DC4D56AFF9E940D66 (void);
// 0x00000278 System.Void Voodoo.Sauce.Internal.Analytics.GameAnalyticsWrapper_DesignEvent::.ctor()
extern void DesignEvent__ctor_m3FD5746BB7040BD5E5597A05FAC28EA1A2C1DA3D (void);
static Il2CppMethodPointer s_methodPointers[632] = 
{
	FlyCameraBehaviour_Start_mD5F03875DAC326070CF9F7961AAB5299D31E4E0A,
	FlyCameraBehaviour_Update_mAF7B0CD72BF8ED54A0CB605956CB05E018174434,
	FlyCameraBehaviour__ctor_m4E5C91887D435DD045D9D9ED416325789BE586B0,
	BaseCharacter_BeAttack_mAFE4638099EAB407E585F2E4815EA8065E2EBCC0,
	BaseCharacter_Hit_m54604A801C42CBEAA4E302B222B601D03CF500CC,
	BaseCharacter_Die_m04BB50CED114B10465F1DBD123CABDF1650E84FC,
	BaseCharacter__ctor_m2AC0EED18CBC80E7736FAE3FB1A0C805C3DD27BC,
	ComboEnemyController_Awake_m83309054411D23A79E360D4ADA203DAB499167B1,
	ComboEnemyController_BeAttack_mF06C7CF1F95FD2D432CA29DDDC781A8DDEADB9E1,
	ComboEnemyController__ctor_m365805A374A6039F2BBC6B42A23D77687463243D,
	BossController_Awake_mEA7B3F03478FE1035E5D1CFEA0EE8F09FD736995,
	BossController_Start_mE31D8F5F0F3B8AC1AC51D52700FDDA343595465D,
	BossController_Update_mF0D7102428C7E3CF059FECD6A33B4D69B43DA045,
	BossController_FixedUpdate_m4B86DA9FB846643BFC351DE8D920908505B99538,
	BossController_SpawnMoney_mD00D66AFC5730469D77014B6A9D7711F4B8208D7,
	BossController_OnTriggerEnter_m7A18EBED9F1A80698AA4978DA08493D3134CFBB0,
	BossController__ctor_m972067AE5DABD9A9EF17BF56DC497D5BAB15B772,
	FootBoss_Start_m7C04B963FE801A1C32FD16154866AD7F483E331D,
	FootBoss_Update_m844F55F6C735E8035A673497A1106449414EA9C4,
	FootBoss__ctor_m5A2684CC64C8058D1DAF30F7BF4BF9561E6F66CB,
	ResizeColider_Start_mC85D80CD9D24D882BEAFDB1CC00E599B6A59991D,
	ResizeColider_Update_mF30C2561F2F9E9B35A5361146078093398239850,
	ResizeColider__ctor_m6ABF8D653C6C3F407C7556ADAD7EBB9C41845143,
	DieState_OnStateMachineEnter_m95E274B3BF7012F35BE1A6AE38DE6573085D94E2,
	DieState_OnStateMachineExit_mA522AC28AD5A0829064FA6F60EB4A097A2169756,
	DieState__ctor_m7BCD83358DCB7951D2790FDFE1FCF3B5F3F9C15E,
	EnemyController_Awake_m88AE34A0729F468C299B20EF870C5BDB490144F0,
	EnemyController_Start_m098A57DECB6D20D8A8CF100D55F3814A033BEDD0,
	EnemyController_Update_mB42FF5202BE5953ED1E726A86BF4B61F19F8BB3B,
	EnemyController_OnTriggerEnter_m9AC93CB8FA65CCD0733581A726E3C92D7715B9F8,
	EnemyController_RandomSpawnMoney_m79B33C54446666FD9B8E3ECA50DEFD353E07B7DE,
	EnemyController_BeAttack_m61C66E448E4E6E7C53A6F86B81B5AA8FFCDD1067,
	EnemyController__ctor_m76A2014F0A5962202971DC4401046D10B7A6FF53,
	BillBoard_Awake_mD497D82339F65DD1BB5CA57613181BC9B137FA6E,
	BillBoard_LateUpdate_m944542FEC57F6337840522F2E34AAABDE1F5FF05,
	BillBoard__ctor_m4B75AB6699E2B247FC60C56E09154E2AACDAD6FD,
	DieStatePlayer_OnStateMachineEnter_m9962C36B18ABCB69DA30C480DC2B6F3451C85350,
	DieStatePlayer_OnStateMachineExit_m05036B316B00DE69D657C04DF509EF5CA5D37991,
	DieStatePlayer__ctor_m29FF16A7FB896613F785B07DBC81240439E8933E,
	HearthBar_SetMaxHearth_m93FE7B1CF1391CD5A628DCE8EDFC0332D5296035,
	HearthBar_SetHearth_m4D121923ACF5CEF8AD08F7827211D2D717CC7E51,
	HearthBar__ctor_mDB543348EE500E438723F483D42D5AB7B3D4EC1F,
	PlayerController_Start_mC0C9B9461D0BDAC48EC43715818A4BA63C4F45EF,
	PlayerController_Update_m38903EF1C8F12B9388303741F8040EE26C33DC33,
	PlayerController_FixedUpdate_m914EA3E3CCE4DF6AEB2E78317FFC1D507DACEBDE,
	PlayerController_WinGame_m19B8CF2287B77BCED011B003A937D999F8F0BAD9,
	PlayerController_GameOver_mDC8366D3AA34C7CF8E5FD702A40958732F8E0310,
	PlayerController_EarnMoney_mCF97FB39C370D904F81585D5126CB3B835EC7D21,
	PlayerController_OnTriggerEnter_m2F6A24BACBA236DC01D4689BD68C270C29607E44,
	PlayerController_OnCollisionEnter_mC64545E3CBFFED17020D4B52BB65359240FA581D,
	PlayerController_SelectedGun_mF02792CDB46FD6FA983CFE0C9CCE5EF10924FD15,
	PlayerController_StartGame_m17B78A00C65313CC3417B68DAB9DA210B5BB7158,
	PlayerController__ctor_m648E40092E37395F4307411E855445886113CD60,
	PlayerMoving_Start_mD4760421AF2A736192DBBBE7C9D30A2724ED928E,
	PlayerMoving_Update_m212584CDC636944C30790CDA935C4A9407D4B6D5,
	PlayerMoving_FixedUpdate_m1F513EDC0DEB19C6581AB6F2735A90A7A10911EF,
	PlayerMoving__ctor_m66082909DE78580A5F872605E84D17F07ACA7855,
	BlockSpawnController_GetSpawnPositionByBlock_m4D8811ADA2D5272A7C0E99E101CBBBD281F93C79,
	BlockSpawnController_IsFar_m8EB99CC1D75774EAE6BFFE4B72FCB50469E443D3,
	BlockSpawnController_IsChoose_mB3794E5F6EF8D3FBF2003618424CDC8B66CB0A52,
	BlockSpawnController__ctor_m1FEABEDD39BD490BA3B38EB1DC844DB1FFC7A201,
	GUIManager_Start_mE7B72986FCB050249234403FFC183D8D65426E2C,
	GUIManager_ShowGUI_mC2422C853AEC7AD9FC28FFB719CFFA379AB59DB3,
	GUIManager__ctor_m6EF2692AB15663AE9F7F3C97197A77B763306D57,
	LevelController_Start_m717BD7EE0EDB69590F76AC78193EA9233DFCCFF6,
	LevelController_SetLevel_mF0369952038B38BA24BC07361B8C222D6CCC967A,
	LevelController_SetProgressLevel_m38895D11578A4B7130E83E664203186580EF9017,
	LevelController_SetStep_mD711F9A3CA6460DE7B37EC2F763F824BC48AA19C,
	LevelController__ctor_m2EE9AEC5CE53E6D07C0AE13875CA5B6C1BB91AB7,
	MapController_Awake_m5AD7E95093E9291FCB3D073D6E9F63F7EF83191A,
	MapController_ActiveMapByNumber_m01DEACF1105EDAAC907728F028FF15984346DA8F,
	MapController__ctor_m1D5CBB76A4B511669448FBE0AEA15B4120FEC11D,
	ScoreController_Start_mF4A294D5E420788B89914F345C07BC2EE8B2D01F,
	ScoreController_SetMoney_m2822A9FD42CFB3D1E9BFE71F5734B6757EFA824D,
	ScoreController_CollectMoney_m1AC51A34557358C15558EC1FAAEC591E8B72731C,
	ScoreController_GotoIcon_mC90FC47ACE641CABF896ECA5D202C268FDACA6BF,
	ScoreController__ctor_mF696E9AEDE1200519DF2EEF752C5C1E0810EF649,
	SettingController_Start_mFF2A2A98C7658D187B675AF0AC32E4BA37394F6B,
	SettingController_ChangeValueToggle_mD3AEC69AB18D86A6C8E7EC1E0CBA461DF6461C15,
	SettingController_BackToScreen_m38DC988A7C92DDA4BFB2EEF9B2CA25D2E4F438F6,
	SettingController__ctor_mA8AA8CB0AF797CBECDB0FFB2AFE2BF6A1318D3E8,
	SettingController_U3CStartU3Eb__2_0_m7C984BBC2A77BB52422AE0D09F8232E887AD75B7,
	ShopController_Awake_m0BABF499639BABDBE3A020F6C633ADB32732F0FE,
	ShopController_Start_m5145579322746B255CD3FA74A7AC147193CBD177,
	ShopController_Back_m43C87163BA25CE96850883E3736DE260C75CF60E,
	ShopController_SelectItem_mB28DC20388B91381CCAE3F249973A8C51BB9CAA4,
	ShopController_BuyItem_mE0591DBE9C24538E08A9B720CF8E526123670B21,
	ShopController_RefreshShop_m530DDD0A7FE4BCDA539AA9A3423F49023C71FA65,
	ShopController_GetShopItemByName_mEDECE62D4C6F365CF9F6CBE96611E66B061B953F,
	ShopController__ctor_mFA8709B7E257556CF9A2D80B2048DC59331BE291,
	SoundController_Awake_m7D4556CC74ED46B01483640F957997A2F5BC903D,
	SoundController_Play_m87ACE746BE8079D0554CBFE330575CE5221ADE0F,
	SoundController__ctor_m6D8FCBBD8F2A1B7AC1D0D87211405ED5CEE72EDB,
	CameraFollow_Awake_m01D7A81EF199D2F15E835CC795A9DA324EE127A2,
	CameraFollow_FixedUpdate_mBF9AC25722C9FF8A0251A308BEC67DC528150756,
	CameraFollow__ctor_m27AF37B0C19243374F9376EBB2C40A2F605DB16E,
	CameraShake_Update_m39B56B99762CE6FA09C4205F478154F15C52A38A,
	CameraShake_InduceStress_m7FEF6744880B524AEDF4B49B393C28599BD2F233,
	CameraShake__ctor_m8960CA715C5646BD0F1325FAAA552854EC3A9F84,
	ControlShopButton_Update_mD1DEDF92ACE1CEAF73C217866C3A1A5510E441B7,
	ControlShopButton__ctor_m2327166314835D13F833D45FFCEDE8AA2E5B16AA,
	FinishLinePosition_Start_mFF95489D0ABDF0FF0BEADF637276A8B6706E17AC,
	FinishLinePosition_ShowFinishLine_m15DD6F03E791A3D16BC421A82ED45B31300691E7,
	FinishLinePosition_PingpongFinishLine_m8FE4DEB636ACC62918E8ADE93A30C54AA6C4D754,
	FinishLinePosition__ctor_m25792EA6D6BB9595C0150139088BEC1BC816ECC3,
	Firework_OnTriggerEnter_mDEACCA5B9A344986334E4709C6E3BF4005B06569,
	Firework__ctor_m1674CEEEFEAA38CD6685C90CCF4A34F6D8832774,
	GunPreviewBehaviour_Update_m09F6FE8F7C2D5C52B9DBB84DE650EACC423901DB,
	GunPreviewBehaviour__ctor_mB4BB9151146B8E8629B4A5140620453AAC281D82,
	HPEffectState_OnStateExit_m5AFF644C0ABEB21D495910A0228688A20541176F,
	HPEffectState__ctor_m81432E17061233B2A1E4DEFDE5FBA4D10C846989,
	Losepopup_Awake_mDCF2BF073DF5F40068664876BB0660FB1C17F250,
	Losepopup_Visible_m17BF95189BE6D0768E94B492F0A2AAA85A8ABDAC,
	Losepopup_Retry_mF76B438A63DADAC68D43ADC3FADA22BC5CC20C67,
	Losepopup__ctor_m25B39748C9FD45EFCE2DA23DA205327EDE15F485,
	RedDotBuyGun_Awake_mB9913ABE7F95976B30376452E85F6536AA688C0E,
	RedDotBuyGun_SetVisibleDot_m62A22690099C5813D6A1FE057B10D545A1481267,
	RedDotBuyGun__ctor_mD0CA736AFBE52C3CD74156EA3DE3D885B32093BC,
	ResourceManager_Start_mE850CB356A63B42B45634184F0AA97024B1A0551,
	ResourceManager_Update_mB5D3DD122FE4428573E273F5A534D4A3F99D43E9,
	ResourceManager_DetectInput_mA7FA4B61B501FEBCF43546D5A6E4F000D509CA72,
	ResourceManager_MouseDetected_m1B4EFB095B30136627BAF0EA82EDF7057A4F44A0,
	ResourceManager_TouchDetected_m5A37AA077F1F33F019F987DDE6929E26FA1CBF34,
	ResourceManager_RayCast3D_m071CFBF59DE0FBD3ECD05625A618D841F75B3A7A,
	ResourceManager_CollectResource_m83A4572E62DD6845FE8EA22DC8E5E4F38381D412,
	ResourceManager__ctor_m30280EBBE503FC7B00E72E0D17FE5C772B4383FB,
	Vibrator_Vibrate_m0DBCEF0AFBEB77D8FFB27E78064446BFAD553466,
	Vibrator_Cancel_m6D5C50E05A6E29CA47F9C45409842F10D9F9D9DD,
	Vibrator_IsAndroid_m970EB8F3BFB2AA98A3A9312FB7D0972A4F415D07,
	Vibrator_StartCoroutine_m020CEDB1B1F80648B7EA9B63509EB52250351DB6,
	Winpopup_Awake_mB4049D084B9D7FD1A69267D9FE3051DDBC5FF066,
	Winpopup_Visible_m1A79AFA1415DE6AFA54D3736B3E030C85C67588D,
	Winpopup_NextLevel_m32266E8FE87DC639A4A06D85480C43F2806B7FC7,
	Winpopup__ctor_m9820769F6988324A8E39A6349C08978C8C06FF62,
	GameManager_Awake_mE60F41F3186E80B2BAB293918745366D18508C0F,
	GameManager_Start_mD77CCDBF1DA8EC5C3AE7ED955DE4E7F54B79C88E,
	GameManager_OnStartGame_mC704DB9B0B4D020A5EB1CA056CF7C451B65E3B22,
	GameManager_OpenSettingModal_mAB539847E383C79305AD6BA349B32A3FCCF5E104,
	GameManager_OpenShopPanel_m8ADDDD7BB6ECF75F1B68CF6004E964EA49AC1FAC,
	GameManager_CloseShop_mB6B07F87CDB49666DDD64BD71D6B86612D7F9379,
	GameManager_PlayerDie_mA7289673C8FEE00849AE786E0B9D203485ACDDAB,
	GameManager_FinishLevel_mA0148AD6040ADCA0B9E4E32E12C013790CAE9129,
	GameManager_FinishEarlyLevel_mE2AFC6C92026DD5AAAD3C3A0DCBE5CB8ED5B84E3,
	GameManager_NextLevel_mF6C956ECA09992B9D36B913E2273B39C076B71F2,
	GameManager_EnemyDie_mB7532C7B1C48FCA8DF49A73BE5E801C9CC823B01,
	GameManager_BossDie_m0DAD0350B8FC6F2C10CAE0DE17BEE05AE67DB96E,
	GameManager_EarnMoney_mFE2FDFEA7988052729AFA7D64532CAF6E85FE5E5,
	GameManager_EarnMoney_mC0F5E515019966641AB8113DB179BBEC03BEE5B2,
	GameManager_FightBoss_m4B4496B11CEA2E7EEA425EDE473488B548EB1E2C,
	GameManager_EnemyBeAttach_m29814C9FBE71A9421DD23C7E0A49F64E2F93E699,
	GameManager_IncrementEnemy_m33474A553E437E9F90E22A0FF1F7FF1DE3199DD7,
	GameManager_PingWarning_m1F780B5BB20AEB348A97641601BE7560D7A725F1,
	GameManager__ctor_mF7F1107D38DE91EB8A57C1C3BB1A932C50CD9693,
	BaseGunController_Start_m26D375003C4AC0D8FD72A1A265EE7F14D6C0AA06,
	BaseGunController_Update_mA1828106F06A043F2AAB7C359CE2572E226C9307,
	NULL,
	BaseGunController__ctor_mBA3CAC22BA4040C824F973DD318A60E51BF3AA1D,
	BossGunController_Start_mAEE91505DB06B20552B3A596A006E89F3DA578AA,
	BossGunController_SpawnVFX_mFA7B327A0C9701E7C3E4F057B64C68668496CD61,
	BossGunController_BossFight_mF62AE5D455CC4F9A99DE56571ADAF01D0C07AD5C,
	BossGunController_Shoot_m78AD993F76A7E6A96EE3315F0A347F28F8B55070,
	BossGunController__ctor_mBDAA83E836C55E2885FC9F204087E86412ED82FC,
	BaseBullet_Start_mDCCF822701BABE995CAAC44183BA8CFAAA251764,
	BaseBullet_Update_mCBE9F62B8DDB81CFE10338FCC0A12268580D0D0B,
	BaseBullet_SetupBullet_m057BE05673905E808EFCDE956231A755D50AE469,
	BaseBullet_SetupBullet_mD41D24128A9D5DAEF3D77BF464C28032EB9C0B11,
	BaseBullet_OnTriggerEnter_m92B4B94859144799A7C0746517FC48C25F8D338E,
	BaseBullet__ctor_mAF6F15CFCB1ECF48D950BD4FAD77E5D1BAA89E5E,
	Bullet_Start_m27CD1C7B5B3C9EF7862D40F5F167E9B3B11EA81B,
	Bullet__ctor_mFBA1E7297C133AE06ADA2EF2BA04567AD213A9D4,
	BulletAutoDestroy_Start_mCF8E562165E4638DD8A8593735BB85CAF5257B65,
	BulletAutoDestroy__ctor_mEB77369F9E2C134B9AF8B9404543810B9B721BC8,
	BulletBoss_Start_m5222DCFBD2C9B4C1DDE13043F02DC164FBE0F678,
	BulletBoss__ctor_mC08116DD356C329FFABB1D87FE79246DBD863B97,
	EnemyGunController_SpawnVFX_m2B4B7A6D276A2BF5EBC6745DD106AEC00F16311C,
	EnemyGunController__ctor_m82B8AAF36D09E1C8E7006466065A2641380BD2BC,
	PlayerGunController_SpawnVFX_m475250EDD17CF1B14DA66F46BCBFFA2C9651916B,
	PlayerGunController_SetGunValue_mA68D8C687D455AA454B9E9E110061FABACBE2EF4,
	PlayerGunController__ctor_m34D8F3C4BC3B433E2357B5A05FB27DC62560C8B9,
	ShopGunItem_Awake_m680779B042C2E90A2A582BB68089D722A801CF3C,
	ShopGunItem_Start_m315EADBCE010ED49783340B49E35519073D23FDC,
	ShopGunItem_DetectItem_m5A14E56B58B33BC1FF8524273E2DF3AC23D715CB,
	ShopGunItem_SetStateItem_m0C253E4348E641B2F496B23B9E15F13E8BB3F4AB,
	ShopGunItem__ctor_mDBBAA2333BD9CC2C8B51BE3646C571512D942240,
	ShopGunItem_U3CStartU3Eb__5_0_m1E5DF1C5662C5719E965245857363E03E6253ACF,
	ShopGunItem_U3CStartU3Eb__5_1_mB62A0067CDCCF32E3505F8D44187447D2B3B37B7,
	Barel_Update_m49ECB146394C2B0309FEF964EB517BC867F40B6E,
	Barel_OnTriggerEnter_m1B487FF2F4C0B18F6B1E510D85F50C0EC5341681,
	Barel__ctor_m18516609D99D382904DA9699581C7869E271583D,
	BaseObserver_OnTriggerEnter_m49AE93BA8C4B7CB64A1FBE5C85B90EC54EA8E5AB,
	BaseObserver__ctor_m0D300FC92BD5676D2E3950534BD377ABB098CD8B,
	Crate_Update_mC589F67907AE3D98186803873973832249D8336D,
	Crate_OnTriggerEnter_m6A24A8E8D867235FFF63ACC88C4E0A199D3BBE44,
	Crate__ctor_mE06B10DE1316EEEFA7451AAFA0DB28B4945A0BD2,
	Money_Update_mBB594F917756D9128F9E24A14FF31AC213A7FDDF,
	Money_OnTriggerEnter_m890A35FDD3F1BFBA3B99B67091947C086FD220C0,
	Money_OnTriggerStay_m52B65EE001C63E2808665A327E08B64BF9C34116,
	Money_SetValueBill_m4B70706187C46FE6689B7E3A2DCB028DBEBE398C,
	Money__ctor_mD69086E9FA0B1F09BC8D102B7B02A04B51ECD716,
	NULL,
	NULL,
	NULL,
	GAMopubIntegration_ListenForImpressions_mD66781CBAAD09D59461D0C987F69981D88D61303,
	GAMopubIntegration__ctor_m298C59618B1C557FB5F52E0E3D950C23BEE77F46,
	TinySauce_OnGameStarted_mBDBEAC3AA051B96C51605149C820E7B5D6080655,
	TinySauce_OnGameFinished_m9FB59E27A476401E9E415EB1E2C80F578CD09C46,
	TinySauce_OnGameFinished_m71622801396A3CD8588DB046C7C2288E4025F821,
	TinySauce_OnGameFinished_mBFCBC99B6A8521D0EC482708746605FF3DD31E5B,
	TinySauce_TrackCustomEvent_mBD460F784CAF35BB642211C4ADF2A41AF6214487,
	TinySauce_TrackCustomEvent_mB24087301BA08BD977A957A729F12C9AE4C22691,
	GameAnalytics_get_SettingsGA_mCAFF4A8EC0F3E46C746EC483505838E656569FA8,
	GameAnalytics_set_SettingsGA_mA7BBCB88ED0C5491D00AF3B065009D3DCA474F25,
	GameAnalytics_OnEnable_m071A686DFE8C22F0696F8776865A3E74B02802EB,
	GameAnalytics_OnDisable_m9FFCE859B095AC7F55F2B9A0187F8DBD825FB86B,
	GameAnalytics_Awake_mB074269E53A413A84CECF5A8C74EE40B899266E9,
	GameAnalytics_OnDestroy_m5D1722C5EAC1F42ABBC1DA7489D7D30021221749,
	GameAnalytics_OnApplicationQuit_m094E0FFD31EC7EEBE13E21F28EB77752DD9E6618,
	GameAnalytics_InitAPI_mB619C7BB48F828E656304606D6D83ECFA6C13386,
	GameAnalytics_InternalInitialize_m04C1EA37B3A4FC8F27DFE83F12541F4CFFB42E57,
	GameAnalytics_Initialize_mD34FBE312428CEDD5DAC3FD0ED1234D9A4339C52,
	GameAnalytics_NewBusinessEvent_m48DD2E73C17BB4D8E22391616F63A424D3886DE2,
	GameAnalytics_NewBusinessEventIOS_mE8D2C53603C5C6A048FF963F4B6F43D0DD4BC69F,
	GameAnalytics_NewBusinessEventIOSAutoFetchReceipt_m833D7727A25A14E3821D959D969EEDDF0BFF458E,
	GameAnalytics_NewDesignEvent_mA0F014656A8DE873F7282DF2E2DA07F6FAD601B8,
	GameAnalytics_NewDesignEvent_mEF36424787F728DF2FE0209B114E222974A27039,
	GameAnalytics_NewProgressionEvent_m6946828FEB1C33A578CDCB9F731AAB49121A353F,
	GameAnalytics_NewProgressionEvent_m2F24D38D91C73AB66AAFCA865F9CB157EF7EE45D,
	GameAnalytics_NewProgressionEvent_mC89146B9B7344E90BE2F49A691521BD486C2D4F6,
	GameAnalytics_NewProgressionEvent_mA689664EDAEB841FE2CC133595029D2316B74339,
	GameAnalytics_NewProgressionEvent_m1BFADBDA2DAA8DC9041808C3757E597F0B4BEE13,
	GameAnalytics_NewProgressionEvent_mD697E2064CBBF4306889162F7094E7E7784D339B,
	GameAnalytics_NewResourceEvent_mFE8F1D00B3C14F78D4B76CFA40474FD026C549A8,
	GameAnalytics_NewErrorEvent_mFE9F68F20AE5A5C82D1768F1671E443949489364,
	GameAnalytics_NewAdEvent_mBE879EA4FA9D35E6B1B526DE94CFCB13DF012F2D,
	GameAnalytics_NewAdEvent_m08BEC9267AA5E80B28917C49D82C56BDB8D6C123,
	GameAnalytics_NewAdEvent_mFD9D91653EFABC4A96873846D088F36DAC89A2DD,
	GameAnalytics_SetCustomId_m4DB8288F2E96CC9EFC63CCA651E42C0B97069D91,
	GameAnalytics_SetEnabledManualSessionHandling_mBBEBE37CEABE38F38FBD95DE37F97247D8EA4CE6,
	GameAnalytics_SetEnabledEventSubmission_m2889A9B72FF0614D67EAAE23AB4D4E01E151741A,
	GameAnalytics_StartSession_m6914518259F6DDC722BE25FE34D14F24CFABEDE0,
	GameAnalytics_EndSession_m48B3DFBBC15C0C2741D7892A85FE76D4906E164E,
	GameAnalytics_SetCustomDimension01_m1CEC6213E2AD7AC33ED04068A45C3B647B963530,
	GameAnalytics_SetCustomDimension02_mF47995B2C7590BAB559FD80B742FB05E55355E44,
	GameAnalytics_SetCustomDimension03_m95974AC0DCF3A52472B20C4ED0F50BF1C2B075FE,
	GameAnalytics_add_OnRemoteConfigsUpdatedEvent_m512F31B3B78EBF245354E3F1AC53DC7DB366EF5B,
	GameAnalytics_remove_OnRemoteConfigsUpdatedEvent_m1933A49ED802C97FA79102535BD8EAE460A57F47,
	GameAnalytics_OnRemoteConfigsUpdated_mA8F148C8514C84613B18863D8BF533A4D5F0D549,
	GameAnalytics_RemoteConfigsUpdated_mC7C245A598C040B36A6BF625883641B68E6110A3,
	GameAnalytics_GetRemoteConfigsValueAsString_mD96ACDD18E0561B363F6D6C8F18814515AF1CC31,
	GameAnalytics_GetRemoteConfigsValueAsString_m4E0E4C9653D9768B74109F412C6A2062092EAC90,
	GameAnalytics_IsRemoteConfigsReady_m064AC9F7A0E7FFFFD05B06344EEFB2DE3F94187C,
	GameAnalytics_GetRemoteConfigsContentAsString_mBC50EB193D05F0A5E901DF14BC5B3EE26B6EA786,
	GameAnalytics_GetABTestingId_mFC77C1556D51A140E65D0AAD9A6132E46561C4CB,
	GameAnalytics_GetABTestingVariantId_mB0D7D90532C98949F61FC2301FDF249E9B16BD0D,
	GameAnalytics_SubscribeMoPubImpressions_m5A77A2558D37ACE8E04F2CE9E94F4DC61E565821,
	GameAnalytics_StartTimer_m30CC464D3FAE9848B4000CFCB7D9F05852BD83D9,
	GameAnalytics_PauseTimer_m311D0A17F1FA844FE2F3A02FE6618FCAF6E303D0,
	GameAnalytics_ResumeTimer_m09F560FFB1D2FA550D7D3D0E8D389E0A7CFEC8F3,
	GameAnalytics_StopTimer_m6723A5845A33E30DED376BD9801D1B846B5C48BE,
	GameAnalytics_GetUnityVersion_m35C794B4A79C906922B19F07BAF875893DC2B4A8,
	GameAnalytics_GetPlatformIndex_mD6A1E752CD0045B564B9C8B46FC074B35880F630,
	GameAnalytics_SetBuildAllPlatforms_m079A42CA8E6208D1A243FDE6BF949FF08342F87C,
	GameAnalytics__ctor_m0E78CEF8FDA5540BE58221BA5B2619ED182DD5AC,
	GA_Wrapper_SetAvailableCustomDimensions01_m49226B72676D4A316ACCB6B49AC01E67B7D18E35,
	GA_Wrapper_SetAvailableCustomDimensions02_mA84F6096DFD803E7642FD4F252D2009D3F3E31F9,
	GA_Wrapper_SetAvailableCustomDimensions03_m28C56480CDD3890A28060F5474E9E784B14DE4F8,
	GA_Wrapper_SetAvailableResourceCurrencies_m1A75453DCE5B4DDADF6CCEB7C9D663B5041CD202,
	GA_Wrapper_SetAvailableResourceItemTypes_mE57F26C669230ABFB3543D65B63ECF14BA299BFB,
	GA_Wrapper_SetUnitySdkVersion_mF73410E2221DED07BA469FCCAD1156320EFBE2F2,
	GA_Wrapper_SetUnityEngineVersion_mA39EDABE836FBD738EF9AF854303F812B6EB7C41,
	GA_Wrapper_SetBuild_mDE4539D365AD9CAC3FA58E929213FBE9A24A54C2,
	GA_Wrapper_SetCustomUserId_m9093B4AECF1BBA391F4FD37C19660178FB457D08,
	GA_Wrapper_SetEnabledManualSessionHandling_mF8981E6D63AB1B92BE5EFA19F8C5FD12942B7D23,
	GA_Wrapper_SetEnabledEventSubmission_m3864DA950A56CA4CFAC4E389703E7F36AC04C26C,
	GA_Wrapper_SetAutoDetectAppVersion_m4C267049A1B829DE80AC1EB483C2F107785852C9,
	GA_Wrapper_StartSession_m96E51C8838738FE69E6EF2455793067F7568F0DE,
	GA_Wrapper_EndSession_mBFBC5629B6C9A8B23863243F0D3943A5DC2622C8,
	GA_Wrapper_Initialize_mF32F62DA19DCDD83E87EAF4056119193A5E3224F,
	GA_Wrapper_SetCustomDimension01_mCFFCB1F233EBADAAF8A64BE093EA7D02840F2D2A,
	GA_Wrapper_SetCustomDimension02_m1945A1A027B97EDA5B2FA7A7295E6180C95466A9,
	GA_Wrapper_SetCustomDimension03_mC3081F7772D8E40AAFE570D9DBF25816690DF186,
	GA_Wrapper_AddBusinessEvent_mF63AC5327C541D6E0E94B6B969EC926B30C094CD,
	GA_Wrapper_AddBusinessEventAndAutoFetchReceipt_m4C1CE54B14044A4FEED94368B15666C567B17D05,
	GA_Wrapper_AddResourceEvent_mDC824665294BA78AE458873F50819E22772194A0,
	GA_Wrapper_AddProgressionEvent_mEEEFE6F3DEAAECA7149DCA928AB6EF7772EDF25F,
	GA_Wrapper_AddProgressionEventWithScore_m79C70A858DA23457F92201E3101A68755B5EBB06,
	GA_Wrapper_AddDesignEvent_m0B4F894A199FF19DB4AA340B49724D50FBADC815,
	GA_Wrapper_AddDesignEvent_m2A6381DB281AFFACF8FE17BB79D57CB5060FAA1F,
	GA_Wrapper_AddErrorEvent_mB8CE6DFFF292AE95E7C4F92EDC4B33F54D1E4833,
	GA_Wrapper_AddAdEventWithDuration_m2FCCC0B17CFA287EEF422DBA69376716BAE45170,
	GA_Wrapper_AddAdEventWithReason_m0306BC0F90965B39557DCF95C435C71E552B3D71,
	GA_Wrapper_AddAdEvent_m818905A1569B1D466EA9CF9C80B1DE0A70F7FB2A,
	GA_Wrapper_SetInfoLog_m6F095E7ABF2C0BC938A06D5D5443AF76BEE7570A,
	GA_Wrapper_SetVerboseLog_mD191337555FF27FDC53813EF4AD72146EFBB2792,
	GA_Wrapper_GetRemoteConfigsValueAsString_m5F5D6BC1B3D423AEF86E656AEF4E06EBD1979866,
	GA_Wrapper_IsRemoteConfigsReady_m7767F545B020516ABCB62B206E81F04664B5C175,
	GA_Wrapper_GetRemoteConfigsContentAsString_m63745240E6A730FE913EA6D4FAADFCEA50E84DA8,
	GA_Wrapper_GetABTestingId_mD97A2483C9062AA2C965FDD603B9B4ECBA998718,
	GA_Wrapper_GetABTestingVariantId_m1BFB815F0E4AE0464A446B50E593E55BBEABD20A,
	GA_Wrapper_DictionaryToJsonString_mFE250BCB996E6F72B76E0DA60DEB376709CA578A,
	GA_Wrapper_SubscribeMoPubImpressions_mCF0DA2849B3FD21CAB7D681C80A36ABEA97B8A16,
	GA_Wrapper_StartTimer_m4BE2B8B3A232E05F0BE26785F2F7280BFBE8527E,
	GA_Wrapper_PauseTimer_mEBFFE4FBF92625156552278F545FF4E4F7BD2275,
	GA_Wrapper_ResumeTimer_m7F1E247E742FF5F478C495A364C055955E85D4C3,
	GA_Wrapper_StopTimer_m47AB83EFDFD5526E9B96EE0B505C5A048E4C3FE5,
	GA_Wrapper_configureAvailableCustomDimensions01_m2201795FEA65EB2F040D06084B708D1DC46CB1A5,
	GA_Wrapper_configureAvailableCustomDimensions02_m283315D2811A7FB46F7DF0B050A154DEBC5335CB,
	GA_Wrapper_configureAvailableCustomDimensions03_mBFE26A31BBAA35345BF15E7EB161DE48C4FDB62D,
	GA_Wrapper_configureAvailableResourceCurrencies_m88A05D54BD004F8C3B55592F67589DA8167D1E00,
	GA_Wrapper_configureAvailableResourceItemTypes_mA13D8043D108C76BE2152C265F3E8C07D3330878,
	GA_Wrapper_configureSdkGameEngineVersion_m35AC3C364EC431788377710DC202501F0D5D164D,
	GA_Wrapper_configureGameEngineVersion_m1CCF8F625BC52F6239EBD959F81134DC4DAF7A28,
	GA_Wrapper_configureBuild_m6DB9826BF4C87B4DDBE18BB73A7FEA16B1AE46FA,
	GA_Wrapper_configureUserId_m0608B6B8E813B3D43A3E034E0AB06726F8A3F849,
	GA_Wrapper_configureAutoDetectAppVersion_m3A32F68A36BDED5384B424226C794E846608B339,
	GA_Wrapper_initialize_mE7EA700D00EA07D5CBD98CBAAADC400BD9E9418D,
	GA_Wrapper_setCustomDimension01_mB671735540B49EED976610899B5357A5FBFD4305,
	GA_Wrapper_setCustomDimension02_mCCFD40EFF7DC146EFDE106C9F7B179B6FF534251,
	GA_Wrapper_setCustomDimension03_m551223E5880600B2213D67EAB42A9CAFC8442818,
	GA_Wrapper_addBusinessEvent_m4E34AE6D51EF26F6E04140834418874998B81E67,
	GA_Wrapper_addBusinessEventAndAutoFetchReceipt_m5D8C0B19387CAD3A087E5992327AE337EB55DC2E,
	GA_Wrapper_addResourceEvent_m741BA78E5201D49AE9A9C237863B337A6293A0EB,
	GA_Wrapper_addProgressionEvent_m4C5C71B6CA8BAF636FC42ECD6C73816C82D33D1D,
	GA_Wrapper_addProgressionEventWithScore_mA8672F6A5FCA155242F35326E73662A6473610C7,
	GA_Wrapper_addDesignEvent_m0F61AD1A2473129C965DD538A74B317C1826ECF3,
	GA_Wrapper_addDesignEventWithValue_m0F114C2A873C9D22507357CD1F7AF02C9487F396,
	GA_Wrapper_addErrorEvent_m3863F9CAE4DDCC5DDA30EDD247BAC56457881359,
	GA_Wrapper_addAdEventWithDuration_m5C51B7153B83D5F0E63076058ABBD6A831A5D060,
	GA_Wrapper_addAdEventWithReason_m4693C2D4F5022E1791E2ABB59FBEB27546553B31,
	GA_Wrapper_addAdEvent_mDCFA2FE2C6FADE21D75D5C74882B980C674BC74C,
	GA_Wrapper_addImpressionEvent_m8D107EFAD33B608EC7EB9B6B1F81485AD3EC5D98,
	GA_Wrapper_setEnabledInfoLog_mBBEFB5A46E527721907563B0F81C858F13A4E29A,
	GA_Wrapper_setEnabledVerboseLog_m375C9E881C2EADB10A8EF8FC314A3D9B06898B0C,
	GA_Wrapper_setManualSessionHandling_mD95B2B97D7B250DAA1DC8C52413F8314125BDF14,
	GA_Wrapper_setEventSubmission_m2383E511BB0B5006B87B4EE5C163553746302732,
	GA_Wrapper_gameAnalyticsStartSession_mF3917115845892455B7D1BEC7D2E796A1191AC4A,
	GA_Wrapper_gameAnalyticsEndSession_mF8FE2CA5F1ADA7DCE87A8A1EECDC1D0E7D534CBB,
	GA_Wrapper_getRemoteConfigsValueAsString_m43C2DDE3F337C2323FB4DDDC973FBD3624190D14,
	GA_Wrapper_isRemoteConfigsReady_mBA324FB96FAE61B5B2BB4535F7434B0473D2B015,
	GA_Wrapper_getRemoteConfigsContentAsString_m7A3F9B990174D6EC6624AE07BF98F752005387AC,
	GA_Wrapper_getABTestingId_mC1D8073809041C7CA2ABD012600475100F6DD03D,
	GA_Wrapper_getABTestingVariantId_mBA50F987FFCD55E533252CE4FF8A97A7330805B0,
	GA_Wrapper_startTimer_m04AF910C4442C18721B7E683DE6E03FAD7B1941A,
	GA_Wrapper_pauseTimer_m242C0C032CD8BF3D2DD0DA2A690EBE4B2AA960D8,
	GA_Wrapper_resumeTimer_mE173112639D1CAFFF96E03B63B196E7B11C8E7FC,
	GA_Wrapper_stopTimer_m7E0218272ADF8CBC815FF12C8ACDFD8899A35979,
	GA_Wrapper_subscribeMoPubImpressions_mE83B0EC38551E511B7B4289D7AA42C24EF61582F,
	GA_Wrapper_ImpressionHandler_m13E386DFB9F943B8E4ADF84CF022161A09735EBF,
	GA_Wrapper__ctor_m19545E7D36FD7EF37D3FAB7B96A00106BA110AF9,
	GA_MiniJSON_Deserialize_m94CD95ACCB8707F15E65B527040F38105ADED1B7,
	GA_MiniJSON_Serialize_m3AEA946EB4FD81E71C4989DAB12E20777D4C44B4,
	GA_MiniJSON__ctor_m5C3A474EA564FDA0371996ED03066A5B0E59AD2E,
	GAState_Init_mAF4D2301C4FD7FFD247BE60560FE7F369AD5A135,
	GAState_ListContainsString_mADB9A0E0980737021B99E6F9DEDCED2D787482FA,
	GAState_IsManualSessionHandlingEnabled_mA97CF570ABE9A569D185A22F868C77CEC3011074,
	GAState_HasAvailableResourceCurrency_m7B18CAA90BF7B42142729C70BF66FE08776AEF97,
	GAState_HasAvailableResourceItemType_m7DBC3CAC6E13671D1F8A78E0E5D071CAAD727DEC,
	GAState_HasAvailableCustomDimensions01_m0EB9E901216824580B0BCA8152FD468066D46AA7,
	GAState_HasAvailableCustomDimensions02_m8E0BA177C69FB399A03ECDF5ADFBDEF1E4E83590,
	GAState_HasAvailableCustomDimensions03_mCAB90F3AA79C5FA306A5D55D727C021DE8755DAA,
	Settings_SetCustomUserID_m1DAB095E8930ECB2921C5126B64010AA02647000,
	Settings_RemovePlatformAtIndex_mFCF5D691A6CF21DDEAA77613C205277704F3521B,
	Settings_AddPlatform_m634DA5908D5DE08BB3D292161268153A219B024A,
	Settings_GetAvailablePlatforms_m45D43B36F53F04AF60ACAF8CE467BBE9EAED4AF6,
	Settings_IsGameKeyValid_m0F6348C1B68D0E5B29E986FDDC4A9A59778DAB09,
	Settings_IsSecretKeyValid_m5E9A73E8CD944D769EB0070CFF9C2029C55E2759,
	Settings_UpdateGameKey_m412823F3A86356F48F186A3ECFE3BC108D6BC2F0,
	Settings_UpdateSecretKey_m43D6A554BA7BB3899A859A1332745D7C679FF8EA,
	Settings_GetGameKey_m1B6690326EE7FF236F71F82E87A1DD88CC1C1100,
	Settings_GetSecretKey_m6F6E1881171A2E1F0E5472AF1AF23700FC7F10C0,
	Settings_SetCustomArea_mD66A8D31E012B2944973EB54FC544168BEDD4B08,
	Settings_SetKeys_m413E0ADFCDE90525929A8700E4B352748858CDD0,
	Settings__ctor_mD348605211EA0147657EFDA462B03E21230FC7BE,
	Settings__cctor_mE297E9AA997520D04F63BC250A7B7B957B2DD098,
	Organization_get_Name_m279E143F49217DC785781A9337C5ECF1DDC3ABE6,
	Organization_set_Name_m83DF5003B40FBA8DDC4A240B2E3693B05C1EBC10,
	Organization_get_ID_mD7C05E4703E9748261E8C3F0926405102E6CCADE,
	Organization_set_ID_mC3A097F532C20EF8F3E736716C2172CED9F6558E,
	Organization_get_Studios_m6DB282D03656B16BF4CBF3C563BEB6D3977E9B84,
	Organization_set_Studios_m3F27B87BB3970C7911F58FC5E2C4684BCE44B30B,
	Organization__ctor_m3589A2B5F25476CD5834F8BCC7682898D34A92CB,
	Organization_GetOrganizationNames_m3F721B0E0AB3FF174F57D9708C34DD203D509B4F,
	Studio_get_Name_m9C38B3CD0BB49C3574F8D330A0AE75B097A10D56,
	Studio_set_Name_mC2B8B607628B1890C8C53865A710D0AC439F3C84,
	Studio_get_ID_mED3314E2F75CE2634364713724D1BE310C78B448,
	Studio_set_ID_m60CB3D539BBBC2F28A2081BF3F0A98B49EAC1586,
	Studio_get_OrganizationID_mD780092B6DBD6CBD110A053A861EA5619D734235,
	Studio_set_OrganizationID_m1E2C735C9B8D045C7ECCA42780FC3560F64E0D8D,
	Studio_get_Games_mC820F41524477421D0079FBD83A61F7961133AE7,
	Studio_set_Games_m12820045A18B4AC14E5D3B796A57DE26FDB6A390,
	Studio__ctor_mEED52D47755729201C2D6CB13A99E795CCDE6E3D,
	Studio_GetStudioNames_mB39A53EAFE0D941CC58CF87A4D48A7B2F4EB48C9,
	Studio_GetGameNames_m01EB13121F696288EFA878CD000CCCE7A1E61CD8,
	Game_get_Name_m03D31A24CE65EA926646E4433920863EB069CA51,
	Game_set_Name_m6493C376BC904499578834BAC51D422CC54D0F69,
	Game_get_ID_mC82557B878105F51F707E99FF1A03DF8C3A0FF17,
	Game_set_ID_mD0FC95C16C349C673AC1DBAD7AB6FCFCF37D8DD1,
	Game_get_GameKey_m3E6F700AB988ED71EFA49FFAA7B2220713814A9C,
	Game_set_GameKey_mF0C84B1943CFA6FF686554E5A0E5B01AEFBED3DB,
	Game_get_SecretKey_m6F2AECEC7AD8C39404A4D46CC1272E5557CCD488,
	Game_set_SecretKey_mF39915667EC0C739C743ABADF5A8451DB490E531,
	Game__ctor_m802F2A46F86F61DBEB64CFC6824146F6C4C84A03,
	GA_Ads_NewEvent_mE9E5A9CD4FDDC15FA2846855B35B3C9521A3AC54,
	GA_Ads_NewEvent_m9CFC13BC346529012BAB2CC23ADF022D77D1AEB6,
	GA_Ads_NewEvent_m5F9DD89A371D9A3008513EEE3477A7E255583226,
	GA_Business_NewEvent_m8DD67C87CF1AD6BCE4D2A2AA987E34FBF16E969A,
	GA_Business_NewEvent_m4A9E8EDEEAC83C6A6A263520805268871370C28A,
	GA_Debug_HandleLog_mE694DF9C6C50D86BD3BD3EF9ADA1E8D49BB6E6EA,
	GA_Debug_SubmitError_mA4013C0234BA3B134764F8B172D190BAE1C27C12,
	GA_Debug_EnabledLog_m62102A6AE3527EA07F61732C29D7D41C98B6A558,
	GA_Debug__cctor_mB8DC7261179EA484102FBEEE4BC76EBA01E270CB,
	GA_Design_NewEvent_m0681AE097D570D727CBA861F97E1D622A19AABBA,
	GA_Design_NewEvent_mE9688AC97B578FDE675C532C5A0409F2FAB67E78,
	GA_Design_CreateNewEvent_m2CA912870B54A47E3624213556A8BB28FE6ACCDF,
	GA_Error_NewEvent_mBF6CCDD3DDBA879AB22FD69E183C4666DBE2958B,
	GA_Error_CreateNewEvent_m7A3E2D6A8AFDED5896C0B830497D8B5B76C68D66,
	GA_Progression_NewEvent_m3D69390A5DD9040131E56165FE2299B534C57368,
	GA_Progression_NewEvent_mAC7703988CFDF13A6D8FFB1F1018C228CB502305,
	GA_Progression_NewEvent_mE7251147E816C4DC954B4BB1C86820B5CD1C4FB9,
	GA_Progression_NewEvent_m89279B921396C02B3901272FBD362EAAEC119831,
	GA_Progression_NewEvent_m457B486D4843F9C046F568C096775EE8D3C320ED,
	GA_Progression_NewEvent_m7492D3124C3666B060D06B18D784679F166C1AE7,
	GA_Progression_CreateEvent_mF8CA9105B01C4AB3950B3E75B4BD361AC81B4DE5,
	GA_Resource_NewEvent_mAB92EAE1B0F94AEA77820026D1273D211C0AB19B,
	GA_Setup_SetAvailableCustomDimensions01_mDD38BA20BA0F2D3F6BE0A9A78965AEDA6F3EE48D,
	GA_Setup_SetAvailableCustomDimensions02_m3F6DBCDA8E25BA6E138F675F28B2CBB8B87C5D64,
	GA_Setup_SetAvailableCustomDimensions03_m814310F3D8C97D1D2F6E63D6F1EC755C931DE7C5,
	GA_Setup_SetAvailableResourceCurrencies_m0A340DC639662C84D04F60BDCC99A0076958BB35,
	GA_Setup_SetAvailableResourceItemTypes_m76764261864B821C3537B2971FE94C3C64D62ED1,
	GA_Setup_SetInfoLog_m9F9DD51F7FB24A2DDE0AB166FE6151C422588248,
	GA_Setup_SetVerboseLog_m0DB3E2B4085A94C4FCBA9D789F2724E57309B560,
	GA_Setup_SetCustomDimension01_m50ED8A54CDA84B5F07FE4DE2F856DE48251DC762,
	GA_Setup_SetCustomDimension02_m37F7B159CB7CD012153763E2432BDE33609B9348,
	GA_Setup_SetCustomDimension03_mDDE3A128E8ADAF9388C6B4D2C200BCE493137013,
	GA_SpecialEvents_Start_mF0CCF7223C003AAA275D2CD1417490D753D39A1C,
	GA_SpecialEvents_SubmitFPSRoutine_mB7ED0FECE94E26541C76004A5AF746E12BB4CA23,
	GA_SpecialEvents_CheckCriticalFPSRoutine_mD7D9FE93AF20F2C17F8197F9C9D2AC5A87A6292C,
	GA_SpecialEvents_Update_m4DB853C10E28CE664D6D577247FCBFA5EDB51F34,
	GA_SpecialEvents_SubmitFPS_m4BB21A0BC71E9BDBD2D30F9DAE46340672D7AB45,
	GA_SpecialEvents_CheckCriticalFPS_mAAAF2395C79FAEAE2CAFF64D2A849A1A70FD215E,
	GA_SpecialEvents__ctor_mDBA1A33FC309E558151F4B469D7C04CD7B808140,
	GA_SpecialEvents__cctor_m4B4688F83329CE504DD70643BDBB8E85F1651D1A,
	GAValidator_StringMatch_mA357E4C8EF025495D522DE696F671A8CD0CBF41C,
	GAValidator_ValidateBusinessEvent_m73C2CD6E99E6BC169C71DA9ABA72B9E4EA2F61D7,
	GAValidator_ValidateResourceEvent_m9D6719DF515F00A84DCD9C13C44D7AE2C8E3316E,
	GAValidator_ValidateProgressionEvent_m37BD46F9638FF5D3F06DBB574E0C42CD601A9897,
	GAValidator_ValidateDesignEvent_m7354B2B20A7917AA5D641611A9061181256BB003,
	GAValidator_ValidateErrorEvent_m979DC06FE0B01857B85D19E1191FA0AA5447B7DD,
	GAValidator_ValidateAdEvent_m2C8A9673537D14386B48D2C99FA777CF3F0BF096,
	GAValidator_ValidateSdkErrorEvent_m57F71173AB1D60C8CA6657228F419C025D8CA114,
	GAValidator_ValidateKeys_mF3DC8DC6E3DEFE05CB91A4C18874ABF1B1A46541,
	GAValidator_ValidateCurrency_m4AA7647E62CAF5E71C97405D941E45D77557D79B,
	GAValidator_ValidateEventPartLength_m3257ECC3162EB9C7331F3C727E6D02BC3A111245,
	GAValidator_ValidateEventPartCharacters_mD7BD8FF487DA788F144C4DF42F21D6A50F033F6B,
	GAValidator_ValidateEventIdLength_m222C2216765B4655CD0ECE098AED4292A0075480,
	GAValidator_ValidateEventIdCharacters_mBDDAB1AAC84BFE0567EC799AC3C96B0DE7CCC599,
	GAValidator_ValidateBuild_m6D1D70A38A270EFFBA517CDCFF1287A9F2BAA99A,
	GAValidator_ValidateUserId_m7992B6F93172799E3C0FA41E123E2462FD76108B,
	GAValidator_ValidateShortString_m64E44353AE7525A1A0AF735D110A84BDED4AF310,
	GAValidator_ValidateString_m762049135FEA5B6439A4D2B5C221E86476B08267,
	GAValidator_ValidateLongString_m66BA7E43A5FE0CA029FF62FB6D0D5AC0EC41404B,
	GAValidator_ValidateConnectionType_m8797FECAC6703D6378334AE1952A32F1E11434E9,
	GAValidator_ValidateCustomDimensions_mB3D912D85F952C2D3DDE597088BA0CBC8456906D,
	GAValidator_ValidateResourceCurrencies_mA6CC78155D70696B4148F5BDE72B83AC95FD1EB4,
	GAValidator_ValidateResourceItemTypes_mF2A140147E85A19318A8BF5F0DE519FC6E8282BF,
	GAValidator_ValidateDimension01_mC47008DEFAC01910F1F8D050D79223B7585BAD9D,
	GAValidator_ValidateDimension02_m44ABDDE77100EF7BB62A3EE043ADA5160CC3F390,
	GAValidator_ValidateDimension03_mE15B6F02F34B4969AE3367FE74D4AD59E4FFA0AD,
	GAValidator_ValidateArrayOfStrings_m218B0989ABE5AA287D9D26C20212CB1E655DE3DD,
	GAValidator_ValidateClientTs_m5F737880925FB1ECD73F944508CA05A4B1F0BD69,
	VoodooLog_Initialize_m9C4412D348DE823CC4535F0327D0985EC68B5B74,
	VoodooLog_Log_mA7C46EE6B665A3BCC85F83F844B24039AEE02D71,
	VoodooLog_LogE_mE4E1FC2258F9C025F5E965B9D6FE302F8346ED69,
	VoodooLog_LogW_m1B8171158AD9DCD531714EA008F3EE5F6115ED74,
	VoodooLog_Format_m3B92528E7DEBA4AE65FE81F97571702522A85DFE,
	VoodooLog_DisableLogs_mB02FC2E65F7485E3AC4F142F6550E5CC21A90B69,
	VoodooLog_EnableLogs_mCF301D8664674A872023E1673BD6A4A65D989B46,
	VoodooLog_IsLogsEnabled_mD4375E25A3B9DDFDF46A87CEA0E869977AAE749B,
	TinySauceBehaviour_Awake_mFAAA01A24AFF626349B4700FE195B4249FB2CCF2,
	TinySauceBehaviour_InitAnalytics_m0E97CA2D3FA96886CD3E6FB732B13084D73C90FA,
	TinySauceBehaviour_OnApplicationPause_m6EA76A02E0EB2780ECE18C572E36F494375C792F,
	TinySauceBehaviour__ctor_m0E88FCBC2D30637F5A566100037FC4894F0D8B96,
	TinySauceSettings_Load_m6B57575CD43B33BAE0FDDDC7CEDCCC3CF850C747,
	TinySauceSettings__ctor_m17B2849E172B818BD924A5473B9E47CD800D3837,
	ManifestUtils_MergeManifestsFiles_m486A18A2433548D7340240185A7B2C415A284131,
	ManifestUtils_SetApplicationAttributes_mD8E4D1597CCD5EB79D87947EB02AC5B781B9A759,
	ManifestUtils_ReplaceKeys_mC82D1EA16D1BB27023A9C83F66B8C59971A0CFD1,
	ManifestUtils_Add_mBFB87E1A709BF3A7EBD8A6D26F6B4FCBBBB66675,
	ManifestUtils_LoadFromFile_m6D0336D0716E4CBF02268B16E0D7099939C301D8,
	ManifestUtils_LoadFromString_m49896DCFF05217BCE4279ACF6CDE7FC7B607825F,
	ManifestUtils_AddChildNode_m0774E2B100503D9E06CB49A9CC70542AB8467B1D,
	ManifestUtils_FindChildNode_m8CAAAC87A37AFCCEAB570F078FEDF556683973F3,
	ManifestUtils_FindAttribute_mADD12182712B3FF205596CDA738DE551C111C7A2,
	ManifestUtils_FindChildNode_mEF66F9E3B4965C85BA9B80EB7DAE77F621F0A1CF,
	ManifestUtils_FindElementWithAndroidName_m040EF519E28229C677A7625260255A813176EC39,
	ManifestUtils_GetAndroidElementName_mC68A244A12CA5110AE421509EC1D90B18F30B050,
	ManifestUtils_SaveDocumentInFile_m7BB668EEBB671FCC3F827635800D4EA483725B57,
	AnalyticsManager_add_OnGameStartedEvent_m2DB92954CA3ACF9DAB2DA17C23D3616BB969683E,
	AnalyticsManager_remove_OnGameStartedEvent_m41D0E1C7DC0A0EA4B0C2962D7817373BDD442F9C,
	AnalyticsManager_add_OnGameFinishedEvent_mD245F0F432F9603FFD7DF7CC2D416E56A308D9B9,
	AnalyticsManager_remove_OnGameFinishedEvent_mAC210AC5EEA8AE878F279C59F4B7C945CAE17AC5,
	AnalyticsManager_add_OnTrackCustomValueEvent_mCA155B97B760B99E6211B88B1ADD56DA51C4DA82,
	AnalyticsManager_remove_OnTrackCustomValueEvent_mF597715E58A5EBE15D1928AEAF8FB8F3A902784F,
	AnalyticsManager_add_OnTrackCustomEvent_m92DD176430D5C061E516428635A3549E25B936D9,
	AnalyticsManager_remove_OnTrackCustomEvent_m8823BAF4923C42C1202DFC4835923343684F1C9B,
	AnalyticsManager_add_OnApplicationResumeEvent_mC1ECC28471F5B3D75CFE4EE9D03B7B1E2A63CBEA,
	AnalyticsManager_remove_OnApplicationResumeEvent_m3AE3CF3CABE9B1B9E874B684659EE452940842E1,
	AnalyticsManager_Initialize_mFFF61A4BE10567C5D6E20DD7A4051B17E9FD5D9A,
	AnalyticsManager_OnGameStarted_m6D59D3F362521E742EAFD0AAE7525F289BD80B51,
	AnalyticsManager_OnGameFinished_m35F8AF37C05A8370BC19981E2D991BDC9EDB21A8,
	AnalyticsManager_TrackCustomEvent_mEE1FAF7E9B09562D95CC12E0235D8CB3F5C04A72,
	AnalyticsManager_TrackCustomEvent_m3E65453D0935B6D21742C2455C515E54435F2238,
	AnalyticsManager_OnApplicationResume_m7C345F057B8BC930D89FE15CEDD83D6A5F650FB8,
	AnalyticsManager__cctor_m48CCE10D4A28C4ECA3B091E7BF705533734329DA,
	NULL,
	GameAnalyticsProvider__ctor_m649CA095AC71C702042C742F7CA5A53F34CE8180,
	GameAnalyticsProvider_Initialize_m294805F45E57906DEE3B2E7633EF71BAC8F14354,
	GameAnalyticsProvider_RegisterEvents_mDE6B88888D1EADBC9A28A94FA3ADB62F3D822388,
	GameAnalyticsProvider_UnregisterEvents_mC334E0CD3E15B9E6775C12989F863BD09A2A50F8,
	GameAnalyticsProvider_OnGameStarted_mEAC6338759F4E680D93D78C630694211CCA4401E,
	GameAnalyticsProvider_OnGameFinished_m9F975CB121DDE865EDDDF875B70FD83136B2A9D8,
	GameAnalyticsProvider_TrackCustomEvent_mB625F71BC4E98012C3393805C0DBAB9FD5F2C66A,
	GameAnalyticsProvider_TrackCustomEvent_m06AEFC1C2384CDF19C60A23DCFB227387598565C,
	GameAnalyticsWrapper_Initialize_m6A2269DECEC4E5C5D8107255BBF78CE06DA96FE5,
	GameAnalyticsWrapper_TrackProgressEvent_m7DE043963ECAC5503F3118CCDD85775EA66B0664,
	GameAnalyticsWrapper_TrackDesignEvent_mEFBC717B6DC026CB9A3CA0A3B9EE55B576D15040,
	GameAnalyticsWrapper_InstantiateGameAnalytics_mD26002F3EBDA3080C19C03B6ACFCDBF428A6D199,
	GameAnalyticsWrapper_Disable_m60C77A631514F58F9C58B4001B27DC46A3D5D22C,
	GameAnalyticsWrapper__cctor_m53D4076D48F07C37FAFF4D0FCB728A7B238DFA51,
	GameState__ctor_m8DA1777D5848F1B7AF01F10AB51A89D6EB690B4B,
	StepLevel__ctor_m26E21E51BD2F5A085B2DD452890F961806C78553,
	BulletAttack__ctor_m3777F5A99C7C59CE9B1D6CDC42CC1A9072EF1A97,
	Gun__ctor_m6D10398582A319E054D4C5B2E76D9C4BB50FD695,
	Sound__ctor_m2ADC59C98B7151620EA490E2745463A5D6603ED2,
	U3CHitU3Ed__8__ctor_m2E2CA499B29C0C2B2F28C33674D87C13B50FF806,
	U3CHitU3Ed__8_System_IDisposable_Dispose_mF101BF3A30D55FC252BC4DAEF2AAF41B84D94A5B,
	U3CHitU3Ed__8_MoveNext_mCD1E534D906D8A80461E1CA6E9F5DECE64020293,
	U3CHitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m798551BB8388864FEEA3870E13A0A2132D33617C,
	U3CHitU3Ed__8_System_Collections_IEnumerator_Reset_mE5795F4E77068893A9866EE941EE1DE9C1FDBB47,
	U3CHitU3Ed__8_System_Collections_IEnumerator_get_Current_mA47D1BA386A499F8CB3E84EC7A112557E2EB6EBE,
	U3CDieU3Ed__9__ctor_mF934CF8A2EB8CD524608CA3C418C711A7867CC66,
	U3CDieU3Ed__9_System_IDisposable_Dispose_m07840CC29ABF13C986E8F2D6486073E755D0C04E,
	U3CDieU3Ed__9_MoveNext_m12176FB55FDD2BD4AC2CDA4C54C4F13365F75A24,
	U3CDieU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88871930AA0A5E8C775394D2693BB47A24AD982D,
	U3CDieU3Ed__9_System_Collections_IEnumerator_Reset_m3FE9C7925EFD40688BB870ED8D38C4BBD1324C03,
	U3CDieU3Ed__9_System_Collections_IEnumerator_get_Current_m9479A448D0CD68F167C0D6CD7DE26B0759ACC0CF,
	U3CGotoIconU3Ed__9__ctor_mF6540977B0C142CE29B6FB9B48259B26CA310DBB,
	U3CGotoIconU3Ed__9_System_IDisposable_Dispose_mF2C58AECBAF42A2942BD19B1E9BDBD4845CF8DEB,
	U3CGotoIconU3Ed__9_MoveNext_m7FF741F1A325DA0008C47DBC10378BD472417CBF,
	U3CGotoIconU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF0FC9B1BC42011E21A401CC06728091D3E18473,
	U3CGotoIconU3Ed__9_System_Collections_IEnumerator_Reset_m0B70AC0B521DE9063E71CD4CB7B974D2059F2835,
	U3CGotoIconU3Ed__9_System_Collections_IEnumerator_get_Current_m306B27D9962BA73B8F17EB45E3A3F1E62E6D526C,
	U3CU3Ec__DisplayClass2_0__ctor_m3E2A097E435942C67F76BF494E18317C76D8C2B2,
	U3CU3Ec__DisplayClass2_0_U3CPlayU3Eb__0_m397A095DF92C801AED5DD3D7387EEE6A5CD40910,
	U3CPingpongFinishLineU3Ed__6__ctor_mA1C50F6FFEC349CA633D75A6E89227C22780DFCB,
	U3CPingpongFinishLineU3Ed__6_System_IDisposable_Dispose_m1AA0F696FCFA4492536A6F86E451314C1CA8140E,
	U3CPingpongFinishLineU3Ed__6_MoveNext_m414FC4BCCE7F4A8D39AC88327733BEB388FA7076,
	U3CPingpongFinishLineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C2114CFB31D90D532397D95880C346C78322045,
	U3CPingpongFinishLineU3Ed__6_System_Collections_IEnumerator_Reset_m31CA2CC3C7B260D511063BC93636603EC3085AED,
	U3CPingpongFinishLineU3Ed__6_System_Collections_IEnumerator_get_Current_mEE21B950B1FA88555505738BF8521F0BF765983E,
	U3CCancelU3Ed__6__ctor_m8A9D78A5A24DE8D8D57E919FFDC0BBFF27A4C626,
	U3CCancelU3Ed__6_System_IDisposable_Dispose_m9A0E056A2C4AC1D78FD7607B30B3EC70EE97D1BB,
	U3CCancelU3Ed__6_MoveNext_mF0821233C774A16953299453667ACBD5CF46689C,
	U3CCancelU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mADEA1A845BA81B998D01926D53593F1910E95E02,
	U3CCancelU3Ed__6_System_Collections_IEnumerator_Reset_m55911FE421A635EDB1BF223D063938E66FEBDB3C,
	U3CCancelU3Ed__6_System_Collections_IEnumerator_get_Current_mB65BC877A2FBF94D80E52DC38EB2C382BDBF11DC,
	U3CPingWarningU3Ed__42__ctor_mA0B7CA28B1F7FBDFF68557DB4168D3F2D897974C,
	U3CPingWarningU3Ed__42_System_IDisposable_Dispose_m8C7F07D5E8A7BAF846334DC8B4239CFD0F90DAC8,
	U3CPingWarningU3Ed__42_MoveNext_m2602F2C84AD68CB7918E16D8F0B45593D74ED77B,
	U3CPingWarningU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3D79B026EB49146712786370033D7FE02A3CE602,
	U3CPingWarningU3Ed__42_System_Collections_IEnumerator_Reset_m46305774EAE7525D01EC79D21E80EA395B8CE422,
	U3CPingWarningU3Ed__42_System_Collections_IEnumerator_get_Current_mE7E64143D6CD100BCA742DBD9435C52BB159F2BD,
	U3CBossFightU3Ed__4__ctor_mA5D7724309E846D4E280F59F410C653674C943CC,
	U3CBossFightU3Ed__4_System_IDisposable_Dispose_mBCAF3B312C9EB44916496A4BB4BCADB4D86BC490,
	U3CBossFightU3Ed__4_MoveNext_m793C392AEE1C3EF966E72AE164A80707B85F1A1C,
	U3CBossFightU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m703481914D909722E5447B427404C8A5E3B38BED,
	U3CBossFightU3Ed__4_System_Collections_IEnumerator_Reset_m57418F64079A821CCDA63406454C7B16729A63DF,
	U3CBossFightU3Ed__4_System_Collections_IEnumerator_get_Current_mB459A871EDA1B08BB12269ED87DA6B8CE92373F2,
	U3CStartU3Ed__5__ctor_mF2961383233177CAFFCCA6D847CEC747807F0489,
	U3CStartU3Ed__5_System_IDisposable_Dispose_m941CBBE9C658E8F68415E14E8C36BACA17394B59,
	U3CStartU3Ed__5_MoveNext_mBCE88E052FA9C8BD91EE16321ECAC6A08EBE6918,
	U3CStartU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFAD7091AA1A4AE96130C0957C9D867FA134B5995,
	U3CStartU3Ed__5_System_Collections_IEnumerator_Reset_m0AA880D00287E8A03A252B7C8400716AEDA4CE48,
	U3CStartU3Ed__5_System_Collections_IEnumerator_get_Current_m36E5719D03288E89813E3AFDD6638494C6E1840F,
	U3CStartU3Ed__1__ctor_m798D636CCF04E07DB9557D36BF3F01362DBE2CD6,
	U3CStartU3Ed__1_System_IDisposable_Dispose_m4E32BEB5EA7E5B42E3425AA81E5CFC373DB731EA,
	U3CStartU3Ed__1_MoveNext_m93192AB72557B282BD46F135D5FFB5F1EB8E6FFA,
	U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC9DC5BC8114331194AD89E3D41E1B106278C638C,
	U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_mDD3CED12E0CD28F02232972A21D92AC8AE1B1FDD,
	U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m5D60D96E86210508BBFD426C0D815F4A97A11E21,
	Parser_IsWordBreak_m7FD116D792B6BA36448E35A51612D36562E90527,
	Parser__ctor_m5B351AF5ABA2A87278A6352C66122EE60E4BCE13,
	Parser_Parse_mEA4E3C744BCC561E81195B809B0AC857416F3072,
	Parser_Dispose_m487491A4438771227B6630A48574B30B52279AC9,
	Parser_ParseObject_m6BC97A0DDE81FBC437E225CAC841641AC0C3623D,
	Parser_ParseArray_mD25D8E36291A19139F54EDAE23CA9A484E9AF06C,
	Parser_ParseValue_mBD8A7D064E636C48DAAAFAB2367F806A235CCC17,
	Parser_ParseByToken_m1264B18AC165C9130808F52DDEE72B2C701B667F,
	Parser_ParseString_mD6827D047119E0C597D701A6FA8AC1AEDC248827,
	Parser_ParseNumber_mD619A21BEED602342A177FA160C0137284125984,
	Parser_EatWhitespace_mDCEC0B86CA61D28F43ADF2B96E9093C9A7F64F1D,
	Parser_get_PeekChar_mE7F60F0949AE0D6979981A7E406DA132A36A5F71,
	Parser_get_NextChar_m6E2FDD55FCA3C4872266C299811C91BE69D01039,
	Parser_get_NextWord_mBB267293F9D244522179C189155DE54D75F86EA5,
	Parser_get_NextToken_m18434DF3642CA5D73174669069B195EABAA4D7F6,
	Serializer__ctor_m94982AC291A8D796ED90138D15C6E6574E69CEE7,
	Serializer_Serialize_m2942C1314A02CE414959A572F6270A2FF60FCB72,
	Serializer_SerializeValue_mEFF93C2569039C69E02F9C836CA4899ADAD22F90,
	Serializer_SerializeObject_mC6A2EA88EED97E459BEAC12FCED66A3E1DF54290,
	Serializer_SerializeArray_m6DD81E55FB7E11A7D8B00201A0D5BB3BEEC47CFE,
	Serializer_SerializeString_mA7658C4CD540D609A5B5FBFFF1978EEF1EACC059,
	Serializer_SerializeOther_m0815A226A5F03D5F9642E190963E99ADD5DE3F82,
	U3CSubmitFPSRoutineU3Ed__6__ctor_m094B86B76E427C5B78588C7897440BB3B3D508F2,
	U3CSubmitFPSRoutineU3Ed__6_System_IDisposable_Dispose_m322DCD72816F3E60A11D5845B914A0FD1871A764,
	U3CSubmitFPSRoutineU3Ed__6_MoveNext_m227A93E5EC1D455A2717B58AFD91C0592A0BA49C,
	U3CSubmitFPSRoutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D31F3D4A34A2E322ACA7A96C2660C90D9127A9B,
	U3CSubmitFPSRoutineU3Ed__6_System_Collections_IEnumerator_Reset_m4850E811C8FB070EB3702003C06053805C845994,
	U3CSubmitFPSRoutineU3Ed__6_System_Collections_IEnumerator_get_Current_m0F55918C9A41E97F9B1216A387579BE5474924D2,
	U3CCheckCriticalFPSRoutineU3Ed__7__ctor_m66D0372FAD02852947FB9DF749EE1E2192671A9E,
	U3CCheckCriticalFPSRoutineU3Ed__7_System_IDisposable_Dispose_m38ACF4A21A1FD5A57F31BFD33758946B77CCFAD6,
	U3CCheckCriticalFPSRoutineU3Ed__7_MoveNext_m1FFA8E50B4763BA86121C98399CC052C4F74C5A0,
	U3CCheckCriticalFPSRoutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7701B52E9326DE5D24AD72122548B785F13184E0,
	U3CCheckCriticalFPSRoutineU3Ed__7_System_Collections_IEnumerator_Reset_m2203B84052ACE97F985E4405A365D1803625D5B5,
	U3CCheckCriticalFPSRoutineU3Ed__7_System_Collections_IEnumerator_get_Current_mED4075309B99F41EA2249CEF351640075A223223,
	U3CU3Ec__DisplayClass17_0__ctor_mFCDB860B35FCAAD8E9866D940D1B1FBF817BE80C,
	U3CU3Ec__DisplayClass17_0_U3CInitializeU3Eb__0_mEC0C58D2E555B5F9A7A2BA7BDA7D27F292CDD1A7,
	NULL,
	GameAnalyticsEvent__ctor_m74986D7CC5BEF02A2DE9B161D30C35F8B6FAA72E,
	ProgressEvent_Track_m756CD0C4E2F7DDA8217449F7B1470385323B9322,
	ProgressEvent__ctor_mF526A5CB445B100C5226C85B54C2269C2AA35F1E,
	DesignEvent_Track_mEAC9146E85F410568B20984DC4D56AFF9E940D66,
	DesignEvent__ctor_m3FD5746BB7040BD5E5597A05FAC28EA1A2C1DA3D,
};
static const int32_t s_InvokerIndices[632] = 
{
	23,
	23,
	23,
	26,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	130,
	130,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	23,
	23,
	23,
	23,
	130,
	130,
	23,
	23,
	325,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	191,
	90,
	89,
	23,
	23,
	26,
	23,
	23,
	32,
	325,
	32,
	23,
	23,
	32,
	23,
	23,
	32,
	26,
	105,
	23,
	23,
	31,
	23,
	23,
	31,
	23,
	23,
	23,
	27,
	27,
	23,
	28,
	23,
	23,
	904,
	23,
	23,
	23,
	23,
	23,
	325,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	26,
	23,
	23,
	23,
	1581,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	89,
	1869,
	23,
	23,
	164,
	43,
	49,
	154,
	23,
	1870,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	26,
	23,
	32,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	34,
	23,
	23,
	14,
	23,
	1426,
	1871,
	26,
	23,
	14,
	23,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	26,
	26,
	32,
	23,
	-1,
	-1,
	-1,
	154,
	23,
	154,
	1475,
	1473,
	1872,
	154,
	1473,
	4,
	154,
	23,
	23,
	23,
	23,
	23,
	3,
	3,
	3,
	1873,
	1874,
	1873,
	154,
	1473,
	566,
	1625,
	1875,
	1876,
	1877,
	1878,
	1879,
	566,
	1880,
	1881,
	1356,
	154,
	821,
	821,
	3,
	3,
	154,
	154,
	154,
	154,
	154,
	23,
	3,
	0,
	1,
	49,
	4,
	4,
	4,
	3,
	154,
	154,
	154,
	151,
	4,
	106,
	154,
	23,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	821,
	821,
	821,
	3,
	3,
	137,
	154,
	154,
	154,
	1833,
	1874,
	1882,
	1864,
	1883,
	1884,
	137,
	1625,
	1880,
	1881,
	1356,
	821,
	821,
	1,
	49,
	4,
	4,
	4,
	0,
	3,
	154,
	154,
	154,
	151,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	821,
	137,
	154,
	154,
	154,
	1833,
	1874,
	1882,
	1864,
	1883,
	137,
	1884,
	1625,
	1880,
	1881,
	1356,
	137,
	821,
	821,
	821,
	821,
	3,
	3,
	1,
	49,
	4,
	4,
	4,
	154,
	154,
	154,
	151,
	3,
	154,
	23,
	0,
	0,
	23,
	3,
	135,
	49,
	114,
	114,
	114,
	114,
	114,
	26,
	32,
	32,
	14,
	1038,
	1038,
	62,
	62,
	34,
	34,
	26,
	27,
	23,
	3,
	14,
	26,
	14,
	26,
	14,
	26,
	27,
	153,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	428,
	153,
	132,
	14,
	26,
	10,
	32,
	14,
	26,
	14,
	26,
	1843,
	1880,
	1881,
	1356,
	1885,
	1874,
	195,
	361,
	3,
	3,
	1884,
	137,
	1838,
	1625,
	1625,
	1625,
	1875,
	1864,
	909,
	1886,
	1883,
	1887,
	1882,
	154,
	154,
	154,
	154,
	154,
	821,
	821,
	154,
	154,
	154,
	23,
	14,
	14,
	23,
	3,
	23,
	23,
	3,
	135,
	1888,
	1889,
	1890,
	114,
	1308,
	1891,
	475,
	135,
	114,
	606,
	114,
	114,
	114,
	114,
	114,
	606,
	606,
	606,
	114,
	114,
	114,
	114,
	114,
	114,
	114,
	1892,
	242,
	164,
	137,
	137,
	137,
	1,
	3,
	3,
	49,
	23,
	23,
	31,
	23,
	4,
	23,
	135,
	1,
	135,
	1,
	0,
	0,
	186,
	1,
	1,
	1,
	135,
	1,
	137,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	154,
	599,
	154,
	1893,
	154,
	1473,
	3,
	3,
	31,
	23,
	31,
	3,
	3,
	154,
	1893,
	154,
	1473,
	5,
	1894,
	1895,
	3,
	3,
	3,
	23,
	23,
	1486,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	9,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	48,
	26,
	0,
	23,
	14,
	14,
	14,
	34,
	14,
	14,
	23,
	238,
	238,
	14,
	10,
	23,
	0,
	26,
	26,
	26,
	26,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x02000031, { 0, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[5] = 
{
	{ (Il2CppRGCTXDataType)2, 17561 },
	{ (Il2CppRGCTXDataType)2, 17196 },
	{ (Il2CppRGCTXDataType)3, 9928 },
	{ (Il2CppRGCTXDataType)1, 17196 },
	{ (Il2CppRGCTXDataType)3, 9929 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	632,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	5,
	s_rgctxValues,
	NULL,
};
