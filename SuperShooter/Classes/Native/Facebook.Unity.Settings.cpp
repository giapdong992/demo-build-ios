﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// Facebook.Unity.Settings.FacebookSettings
struct FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA;
// Facebook.Unity.Settings.FacebookSettings/<>c
struct U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471;
// Facebook.Unity.Settings.FacebookSettings/OnChangeCallback
struct OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1;
// Facebook.Unity.Settings.FacebookSettings/OnChangeCallback[]
struct OnChangeCallbackU5BU5D_t7D4BE5E98CA13C1DCD170ECB27AFF039C478E6DB;
// Facebook.Unity.Settings.FacebookSettings/UrlSchemes
struct UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48;
// Facebook.Unity.Settings.FacebookSettings/UrlSchemes[]
struct UrlSchemesU5BU5D_t121767C239CF75C0E7587632E6816BE76FC9B1A5;
// System.Action`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>
struct Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>
struct List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8;
// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes>
struct List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734;

IL2CPP_EXTERN_C RuntimeClass* Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral11A3B430794567143B2D380848C73908507F1D59;
IL2CPP_EXTERN_C String_t* _stringLiteral3D7479543F221F46EE46D1F7AB8E7AD53734F2B9;
IL2CPP_EXTERN_C String_t* _stringLiteral4FB43108E78F4D80425CB74F01AC6D0A59E0A3D3;
IL2CPP_EXTERN_C String_t* _stringLiteralB6589FC6AB0DC82CF12099D1C2D40AB994E8410C;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m052DDDED017E7D987AEE5637A3FEA43DF771172A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mB8B305BA07510D89B7080577D4CAE7D227975FC8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mE79048C11E65015096F0E0ECB32D2C56EA838B8E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_ForEach_m161DC48D7CAFFC09C59EC204BFF16C7FAB4DA12F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m39EF2DAF132C555C0C0D2964DA1E18641CED00C7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m7B88C102B657EB6875FBFC582C86C49CE1DB4486_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mCA14223B544692C7F7021CB026A222E347BE9935_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mB739B0066E5F7EBDBA9978F24A73D26D4FAE5BED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ScriptableObject_CreateInstance_TisFacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_m3C1138AD69C5490AD30BB8BCD08302D25257C35A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CSettingsChangedU3Eb__76_0_m0C9EF39EDE99D2588A5985F239B388C18BBC8A66_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_RegisterChangeEventCallback_mD13D0239F493B9701BC8891ABBC5A00980F5BA14_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_UnregisterChangeEventCallback_m64059E33CBD8EDDC51B52AB839554C5BA23A8D16_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings__cctor_m6E56A8CDDB184AD6C0DA6C146401499E39B30BA3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings__ctor_m057BF97646BE5245A135F7D9B2357458224FF1DD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_AdvertiserIDCollectionEnabled_mC0BBD06C17C6DA1DE04AFF774A80D0A841FA33B0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_AppId_m006AE7342E7C2E7A46BEDE9BF5F1E9F3B533AF1B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_AppIds_m213F9082C11ED86A61337946DBA5881971DB389A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_AppLabels_m0B090EF43E95C8FB73619F3AEF0C75D3AA3BA359_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_AppLinkSchemes_m981840EDB39AB30ABC84D4A7236B339797D3124D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_AutoLogAppEventsEnabled_m70BF5DEC22AF2E878525B1E1E5C58743B361402F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_ChannelUrl_m136B1C28CEB1B078BA95F45F96C22FCC4CFD1B9C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_ClientToken_mA9CF546F9A73544DE706EC537F8FDE24EB8FB2A4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_ClientTokens_mDF6BC3A0D7BC2CD76E1181445577437A491AA341_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_Cookie_m0C87EA644FF0E4AF344A849466638C196C888C20_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_FrictionlessRequests_mE87DE80B8A25477536E5C56D040BC1AB83815641_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_IosURLSuffix_m472E416713BBDD6653141AB0FB400A5270063B39_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_IsValidAppId_mF0D6D09A9632E2DE2C51F515BA990C0ABABF90D9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_Logging_m693DFA3C03B6CB9BD152F452663C2CB7476AE27B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_NullableInstance_mE0BB0B0DB862CA279FBD0C2E792EFCE753D66612_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_SelectedAppIndex_m2EC480ADF724345DFD8E05519877A2F46A1238FC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_Status_mE4DAEE25F212765BAEAA789789D485B393186679_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_UploadAccessToken_mC2EE58F6EE93AAA5552353390EC80E27E1A91F4A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_get_Xfbml_m5C1F97F309C373134DD15DFCE257C209DD1AA385_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_set_AdvertiserIDCollectionEnabled_m68A9D35770597F1F6E90BB60843830F890600965_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_set_AppIds_mFEAE5C06EBBFE37F87D23ABC9D6E2F52229890F8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_set_AppLabels_m5731795E2EDB99EB7C0F0A61C9DA7098635B7357_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_set_AppLinkSchemes_m4626609FA7E32934E12FA655D641B6735BB788C0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_set_AutoLogAppEventsEnabled_m1D92F0AD0A3AF85A4FF5B77441757601C21F53AE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_set_ClientTokens_m2B44D9306D07402C28C80D052D1F4E100BB7B39E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_set_Cookie_m75D92C414BCD12999B0252B48E3DA92862C98C6B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_set_FrictionlessRequests_m506B05AAACB062C97B9889BC40184801ACFFE10F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_set_IosURLSuffix_m3C0E030A70E79C62302F1E4FDE305B4471E11F2B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_set_Logging_mB6B75791D46B8B8F576694DEC4F2EC35F7A9EBB9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_set_SelectedAppIndex_mFEE448C1AC465FC83B45462B48026D700A4E468D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_set_Status_m5B328C7ED836EACC4589E195F2D401007DC2253D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_set_UploadAccessToken_m0E7538A7F94CF09920976B1BB642059D0AED1A88_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookSettings_set_Xfbml_mAF217018F5283A25BA1755E620CB3AC84C7EC917_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_m40EF99CA45A4E5F71A02C65A95129BFAC50AC840_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UrlSchemes__ctor_m78C1F53E102E5C40F1BC41F4573CA6E32B3979BF_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t5F725CBEEC703EA097AB285FDE44E5474F65BD0B 
{
public:

public:
};


// System.Object


// Facebook.Unity.Settings.FacebookSettings_<>c
struct  U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_StaticFields
{
public:
	// Facebook.Unity.Settings.FacebookSettings_<>c Facebook.Unity.Settings.FacebookSettings_<>c::<>9
	U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471 * ___U3CU3E9_0;
	// System.Action`1<Facebook.Unity.Settings.FacebookSettings_OnChangeCallback> Facebook.Unity.Settings.FacebookSettings_<>c::<>9__76_0
	Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 * ___U3CU3E9__76_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__76_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_StaticFields, ___U3CU3E9__76_0_1)); }
	inline Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 * get_U3CU3E9__76_0_1() const { return ___U3CU3E9__76_0_1; }
	inline Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 ** get_address_of_U3CU3E9__76_0_1() { return &___U3CU3E9__76_0_1; }
	inline void set_U3CU3E9__76_0_1(Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 * value)
	{
		___U3CU3E9__76_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__76_0_1), (void*)value);
	}
};


// Facebook.Unity.Settings.FacebookSettings_UrlSchemes
struct  UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings_UrlSchemes::list
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48, ___list_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_list_0() const { return ___list_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_OnChangeCallback>
struct  List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	OnChangeCallbackU5BU5D_t7D4BE5E98CA13C1DCD170ECB27AFF039C478E6DB* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8, ____items_1)); }
	inline OnChangeCallbackU5BU5D_t7D4BE5E98CA13C1DCD170ECB27AFF039C478E6DB* get__items_1() const { return ____items_1; }
	inline OnChangeCallbackU5BU5D_t7D4BE5E98CA13C1DCD170ECB27AFF039C478E6DB** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(OnChangeCallbackU5BU5D_t7D4BE5E98CA13C1DCD170ECB27AFF039C478E6DB* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	OnChangeCallbackU5BU5D_t7D4BE5E98CA13C1DCD170ECB27AFF039C478E6DB* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8_StaticFields, ____emptyArray_5)); }
	inline OnChangeCallbackU5BU5D_t7D4BE5E98CA13C1DCD170ECB27AFF039C478E6DB* get__emptyArray_5() const { return ____emptyArray_5; }
	inline OnChangeCallbackU5BU5D_t7D4BE5E98CA13C1DCD170ECB27AFF039C478E6DB** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(OnChangeCallbackU5BU5D_t7D4BE5E98CA13C1DCD170ECB27AFF039C478E6DB* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_UrlSchemes>
struct  List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UrlSchemesU5BU5D_t121767C239CF75C0E7587632E6816BE76FC9B1A5* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA, ____items_1)); }
	inline UrlSchemesU5BU5D_t121767C239CF75C0E7587632E6816BE76FC9B1A5* get__items_1() const { return ____items_1; }
	inline UrlSchemesU5BU5D_t121767C239CF75C0E7587632E6816BE76FC9B1A5** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UrlSchemesU5BU5D_t121767C239CF75C0E7587632E6816BE76FC9B1A5* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	UrlSchemesU5BU5D_t121767C239CF75C0E7587632E6816BE76FC9B1A5* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA_StaticFields, ____emptyArray_5)); }
	inline UrlSchemesU5BU5D_t121767C239CF75C0E7587632E6816BE76FC9B1A5* get__emptyArray_5() const { return ____emptyArray_5; }
	inline UrlSchemesU5BU5D_t121767C239CF75C0E7587632E6816BE76FC9B1A5** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(UrlSchemesU5BU5D_t121767C239CF75C0E7587632E6816BE76FC9B1A5* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.String>
struct  List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____items_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};

// Facebook.Unity.Settings.FacebookSettings
struct  FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Int32 Facebook.Unity.Settings.FacebookSettings::selectedAppIndex
	int32_t ___selectedAppIndex_9;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::clientTokens
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___clientTokens_10;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::appIds
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___appIds_11;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::appLabels
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___appLabels_12;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::cookie
	bool ___cookie_13;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::logging
	bool ___logging_14;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::status
	bool ___status_15;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::xfbml
	bool ___xfbml_16;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::frictionlessRequests
	bool ___frictionlessRequests_17;
	// System.String Facebook.Unity.Settings.FacebookSettings::iosURLSuffix
	String_t* ___iosURLSuffix_18;
	// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_UrlSchemes> Facebook.Unity.Settings.FacebookSettings::appLinkSchemes
	List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA * ___appLinkSchemes_19;
	// System.String Facebook.Unity.Settings.FacebookSettings::uploadAccessToken
	String_t* ___uploadAccessToken_20;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::autoLogAppEventsEnabled
	bool ___autoLogAppEventsEnabled_21;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::advertiserIDCollectionEnabled
	bool ___advertiserIDCollectionEnabled_22;

public:
	inline static int32_t get_offset_of_selectedAppIndex_9() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA, ___selectedAppIndex_9)); }
	inline int32_t get_selectedAppIndex_9() const { return ___selectedAppIndex_9; }
	inline int32_t* get_address_of_selectedAppIndex_9() { return &___selectedAppIndex_9; }
	inline void set_selectedAppIndex_9(int32_t value)
	{
		___selectedAppIndex_9 = value;
	}

	inline static int32_t get_offset_of_clientTokens_10() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA, ___clientTokens_10)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_clientTokens_10() const { return ___clientTokens_10; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_clientTokens_10() { return &___clientTokens_10; }
	inline void set_clientTokens_10(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___clientTokens_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clientTokens_10), (void*)value);
	}

	inline static int32_t get_offset_of_appIds_11() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA, ___appIds_11)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_appIds_11() const { return ___appIds_11; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_appIds_11() { return &___appIds_11; }
	inline void set_appIds_11(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___appIds_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___appIds_11), (void*)value);
	}

	inline static int32_t get_offset_of_appLabels_12() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA, ___appLabels_12)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_appLabels_12() const { return ___appLabels_12; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_appLabels_12() { return &___appLabels_12; }
	inline void set_appLabels_12(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___appLabels_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___appLabels_12), (void*)value);
	}

	inline static int32_t get_offset_of_cookie_13() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA, ___cookie_13)); }
	inline bool get_cookie_13() const { return ___cookie_13; }
	inline bool* get_address_of_cookie_13() { return &___cookie_13; }
	inline void set_cookie_13(bool value)
	{
		___cookie_13 = value;
	}

	inline static int32_t get_offset_of_logging_14() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA, ___logging_14)); }
	inline bool get_logging_14() const { return ___logging_14; }
	inline bool* get_address_of_logging_14() { return &___logging_14; }
	inline void set_logging_14(bool value)
	{
		___logging_14 = value;
	}

	inline static int32_t get_offset_of_status_15() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA, ___status_15)); }
	inline bool get_status_15() const { return ___status_15; }
	inline bool* get_address_of_status_15() { return &___status_15; }
	inline void set_status_15(bool value)
	{
		___status_15 = value;
	}

	inline static int32_t get_offset_of_xfbml_16() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA, ___xfbml_16)); }
	inline bool get_xfbml_16() const { return ___xfbml_16; }
	inline bool* get_address_of_xfbml_16() { return &___xfbml_16; }
	inline void set_xfbml_16(bool value)
	{
		___xfbml_16 = value;
	}

	inline static int32_t get_offset_of_frictionlessRequests_17() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA, ___frictionlessRequests_17)); }
	inline bool get_frictionlessRequests_17() const { return ___frictionlessRequests_17; }
	inline bool* get_address_of_frictionlessRequests_17() { return &___frictionlessRequests_17; }
	inline void set_frictionlessRequests_17(bool value)
	{
		___frictionlessRequests_17 = value;
	}

	inline static int32_t get_offset_of_iosURLSuffix_18() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA, ___iosURLSuffix_18)); }
	inline String_t* get_iosURLSuffix_18() const { return ___iosURLSuffix_18; }
	inline String_t** get_address_of_iosURLSuffix_18() { return &___iosURLSuffix_18; }
	inline void set_iosURLSuffix_18(String_t* value)
	{
		___iosURLSuffix_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iosURLSuffix_18), (void*)value);
	}

	inline static int32_t get_offset_of_appLinkSchemes_19() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA, ___appLinkSchemes_19)); }
	inline List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA * get_appLinkSchemes_19() const { return ___appLinkSchemes_19; }
	inline List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA ** get_address_of_appLinkSchemes_19() { return &___appLinkSchemes_19; }
	inline void set_appLinkSchemes_19(List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA * value)
	{
		___appLinkSchemes_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___appLinkSchemes_19), (void*)value);
	}

	inline static int32_t get_offset_of_uploadAccessToken_20() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA, ___uploadAccessToken_20)); }
	inline String_t* get_uploadAccessToken_20() const { return ___uploadAccessToken_20; }
	inline String_t** get_address_of_uploadAccessToken_20() { return &___uploadAccessToken_20; }
	inline void set_uploadAccessToken_20(String_t* value)
	{
		___uploadAccessToken_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___uploadAccessToken_20), (void*)value);
	}

	inline static int32_t get_offset_of_autoLogAppEventsEnabled_21() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA, ___autoLogAppEventsEnabled_21)); }
	inline bool get_autoLogAppEventsEnabled_21() const { return ___autoLogAppEventsEnabled_21; }
	inline bool* get_address_of_autoLogAppEventsEnabled_21() { return &___autoLogAppEventsEnabled_21; }
	inline void set_autoLogAppEventsEnabled_21(bool value)
	{
		___autoLogAppEventsEnabled_21 = value;
	}

	inline static int32_t get_offset_of_advertiserIDCollectionEnabled_22() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA, ___advertiserIDCollectionEnabled_22)); }
	inline bool get_advertiserIDCollectionEnabled_22() const { return ___advertiserIDCollectionEnabled_22; }
	inline bool* get_address_of_advertiserIDCollectionEnabled_22() { return &___advertiserIDCollectionEnabled_22; }
	inline void set_advertiserIDCollectionEnabled_22(bool value)
	{
		___advertiserIDCollectionEnabled_22 = value;
	}
};

struct FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_StaticFields
{
public:
	// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_OnChangeCallback> Facebook.Unity.Settings.FacebookSettings::onChangeCallbacks
	List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 * ___onChangeCallbacks_7;
	// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::instance
	FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * ___instance_8;

public:
	inline static int32_t get_offset_of_onChangeCallbacks_7() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_StaticFields, ___onChangeCallbacks_7)); }
	inline List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 * get_onChangeCallbacks_7() const { return ___onChangeCallbacks_7; }
	inline List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 ** get_address_of_onChangeCallbacks_7() { return &___onChangeCallbacks_7; }
	inline void set_onChangeCallbacks_7(List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 * value)
	{
		___onChangeCallbacks_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onChangeCallbacks_7), (void*)value);
	}

	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_StaticFields, ___instance_8)); }
	inline FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * get_instance_8() const { return ___instance_8; }
	inline FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_8), (void*)value);
	}
};


// Facebook.Unity.Settings.FacebookSettings_OnChangeCallback
struct  OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<Facebook.Unity.Settings.FacebookSettings_OnChangeCallback>
struct  Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83  : public MulticastDelegate_t
{
public:

public:
};


// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObject_CreateInstance_TisRuntimeObject_m7A8F75139352BA04C2EEC1D72D430FAC94C753DE_gshared (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m908B647BB9F807676DACE34E3E73475C3C3751D4_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::ForEach(System.Action`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_ForEach_m1FF0FB75577597F41D35ED6D7471CD96BA2CA65F_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___action0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);

// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880 (const RuntimeMethod* method);
// System.Void Facebook.Unity.Settings.FacebookSettings::SettingsChanged()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC (const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppIds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * FacebookSettings_get_AppIds_m213F9082C11ED86A61337946DBA5881971DB389A (const RuntimeMethod* method);
// System.Int32 Facebook.Unity.Settings.FacebookSettings::get_SelectedAppIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FacebookSettings_get_SelectedAppIndex_m2EC480ADF724345DFD8E05519877A2F46A1238FC (const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
inline String_t* List_1_get_Item_mB739B0066E5F7EBDBA9978F24A73D26D4FAE5BED_inline (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  String_t* (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline)(__this, ___index0, method);
}
// System.String System.String::Trim()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Trim_mB52EB7876C7132358B76B7DC95DEACA20722EF4D (String_t* __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_ClientTokens()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * FacebookSettings_get_ClientTokens_mDF6BC3A0D7BC2CD76E1181445577437A491AA341 (const RuntimeMethod* method);
// System.String Facebook.Unity.Settings.FacebookSettings::get_AppId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_AppId_m006AE7342E7C2E7A46BEDE9BF5F1E9F3B533AF1B (const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline (String_t* __this, const RuntimeMethod* method);
// System.Boolean System.String::Equals(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Equals_m9C4D78DFA0979504FE31429B64A4C26DF48020D1 (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_NullableInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * FacebookSettings_get_NullableInstance_mE0BB0B0DB862CA279FBD0C2E792EFCE753D66612 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<Facebook.Unity.Settings.FacebookSettings>()
inline FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * ScriptableObject_CreateInstance_TisFacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_m3C1138AD69C5490AD30BB8BCD08302D25257C35A (const RuntimeMethod* method)
{
	return ((  FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * (*) (const RuntimeMethod*))ScriptableObject_CreateInstance_TisRuntimeObject_m7A8F75139352BA04C2EEC1D72D430FAC94C753DE_gshared)(method);
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1 (String_t* ___path0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::Add(!0)
inline void List_1_Add_mB8B305BA07510D89B7080577D4CAE7D227975FC8 (List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 * __this, OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 *, OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::Remove(!0)
inline bool List_1_Remove_m39EF2DAF132C555C0C0D2964DA1E18641CED00C7 (List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 * __this, OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 *, OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 *, const RuntimeMethod*))List_1_Remove_m908B647BB9F807676DACE34E3E73475C3C3751D4_gshared)(__this, ___item0, method);
}
// System.Void System.Action`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m052DDDED017E7D987AEE5637A3FEA43DF771172A (Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::ForEach(System.Action`1<!0>)
inline void List_1_ForEach_m161DC48D7CAFFC09C59EC204BFF16C7FAB4DA12F (List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 * __this, Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 * ___action0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 *, Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 *, const RuntimeMethod*))List_1_ForEach_m1FF0FB75577597F41D35ED6D7471CD96BA2CA65F_gshared)(__this, ___action0, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
inline void List_1_Add_mA348FA1140766465189459D25B01EB179001DE83 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, String_t* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, String_t*, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes>::.ctor()
inline void List_1__ctor_mCA14223B544692C7F7021CB026A222E347BE9935 (List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void Facebook.Unity.Settings.FacebookSettings/UrlSchemes::.ctor(System.Collections.Generic.List`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UrlSchemes__ctor_m78C1F53E102E5C40F1BC41F4573CA6E32B3979BF (UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48 * __this, List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___schemes0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes>::Add(!0)
inline void List_1_Add_mE79048C11E65015096F0E0ECB32D2C56EA838B8E (List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA * __this, UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA *, UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// System.Void UnityEngine.ScriptableObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B (ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::.ctor()
inline void List_1__ctor_m7B88C102B657EB6875FBFC582C86C49CE1DB4486 (List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void Facebook.Unity.Settings.FacebookSettings/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m66DCDF10B154EA0E16E26F3E08D86F52B2C8915F (U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void Facebook.Unity.Settings.FacebookSettings/OnChangeCallback::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnChangeCallback_Invoke_m78D9FCC43C84E280EB8C185DDED01B0D703BE9F7 (OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 * __this, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 Facebook.Unity.Settings.FacebookSettings::get_SelectedAppIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FacebookSettings_get_SelectedAppIndex_m2EC480ADF724345DFD8E05519877A2F46A1238FC (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_SelectedAppIndex_m2EC480ADF724345DFD8E05519877A2F46A1238FC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = L_0->get_selectedAppIndex_9();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_SelectedAppIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_set_SelectedAppIndex_mFEE448C1AC465FC83B45462B48026D700A4E468D (int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_SelectedAppIndex_mFEE448C1AC465FC83B45462B48026D700A4E468D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = L_0->get_selectedAppIndex_9();
		int32_t L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_3 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		int32_t L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_selectedAppIndex_9(L_4);
		FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppIds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * FacebookSettings_get_AppIds_m213F9082C11ED86A61337946DBA5881971DB389A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AppIds_m213F9082C11ED86A61337946DBA5881971DB389A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = L_0->get_appIds_11();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AppIds(System.Collections.Generic.List`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_set_AppIds_mFEAE5C06EBBFE37F87D23ABC9D6E2F52229890F8 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AppIds_mFEAE5C06EBBFE37F87D23ABC9D6E2F52229890F8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = L_0->get_appIds_11();
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_2 = ___value0;
		if ((((RuntimeObject*)(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)L_1) == ((RuntimeObject*)(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_3 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_appIds_11(L_4);
		FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppLabels()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * FacebookSettings_get_AppLabels_m0B090EF43E95C8FB73619F3AEF0C75D3AA3BA359 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AppLabels_m0B090EF43E95C8FB73619F3AEF0C75D3AA3BA359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = L_0->get_appLabels_12();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AppLabels(System.Collections.Generic.List`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_set_AppLabels_m5731795E2EDB99EB7C0F0A61C9DA7098635B7357 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AppLabels_m5731795E2EDB99EB7C0F0A61C9DA7098635B7357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = L_0->get_appLabels_12();
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_2 = ___value0;
		if ((((RuntimeObject*)(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)L_1) == ((RuntimeObject*)(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_3 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_appLabels_12(L_4);
		FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_ClientTokens()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * FacebookSettings_get_ClientTokens_mDF6BC3A0D7BC2CD76E1181445577437A491AA341 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_ClientTokens_mDF6BC3A0D7BC2CD76E1181445577437A491AA341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = L_0->get_clientTokens_10();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_ClientTokens(System.Collections.Generic.List`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_set_ClientTokens_m2B44D9306D07402C28C80D052D1F4E100BB7B39E (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_ClientTokens_m2B44D9306D07402C28C80D052D1F4E100BB7B39E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = L_0->get_clientTokens_10();
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_2 = ___value0;
		if ((((RuntimeObject*)(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)L_1) == ((RuntimeObject*)(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_3 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_clientTokens_10(L_4);
		FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_AppId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_AppId_m006AE7342E7C2E7A46BEDE9BF5F1E9F3B533AF1B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AppId_m006AE7342E7C2E7A46BEDE9BF5F1E9F3B533AF1B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = FacebookSettings_get_AppIds_m213F9082C11ED86A61337946DBA5881971DB389A(/*hidden argument*/NULL);
		int32_t L_1 = FacebookSettings_get_SelectedAppIndex_m2EC480ADF724345DFD8E05519877A2F46A1238FC(/*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_2 = List_1_get_Item_mB739B0066E5F7EBDBA9978F24A73D26D4FAE5BED_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_mB739B0066E5F7EBDBA9978F24A73D26D4FAE5BED_RuntimeMethod_var);
		NullCheck(L_2);
		String_t* L_3 = String_Trim_mB52EB7876C7132358B76B7DC95DEACA20722EF4D(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_ClientToken()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_ClientToken_mA9CF546F9A73544DE706EC537F8FDE24EB8FB2A4 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_ClientToken_mA9CF546F9A73544DE706EC537F8FDE24EB8FB2A4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = FacebookSettings_get_ClientTokens_mDF6BC3A0D7BC2CD76E1181445577437A491AA341(/*hidden argument*/NULL);
		int32_t L_1 = FacebookSettings_get_SelectedAppIndex_m2EC480ADF724345DFD8E05519877A2F46A1238FC(/*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_2 = List_1_get_Item_mB739B0066E5F7EBDBA9978F24A73D26D4FAE5BED_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_mB739B0066E5F7EBDBA9978F24A73D26D4FAE5BED_RuntimeMethod_var);
		NullCheck(L_2);
		String_t* L_3 = String_Trim_mB52EB7876C7132358B76B7DC95DEACA20722EF4D(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_IsValidAppId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FacebookSettings_get_IsValidAppId_mF0D6D09A9632E2DE2C51F515BA990C0ABABF90D9 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_IsValidAppId_mF0D6D09A9632E2DE2C51F515BA990C0ABABF90D9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		String_t* L_0 = FacebookSettings_get_AppId_m006AE7342E7C2E7A46BEDE9BF5F1E9F3B533AF1B(/*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		String_t* L_1 = FacebookSettings_get_AppId_m006AE7342E7C2E7A46BEDE9BF5F1E9F3B533AF1B(/*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		String_t* L_3 = FacebookSettings_get_AppId_m006AE7342E7C2E7A46BEDE9BF5F1E9F3B533AF1B(/*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = String_Equals_m9C4D78DFA0979504FE31429B64A4C26DF48020D1(L_3, _stringLiteralB6589FC6AB0DC82CF12099D1C2D40AB994E8410C, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}

IL_0027:
	{
		return (bool)0;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Cookie()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FacebookSettings_get_Cookie_m0C87EA644FF0E4AF344A849466638C196C888C20 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Cookie_m0C87EA644FF0E4AF344A849466638C196C888C20_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_cookie_13();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_Cookie(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_set_Cookie_m75D92C414BCD12999B0252B48E3DA92862C98C6B (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_Cookie_m75D92C414BCD12999B0252B48E3DA92862C98C6B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_cookie_13();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_3 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_cookie_13(L_4);
		FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Logging()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FacebookSettings_get_Logging_m693DFA3C03B6CB9BD152F452663C2CB7476AE27B (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Logging_m693DFA3C03B6CB9BD152F452663C2CB7476AE27B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_logging_14();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_Logging(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_set_Logging_mB6B75791D46B8B8F576694DEC4F2EC35F7A9EBB9 (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_Logging_mB6B75791D46B8B8F576694DEC4F2EC35F7A9EBB9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_logging_14();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_3 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_logging_14(L_4);
		FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Status()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FacebookSettings_get_Status_mE4DAEE25F212765BAEAA789789D485B393186679 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Status_mE4DAEE25F212765BAEAA789789D485B393186679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_status_15();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_Status(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_set_Status_m5B328C7ED836EACC4589E195F2D401007DC2253D (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_Status_m5B328C7ED836EACC4589E195F2D401007DC2253D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_status_15();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_3 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_status_15(L_4);
		FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Xfbml()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FacebookSettings_get_Xfbml_m5C1F97F309C373134DD15DFCE257C209DD1AA385 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Xfbml_m5C1F97F309C373134DD15DFCE257C209DD1AA385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_xfbml_16();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_Xfbml(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_set_Xfbml_mAF217018F5283A25BA1755E620CB3AC84C7EC917 (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_Xfbml_mAF217018F5283A25BA1755E620CB3AC84C7EC917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_xfbml_16();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_3 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_xfbml_16(L_4);
		FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_IosURLSuffix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_IosURLSuffix_m472E416713BBDD6653141AB0FB400A5270063B39 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_IosURLSuffix_m472E416713BBDD6653141AB0FB400A5270063B39_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = L_0->get_iosURLSuffix_18();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_IosURLSuffix(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_set_IosURLSuffix_m3C0E030A70E79C62302F1E4FDE305B4471E11F2B (String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_IosURLSuffix_m3C0E030A70E79C62302F1E4FDE305B4471E11F2B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = L_0->get_iosURLSuffix_18();
		String_t* L_2 = ___value0;
		bool L_3 = String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_4 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		String_t* L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_iosURLSuffix_18(L_5);
		FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC(/*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_ChannelUrl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_ChannelUrl_m136B1C28CEB1B078BA95F45F96C22FCC4CFD1B9C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_ChannelUrl_m136B1C28CEB1B078BA95F45F96C22FCC4CFD1B9C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral3D7479543F221F46EE46D1F7AB8E7AD53734F2B9;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_FrictionlessRequests()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FacebookSettings_get_FrictionlessRequests_mE87DE80B8A25477536E5C56D040BC1AB83815641 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_FrictionlessRequests_mE87DE80B8A25477536E5C56D040BC1AB83815641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_frictionlessRequests_17();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_FrictionlessRequests(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_set_FrictionlessRequests_m506B05AAACB062C97B9889BC40184801ACFFE10F (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_FrictionlessRequests_m506B05AAACB062C97B9889BC40184801ACFFE10F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_frictionlessRequests_17();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_3 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_frictionlessRequests_17(L_4);
		FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_UrlSchemes> Facebook.Unity.Settings.FacebookSettings::get_AppLinkSchemes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA * FacebookSettings_get_AppLinkSchemes_m981840EDB39AB30ABC84D4A7236B339797D3124D (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AppLinkSchemes_m981840EDB39AB30ABC84D4A7236B339797D3124D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA * L_1 = L_0->get_appLinkSchemes_19();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AppLinkSchemes(System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings_UrlSchemes>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_set_AppLinkSchemes_m4626609FA7E32934E12FA655D641B6735BB788C0 (List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AppLinkSchemes_m4626609FA7E32934E12FA655D641B6735BB788C0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA * L_1 = L_0->get_appLinkSchemes_19();
		List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA * L_2 = ___value0;
		if ((((RuntimeObject*)(List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA *)L_1) == ((RuntimeObject*)(List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA *)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_3 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA * L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_appLinkSchemes_19(L_4);
		FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_UploadAccessToken()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_UploadAccessToken_mC2EE58F6EE93AAA5552353390EC80E27E1A91F4A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_UploadAccessToken_mC2EE58F6EE93AAA5552353390EC80E27E1A91F4A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = L_0->get_uploadAccessToken_20();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_UploadAccessToken(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_set_UploadAccessToken_m0E7538A7F94CF09920976B1BB642059D0AED1A88 (String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_UploadAccessToken_m0E7538A7F94CF09920976B1BB642059D0AED1A88_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = L_0->get_uploadAccessToken_20();
		String_t* L_2 = ___value0;
		bool L_3 = String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_4 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		String_t* L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_uploadAccessToken_20(L_5);
		FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC(/*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_AutoLogAppEventsEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FacebookSettings_get_AutoLogAppEventsEnabled_m70BF5DEC22AF2E878525B1E1E5C58743B361402F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AutoLogAppEventsEnabled_m70BF5DEC22AF2E878525B1E1E5C58743B361402F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_autoLogAppEventsEnabled_21();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AutoLogAppEventsEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_set_AutoLogAppEventsEnabled_m1D92F0AD0A3AF85A4FF5B77441757601C21F53AE (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AutoLogAppEventsEnabled_m1D92F0AD0A3AF85A4FF5B77441757601C21F53AE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_autoLogAppEventsEnabled_21();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_3 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_autoLogAppEventsEnabled_21(L_4);
		FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_AdvertiserIDCollectionEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FacebookSettings_get_AdvertiserIDCollectionEnabled_mC0BBD06C17C6DA1DE04AFF774A80D0A841FA33B0 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AdvertiserIDCollectionEnabled_mC0BBD06C17C6DA1DE04AFF774A80D0A841FA33B0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_advertiserIDCollectionEnabled_22();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AdvertiserIDCollectionEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_set_AdvertiserIDCollectionEnabled_m68A9D35770597F1F6E90BB60843830F890600965 (bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AdvertiserIDCollectionEnabled_m68A9D35770597F1F6E90BB60843830F890600965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_advertiserIDCollectionEnabled_22();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_3 = FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880(/*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_advertiserIDCollectionEnabled_22(L_4);
		FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC(/*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Instance_m0150A1DB8F205103DA70E1B4975C0C19B7FCA880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = FacebookSettings_get_NullableInstance_mE0BB0B0DB862CA279FBD0C2E792EFCE753D66612(/*hidden argument*/NULL);
		((FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var))->set_instance_8(L_0);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_1 = ((FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var))->get_instance_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_3 = ScriptableObject_CreateInstance_TisFacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_m3C1138AD69C5490AD30BB8BCD08302D25257C35A(/*hidden argument*/ScriptableObject_CreateInstance_TisFacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_m3C1138AD69C5490AD30BB8BCD08302D25257C35A_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		((FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var))->set_instance_8(L_3);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_4 = ((FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var))->get_instance_8();
		return L_4;
	}
}
// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_NullableInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * FacebookSettings_get_NullableInstance_mE0BB0B0DB862CA279FBD0C2E792EFCE753D66612 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_NullableInstance_mE0BB0B0DB862CA279FBD0C2E792EFCE753D66612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_0 = ((FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var))->get_instance_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_2 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(_stringLiteral11A3B430794567143B2D380848C73908507F1D59, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		((FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var))->set_instance_8(((FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA *)IsInstClass((RuntimeObject*)L_2, FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var)));
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * L_3 = ((FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var))->get_instance_8();
		return L_3;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::RegisterChangeEventCallback(Facebook.Unity.Settings.FacebookSettings_OnChangeCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_RegisterChangeEventCallback_mD13D0239F493B9701BC8891ABBC5A00980F5BA14 (OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_RegisterChangeEventCallback_mD13D0239F493B9701BC8891ABBC5A00980F5BA14_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 * L_0 = ((FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var))->get_onChangeCallbacks_7();
		OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 * L_1 = ___callback0;
		NullCheck(L_0);
		List_1_Add_mB8B305BA07510D89B7080577D4CAE7D227975FC8(L_0, L_1, /*hidden argument*/List_1_Add_mB8B305BA07510D89B7080577D4CAE7D227975FC8_RuntimeMethod_var);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::UnregisterChangeEventCallback(Facebook.Unity.Settings.FacebookSettings_OnChangeCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_UnregisterChangeEventCallback_m64059E33CBD8EDDC51B52AB839554C5BA23A8D16 (OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_UnregisterChangeEventCallback_m64059E33CBD8EDDC51B52AB839554C5BA23A8D16_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 * L_0 = ((FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var))->get_onChangeCallbacks_7();
		OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 * L_1 = ___callback0;
		NullCheck(L_0);
		List_1_Remove_m39EF2DAF132C555C0C0D2964DA1E18641CED00C7(L_0, L_1, /*hidden argument*/List_1_Remove_m39EF2DAF132C555C0C0D2964DA1E18641CED00C7_RuntimeMethod_var);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::SettingsChanged()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_SettingsChanged_mD2F94705DD4FFBF0A9C140A7C413AEABF9B26BDC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 * G_B2_0 = NULL;
	List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 * G_B2_1 = NULL;
	Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 * G_B1_0 = NULL;
	List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 * G_B1_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var);
		List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 * L_0 = ((FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var))->get_onChangeCallbacks_7();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_il2cpp_TypeInfo_var);
		Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 * L_1 = ((U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_il2cpp_TypeInfo_var))->get_U3CU3E9__76_0_1();
		Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_il2cpp_TypeInfo_var);
		U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471 * L_3 = ((U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 * L_4 = (Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 *)il2cpp_codegen_object_new(Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83_il2cpp_TypeInfo_var);
		Action_1__ctor_m052DDDED017E7D987AEE5637A3FEA43DF771172A(L_4, L_3, (intptr_t)((intptr_t)U3CU3Ec_U3CSettingsChangedU3Eb__76_0_m0C9EF39EDE99D2588A5985F239B388C18BBC8A66_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m052DDDED017E7D987AEE5637A3FEA43DF771172A_RuntimeMethod_var);
		Action_1_tAFE49CF51090BA12181C602A733327A8EF0A7C83 * L_5 = L_4;
		((U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_il2cpp_TypeInfo_var))->set_U3CU3E9__76_0_1(L_5);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
	}

IL_0024:
	{
		NullCheck(G_B2_1);
		List_1_ForEach_m161DC48D7CAFFC09C59EC204BFF16C7FAB4DA12F(G_B2_1, G_B2_0, /*hidden argument*/List_1_ForEach_m161DC48D7CAFFC09C59EC204BFF16C7FAB4DA12F_RuntimeMethod_var);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings__ctor_m057BF97646BE5245A135F7D9B2357458224FF1DD (FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings__ctor_m057BF97646BE5245A135F7D9B2357458224FF1DD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)il2cpp_codegen_object_new(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var);
		List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06(L_0, /*hidden argument*/List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = L_0;
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		NullCheck(L_1);
		List_1_Add_mA348FA1140766465189459D25B01EB179001DE83(L_1, L_2, /*hidden argument*/List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var);
		__this->set_clientTokens_10(L_1);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_3 = (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)il2cpp_codegen_object_new(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var);
		List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06(L_3, /*hidden argument*/List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_4 = L_3;
		NullCheck(L_4);
		List_1_Add_mA348FA1140766465189459D25B01EB179001DE83(L_4, _stringLiteralB6589FC6AB0DC82CF12099D1C2D40AB994E8410C, /*hidden argument*/List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var);
		__this->set_appIds_11(L_4);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_5 = (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)il2cpp_codegen_object_new(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var);
		List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06(L_5, /*hidden argument*/List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_6 = L_5;
		NullCheck(L_6);
		List_1_Add_mA348FA1140766465189459D25B01EB179001DE83(L_6, _stringLiteral4FB43108E78F4D80425CB74F01AC6D0A59E0A3D3, /*hidden argument*/List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var);
		__this->set_appLabels_12(L_6);
		__this->set_cookie_13((bool)1);
		__this->set_logging_14((bool)1);
		__this->set_status_15((bool)1);
		__this->set_frictionlessRequests_17((bool)1);
		String_t* L_7 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_iosURLSuffix_18(L_7);
		List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA * L_8 = (List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA *)il2cpp_codegen_object_new(List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA_il2cpp_TypeInfo_var);
		List_1__ctor_mCA14223B544692C7F7021CB026A222E347BE9935(L_8, /*hidden argument*/List_1__ctor_mCA14223B544692C7F7021CB026A222E347BE9935_RuntimeMethod_var);
		List_1_tB60565FA161FB7CA3E35A85CFD12DF11630E62DA * L_9 = L_8;
		UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48 * L_10 = (UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48 *)il2cpp_codegen_object_new(UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48_il2cpp_TypeInfo_var);
		UrlSchemes__ctor_m78C1F53E102E5C40F1BC41F4573CA6E32B3979BF(L_10, (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)NULL, /*hidden argument*/NULL);
		NullCheck(L_9);
		List_1_Add_mE79048C11E65015096F0E0ECB32D2C56EA838B8E(L_9, L_10, /*hidden argument*/List_1_Add_mE79048C11E65015096F0E0ECB32D2C56EA838B8E_RuntimeMethod_var);
		__this->set_appLinkSchemes_19(L_9);
		String_t* L_11 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_uploadAccessToken_20(L_11);
		__this->set_autoLogAppEventsEnabled_21((bool)1);
		__this->set_advertiserIDCollectionEnabled_22((bool)1);
		ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookSettings__cctor_m6E56A8CDDB184AD6C0DA6C146401499E39B30BA3 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings__cctor_m6E56A8CDDB184AD6C0DA6C146401499E39B30BA3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 * L_0 = (List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8 *)il2cpp_codegen_object_new(List_1_t2EF748E023E1AF26A64137F52EE81C7487DE92F8_il2cpp_TypeInfo_var);
		List_1__ctor_m7B88C102B657EB6875FBFC582C86C49CE1DB4486(L_0, /*hidden argument*/List_1__ctor_m7B88C102B657EB6875FBFC582C86C49CE1DB4486_RuntimeMethod_var);
		((FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t3C30B5537AE03423495B6E80F21486454C0CCDCA_il2cpp_TypeInfo_var))->set_onChangeCallbacks_7(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Settings.FacebookSettings_<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m40EF99CA45A4E5F71A02C65A95129BFAC50AC840 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_m40EF99CA45A4E5F71A02C65A95129BFAC50AC840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471 * L_0 = (U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471 *)il2cpp_codegen_object_new(U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m66DCDF10B154EA0E16E26F3E08D86F52B2C8915F(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings_<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m66DCDF10B154EA0E16E26F3E08D86F52B2C8915F (U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings_<>c::<SettingsChanged>b__76_0(Facebook.Unity.Settings.FacebookSettings_OnChangeCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CSettingsChangedU3Eb__76_0_m0C9EF39EDE99D2588A5985F239B388C18BBC8A66 (U3CU3Ec_t68945CD81EE8041B1A123C1B4359435B948B1471 * __this, OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 * ___callback0, const RuntimeMethod* method)
{
	{
		OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 * L_0 = ___callback0;
		NullCheck(L_0);
		OnChangeCallback_Invoke_m78D9FCC43C84E280EB8C185DDED01B0D703BE9F7(L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 (OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 * __this, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnChangeCallback__ctor_mB3995D4A5937814ADE512BB368B3C7823A3E6618 (OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnChangeCallback_Invoke_m78D9FCC43C84E280EB8C185DDED01B0D703BE9F7 (OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 * __this, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 0)
			{
				// open
				typedef void (*FunctionPointerType) (const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
	}
}
// System.IAsyncResult Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::BeginInvoke(System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* OnChangeCallback_BeginInvoke_m49246D0FA85E4631BDC7C128360C3BF151A2B2D4 (OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 * __this, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void Facebook.Unity.Settings.FacebookSettings_OnChangeCallback::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnChangeCallback_EndInvoke_m28F0DDFBB86AB43DB00C2BEF2A16EDD9B5AD3243 (OnChangeCallback_t8485A39258C3051944B54EFBD9C6C0157F7606F1 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Settings.FacebookSettings_UrlSchemes::.ctor(System.Collections.Generic.List`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UrlSchemes__ctor_m78C1F53E102E5C40F1BC41F4573CA6E32B3979BF (UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48 * __this, List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___schemes0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UrlSchemes__ctor_m78C1F53E102E5C40F1BC41F4573CA6E32B3979BF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48 * G_B2_0 = NULL;
	UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48 * G_B1_0 = NULL;
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * G_B3_0 = NULL;
	UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48 * G_B3_1 = NULL;
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = ___schemes0;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_000d;
		}
	}
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_1 = ___schemes0;
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0012;
	}

IL_000d:
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_2 = (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)il2cpp_codegen_object_new(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var);
		List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06(L_2, /*hidden argument*/List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var);
		G_B3_0 = L_2;
		G_B3_1 = G_B2_0;
	}

IL_0012:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_list_0(G_B3_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings_UrlSchemes::get_Schemes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * UrlSchemes_get_Schemes_m47CCC295C6B98BC9AEA06CC5E165CE8DCF3B83BB (UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48 * __this, const RuntimeMethod* method)
{
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = __this->get_list_0();
		return L_0;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings_UrlSchemes::set_Schemes(System.Collections.Generic.List`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UrlSchemes_set_Schemes_mA556AE7F1B56A33A701F6C9A0834386457D4891B (UrlSchemes_t4DBDC8A3184BB2B16E84401A085E02137F080F48 * __this, List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = ___value0;
		__this->set_list_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t String_get_Length_mD48C8A16A5CF1914F330DCE82D9BE15C3BEDD018_inline (String_t* __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_stringLength_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_2, (int32_t)L_3);
		return L_4;
	}
}
