#if !INIT_SCRIPTING_BACKEND

extern void RegisterAllClassesGranular();
void RegisterAllClasses()
{
    // Register classes for unit tests
    RegisterAllClassesGranular();
}

void RegisterAllStrippedInternalCalls() {}

void InvokeRegisterStaticallyLinkedModuleClasses() {}
extern "C" void RegisterStaticallyLinkedModulesGranular() {}

#endif // INIT_SCRIPTING_BACKEND

#define HAS_UNITY_VERSION_DEF 1
