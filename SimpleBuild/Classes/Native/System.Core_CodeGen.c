﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000003 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000004 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000007 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000009 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000000A System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000000B System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000000C System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000000D System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000000F System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000010 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000011 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000012 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000013 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000014 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000015 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000016 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000017 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000018 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000019 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000001A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001B System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000001D System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000001E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001F System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000020 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
static Il2CppMethodPointer s_methodPointers[32] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[32] = 
{
	0,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[10] = 
{
	{ 0x02000004, { 22, 4 } },
	{ 0x02000005, { 26, 9 } },
	{ 0x02000006, { 35, 7 } },
	{ 0x02000007, { 42, 10 } },
	{ 0x02000008, { 52, 1 } },
	{ 0x06000003, { 0, 10 } },
	{ 0x06000004, { 10, 5 } },
	{ 0x06000005, { 15, 3 } },
	{ 0x06000006, { 18, 1 } },
	{ 0x06000007, { 19, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[53] = 
{
	{ (Il2CppRGCTXDataType)2, 8607 },
	{ (Il2CppRGCTXDataType)3, 3835 },
	{ (Il2CppRGCTXDataType)2, 8608 },
	{ (Il2CppRGCTXDataType)2, 8609 },
	{ (Il2CppRGCTXDataType)3, 3836 },
	{ (Il2CppRGCTXDataType)2, 8610 },
	{ (Il2CppRGCTXDataType)2, 8611 },
	{ (Il2CppRGCTXDataType)3, 3837 },
	{ (Il2CppRGCTXDataType)2, 8612 },
	{ (Il2CppRGCTXDataType)3, 3838 },
	{ (Il2CppRGCTXDataType)2, 8613 },
	{ (Il2CppRGCTXDataType)3, 3839 },
	{ (Il2CppRGCTXDataType)3, 3840 },
	{ (Il2CppRGCTXDataType)2, 7226 },
	{ (Il2CppRGCTXDataType)3, 3841 },
	{ (Il2CppRGCTXDataType)2, 7228 },
	{ (Il2CppRGCTXDataType)2, 8614 },
	{ (Il2CppRGCTXDataType)3, 3842 },
	{ (Il2CppRGCTXDataType)2, 7231 },
	{ (Il2CppRGCTXDataType)2, 7233 },
	{ (Il2CppRGCTXDataType)2, 8615 },
	{ (Il2CppRGCTXDataType)3, 3843 },
	{ (Il2CppRGCTXDataType)3, 3844 },
	{ (Il2CppRGCTXDataType)3, 3845 },
	{ (Il2CppRGCTXDataType)2, 7238 },
	{ (Il2CppRGCTXDataType)3, 3846 },
	{ (Il2CppRGCTXDataType)3, 3847 },
	{ (Il2CppRGCTXDataType)2, 7247 },
	{ (Il2CppRGCTXDataType)2, 8616 },
	{ (Il2CppRGCTXDataType)3, 3848 },
	{ (Il2CppRGCTXDataType)3, 3849 },
	{ (Il2CppRGCTXDataType)2, 7249 },
	{ (Il2CppRGCTXDataType)2, 8542 },
	{ (Il2CppRGCTXDataType)3, 3850 },
	{ (Il2CppRGCTXDataType)3, 3851 },
	{ (Il2CppRGCTXDataType)3, 3852 },
	{ (Il2CppRGCTXDataType)2, 7256 },
	{ (Il2CppRGCTXDataType)2, 8617 },
	{ (Il2CppRGCTXDataType)3, 3853 },
	{ (Il2CppRGCTXDataType)3, 3854 },
	{ (Il2CppRGCTXDataType)3, 3553 },
	{ (Il2CppRGCTXDataType)3, 3855 },
	{ (Il2CppRGCTXDataType)3, 3856 },
	{ (Il2CppRGCTXDataType)2, 7265 },
	{ (Il2CppRGCTXDataType)2, 8618 },
	{ (Il2CppRGCTXDataType)3, 3857 },
	{ (Il2CppRGCTXDataType)3, 3858 },
	{ (Il2CppRGCTXDataType)3, 3859 },
	{ (Il2CppRGCTXDataType)3, 3860 },
	{ (Il2CppRGCTXDataType)3, 3861 },
	{ (Il2CppRGCTXDataType)3, 3559 },
	{ (Il2CppRGCTXDataType)3, 3862 },
	{ (Il2CppRGCTXDataType)3, 3863 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	32,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	10,
	s_rgctxIndices,
	53,
	s_rgctxValues,
	NULL,
};
