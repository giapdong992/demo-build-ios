﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Playerr::Start()
extern void Playerr_Start_m2420AF550CA1E39B2A385E4E1C55BB2C9A88955A (void);
// 0x00000002 System.Void Playerr::Update()
extern void Playerr_Update_m2C9EA90A1E5C560F1778E10FC17A5D0F4CF1F0B4 (void);
// 0x00000003 System.Void Playerr::CreateLine()
extern void Playerr_CreateLine_mF300DF1A6C1A25C6EDF3EAA272F7FC30075E668E (void);
// 0x00000004 System.Void Playerr::UpdateLine(UnityEngine.Vector2)
extern void Playerr_UpdateLine_mDD6DB436A8701122366BA573B0020ACC7992F254 (void);
// 0x00000005 System.Void Playerr::MovingLine()
extern void Playerr_MovingLine_mCE8077B4ABCCE376ED4221AB5A55C3D08627485A (void);
// 0x00000006 System.Int32 Playerr::SetIndexOfEdge()
extern void Playerr_SetIndexOfEdge_mCDB869015EC44965EA72EBF00C9D4DEBE4DA793A (void);
// 0x00000007 System.Void Playerr::.ctor()
extern void Playerr__ctor_m07EE8F1DE44A9A029F419740C4CFF0F561F3ECA2 (void);
// 0x00000008 System.Collections.IEnumerator BulletBehaviour::Start()
extern void BulletBehaviour_Start_m1E3BBE43DD24C1C77F0FCD3F02A30C76ED4D713A (void);
// 0x00000009 System.Void BulletBehaviour::Update()
extern void BulletBehaviour_Update_m698194B4650A4694207771E3235DDA82C86F7AC5 (void);
// 0x0000000A System.Void BulletBehaviour::.ctor()
extern void BulletBehaviour__ctor_m0B6A95E803B1DD5C24CD010F8B86816E42D07CE2 (void);
// 0x0000000B System.Collections.IEnumerator Player::Start()
extern void Player_Start_m0F7E658A2B4B4A0DECDC461053DB7C1CC49224F3 (void);
// 0x0000000C System.Void Player::Update()
extern void Player_Update_m6F977BAE3756AB7073D64042B766B442E4EC6FD2 (void);
// 0x0000000D System.Void Player::.ctor()
extern void Player__ctor_m8F4AB650C5E2DE406B3C65EA8F662013458D85E2 (void);
// 0x0000000E System.Void abc::Start()
extern void abc_Start_m88876DA51CDC03B7BEA2B1C7F36A2032F0A9E88B (void);
// 0x0000000F UnityEngine.Vector2 abc::RotateVector(UnityEngine.Vector2,System.Single)
extern void abc_RotateVector_mBC78C0CBF2DF73AB25EDFAD5DC745592C736C5A0 (void);
// 0x00000010 System.Void abc::OnDrawGizmosSelected()
extern void abc_OnDrawGizmosSelected_mB1FFE94128B84BA03B07A80665274D9F2AA7455C (void);
// 0x00000011 UnityEngine.Vector3 abc::GetPos(System.Int32)
extern void abc_GetPos_mFC2EEA8A9965C6E45462209FDB068DAE61F7CF4A (void);
// 0x00000012 System.Void abc::.ctor()
extern void abc__ctor_mB1D05F41B8DEFA360E09DCF859203B4A2ED7767B (void);
// 0x00000013 System.Void BulletBehaviour_<Start>d__0::.ctor(System.Int32)
extern void U3CStartU3Ed__0__ctor_m1085904516FD2815777A7F13370F1806A96A6E0E (void);
// 0x00000014 System.Void BulletBehaviour_<Start>d__0::System.IDisposable.Dispose()
extern void U3CStartU3Ed__0_System_IDisposable_Dispose_m6CA89C9256BA1BC30E430EB203BCB17FB9713871 (void);
// 0x00000015 System.Boolean BulletBehaviour_<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m42309C775FA9D0A1564698AA65CC8D3C38FF7DB0 (void);
// 0x00000016 System.Object BulletBehaviour_<Start>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D0244E325F8A4525F1BD2F95200F528EBE90118 (void);
// 0x00000017 System.Void BulletBehaviour_<Start>d__0::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__0_System_Collections_IEnumerator_Reset_mF964FF8044E1E72FF4C6465E6F558FD05B6AB580 (void);
// 0x00000018 System.Object BulletBehaviour_<Start>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__0_System_Collections_IEnumerator_get_Current_mF666627C87AD51DADB08F343384F86771A431666 (void);
// 0x00000019 System.Void Player_<Start>d__3::.ctor(System.Int32)
extern void U3CStartU3Ed__3__ctor_m169D978A6538D6D7EF0049E62CDF357A6F589F9D (void);
// 0x0000001A System.Void Player_<Start>d__3::System.IDisposable.Dispose()
extern void U3CStartU3Ed__3_System_IDisposable_Dispose_mCED044FC7152A58AAA78E7FAA7AB62105001E554 (void);
// 0x0000001B System.Boolean Player_<Start>d__3::MoveNext()
extern void U3CStartU3Ed__3_MoveNext_mD7DBFAFBA3E4327B35BE6373E48F835966E63026 (void);
// 0x0000001C System.Object Player_<Start>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF99B255B5E430DB971986EEDD54D90DB80E0DEF8 (void);
// 0x0000001D System.Void Player_<Start>d__3::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__3_System_Collections_IEnumerator_Reset_m79DE7B28665C1C8789656344B6F8E5235510FFE5 (void);
// 0x0000001E System.Object Player_<Start>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__3_System_Collections_IEnumerator_get_Current_mBCB37B32FE466A446EF8B67D11D7E21937306B55 (void);
static Il2CppMethodPointer s_methodPointers[30] = 
{
	Playerr_Start_m2420AF550CA1E39B2A385E4E1C55BB2C9A88955A,
	Playerr_Update_m2C9EA90A1E5C560F1778E10FC17A5D0F4CF1F0B4,
	Playerr_CreateLine_mF300DF1A6C1A25C6EDF3EAA272F7FC30075E668E,
	Playerr_UpdateLine_mDD6DB436A8701122366BA573B0020ACC7992F254,
	Playerr_MovingLine_mCE8077B4ABCCE376ED4221AB5A55C3D08627485A,
	Playerr_SetIndexOfEdge_mCDB869015EC44965EA72EBF00C9D4DEBE4DA793A,
	Playerr__ctor_m07EE8F1DE44A9A029F419740C4CFF0F561F3ECA2,
	BulletBehaviour_Start_m1E3BBE43DD24C1C77F0FCD3F02A30C76ED4D713A,
	BulletBehaviour_Update_m698194B4650A4694207771E3235DDA82C86F7AC5,
	BulletBehaviour__ctor_m0B6A95E803B1DD5C24CD010F8B86816E42D07CE2,
	Player_Start_m0F7E658A2B4B4A0DECDC461053DB7C1CC49224F3,
	Player_Update_m6F977BAE3756AB7073D64042B766B442E4EC6FD2,
	Player__ctor_m8F4AB650C5E2DE406B3C65EA8F662013458D85E2,
	abc_Start_m88876DA51CDC03B7BEA2B1C7F36A2032F0A9E88B,
	abc_RotateVector_mBC78C0CBF2DF73AB25EDFAD5DC745592C736C5A0,
	abc_OnDrawGizmosSelected_mB1FFE94128B84BA03B07A80665274D9F2AA7455C,
	abc_GetPos_mFC2EEA8A9965C6E45462209FDB068DAE61F7CF4A,
	abc__ctor_mB1D05F41B8DEFA360E09DCF859203B4A2ED7767B,
	U3CStartU3Ed__0__ctor_m1085904516FD2815777A7F13370F1806A96A6E0E,
	U3CStartU3Ed__0_System_IDisposable_Dispose_m6CA89C9256BA1BC30E430EB203BCB17FB9713871,
	U3CStartU3Ed__0_MoveNext_m42309C775FA9D0A1564698AA65CC8D3C38FF7DB0,
	U3CStartU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D0244E325F8A4525F1BD2F95200F528EBE90118,
	U3CStartU3Ed__0_System_Collections_IEnumerator_Reset_mF964FF8044E1E72FF4C6465E6F558FD05B6AB580,
	U3CStartU3Ed__0_System_Collections_IEnumerator_get_Current_mF666627C87AD51DADB08F343384F86771A431666,
	U3CStartU3Ed__3__ctor_m169D978A6538D6D7EF0049E62CDF357A6F589F9D,
	U3CStartU3Ed__3_System_IDisposable_Dispose_mCED044FC7152A58AAA78E7FAA7AB62105001E554,
	U3CStartU3Ed__3_MoveNext_mD7DBFAFBA3E4327B35BE6373E48F835966E63026,
	U3CStartU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF99B255B5E430DB971986EEDD54D90DB80E0DEF8,
	U3CStartU3Ed__3_System_Collections_IEnumerator_Reset_m79DE7B28665C1C8789656344B6F8E5235510FFE5,
	U3CStartU3Ed__3_System_Collections_IEnumerator_get_Current_mBCB37B32FE466A446EF8B67D11D7E21937306B55,
};
static const int32_t s_InvokerIndices[30] = 
{
	23,
	23,
	23,
	1179,
	23,
	10,
	23,
	14,
	23,
	23,
	14,
	23,
	23,
	23,
	1180,
	23,
	1181,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	30,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
